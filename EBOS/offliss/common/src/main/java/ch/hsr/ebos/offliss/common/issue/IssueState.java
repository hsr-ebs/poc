package ch.hsr.ebos.offliss.common.issue;

public enum IssueState {
    OPEN, CLOSED
}
