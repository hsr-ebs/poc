package ch.hsr.ebos.offliss.common;

import ch.hsr.ebos.aion.core.event.IEvent;

public abstract class StateChangeEvent implements IEvent {
    private String id;
    private String name;

    public StateChangeEvent() {

    }

    public StateChangeEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
