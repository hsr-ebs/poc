package ch.hsr.ebos.offliss.common.user.inactivate;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.offliss.common.StateChangeEvent;

@EventType("offliss.user.inactivate")
@Description("Inactivate User")
public class InactivateUserEvent extends StateChangeEvent {

    public InactivateUserEvent() {
    }

    public InactivateUserEvent(String id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Inactivate user \"" + getName() + "\"";
    }
}
