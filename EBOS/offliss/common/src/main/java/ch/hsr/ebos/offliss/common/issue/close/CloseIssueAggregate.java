package ch.hsr.ebos.offliss.common.issue.close;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.issue.IssueService;
import ch.hsr.ebos.offliss.common.issue.IssueState;

@AllowRole(OfflissRoles.ProjectManager)
@AllowRole(OfflissRoles.Developer)
public class CloseIssueAggregate extends EventAggregate<CloseIssueEvent> {
    @Inject
    IssueService issueService;

    @Override
    public void aggregate(ApplicationEvent<CloseIssueEvent> event) throws Throwable {
        issueService.changeIssueState(event.getData().getId(), IssueState.CLOSED);
    }
}
