package ch.hsr.ebos.offliss.common.project.close;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.offliss.common.StateChangeEvent;

@EventType("offliss.project.close")
@Description("Close Project")
public class CloseProjectEvent extends StateChangeEvent {

    public CloseProjectEvent() {
    }

    public CloseProjectEvent(String id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Close project \"" + getName() + "\"";
    }
}
