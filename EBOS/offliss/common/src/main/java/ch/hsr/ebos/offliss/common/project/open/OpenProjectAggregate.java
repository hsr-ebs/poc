package ch.hsr.ebos.offliss.common.project.open;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.project.ProjectService;
import ch.hsr.ebos.offliss.common.project.ProjectState;

@AllowRole(OfflissRoles.ProjectManager)
public class OpenProjectAggregate extends EventAggregate<OpenProjectEvent> {
    @Inject
    ProjectService projectService;

    @Override
    public void aggregate(ApplicationEvent<OpenProjectEvent> event) throws Throwable {
        projectService.changeProjectState(event, ProjectState.ACTIVE);
    }
}
