package ch.hsr.ebos.offliss.common.user.create;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;
import ch.hsr.ebos.offliss.common.user.UserRole;

@EventType("offliss.user.create")
@Description("Create User")
public class CreateUserEvent implements IEvent {
    private String id;
    private String username;
    private String fullName;
    private String email;
    private String avatar;
    private UserRole role;

    public CreateUserEvent() {

    }

    public CreateUserEvent(String id, String username, String fullName, String email, String avatar,
                           UserRole role) {
        this.id = id;
        this.username = username;
        this.fullName = fullName;
        this.email = email;
        this.avatar = avatar;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Create user with username \"" + username + "\", name \"" + fullName + "\", email \""
                + email + "\" and \"" + role + "\"";
    }
}
