package ch.hsr.ebos.offliss.common.timebooking;

import java.util.Date;

import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.aion.data.orm.annotation.Field;
import ch.hsr.ebos.aion.data.orm.annotation.Required;

public class TimeBookingEntity extends Entity {
    @Field
    @Required
    private String issueId;
    @Field
    @Required
    private String userId;
    @Field
    @Required
    private Date start;
    @Field
    @Required
    private Date stop;

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

}
