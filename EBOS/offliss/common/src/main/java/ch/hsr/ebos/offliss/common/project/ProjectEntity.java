package ch.hsr.ebos.offliss.common.project;

import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.aion.data.orm.annotation.Field;
import ch.hsr.ebos.aion.data.orm.annotation.Required;
import ch.hsr.ebos.aion.data.orm.annotation.Unique;

public class ProjectEntity extends Entity {

    @Unique
    @Required
    @Field
    private String name;

    @Field
    private ProjectState state;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProjectState getState() {
        return state;
    }

    public void setState(ProjectState state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
