package ch.hsr.ebos.offliss.common.user.inactivate;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.user.UserService;
import ch.hsr.ebos.offliss.common.user.UserState;

@AllowRole(OfflissRoles.Administrator)
public class InactivateUserAggregate extends EventAggregate<InactivateUserEvent> {
    @Inject
    UserService userService;

    @Override
    public void aggregate(ApplicationEvent<InactivateUserEvent> event) throws Throwable {
        userService.changeUserState(event, UserState.INACTIVE);
    }
}
