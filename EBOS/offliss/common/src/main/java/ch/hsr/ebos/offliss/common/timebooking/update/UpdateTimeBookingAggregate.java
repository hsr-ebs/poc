package ch.hsr.ebos.offliss.common.timebooking.update;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingEntity;

@AllowRole(OfflissRoles.ProjectManager)
@AllowRole(OfflissRoles.Developer)
public class UpdateTimeBookingAggregate extends EventAggregate<UpdateTimeBookingEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<UpdateTimeBookingEvent> event) throws Throwable {
        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, event.getData().getId());

        if (event.getData().getStart().hasChanged(timeBooking.getStart())) {
            timeBooking.setStart(event.getData().getStart().getNewValue());
        }
        if (event.getData().getStop().hasChanged(timeBooking.getStop())) {
            timeBooking.setStop(event.getData().getStop().getNewValue());
        }

        entityService.update(timeBooking);
    }
}
