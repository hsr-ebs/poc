package ch.hsr.ebos.offliss.common.issue;

import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.offliss.common.OfflissModule;
import ch.hsr.ebos.offliss.common.issue.assign.AssignIssueAggregate;
import ch.hsr.ebos.offliss.common.issue.assign.AssignIssueEvent;
import ch.hsr.ebos.offliss.common.issue.close.CloseAllIssuesAggregate;
import ch.hsr.ebos.offliss.common.issue.close.CloseIssueAggregate;
import ch.hsr.ebos.offliss.common.issue.close.CloseIssueEvent;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueAggregate;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueEvent;
import ch.hsr.ebos.offliss.common.issue.open.OpenIssueAggregate;
import ch.hsr.ebos.offliss.common.issue.open.OpenIssueEvent;
import ch.hsr.ebos.offliss.common.issue.unassign.UnassignIssueAggregate;
import ch.hsr.ebos.offliss.common.issue.unassign.UnassignIssueEvent;
import ch.hsr.ebos.offliss.common.issue.update.UpdateIssueAggregate;
import ch.hsr.ebos.offliss.common.issue.update.UpdateIssueEvent;
import ch.hsr.ebos.offliss.common.project.close.CloseProjectEvent;

public class IssueModule extends OfflissModule {

    @Override
    protected void setup() {
        addEventAggregate(AssignIssueEvent.class, AssignIssueAggregate.class);
        addEventAggregate(CloseIssueEvent.class, CloseIssueAggregate.class);
        addEventAggregate(CloseProjectEvent.class, CloseAllIssuesAggregate.class);
        addEventAggregate(CreateIssueEvent.class, CreateIssueAggregate.class);
        addEventAggregate(OpenIssueEvent.class, OpenIssueAggregate.class);
        addEventAggregate(UnassignIssueEvent.class, UnassignIssueAggregate.class);
        addEventAggregate(UpdateIssueEvent.class, UpdateIssueAggregate.class);
        bind(IssueService.class);
    }

    @Override
    protected void setupData(DataModule dataModule) {
        dataModule.addEntity(IssueEntity.class);
        dataModule.addEntity(IssueMemberEntity.class);
        dataModule.addSequence(IssueSequence.class);
    }
}
