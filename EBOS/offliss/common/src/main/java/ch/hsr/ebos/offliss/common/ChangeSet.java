package ch.hsr.ebos.offliss.common;

public class ChangeSet<T> {

    private T oldValue;
    private T newValue;

    public ChangeSet(T oldValue, T newValue) {
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public static <V> ChangeSet<V> Empty() {
        return new ChangeSet<>(null, null);
    }

    public T getNewValue() {
        return newValue;
    }

    public boolean hasChanged(T originValue) throws Exception {
        if (oldValue == null) {
            return newValue != null && originValue == null;
        } else if (!oldValue.equals(newValue)) {
            if (!oldValue.equals(originValue)) {
                throw new Exception("The value has been already changed to " + originValue);
            }
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "change value from \"" + oldValue + "\" to \"" + newValue + "\"";
    }
}
