package ch.hsr.ebos.offliss.common.user;

public enum UserRole {
    PROJECT_MANAGER("Project Manager"), DEVELOPER("Developer"), ADMINISTRATOR("Administrator");

    private String name;

    UserRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return this.name().toLowerCase();
    }
}
