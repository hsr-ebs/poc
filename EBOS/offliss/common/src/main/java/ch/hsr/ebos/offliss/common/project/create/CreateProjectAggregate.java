package ch.hsr.ebos.offliss.common.project.create;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.project.ProjectState;

@AllowRole(OfflissRoles.ProjectManager)
public class CreateProjectAggregate extends EventAggregate<CreateProjectEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<CreateProjectEvent> event) throws Throwable {
        ProjectEntity project = new ProjectEntity();
        project.setId(event.getData().getId());
        project.setState(ProjectState.ACTIVE);
        project.setName(event.getData().getName());

        entityService.insert(project);
    }
}
