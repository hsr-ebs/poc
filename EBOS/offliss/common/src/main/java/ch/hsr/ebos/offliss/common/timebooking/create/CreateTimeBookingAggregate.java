package ch.hsr.ebos.offliss.common.timebooking.create;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingEntity;

@AllowRole(OfflissRoles.ProjectManager)
@AllowRole(OfflissRoles.Developer)
public class CreateTimeBookingAggregate extends EventAggregate<CreateTimeBookingEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<CreateTimeBookingEvent> event) throws Throwable {
        TimeBookingEntity timeBooking = new TimeBookingEntity();
        timeBooking.setId(event.getData().getId());
        timeBooking.setIssueId(event.getData().getIssueId());
        timeBooking.setUserId(event.getData().getUserId());
        timeBooking.setStart(event.getData().getStart());
        timeBooking.setStop(event.getData().getStop());

        entityService.insert(timeBooking);
    }
}
