package ch.hsr.ebos.offliss.common.issue.assign;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("offliss.issue.assign")
@Description("Assign Issue")
public class AssignIssueEvent implements IEvent {
    private String id;
    private String userId;
    private String userFullName;
    private String issueName;

    public AssignIssueEvent() {
    }

    public AssignIssueEvent(String id, String userId) {
        this.id = id;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Assign user \"" + userFullName + "\" to issue \"" + issueName + "\"";
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }
}
