package ch.hsr.ebos.offliss.common.project.close;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.project.ProjectService;
import ch.hsr.ebos.offliss.common.project.ProjectState;

@AllowRole(OfflissRoles.ProjectManager)
public class CloseProjectAggregate extends EventAggregate<CloseProjectEvent> {
    @Inject
    ProjectService projectService;

    @Override
    public void aggregate(ApplicationEvent<CloseProjectEvent> event) throws Exception {
        projectService.changeProjectState(event, ProjectState.INACTIVE);
    }
}
