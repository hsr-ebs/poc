package ch.hsr.ebos.offliss.common.project.update;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;


@AllowRole(OfflissRoles.ProjectManager)
public class UpdateProjectAggregate extends EventAggregate<UpdateProjectEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<UpdateProjectEvent> event) throws Throwable {
        ProjectEntity project = entityService.get(ProjectEntity.class, event.getData().getId());

        if (event.getData().getName().hasChanged(project.getName())) {
            project.setName(event.getData().getName().getNewValue());
        }

        entityService.update(project);
    }
}
