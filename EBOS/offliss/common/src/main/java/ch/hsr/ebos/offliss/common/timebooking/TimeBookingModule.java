package ch.hsr.ebos.offliss.common.timebooking;

import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.offliss.common.OfflissModule;
import ch.hsr.ebos.offliss.common.timebooking.create.CreateTimeBookingAggregate;
import ch.hsr.ebos.offliss.common.timebooking.create.CreateTimeBookingEvent;
import ch.hsr.ebos.offliss.common.timebooking.remove.RemoveTimeBookingAggregate;
import ch.hsr.ebos.offliss.common.timebooking.remove.RemoveTimeBookingEvent;
import ch.hsr.ebos.offliss.common.timebooking.update.UpdateTimeBookingAggregate;
import ch.hsr.ebos.offliss.common.timebooking.update.UpdateTimeBookingEvent;

public class TimeBookingModule extends OfflissModule {

    @Override
    protected void setup() {
        addEventAggregate(CreateTimeBookingEvent.class, CreateTimeBookingAggregate.class);
        addEventAggregate(RemoveTimeBookingEvent.class, RemoveTimeBookingAggregate.class);
        addEventAggregate(UpdateTimeBookingEvent.class, UpdateTimeBookingAggregate.class);
    }

    @Override
    protected void setupData(DataModule dataModule) {
        dataModule.addEntity(TimeBookingEntity.class);
    }
}
