package ch.hsr.ebos.offliss.common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatUtils {
    public static String format(Date date, String format) {
        if (date == null) {
            return "-";
        }
        return new SimpleDateFormat(format).format(date);
    }

    public static String formatFullDate(Date date) {
        return format(date, "dd.MM.yyyy HH:mm");
    }

    public static String formatDate(Date date) {
        return format(date, "dd.MM.yyyy");
    }

    public static String formatTime(Date date) {
        return format(date, "HH:mm");
    }
}
