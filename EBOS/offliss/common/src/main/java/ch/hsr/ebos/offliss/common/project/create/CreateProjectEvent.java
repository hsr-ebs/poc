package ch.hsr.ebos.offliss.common.project.create;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("offliss.project.create")
@Description("Create Project")
public class CreateProjectEvent implements IEvent {
    private String id;
    private String name;

    public CreateProjectEvent() {

    }

    public CreateProjectEvent(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Create project \"" + name + "\"";
    }
}
