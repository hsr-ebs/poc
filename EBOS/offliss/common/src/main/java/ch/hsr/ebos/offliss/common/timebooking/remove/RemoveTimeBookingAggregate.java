package ch.hsr.ebos.offliss.common.timebooking.remove;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingEntity;

@AllowRole(OfflissRoles.ProjectManager)
@AllowRole(OfflissRoles.Developer)
public class RemoveTimeBookingAggregate extends EventAggregate<RemoveTimeBookingEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<RemoveTimeBookingEvent> event) throws Throwable {
        entityService.delete(TimeBookingEntity.class, event.getData().getId());
    }
}
