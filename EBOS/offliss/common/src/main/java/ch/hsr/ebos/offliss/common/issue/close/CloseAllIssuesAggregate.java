package ch.hsr.ebos.offliss.common.issue.close;

import com.google.inject.Inject;

import java.util.List;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueService;
import ch.hsr.ebos.offliss.common.issue.IssueState;
import ch.hsr.ebos.offliss.common.project.close.CloseProjectEvent;

@AllowRole(OfflissRoles.ProjectManager)
public class CloseAllIssuesAggregate extends EventAggregate<CloseProjectEvent> {
    @Inject
    EntityService entityService;

    @Inject
    IssueService issueService;

    @Override
    public void aggregate(ApplicationEvent<CloseProjectEvent> event) throws Throwable {
        Query issueQuery = Query.build().and(Filter.equals("projectId", event.getData().getId()));
        List<IssueEntity> issues = entityService.query(IssueEntity.class, issueQuery);

        for (IssueEntity issue : issues) {
            issueService.changeIssueState(issue, IssueState.CLOSED);
        }
    }
}
