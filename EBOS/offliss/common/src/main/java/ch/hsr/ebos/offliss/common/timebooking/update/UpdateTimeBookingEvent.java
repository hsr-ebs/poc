package ch.hsr.ebos.offliss.common.timebooking.update;

import java.util.Date;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.FormatUtils;

@EventType("offliss.timebooking.update")
@Description("Update Time Booking")
public class UpdateTimeBookingEvent implements IEvent {
    private String id;
    private ChangeSet<Date> start;
    private ChangeSet<Date> stop;

    public UpdateTimeBookingEvent() {
    }

    public UpdateTimeBookingEvent(String id, ChangeSet<Date> start, ChangeSet<Date> stop) {
        this.id = id;
        this.start = start;
        this.stop = stop;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ChangeSet<Date> getStart() {
        return start;
    }

    public void setStart(ChangeSet<Date> start) {
        this.start = start;
    }

    public ChangeSet<Date> getStop() {
        return stop;
    }

    public void setStop(ChangeSet<Date> stop) {
        this.stop = stop;
    }

    @Override
    public String toString() {
        return "Update timebooking with start " + FormatUtils.formatFullDate(start.getNewValue()) +
                " and stop " + FormatUtils.formatFullDate(stop.getNewValue());
    }
}
