package ch.hsr.ebos.offliss.common.issue;

import com.google.inject.Inject;

import java.util.List;

import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;

public class IssueService {
    @Inject
    protected EntityService entityService;

    public void changeIssueState(String issueId, IssueState state) throws Exception {
        IssueEntity issue = entityService.get(IssueEntity.class, issueId);
        changeIssueState(issue, state);
    }

    public void changeIssueState(IssueEntity issue, IssueState state) throws Exception {
        issue.setState(state);

        entityService.update(issue);

        Query issueQuery = Query.build().and(Filter.equals("parentId", issue.getId()));
        List<IssueEntity> childIssues = entityService.query(IssueEntity.class, issueQuery);

        for (IssueEntity childIssue : childIssues) {
            changeIssueState(childIssue, IssueState.CLOSED);
        }
    }
}
