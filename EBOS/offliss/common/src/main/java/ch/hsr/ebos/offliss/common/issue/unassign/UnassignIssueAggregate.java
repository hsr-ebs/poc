package ch.hsr.ebos.offliss.common.issue.unassign;

import com.google.inject.Inject;

import java.util.List;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.issue.IssueMemberEntity;

@AllowRole(OfflissRoles.ProjectManager)
@AllowRole(OfflissRoles.Developer)
public class UnassignIssueAggregate extends EventAggregate<UnassignIssueEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<UnassignIssueEvent> event) throws Throwable {
        Query memberQuery = Query.build().and(Filter.equals("userId", event.getData().getUserId()), Filter.equals("issueId", event.getData().getId()));
        List<IssueMemberEntity> issueMembers = entityService.query(IssueMemberEntity.class, memberQuery);
        if (issueMembers.isEmpty()) {
            throw new Exception("Could not found issue member with issue id " + event.getData().getId() + " and user id " + event.getData().getUserId());
        } else if (issueMembers.size() > 1) {
            throw new Exception("Found more than one issue member with issue id " + event.getData().getId() + " and user id " + event.getData().getUserId());
        }

        IssueMemberEntity issueMember = issueMembers.get(0);
        try {
            entityService.delete(IssueMemberEntity.class, issueMember.getId());
        } catch (Exception e) {
            throw new Exception("Could not unassign issue member with id " + issueMember.getId());
        }
    }
}
