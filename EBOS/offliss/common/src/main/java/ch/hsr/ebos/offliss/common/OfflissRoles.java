package ch.hsr.ebos.offliss.common;

public class OfflissRoles {
    public static final String ProjectManager = "project_manager";
    public static final String Developer = "developer";
    public static final String Administrator = "administrator";
}
