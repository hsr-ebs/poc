package ch.hsr.ebos.offliss.common.issue.update;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;

@AllowRole(OfflissRoles.ProjectManager)
@AllowRole(OfflissRoles.Developer)
public class UpdateIssueAggregate extends EventAggregate<UpdateIssueEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<UpdateIssueEvent> event) throws Throwable {
        IssueEntity issue = entityService.get(IssueEntity.class, event.getData().getId());

        if (event.getData().getTitle().hasChanged(issue.getTitle())) {
            issue.setTitle(event.getData().getTitle().getNewValue());
        }
        if (event.getData().getDescription().hasChanged(issue.getDescription())) {
            issue.setDescription(event.getData().getDescription().getNewValue());
        }
        if (event.getData().getParentId().hasChanged(issue.getParentId())) {
            issue.setParentId(event.getData().getParentId().getNewValue());
        }

        entityService.update(issue);
    }
}
