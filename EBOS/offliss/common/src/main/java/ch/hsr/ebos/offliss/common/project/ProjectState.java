package ch.hsr.ebos.offliss.common.project;

public enum ProjectState {
    ACTIVE, INACTIVE
}
