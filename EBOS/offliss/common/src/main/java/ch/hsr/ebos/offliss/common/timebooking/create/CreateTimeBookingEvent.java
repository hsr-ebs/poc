package ch.hsr.ebos.offliss.common.timebooking.create;

import java.util.Date;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;
import ch.hsr.ebos.offliss.common.FormatUtils;

@EventType("offliss.timebooking.create")
@Description("Create Time Booking")
public class CreateTimeBookingEvent implements IEvent {
    private String id;
    private String issueId;
    private String issueName;
    private String userId;
    private Date start;
    private Date stop;

    public CreateTimeBookingEvent() {
    }

    public CreateTimeBookingEvent(String id, String issueId, String userId, Date start, Date stop) {
        this.id = id;
        this.issueId = issueId;
        this.userId = userId;
        this.start = start;
        this.stop = stop;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

    @Override
    public String toString() {
        return "Create timebooking on issue \"" + issueName + "\" with start " +
                FormatUtils.formatFullDate(start) + " and stop " + FormatUtils.formatFullDate(stop);
    }

    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }
}
