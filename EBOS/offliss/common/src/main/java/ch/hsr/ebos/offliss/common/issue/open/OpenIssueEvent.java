package ch.hsr.ebos.offliss.common.issue.open;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.offliss.common.StateChangeEvent;

@EventType("offliss.issue.open")
@Description("Open Issue")
public class OpenIssueEvent extends StateChangeEvent {

    public OpenIssueEvent() {
    }

    public OpenIssueEvent(String id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Reopen issue \"" + getName() + "\"";
    }
}
