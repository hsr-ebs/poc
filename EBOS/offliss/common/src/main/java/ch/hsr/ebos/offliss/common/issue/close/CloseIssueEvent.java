package ch.hsr.ebos.offliss.common.issue.close;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.offliss.common.StateChangeEvent;

@EventType("offliss.issue.close")
@Description("Close Issue")
public class CloseIssueEvent extends StateChangeEvent {

    public CloseIssueEvent() {
    }

    public CloseIssueEvent(String id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Close issue \"" + getName() + "\"";
    }
}
