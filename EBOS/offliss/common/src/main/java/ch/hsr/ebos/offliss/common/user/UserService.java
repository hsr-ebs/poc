package ch.hsr.ebos.offliss.common.user;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.StateChangeEvent;
import ch.hsr.ebos.offliss.common.user.update.UpdateUserEvent;

public class UserService {

    @Inject
    EntityService entityService;

    public void changeUserState(ApplicationEvent<? extends StateChangeEvent> event, UserState state) throws Exception {
        UserEntity user = entityService.get(UserEntity.class, event.getData().getId());

        user.setState(state);

        entityService.update(user);
    }

    public void updateUser(ApplicationEvent<UpdateUserEvent> event) throws Exception {
        UserEntity user = entityService.get(UserEntity.class, event.getData().getId());
        if (user == null) {
            throw new Exception("No User found with id " + event.getData().getId());
        } else {
            if (event.getData().getFullName().hasChanged(user.getFullName())) {
                user.setFullName(event.getData().getFullName().getNewValue());
            }
            if (event.getData().getEmail().hasChanged(user.getEmail())) {
                user.setEmail(event.getData().getEmail().getNewValue());
            }
            if (event.getData().getAvatar().hasChanged(user.getAvatar())) {
                user.setAvatar(event.getData().getAvatar().getNewValue());
            }
            if (event.getData().getRole().hasChanged(user.getRole())) {
                user.setRole(event.getData().getRole().getNewValue());
            }

            entityService.update(user);
        }
    }

}
