package ch.hsr.ebos.offliss.common.project;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.StateChangeEvent;

public class ProjectService {
    @Inject
    EntityService entityService;

    public void changeProjectState(ApplicationEvent<? extends StateChangeEvent> event, ProjectState state) throws Exception {
        ProjectEntity project = entityService.get(ProjectEntity.class, event.getData().getId());

        project.setState(state);

        entityService.update(project);
    }

}
