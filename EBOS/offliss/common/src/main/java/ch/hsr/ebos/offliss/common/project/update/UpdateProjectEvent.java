package ch.hsr.ebos.offliss.common.project.update;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;
import ch.hsr.ebos.offliss.common.ChangeSet;

@EventType("offliss.project.update")
@Description("Update Project")
public class UpdateProjectEvent implements IEvent {
    private String id;
    private ChangeSet<String> name;

    public UpdateProjectEvent() {
    }

    public UpdateProjectEvent(String id, ChangeSet<String> name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ChangeSet<String> getName() {
        return name;
    }

    public void setName(ChangeSet<String> name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Update project name \"" + name + "\"";
    }
}
