package ch.hsr.ebos.offliss.common.user.create;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.UserState;

@AllowRole(OfflissRoles.Administrator)
public class CreateUserAggregate extends EventAggregate<CreateUserEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<CreateUserEvent> event) throws Throwable {
        UserEntity user = new UserEntity();
        user.setId(event.getData().getId());
        user.setUsername(event.getData().getUsername());
        user.setFullName(event.getData().getFullName());
        user.setEmail(event.getData().getEmail());
        user.setAvatar(event.getData().getAvatar());
        user.setState(UserState.ACTIVE);
        user.setRole(event.getData().getRole());

        entityService.insert(user);
    }
}
