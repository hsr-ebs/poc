package ch.hsr.ebos.offliss.common.timebooking.remove;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("offliss.timebooking.remove")
@Description("Remove Time Booking")
public class RemoveTimeBookingEvent implements IEvent {
    private String id;

    public RemoveTimeBookingEvent() {
    }

    public RemoveTimeBookingEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Remove timebooking";
    }
}
