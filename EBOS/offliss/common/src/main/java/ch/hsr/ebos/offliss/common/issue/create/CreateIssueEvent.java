package ch.hsr.ebos.offliss.common.issue.create;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("offliss.issue.create")
@Description("Create Issue")
public class CreateIssueEvent implements IEvent {
    private String id;
    private String title;
    private String description;
    private String projectId;
    private String parentId;

    public CreateIssueEvent() {
    }

    public CreateIssueEvent(String id, String title, String description, String projectId, String parentId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.projectId = projectId;
        this.parentId = parentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "Create issue \"" + title + "\"";
    }
}
