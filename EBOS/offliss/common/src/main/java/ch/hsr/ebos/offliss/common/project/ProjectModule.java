package ch.hsr.ebos.offliss.common.project;

import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.offliss.common.OfflissModule;
import ch.hsr.ebos.offliss.common.project.close.CloseProjectAggregate;
import ch.hsr.ebos.offliss.common.project.close.CloseProjectEvent;
import ch.hsr.ebos.offliss.common.project.create.CreateProjectAggregate;
import ch.hsr.ebos.offliss.common.project.create.CreateProjectEvent;
import ch.hsr.ebos.offliss.common.project.open.OpenProjectAggregate;
import ch.hsr.ebos.offliss.common.project.open.OpenProjectEvent;
import ch.hsr.ebos.offliss.common.project.update.UpdateProjectAggregate;
import ch.hsr.ebos.offliss.common.project.update.UpdateProjectEvent;

public class ProjectModule extends OfflissModule {

    @Override
    protected void setup() {
        addEventAggregate(CreateProjectEvent.class, CreateProjectAggregate.class);
        addEventAggregate(UpdateProjectEvent.class, UpdateProjectAggregate.class);
        addEventAggregate(CloseProjectEvent.class, CloseProjectAggregate.class);
        addEventAggregate(OpenProjectEvent.class, OpenProjectAggregate.class);
        bind(ProjectService.class);
    }

    @Override
    protected void setupData(DataModule dataModule) {
        dataModule.addEntity(ProjectEntity.class);
    }
}
