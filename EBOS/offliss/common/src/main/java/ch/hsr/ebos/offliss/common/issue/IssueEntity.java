package ch.hsr.ebos.offliss.common.issue;

import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.aion.data.orm.annotation.Field;
import ch.hsr.ebos.aion.data.orm.annotation.Required;
import ch.hsr.ebos.aion.data.orm.annotation.Unique;

public class IssueEntity extends Entity {
    @Unique
    @Field
    @Required
    private String title;
    @Unique
    @Field
    @Required
    private int number;
    @Field
    private String description;
    @Field
    @Required
    private IssueState state;
    @Field
    @Required
    private String projectId;
    @Field
    private String parentId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public IssueState getState() {
        return state;
    }

    public void setState(IssueState state) {
        this.state = state;
    }

    public String getName() {
        return "#" + number + ": " + title;
    }

    @Override
    public String toString() {
        return getName();
    }
}
