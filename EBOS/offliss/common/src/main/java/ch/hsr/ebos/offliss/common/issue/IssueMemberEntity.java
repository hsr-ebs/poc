package ch.hsr.ebos.offliss.common.issue;

import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.aion.data.orm.annotation.Field;
import ch.hsr.ebos.aion.data.orm.annotation.Required;
import ch.hsr.ebos.aion.data.orm.annotation.Unique;

public class IssueMemberEntity extends Entity {
    @Field
    @Required
    @Unique(name = "unique_title_number")
    private String issueId;
    @Field
    @Required
    @Unique(name = "unique_title_number")
    private String userId;

    public IssueMemberEntity() {
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
