package ch.hsr.ebos.offliss.common.user.update;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.user.UserRole;

@EventType("offliss.user.update")
@Description("Update User")
public class UpdateUserEvent implements IEvent {
    private String id;
    private String username;
    private ChangeSet<String> fullName;
    private ChangeSet<String> email;
    private ChangeSet<String> avatar;
    private ChangeSet<UserRole> role;

    public UpdateUserEvent() {
    }

    public UpdateUserEvent(String id, String username, ChangeSet<String> fullName,
                           ChangeSet<String> email, ChangeSet<String> avatar, ChangeSet<UserRole> role) {
        this.id = id;
        this.username = username;
        this.fullName = fullName;
        this.email = email;
        this.avatar = avatar;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ChangeSet<String> getFullName() {
        return fullName;
    }

    public void setFullName(ChangeSet<String> fullName) {
        this.fullName = fullName;
    }

    public ChangeSet<String> getEmail() {
        return email;
    }

    public void setEmail(ChangeSet<String> email) {
        this.email = email;
    }

    public ChangeSet<String> getAvatar() {
        return avatar;
    }

    public void setAvatar(ChangeSet<String> avatar) {
        this.avatar = avatar;
    }

    public ChangeSet<UserRole> getRole() {
        return role;
    }

    public void setRole(ChangeSet<UserRole> role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Update user name \"" + fullName + "\", email \"" + email + "\" and \""
                + role + "\"";
    }
}
