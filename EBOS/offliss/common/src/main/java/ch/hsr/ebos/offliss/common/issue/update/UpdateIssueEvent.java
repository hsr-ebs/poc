package ch.hsr.ebos.offliss.common.issue.update;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;
import ch.hsr.ebos.offliss.common.ChangeSet;

@EventType("offliss.issue.update")
@Description("Update Issue")
public class UpdateIssueEvent implements IEvent {
    private String id;
    private ChangeSet<String> title;
    private ChangeSet<String> description;
    private ChangeSet<String> parentId;

    public UpdateIssueEvent() {
    }

    public UpdateIssueEvent(String id, ChangeSet<String> title, ChangeSet<String> description, ChangeSet<String> parentId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.parentId = parentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ChangeSet<String> getTitle() {
        return title;
    }

    public void setTitle(ChangeSet<String> title) {
        this.title = title;
    }

    public ChangeSet<String> getDescription() {
        return description;
    }

    public void setDescription(ChangeSet<String> description) {
        this.description = description;
    }

    public ChangeSet<String> getParentId() {
        return parentId;
    }

    public void setParentId(ChangeSet<String> parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "Update issue \"" + title + "\"";
    }
}
