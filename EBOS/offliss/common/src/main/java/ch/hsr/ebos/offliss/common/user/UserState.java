package ch.hsr.ebos.offliss.common.user;

public enum UserState {
    ACTIVE, INACTIVE
}
