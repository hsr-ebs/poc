package ch.hsr.ebos.offliss.common.user.reset;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("offliss.user.password.reset")
@Description("Reset User Password")
public class ResetUserPasswordEvent implements IEvent {
    private String id;
    private String username;

    public ResetUserPasswordEvent() {

    }

    public ResetUserPasswordEvent(String id, String username) {
        this.id = id;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Reset password for user with username \"" + username + "\"";
    }
}
