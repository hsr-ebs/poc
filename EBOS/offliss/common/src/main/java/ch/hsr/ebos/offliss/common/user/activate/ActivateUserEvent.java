package ch.hsr.ebos.offliss.common.user.activate;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.offliss.common.StateChangeEvent;

@EventType("offliss.user.activate")
@Description("Activate User")
public class ActivateUserEvent extends StateChangeEvent {

    public ActivateUserEvent() {
    }

    public ActivateUserEvent(String id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Activate user \"" + getName() + "\"";
    }
}
