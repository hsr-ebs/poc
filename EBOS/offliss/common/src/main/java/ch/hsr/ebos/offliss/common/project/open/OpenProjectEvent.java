package ch.hsr.ebos.offliss.common.project.open;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.offliss.common.StateChangeEvent;

@EventType("offliss.project.open")
@Description("Open Project")
public class OpenProjectEvent extends StateChangeEvent {

    public OpenProjectEvent() {
    }

    public OpenProjectEvent(String id) {
        super(id);
    }

    @Override
    public String toString() {
        return "Reopen project \"" + getName() + "\"";
    }
}
