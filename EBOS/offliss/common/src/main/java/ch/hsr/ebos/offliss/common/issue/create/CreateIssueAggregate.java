package ch.hsr.ebos.offliss.common.issue.create;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueSequence;
import ch.hsr.ebos.offliss.common.issue.IssueState;

@AllowRole(OfflissRoles.ProjectManager)
@AllowRole(OfflissRoles.Developer)
public class CreateIssueAggregate extends EventAggregate<CreateIssueEvent> {
    @Inject
    EntityService entityService;

    @Inject
    IssueSequence issueSequence;

    @Override
    public void aggregate(ApplicationEvent<CreateIssueEvent> event) throws Throwable {
        IssueEntity issue = new IssueEntity();
        issue.setId(event.getData().getId());
        issue.setDescription(event.getData().getDescription());
        issue.setTitle(event.getData().getTitle());
        issue.setState(IssueState.OPEN);
        issue.setNumber(issueSequence.getNext());
        issue.setProjectId(event.getData().getProjectId());
        issue.setParentId(event.getData().getParentId());

        entityService.insert(issue);
    }
}
