package ch.hsr.ebos.offliss.common;

import ch.hsr.ebos.aion.auth.AuthModule;
import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.aion.logging.LogLevel;
import ch.hsr.ebos.aion.logging.LoggingModule;

public abstract class OfflissModule extends Module {

    public void setDataModule(DataModule dataModule) {
        setupData(dataModule);
        addModule(dataModule);
        addModule(new LoggingModule(LogLevel.DEBUG));
        addModule(new AuthModule());
    }

    protected abstract void setupData(DataModule dataModule);
}
