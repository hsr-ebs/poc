package ch.hsr.ebos.offliss.common.user;

import java.util.Objects;

import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.aion.data.orm.annotation.Field;
import ch.hsr.ebos.aion.data.orm.annotation.Required;
import ch.hsr.ebos.aion.data.orm.annotation.Unique;

public class UserEntity extends Entity {
    @Unique
    @Field
    @Required
    private String username;
    @Unique
    @Field
    @Required
    private String email;
    @Field
    @Required
    private String fullName;
    @Field
    private String avatar;
    @Field
    @Required
    private UserState state;
    @Field
    @Required
    private UserRole role;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public UserState getState() {
        return state;
    }

    public void setState(UserState state) {
        this.state = state;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return this.username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserEntity)) return false;
        if (!super.equals(o)) return false;
        UserEntity that = (UserEntity) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(email, that.email) &&
                Objects.equals(fullName, that.fullName) &&
                Objects.equals(avatar, that.avatar) &&
                state == that.state &&
                role == that.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, email, fullName, avatar, state, role);
    }
}
