package ch.hsr.ebos.offliss.common.issue.unassign;

import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("offliss.issue.unassign")
@Description("Unassign Issue")
public class UnassignIssueEvent implements IEvent {
    private String id;
    private String userId;
    private String issueName;
    private String userFullName;

    public UnassignIssueEvent() {
    }

    public UnassignIssueEvent(String id, String userId) {
        this.id = id;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Unassign user \"" + getUserFullName() + "\" from issue \"" + getIssueName() + "\"";
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getIssueName() {
        return issueName;
    }

    public void setIssueName(String issueName) {
        this.issueName = issueName;
    }
}
