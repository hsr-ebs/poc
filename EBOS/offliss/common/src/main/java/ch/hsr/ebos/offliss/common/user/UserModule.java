package ch.hsr.ebos.offliss.common.user;

import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.offliss.common.OfflissModule;
import ch.hsr.ebos.offliss.common.user.activate.ActivateUserAggregate;
import ch.hsr.ebos.offliss.common.user.activate.ActivateUserEvent;
import ch.hsr.ebos.offliss.common.user.create.CreateUserAggregate;
import ch.hsr.ebos.offliss.common.user.create.CreateUserEvent;
import ch.hsr.ebos.offliss.common.user.inactivate.InactivateUserAggregate;
import ch.hsr.ebos.offliss.common.user.inactivate.InactivateUserEvent;
import ch.hsr.ebos.offliss.common.user.update.UpdateUserAggregate;
import ch.hsr.ebos.offliss.common.user.update.UpdateUserEvent;

public class UserModule extends OfflissModule {

    @Override
    protected void setup() {
        addEventAggregate(CreateUserEvent.class, CreateUserAggregate.class);
        addEventAggregate(ActivateUserEvent.class, ActivateUserAggregate.class);
        addEventAggregate(InactivateUserEvent.class, InactivateUserAggregate.class);
        addEventAggregate(UpdateUserEvent.class, UpdateUserAggregate.class);
        bind(UserService.class);
    }

    @Override
    protected void setupData(DataModule dataModule) {
        dataModule.addEntity(UserEntity.class);
    }
}
