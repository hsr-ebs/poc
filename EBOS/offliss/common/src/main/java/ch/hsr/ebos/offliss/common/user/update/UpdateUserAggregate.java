package ch.hsr.ebos.offliss.common.user.update;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.user.UserService;

@AllowRole(OfflissRoles.Administrator)
public class UpdateUserAggregate extends EventAggregate<UpdateUserEvent> {

    @Inject
    UserService userService;

    @Override
    public void aggregate(ApplicationEvent<UpdateUserEvent> event) throws Throwable {
        userService.updateUser(event);
    }
}
