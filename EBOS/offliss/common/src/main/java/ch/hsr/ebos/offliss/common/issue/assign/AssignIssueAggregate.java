package ch.hsr.ebos.offliss.common.issue.assign;

import com.google.inject.Inject;

import java.util.List;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.issue.IssueMemberEntity;

@AllowRole(OfflissRoles.ProjectManager)
@AllowRole(OfflissRoles.Developer)
public class AssignIssueAggregate extends EventAggregate<AssignIssueEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<AssignIssueEvent> event) throws Throwable {
        Query memberQuery = Query.build().and(Filter.equals("userId", event.getData().getUserId()), Filter.equals("issueId", event.getData().getId()));
        List<IssueMemberEntity> issueMembers = entityService.query(IssueMemberEntity.class, memberQuery);

        if (!issueMembers.isEmpty()) {
            return;
        }

        IssueMemberEntity member = new IssueMemberEntity();
        member.setId(event.getId());
        member.setIssueId(event.getData().getId());
        member.setUserId(event.getData().getUserId());

        entityService.insert(member);
    }
}
