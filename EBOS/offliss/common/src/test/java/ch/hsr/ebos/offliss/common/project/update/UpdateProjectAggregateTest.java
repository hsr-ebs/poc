package ch.hsr.ebos.offliss.common.project.update;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.project.ProjectAggregateTest;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.project.ProjectState;
import ch.hsr.ebos.offliss.common.project.create.CreateProjectEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class UpdateProjectAggregateTest extends ProjectAggregateTest {

    @Test
    public void testUpdateName() throws Exception {
        EmitResult createProject = application.emit(new CreateProjectEvent("a", "Smile"), getProjectManagerAuth()).get();
        assertTrue(createProject.isSuccessfull());

        EmitResult updateProject = application.emit(new UpdateProjectEvent("a",
                new ChangeSet<>("Smile", "Innocent")), getProjectManagerAuth()).get();
        assertTrue(updateProject.isSuccessfull());

        ProjectEntity project = entityService.get(ProjectEntity.class, "a");
        assertNotNull(project);
        assertEquals("Innocent", project.getName());
        assertEquals(ProjectState.ACTIVE, project.getState());
    }

    @Test
    public void testUpdateNameConflict() throws Exception {
        EmitResult createProject = application.emit(new CreateProjectEvent("a", "Smile"), getProjectManagerAuth()).get();
        assertTrue(createProject.isSuccessfull());

        EmitResult updateProject = application.emit(new UpdateProjectEvent("a",
                new ChangeSet<>("Other Change", "Innocent")), getProjectManagerAuth()).get();
        assertFalse(updateProject.isSuccessfull());

        ProjectEntity project = entityService.get(ProjectEntity.class, "a");
        assertNotNull(project);
        assertEquals("Smile", project.getName());
        assertEquals(ProjectState.ACTIVE, project.getState());
    }

    @Test
    public void testUniqueConstraint() throws Exception {
        EmitResult createProject = application.emit(new CreateProjectEvent("a", "Smile"), getProjectManagerAuth()).get();
        assertTrue(createProject.isSuccessfull());

        EmitResult createOtherProject = application.emit(new CreateProjectEvent("b", "Innocent"), getProjectManagerAuth()).get();
        assertTrue(createOtherProject.isSuccessfull());

        EmitResult updateProject = application.emit(new UpdateProjectEvent("b",
                new ChangeSet<>("Innocent", "Smile")), getProjectManagerAuth()).get();
        assertFalse(updateProject.isSuccessfull());

        ProjectEntity project = entityService.get(ProjectEntity.class, "b");
        assertNotNull(project);
        assertEquals("Innocent", project.getName());
        assertEquals(ProjectState.ACTIVE, project.getState());
    }

    @Test
    public void testUpdateSameValue() throws Exception {
        EmitResult createProject = application.emit(new CreateProjectEvent("a", "Smile"), getProjectManagerAuth()).get();
        assertTrue(createProject.isSuccessfull());

        EmitResult updateProject = application.emit(new UpdateProjectEvent("a",
                new ChangeSet<>("Smile", "Smile")), getProjectManagerAuth()).get();
        assertTrue(updateProject.isSuccessfull());

        ProjectEntity project = entityService.get(ProjectEntity.class, "a");
        assertNotNull(project);
        assertEquals("Smile", project.getName());
        assertEquals(ProjectState.ACTIVE, project.getState());
    }

    @Test
    public void testProjectIsNotFound() throws ExecutionException, InterruptedException {
        EmitResult noProjectFound = application.emit(new UpdateProjectEvent("a", null), getProjectManagerAuth()).get();
        assertFalse(noProjectFound.isSuccessfull());
    }
}