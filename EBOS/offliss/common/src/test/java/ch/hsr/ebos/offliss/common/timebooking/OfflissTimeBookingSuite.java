package ch.hsr.ebos.offliss.common.timebooking;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ch.hsr.ebos.offliss.common.timebooking.create.CreateTimeBookingAggregateTest;
import ch.hsr.ebos.offliss.common.timebooking.remove.RemoveTimeBookingAggregateTest;
import ch.hsr.ebos.offliss.common.timebooking.update.UpdateTimeBookingAggregateTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CreateTimeBookingAggregateTest.class,
        RemoveTimeBookingAggregateTest.class,
        UpdateTimeBookingAggregateTest.class
})
public class OfflissTimeBookingSuite {
}
