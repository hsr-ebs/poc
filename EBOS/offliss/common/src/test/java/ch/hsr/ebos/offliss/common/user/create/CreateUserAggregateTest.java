package ch.hsr.ebos.offliss.common.user.create;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.user.UserAggregateTest;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.UserRole;
import ch.hsr.ebos.offliss.common.user.UserState;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CreateUserAggregateTest extends UserAggregateTest {

    @Test
    public void testIsUserCreated() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a", "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testCreateUserWithSameValues() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a", "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult createOtherUser = application.emit(new CreateUserEvent("b", "hansmuster1", "Hans Muster",
                "muster.hans1@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createOtherUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "b");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans1@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testErrorOnUniqueEmail() throws ExecutionException, InterruptedException {
        EmitResult createUser = application.emit(new CreateUserEvent("a", "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult createUniqueConstraintError = application.emit(new CreateUserEvent("b",
                "Hans Meier", "hansmuster1", "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes9/nose8/mouth9/ff00ff",
                UserRole.DEVELOPER)).get();
        assertFalse(createUniqueConstraintError.isSuccessfull());
    }

}