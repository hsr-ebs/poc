package ch.hsr.ebos.offliss.common;

import org.junit.After;
import org.junit.Before;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.data.sqlite.connection.JavaSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;

public abstract class AggregateTest {
    protected static final String databaseFile = "test.db";
    protected Application application;
    protected EntityService entityService;
    protected SqliteConnectionFactory connectionFactory;

    protected abstract Application getApplication() throws ExecutionException, InterruptedException;

    @Before
    public void setUp() throws ExecutionException, InterruptedException {
        clearDatabase();
        connectionFactory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
        application = getApplication();
        entityService = application.injector.getInstance(EntityService.class);
    }

    public TimelineEventAuth getAdminAuth() {
        List<String> roles = new ArrayList<>();
        roles.add(OfflissRoles.Administrator);
        return new TimelineEventAuth(null, "admin", roles);
    }

    public TimelineEventAuth getDeveloperAuth() {
        List<String> roles = new ArrayList<>();
        roles.add(OfflissRoles.Developer);
        return new TimelineEventAuth(null, "developer", roles);
    }

    public TimelineEventAuth getProjectManagerAuth() {
        List<String> roles = new ArrayList<>();
        roles.add(OfflissRoles.ProjectManager);
        return new TimelineEventAuth(null, "project_manager", roles);
    }

    @After
    public void tearDown() throws SQLException {
        connectionFactory.close(databaseFile);
        connectionFactory = null;
        this.clearDatabase();
    }

    private void clearDatabase() {
        File dataFile = new File(databaseFile);
        if (dataFile.exists()) {
            dataFile.delete();
        }
    }
}
