package ch.hsr.ebos.offliss.common.project.open;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.project.ProjectAggregateTest;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.project.ProjectService;
import ch.hsr.ebos.offliss.common.project.ProjectState;
import ch.hsr.ebos.offliss.common.project.close.CloseProjectEvent;
import ch.hsr.ebos.offliss.common.project.create.CreateProjectEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class OpenProjectAggregateTest extends ProjectAggregateTest {

    @Override
    public void setUp() throws ExecutionException, InterruptedException {
        super.setUp();
        application.injector.getInstance(ProjectService.class);
    }

    @Test
    public void testIsProjectOpen() throws Exception {
        EmitResult createProject = application.emit(new CreateProjectEvent("a", "Smile"), getProjectManagerAuth()).get();
        assertTrue(createProject.isSuccessfull());

        EmitResult inactivateProject = application.emit(new CloseProjectEvent("a"), getProjectManagerAuth()).get();
        assertTrue(inactivateProject.isSuccessfull());

        EmitResult activateProject = application.emit(new OpenProjectEvent("a"), getProjectManagerAuth()).get();
        assertTrue(activateProject.isSuccessfull());

        ProjectEntity project = entityService.get(ProjectEntity.class, "a");
        assertNotNull(project);
        assertEquals("Smile", project.getName());
        assertEquals(ProjectState.ACTIVE, project.getState());
    }

    @Test
    public void testProjectIsNotFound() throws ExecutionException, InterruptedException {
        EmitResult noProjectFound = application.emit(new OpenProjectEvent("a"), getProjectManagerAuth()).get();
        assertFalse(noProjectFound.isSuccessfull());
    }
}