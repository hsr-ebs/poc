package ch.hsr.ebos.offliss.common.issue.close;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.issue.IssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueState;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CloseIssueAggregateTest extends IssueAggregateTest {

    @Test
    public void testIsIssueClosed() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult inactivateIssue = application.emit(new CloseIssueEvent("a"), getProjectManagerAuth()).get();
        assertTrue(inactivateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.CLOSED, issue.getState());
    }

    @Test
    public void testCloseParentIssue() throws Exception {
        EmitResult createParentIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", null), getProjectManagerAuth()).get();
        assertTrue(createParentIssue.isSuccessfull());

        EmitResult createChildIssue = application.emit(new CreateIssueEvent("b", "Start Sprint", "My description", "projectId", "a"), getProjectManagerAuth()).get();
        assertTrue(createChildIssue.isSuccessfull());

        EmitResult inactivateParentIssue = application.emit(new CloseIssueEvent("a"), getProjectManagerAuth()).get();
        assertTrue(inactivateParentIssue.isSuccessfull());

        IssueEntity parent = entityService.get(IssueEntity.class, "a");
        assertNotNull(parent);
        assertEquals("Plan Sprint", parent.getTitle());
        assertEquals("My description", parent.getDescription());
        assertEquals(1, parent.getNumber());
        assertEquals("projectId", parent.getProjectId());
        assertNull(parent.getParentId());
        assertEquals(IssueState.CLOSED, parent.getState());

        IssueEntity child = entityService.get(IssueEntity.class, "b");
        assertNotNull(child);
        assertEquals("Start Sprint", child.getTitle());
        assertEquals("My description", child.getDescription());
        assertEquals(2, child.getNumber());
        assertEquals("projectId", child.getProjectId());
        assertEquals("a", child.getParentId());
        assertEquals(IssueState.CLOSED, child.getState());
    }

    @Test
    public void testIssueIsNotFound() throws ExecutionException, InterruptedException {
        EmitResult noIssueFound = application.emit(new CloseIssueEvent("a"), getProjectManagerAuth()).get();
        assertFalse(noIssueFound.isSuccessfull());
    }

}