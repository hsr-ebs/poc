package ch.hsr.ebos.offliss.common.project.create;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.project.ProjectAggregateTest;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.project.ProjectState;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CreateProjectAggregateTest extends ProjectAggregateTest {

    @Test
    public void testIsProjectCreated() throws Exception {
        EmitResult createProject = application.emit(new CreateProjectEvent("a", "Smile"), getProjectManagerAuth()).get();
        assertTrue(createProject.isSuccessfull());

        ProjectEntity project = entityService.get(ProjectEntity.class, "a");
        assertNotNull(project);
        assertEquals("Smile", project.getName());
        assertEquals(ProjectState.ACTIVE, project.getState());
    }

    @Test
    public void testErrorOnUniqueName() throws ExecutionException, InterruptedException {
        EmitResult createProject = application.emit(new CreateProjectEvent("a", "Smile"), getProjectManagerAuth()).get();
        assertTrue(createProject.isSuccessfull());

        EmitResult createUniqueConstraintError = application.emit(new CreateProjectEvent("b", "Smile"), getProjectManagerAuth()).get();
        assertFalse(createUniqueConstraintError.isSuccessfull());
    }
}
