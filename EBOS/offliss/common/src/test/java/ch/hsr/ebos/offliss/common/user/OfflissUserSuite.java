package ch.hsr.ebos.offliss.common.user;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ch.hsr.ebos.offliss.common.user.activate.ActivateUserAggregateTest;
import ch.hsr.ebos.offliss.common.user.create.CreateUserAggregateTest;
import ch.hsr.ebos.offliss.common.user.inactivate.InactivateUserAggregateTest;
import ch.hsr.ebos.offliss.common.user.update.UpdateUserAggregateTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ActivateUserAggregateTest.class,
        CreateUserAggregateTest.class,
        InactivateUserAggregateTest.class,
        UpdateUserAggregateTest.class
})
public class OfflissUserSuite {
}
