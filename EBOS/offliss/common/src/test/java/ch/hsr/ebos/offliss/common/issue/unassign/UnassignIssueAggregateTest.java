package ch.hsr.ebos.offliss.common.issue.unassign;

import org.junit.Test;

import java.util.List;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.offliss.common.issue.IssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.IssueMemberEntity;
import ch.hsr.ebos.offliss.common.issue.assign.AssignIssueEvent;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueEvent;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class UnassignIssueAggregateTest extends IssueAggregateTest {

    @Test
    public void testIsIssueUnassigned() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult assigneIssue = application.emit(new AssignIssueEvent("a", "u"), getProjectManagerAuth()).get();
        assertTrue(assigneIssue.isSuccessfull());

        EmitResult unassigneIssue = application.emit(new UnassignIssueEvent("a", "u"), getProjectManagerAuth()).get();
        assertTrue(unassigneIssue.isSuccessfull());

        Query memberQuery = Query.build().and(Filter.equals("userId", "u"), Filter.equals("issueId", "a"));
        List<IssueMemberEntity> issueMember = entityService.query(IssueMemberEntity.class, memberQuery);
        assertNotNull(issueMember);
        assertTrue(issueMember.isEmpty());
    }

}