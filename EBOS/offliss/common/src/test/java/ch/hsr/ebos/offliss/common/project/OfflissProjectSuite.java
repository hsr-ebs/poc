package ch.hsr.ebos.offliss.common.project;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ch.hsr.ebos.offliss.common.project.close.CloseProjectAggregateTest;
import ch.hsr.ebos.offliss.common.project.create.CreateProjectAggregateTest;
import ch.hsr.ebos.offliss.common.project.open.OpenProjectAggregateTest;
import ch.hsr.ebos.offliss.common.project.update.UpdateProjectAggregateTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        CloseProjectAggregateTest.class,
        CreateProjectAggregateTest.class,
        OpenProjectAggregateTest.class,
        UpdateProjectAggregateTest.class
})
public class OfflissProjectSuite {
}
