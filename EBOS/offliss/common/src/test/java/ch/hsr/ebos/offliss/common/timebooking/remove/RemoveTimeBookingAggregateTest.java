package ch.hsr.ebos.offliss.common.timebooking.remove;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingAggregateTest;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingEntity;
import ch.hsr.ebos.offliss.common.timebooking.create.CreateTimeBookingEvent;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class RemoveTimeBookingAggregateTest extends TimeBookingAggregateTest {

    @Test
    public void testIsTimeBookingDeleted() throws Exception {
        Calendar start = Calendar.getInstance();
        start.set(1, Calendar.JANUARY, 2010, 10, 0, 0);
        Calendar end = Calendar.getInstance();
        end.set(1, Calendar.JANUARY, 2010, 16, 30, 0);
        EmitResult createTimeBooking = application.emit(new CreateTimeBookingEvent("a", "i", "u", new Date(), new Date()), getDeveloperAuth()).get();
        assertTrue(createTimeBooking.isSuccessfull());

        EmitResult deleteTimeBooking = application.emit(new RemoveTimeBookingEvent("a")).get();
        assertTrue(deleteTimeBooking.isSuccessfull());

        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, "a");
        assertNull(timeBooking);
    }

    @Test
    public void testTimeBookingIsNotFound() throws Exception {
        EmitResult deleteTimeBooking = application.emit(new RemoveTimeBookingEvent("a"), getDeveloperAuth()).get();
        assertFalse(deleteTimeBooking.isSuccessfull());
    }
}