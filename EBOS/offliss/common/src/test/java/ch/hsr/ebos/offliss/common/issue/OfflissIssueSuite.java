package ch.hsr.ebos.offliss.common.issue;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ch.hsr.ebos.offliss.common.issue.assign.AssignIssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.close.CloseAllIssuesAggregateTest;
import ch.hsr.ebos.offliss.common.issue.close.CloseIssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.open.OpenIssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.unassign.UnassignIssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.update.UpdateIssueAggregateTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        UpdateIssueAggregateTest.class,
        UnassignIssueAggregateTest.class,
        OpenIssueAggregateTest.class,
        CreateIssueAggregateTest.class,
        CloseIssueAggregateTest.class,
        CloseAllIssuesAggregateTest.class,
        AssignIssueAggregateTest.class
})
public class OfflissIssueSuite {
}
