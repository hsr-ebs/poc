package ch.hsr.ebos.offliss.common.project;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationFactory;
import ch.hsr.ebos.aion.data.sqlite.state.SqliteDataModule;
import ch.hsr.ebos.offliss.common.AggregateTest;

public class ProjectAggregateTest extends AggregateTest {
    @Override
    protected Application getApplication() throws ExecutionException, InterruptedException {
        ProjectModule module = new ProjectModule();
        module.setDataModule(new SqliteDataModule(connectionFactory, databaseFile, true));
        return application = ApplicationFactory.create(module).get();
    }
}
