package ch.hsr.ebos.offliss.common.issue.close;

import org.junit.Test;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.issue.IssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueState;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueEvent;
import ch.hsr.ebos.offliss.common.project.close.CloseProjectEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CloseAllIssuesAggregateTest extends IssueAggregateTest {

    @Test
    public void testAreAllIssueClosed() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("b", "Plan Sprint", "My description", "a", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult inactivateProject = application.emit(new CloseProjectEvent("a"), getProjectManagerAuth()).get();
        assertTrue(inactivateProject.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "b");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("a", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.CLOSED, issue.getState());
    }

    @Test
    public void testCloseNoIssues() throws Exception {
        EmitResult inactivateProject = application.emit(new CloseProjectEvent("a"), getProjectManagerAuth()).get();
        assertTrue(inactivateProject.isSuccessfull());
    }

}