package ch.hsr.ebos.offliss.common.timebooking.update;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingAggregateTest;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingEntity;
import ch.hsr.ebos.offliss.common.timebooking.create.CreateTimeBookingEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class UpdateTimeBookingAggregateTest extends TimeBookingAggregateTest {

    @Test
    public void testUpdateStartDate() throws Exception {
        Calendar start = Calendar.getInstance();
        start.set(1, Calendar.JANUARY, 2010, 10, 0, 0);
        Calendar end = Calendar.getInstance();
        end.set(1, Calendar.JANUARY, 2010, 16, 30, 0);
        EmitResult createTimeBooking = application.emit(new CreateTimeBookingEvent("a",
                "i", "u", start.getTime(), end.getTime()), getDeveloperAuth()).get();
        assertTrue(createTimeBooking.isSuccessfull());

        Calendar newStart = Calendar.getInstance();
        newStart.set(1, Calendar.JANUARY, 2010, 11, 15, 0);

        EmitResult updateTimeBooking = application.emit(new UpdateTimeBookingEvent("a",
                new ChangeSet<>(start.getTime(), newStart.getTime()),
                new ChangeSet<>(end.getTime(), end.getTime())), getDeveloperAuth()).get();
        assertTrue(updateTimeBooking.isSuccessfull());

        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, "a");
        assertNotNull(timeBooking);
        assertEquals("i", timeBooking.getIssueId());
        assertEquals("u", timeBooking.getUserId());
        assertEquals(newStart.getTime(), timeBooking.getStart());
        assertEquals(end.getTime(), timeBooking.getStop());
    }

    @Test
    public void testUpdateStartDateConflict() throws Exception {
        Calendar start = Calendar.getInstance();
        start.set(1, Calendar.JANUARY, 2010, 10, 0, 0);
        Calendar end = Calendar.getInstance();
        end.set(1, Calendar.JANUARY, 2010, 16, 30, 0);
        EmitResult createTimeBooking = application.emit(new CreateTimeBookingEvent("a",
                "i", "u", start.getTime(), end.getTime()), getDeveloperAuth()).get();
        assertTrue(createTimeBooking.isSuccessfull());

        Calendar newStart = Calendar.getInstance();
        newStart.set(1, Calendar.JANUARY, 2010, 11, 15, 0);

        EmitResult updateTimeBooking = application.emit(new UpdateTimeBookingEvent("a",
                new ChangeSet<>(new Date(), newStart.getTime()),
                new ChangeSet<>(end.getTime(), end.getTime())), getDeveloperAuth()).get();
        assertFalse(updateTimeBooking.isSuccessfull());

        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, "a");
        assertNotNull(timeBooking);
        assertEquals("i", timeBooking.getIssueId());
        assertEquals("u", timeBooking.getUserId());
        assertEquals(start.getTime(), timeBooking.getStart());
        assertEquals(end.getTime(), timeBooking.getStop());
    }

    @Test
    public void testUpdateEndDate() throws Exception {
        Calendar start = Calendar.getInstance();
        start.set(1, Calendar.JANUARY, 2010, 10, 0, 0);

        Calendar end = Calendar.getInstance();
        end.set(1, Calendar.JANUARY, 2010, 16, 30, 0);

        EmitResult createTimeBooking = application.emit(new CreateTimeBookingEvent("a",
                "i", "u", start.getTime(), end.getTime()), getDeveloperAuth()).get();
        assertTrue(createTimeBooking.isSuccessfull());

        Calendar newEnd = Calendar.getInstance();
        newEnd.set(1, Calendar.JANUARY, 2010, 11, 15, 0);

        EmitResult updateTimeBooking = application.emit(new UpdateTimeBookingEvent("a",
                new ChangeSet<>(start.getTime(), start.getTime()),
                new ChangeSet<>(end.getTime(), newEnd.getTime())), getDeveloperAuth()).get();
        assertTrue(updateTimeBooking.isSuccessfull());

        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, "a");
        assertNotNull(timeBooking);
        assertEquals("i", timeBooking.getIssueId());
        assertEquals("u", timeBooking.getUserId());
        assertEquals(start.getTime(), timeBooking.getStart());
        assertEquals(newEnd.getTime(), timeBooking.getStop());
    }

    @Test
    public void testUpdateEndDateConflict() throws Exception {
        Calendar start = Calendar.getInstance();
        start.set(1, Calendar.JANUARY, 2010, 10, 0, 0);

        Calendar end = Calendar.getInstance();
        end.set(1, Calendar.JANUARY, 2010, 16, 30, 0);

        EmitResult createTimeBooking = application.emit(new CreateTimeBookingEvent("a",
                "i", "u", start.getTime(), end.getTime()), getDeveloperAuth()).get();
        assertTrue(createTimeBooking.isSuccessfull());

        Calendar newEnd = Calendar.getInstance();
        newEnd.set(1, Calendar.JANUARY, 2010, 11, 15, 0);

        EmitResult updateTimeBooking = application.emit(new UpdateTimeBookingEvent("a",
                new ChangeSet<>(start.getTime(), start.getTime()),
                new ChangeSet<>(new Date(), newEnd.getTime())), getDeveloperAuth()).get();
        assertFalse(updateTimeBooking.isSuccessfull());

        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, "a");
        assertNotNull(timeBooking);
        assertEquals("i", timeBooking.getIssueId());
        assertEquals("u", timeBooking.getUserId());
        assertEquals(start.getTime(), timeBooking.getStart());
        assertEquals(end.getTime(), timeBooking.getStop());
    }

    @Test
    public void testUpdateAllValues() throws Exception {
        Calendar start = Calendar.getInstance();
        start.set(1, Calendar.JANUARY, 2010, 10, 0, 0);
        Calendar end = Calendar.getInstance();
        end.set(1, Calendar.JANUARY, 2010, 16, 30, 0);
        EmitResult createTimeBooking = application.emit(new CreateTimeBookingEvent("a",
                "i", "u", start.getTime(), end.getTime()), getDeveloperAuth()).get();
        assertTrue(createTimeBooking.isSuccessfull());

        Calendar newStart = Calendar.getInstance();
        newStart.set(1, Calendar.JANUARY, 2010, 10, 15, 0);

        Calendar newEnd = Calendar.getInstance();
        newEnd.set(1, Calendar.JANUARY, 2010, 11, 15, 0);

        EmitResult updateTimeBooking = application.emit(new UpdateTimeBookingEvent("a",
                new ChangeSet<>(start.getTime(), newStart.getTime()),
                new ChangeSet<>(end.getTime(), newEnd.getTime())), getDeveloperAuth()).get();
        assertTrue(updateTimeBooking.isSuccessfull());

        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, "a");
        assertNotNull(timeBooking);
        assertEquals("i", timeBooking.getIssueId());
        assertEquals("u", timeBooking.getUserId());
        assertEquals(newStart.getTime(), timeBooking.getStart());
        assertEquals(newEnd.getTime(), timeBooking.getStop());
    }

    @Test
    public void testUpdateSameValues() throws Exception {
        Calendar start = Calendar.getInstance();
        start.set(1, Calendar.JANUARY, 2010, 10, 0, 0);
        Calendar end = Calendar.getInstance();
        end.set(1, Calendar.JANUARY, 2010, 16, 30, 0);
        EmitResult createTimeBooking = application.emit(new CreateTimeBookingEvent("a", "i", "u", start.getTime(), end.getTime()), getDeveloperAuth()).get();
        assertTrue(createTimeBooking.isSuccessfull());

        EmitResult updateTimeBooking = application.emit(new UpdateTimeBookingEvent("a",
                new ChangeSet<>(start.getTime(), start.getTime()),
                new ChangeSet<>(end.getTime(), end.getTime())), getDeveloperAuth()).get();
        assertTrue(updateTimeBooking.isSuccessfull());

        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, "a");
        assertNotNull(timeBooking);
        assertEquals("i", timeBooking.getIssueId());
        assertEquals("u", timeBooking.getUserId());
        assertEquals(start.getTime(), timeBooking.getStart());
        assertEquals(end.getTime(), timeBooking.getStop());
    }

    @Test
    public void testTimeBookingIsNotFound() throws ExecutionException, InterruptedException {
        EmitResult deleteTimeBooking = application.emit(new UpdateTimeBookingEvent("a", null, null), getDeveloperAuth()).get();
        assertFalse(deleteTimeBooking.isSuccessfull());
    }
}