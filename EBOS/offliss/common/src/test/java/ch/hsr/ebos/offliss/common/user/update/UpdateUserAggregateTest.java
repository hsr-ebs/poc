package ch.hsr.ebos.offliss.common.user.update;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.user.UserAggregateTest;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.UserRole;
import ch.hsr.ebos.offliss.common.user.UserState;
import ch.hsr.ebos.offliss.common.user.create.CreateUserEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class UpdateUserAggregateTest extends UserAggregateTest {

    @Test
    public void testUpdateName() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Hans Muster-Meier"),
                new ChangeSet<>("muster.hans@labank.ch", "muster.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.DEVELOPER)), getAdminAuth()).get();
        assertTrue(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster-Meier", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUpdateNameConflict() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Other Change", "Hans Muster-Meier"),
                new ChangeSet<>("muster.hans@labank.ch", "muster.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.DEVELOPER)), getAdminAuth()).get();
        assertFalse(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUpdateEmail() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Hans Muster"),
                new ChangeSet<>("muster.hans@labank.ch", "muster-meier.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.DEVELOPER)), getAdminAuth()).get();
        assertTrue(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster-meier.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUpdateEmailConflict() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Hans Muster"),
                new ChangeSet<>("Other Change", "muster-meier.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.DEVELOPER)), getAdminAuth()).get();
        assertFalse(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUpdateAvatar() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Hans Muster"),
                new ChangeSet<>("muster.hans@labank.ch", "muster.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.DEVELOPER)), getAdminAuth()).get();
        assertTrue(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUpdateAvatarConflict() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Hans Muster"),
                new ChangeSet<>("muster.hans@labank.ch", "muster.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes0/nose0/mouth0/000000",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.DEVELOPER)), getAdminAuth()).get();
        assertFalse(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUpdateRole() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Hans Muster"),
                new ChangeSet<>("muster.hans@labank.ch", "muster.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.PROJECT_MANAGER)), getAdminAuth()).get();
        assertTrue(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.PROJECT_MANAGER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUpdateRoleConflict() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Hans Muster"),
                new ChangeSet<>("muster.hans@labank.ch", "muster.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes0/nose0/mouth0/000000",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.DEVELOPER)), getAdminAuth()).get();
        assertFalse(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUniqueConstraint() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult createOtherUser = application.emit(new CreateUserEvent("b",
                "hansmeier",
                "Hans Meier",
                "meier.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createOtherUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("b",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Hans Muster"),
                new ChangeSet<>("meier.hans@labank.ch", "muster.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.DEVELOPER)), getAdminAuth()).get();
        assertFalse(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "b");
        assertNotNull(user);
        assertEquals("Hans Meier", user.getFullName());
        assertEquals("meier.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUpdateAllValues() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Anna Meier"),
                new ChangeSet<>("muster.hans@labank.ch", "anna.meier@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.PROJECT_MANAGER)), getAdminAuth()).get();
        assertTrue(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Anna Meier", user.getFullName());
        assertEquals("anna.meier@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth7/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.PROJECT_MANAGER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUpdateSameValues() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult updateUser = application.emit(new UpdateUserEvent("a",
                "hansmuster",
                new ChangeSet<>("Hans Muster", "Hans Muster"),
                new ChangeSet<>("muster.hans@labank.ch", "muster.hans@labank.ch"),
                new ChangeSet<>("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                        "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff"),
                new ChangeSet<>(UserRole.DEVELOPER, UserRole.DEVELOPER)), getAdminAuth()).get();
        assertTrue(updateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.ACTIVE, user.getState());
    }

    @Test
    public void testUserIsNotFound() throws ExecutionException, InterruptedException {
        EmitResult noUserFound = application.emit(new UpdateUserEvent("a",
                "hansmuster", null,
                null, null, null), getAdminAuth()).get();
        assertFalse(noUserFound.isSuccessfull());
    }
}