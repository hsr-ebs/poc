package ch.hsr.ebos.offliss.common.issue.create;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.issue.IssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueState;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CreateIssueAggregateTest extends IssueAggregateTest {

    @Test
    public void testIsIssueCreatedWihAllValues() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testIsIssueCreatedWithRequiredValues() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", null, "projectId", null), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertNull(issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertNull(issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testCreateUserWithSameValues() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult createOtherIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertFalse(createOtherIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testIssueNumberSequence() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult createOtherIssue = application.emit(new CreateIssueEvent("b", "Plan Sprint #2", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createOtherIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertEquals(1, issue.getNumber());

        IssueEntity otherIssue = entityService.get(IssueEntity.class, "b");
        assertEquals(2, otherIssue.getNumber());
    }

    @Test
    public void testErrorOnUniqueNumber() throws ExecutionException, InterruptedException {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult createUniqueConstraintError = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertFalse(createUniqueConstraintError.isSuccessfull());
    }
}