package ch.hsr.ebos.offliss.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ch.hsr.ebos.offliss.common.issue.OfflissIssueSuite;
import ch.hsr.ebos.offliss.common.project.OfflissProjectSuite;
import ch.hsr.ebos.offliss.common.timebooking.OfflissTimeBookingSuite;
import ch.hsr.ebos.offliss.common.user.OfflissUserSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        OfflissUserSuite.class,
        OfflissTimeBookingSuite.class,
        OfflissIssueSuite.class,
        OfflissProjectSuite.class
})
public class OfflissTestSuite {
}
