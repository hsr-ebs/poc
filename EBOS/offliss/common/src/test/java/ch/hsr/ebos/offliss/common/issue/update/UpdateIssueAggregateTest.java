package ch.hsr.ebos.offliss.common.issue.update;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.issue.IssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueState;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class UpdateIssueAggregateTest extends IssueAggregateTest {
    @Test
    public void testUpdateTitle() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Plan Sprint", "Create Sprints"),
                new ChangeSet<>("My description", "My description"),
                new ChangeSet<>("parentId", "parentId")), getProjectManagerAuth()).get();
        assertTrue(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Create Sprints", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testUpdateTitleConflict() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Other Change", "Create Sprints"),
                new ChangeSet<>("My description", "My description"),
                new ChangeSet<>("parentId", "parentId")), getProjectManagerAuth()).get();
        assertFalse(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testUpdateDescription() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Plan Sprint", "Plan Sprint"),
                new ChangeSet<>("My description", "Other description"),
                new ChangeSet<>("parentId", "parentId")), getProjectManagerAuth()).get();
        assertTrue(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("Other description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testUpdateDescriptionConflict() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Plan Sprint", "Plan Sprint"),
                new ChangeSet<>("Other Change", "Other description"),
                new ChangeSet<>("parentId", "parentId")), getProjectManagerAuth()).get();
        assertFalse(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testUpdateEmptyDescription() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Plan Sprint", "Plan Sprint"),
                new ChangeSet<>("My description", ""),
                new ChangeSet<>("parentId", "parentId")), getProjectManagerAuth()).get();
        assertTrue(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testUpdateParent() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Plan Sprint", "Plan Sprint"),
                new ChangeSet<>("My description", "My description"),
                new ChangeSet<>("parentId", "otherParent")), getProjectManagerAuth()).get();
        assertTrue(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("otherParent", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testUpdateParentConflict() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Plan Sprint", "Plan Sprint"),
                new ChangeSet<>("My description", "My description"),
                new ChangeSet<>("Other Change", "otherParent")), getProjectManagerAuth()).get();
        assertFalse(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testUpdateEmptyParent() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Plan Sprint", "Plan Sprint"),
                new ChangeSet<>("My description", "My description"),
                new ChangeSet<>("parentId", "")), getProjectManagerAuth()).get();
        assertTrue(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }


    @Test
    public void testUpdateAllValues() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Plan Sprint", "New Plan"),
                new ChangeSet<>("My description", "New Description"),
                new ChangeSet<>("parentId", "newParentId")), getProjectManagerAuth()).get();
        assertTrue(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("New Plan", issue.getTitle());
        assertEquals("New Description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("newParentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testUpdateSameValues() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint",
                "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult updateIssue = application.emit(new UpdateIssueEvent("a",
                new ChangeSet<>("Plan Sprint", "Plan Sprint"),
                new ChangeSet<>("My description", "My description"),
                new ChangeSet<>("parentId", "parentId")), getProjectManagerAuth()).get();
        assertTrue(updateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testIssueIsNotFound() throws ExecutionException, InterruptedException {
        EmitResult noIssueFound = application.emit(new UpdateIssueEvent("a", null, null, null), getProjectManagerAuth()).get();
        assertFalse(noIssueFound.isSuccessfull());
    }
}