package ch.hsr.ebos.offliss.common.issue.assign;

import org.junit.Test;

import java.util.List;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.offliss.common.issue.IssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.IssueMemberEntity;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AssignIssueAggregateTest extends IssueAggregateTest {

    @Test
    public void testIsIssueAssigned() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult assigneIssue = application.emit(new AssignIssueEvent("a", "u"), getProjectManagerAuth()).get();
        assertTrue(assigneIssue.isSuccessfull());

        Query memberQuery = Query.build().and(Filter.equals("userId", "u"), Filter.equals("issueId", "a"));
        List<IssueMemberEntity> issueMember = entityService.query(IssueMemberEntity.class, memberQuery);
        assertNotNull(issueMember);
        assertEquals(1, issueMember.size());
        assertEquals("a", issueMember.get(0).getIssueId());
        assertEquals("u", issueMember.get(0).getUserId());
    }

    @Test
    public void testIsIssueAssignedOnce() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult assigneIssue = application.emit(new AssignIssueEvent("a", "u"), getProjectManagerAuth()).get();
        assertTrue(assigneIssue.isSuccessfull());

        EmitResult assigneIssueTwice = application.emit(new AssignIssueEvent("a", "u"), getProjectManagerAuth()).get();
        assertTrue(assigneIssueTwice.isSuccessfull());

        Query memberQuery = Query.build().and(Filter.equals("userId", "u"), Filter.equals("issueId", "a"));
        List<IssueMemberEntity> issueMember = entityService.query(IssueMemberEntity.class, memberQuery);
        assertNotNull(issueMember);
        assertEquals(1, issueMember.size());
        assertEquals("a", issueMember.get(0).getIssueId());
        assertEquals("u", issueMember.get(0).getUserId());
    }
}