package ch.hsr.ebos.offliss.common.user.inactivate;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.user.UserAggregateTest;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.UserRole;
import ch.hsr.ebos.offliss.common.user.UserService;
import ch.hsr.ebos.offliss.common.user.UserState;
import ch.hsr.ebos.offliss.common.user.create.CreateUserEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class InactivateUserAggregateTest extends UserAggregateTest {

    @Override
    public void setUp() throws ExecutionException, InterruptedException {
        super.setUp();
        application.injector.getInstance(UserService.class);
    }

    @Test
    public void testIsUserInactive() throws Exception {
        EmitResult createUser = application.emit(new CreateUserEvent("a",
                "hansmuster", "Hans Muster",
                "muster.hans@labank.ch",
                "https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                UserRole.DEVELOPER), getAdminAuth()).get();
        assertTrue(createUser.isSuccessfull());

        EmitResult inactivateUser = application.emit(new InactivateUserEvent("a")).get();
        assertTrue(inactivateUser.isSuccessfull());

        UserEntity user = entityService.get(UserEntity.class, "a");
        assertNotNull(user);
        assertEquals("hansmuster", user.getUsername());
        assertEquals("Hans Muster", user.getFullName());
        assertEquals("muster.hans@labank.ch", user.getEmail());
        assertEquals("https://api.adorable.io/avatars/face/eyes10/nose7/mouth9/ff00ff",
                user.getAvatar());
        assertEquals(UserRole.DEVELOPER, user.getRole());
        assertEquals(UserState.INACTIVE, user.getState());

    }

    @Test
    public void testUserIsNotFound() throws ExecutionException, InterruptedException {
        EmitResult noUserFound = application.emit(new InactivateUserEvent("a"), getAdminAuth()).get();
        assertFalse(noUserFound.isSuccessfull());
    }
}