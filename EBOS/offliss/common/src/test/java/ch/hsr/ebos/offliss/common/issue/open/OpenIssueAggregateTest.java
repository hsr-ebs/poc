package ch.hsr.ebos.offliss.common.issue.open;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.issue.IssueAggregateTest;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueState;
import ch.hsr.ebos.offliss.common.issue.close.CloseIssueEvent;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueEvent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class OpenIssueAggregateTest extends IssueAggregateTest {

    @Test
    public void testIsIssueActive() throws Exception {
        EmitResult createIssue = application.emit(new CreateIssueEvent("a", "Plan Sprint", "My description", "projectId", "parentId"), getProjectManagerAuth()).get();
        assertTrue(createIssue.isSuccessfull());

        EmitResult inactivateIssue = application.emit(new CloseIssueEvent("a"), getProjectManagerAuth()).get();
        assertTrue(inactivateIssue.isSuccessfull());

        EmitResult activateIssue = application.emit(new OpenIssueEvent("a"), getProjectManagerAuth()).get();
        assertTrue(activateIssue.isSuccessfull());

        IssueEntity issue = entityService.get(IssueEntity.class, "a");
        assertNotNull(issue);
        assertEquals("Plan Sprint", issue.getTitle());
        assertEquals("My description", issue.getDescription());
        assertEquals(1, issue.getNumber());
        assertEquals("projectId", issue.getProjectId());
        assertEquals("parentId", issue.getParentId());
        assertEquals(IssueState.OPEN, issue.getState());
    }

    @Test
    public void testIssueIsNotFound() throws ExecutionException, InterruptedException {
        EmitResult noIssueFound = application.emit(new OpenIssueEvent("a"), getProjectManagerAuth()).get();
        assertFalse(noIssueFound.isSuccessfull());
    }
}