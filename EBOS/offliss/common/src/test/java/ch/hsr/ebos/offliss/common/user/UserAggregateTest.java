package ch.hsr.ebos.offliss.common.user;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationFactory;
import ch.hsr.ebos.aion.data.sqlite.state.SqliteDataModule;
import ch.hsr.ebos.offliss.common.AggregateTest;

public class UserAggregateTest extends AggregateTest {
    @Override
    protected Application getApplication() throws ExecutionException, InterruptedException {
        UserModule module = new UserModule();
        module.setDataModule(new SqliteDataModule(connectionFactory, databaseFile, true));
        return application = ApplicationFactory.create(module).get();
    }
}
