package ch.hsr.ebos.offliss.common.timebooking.create;

import org.junit.Test;

import java.util.Calendar;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingAggregateTest;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CreateTimeBookingAggregateTest extends TimeBookingAggregateTest {

    @Test
    public void testIsTimeBookingCreated() throws Exception {
        Calendar start = Calendar.getInstance();
        start.set(1, Calendar.JANUARY, 2010, 10, 0, 0);
        Calendar end = Calendar.getInstance();
        end.set(1, Calendar.JANUARY, 2010, 16, 30, 0);
        EmitResult createTimeBooking = application.emit(new CreateTimeBookingEvent("a", "i", "u", start.getTime(), end.getTime()), getDeveloperAuth()).get();
        assertTrue(createTimeBooking.isSuccessfull());

        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, "a");
        assertNotNull(timeBooking);
        assertEquals("i", timeBooking.getIssueId());
        assertEquals("u", timeBooking.getUserId());
        assertEquals(start.getTime(), timeBooking.getStart());
        assertEquals(end.getTime(), timeBooking.getStop());
    }

    @Test
    public void testCreateTimeBookingWithSameValues() throws Exception {
        Calendar start = Calendar.getInstance();
        start.set(1, Calendar.JANUARY, 2010, 10, 0, 0);
        Calendar end = Calendar.getInstance();
        end.set(1, Calendar.JANUARY, 2010, 16, 30, 0);
        EmitResult createTimeBooking = application.emit(new CreateTimeBookingEvent("a", "i", "u", start.getTime(), end.getTime()), getDeveloperAuth()).get();
        assertTrue(createTimeBooking.isSuccessfull());

        EmitResult createOtherTimeBooking = application.emit(new CreateTimeBookingEvent("b", "i", "u", start.getTime(), end.getTime()), getDeveloperAuth()).get();
        assertTrue(createOtherTimeBooking.isSuccessfull());

        TimeBookingEntity timeBooking = entityService.get(TimeBookingEntity.class, "b");
        assertNotNull(timeBooking);
        assertEquals("i", timeBooking.getIssueId());
        assertEquals("u", timeBooking.getUserId());
        assertEquals(start.getTime(), timeBooking.getStart());
        assertEquals(end.getTime(), timeBooking.getStop());
    }
}