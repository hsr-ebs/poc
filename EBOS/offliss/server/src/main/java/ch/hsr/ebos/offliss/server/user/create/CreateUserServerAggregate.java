package ch.hsr.ebos.offliss.server.user.create;

import javax.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.email.IEmailService;
import ch.hsr.ebos.offliss.common.user.create.CreateUserEvent;
import ch.hsr.ebos.offliss.server.KeycloakService;
import ch.hsr.ebos.offliss.server.TemplateService;
import ch.hsr.ebos.offliss.server.user.PasswordGeneratorService;
import ch.hsr.ebos.offliss.server.user.update.ServerUserService;

public class CreateUserServerAggregate extends EventAggregate<CreateUserEvent> {
    @Inject
    IEmailService emailService;

    @Inject
    TemplateService templateService;

    @Inject
    PasswordGeneratorService passwordGenerator;

    @Inject
    KeycloakService keycloak;

    @Inject
    ServerUserService userService;

    @Override
    public void aggregate(ApplicationEvent<CreateUserEvent> event) throws Throwable {
        String password = passwordGenerator.generate();
        String username = event.getData().getUsername();

        try {
            userService.createUser(event.getData().getId(), username, password, event.getData().getRole());

            String template = templateService.getTemplate("welcome-mail.html");
            String content = template.replace("{{password}}", password);

            this.emailService.send(event.getData().getEmail(), "Offliss registration", content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
