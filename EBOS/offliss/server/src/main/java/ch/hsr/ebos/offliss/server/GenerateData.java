package ch.hsr.ebos.offliss.server;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.TimelineEvent;
import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;
import ch.hsr.ebos.aion.http.HttpRequestInterceptor;
import ch.hsr.ebos.aion.http.HttpTimelineFactory;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueEvent;
import ch.hsr.ebos.offliss.common.project.create.CreateProjectEvent;
import ch.hsr.ebos.offliss.common.user.UserRole;
import ch.hsr.ebos.offliss.common.user.create.CreateUserEvent;

public class GenerateData {
    public static void main(String[] args) throws Exception {
        Properties properties = Preferences.loadProperties();

        KeycloakService keycloakService = Keycloak.setup(properties);

        HttpTimelineFactory remoteTimelineFactory = new HttpTimelineFactory(properties.getProperty("general.timeline.host"), new HttpRequestInterceptor() {
            @Override
            public CompletableFuture<String> provideAuthorizationHeader() {
                CompletableFuture<String> future = new CompletableFuture<>();
                try {
                    future.complete("Bearer " + keycloakService.getAccessToken());
                } catch (Exception e) {
                    future.completeExceptionally(e);
                }
                return future;
            }
        });

        List<String> roles = new ArrayList<>();
        roles.add(OfflissRoles.Administrator);
        roles.add(OfflissRoles.Developer);
        roles.add(OfflissRoles.ProjectManager);

        TimelineEventAuth eventAuth = new TimelineEventAuth(null, properties.getProperty("keycloak.username"), roles);

        ITimeline user = remoteTimelineFactory.get("user");
        ITimeline project = remoteTimelineFactory.get("project");
        ITimeline issue = remoteTimelineFactory.get("issue");

        Faker faker = new Faker(Locale.GERMAN);

        int adminCount = 2;
        int developerCount = 5;
        int projectManagerCount = 2;
        int projectCount = 10;
        int issueCount = 10;

        List<String> userIds = new ArrayList<>();
        List<String> projectIds = new ArrayList<>();
        List<String> issueIds = new ArrayList<>();

        for (int i = 0; i < adminCount; i++) {
            String userId = UUID.randomUUID().toString();
            userIds.add(userId);
            String firstName = faker.name().firstName();
            String lastName = faker.name().lastName();
            CreateUserEvent event = new CreateUserEvent(userId,
                    firstName, firstName + " " + lastName,
                    firstName + "@example.com", "",
                    UserRole.ADMINISTRATOR);
            System.out.println(event);
            user.add(TimelineEvent.fromObject(event, eventAuth)).get();
        }

        for (int i = 0; i < developerCount; i++) {
            String userId = UUID.randomUUID().toString();
            userIds.add(userId);
            String firstName = faker.name().firstName();
            String lastName = faker.name().lastName();
            CreateUserEvent event = new CreateUserEvent(userId,
                    firstName, firstName + " " + lastName,
                    firstName + "@example.com", "",
                    UserRole.DEVELOPER);
            System.out.println(event);
            user.add(TimelineEvent.fromObject(event, eventAuth)).get();
        }

        for (int i = 0; i < projectManagerCount; i++) {
            String userId = UUID.randomUUID().toString();
            userIds.add(userId);
            String firstName = faker.name().firstName();
            String lastName = faker.name().lastName();
            CreateUserEvent event = new CreateUserEvent(userId,
                    firstName, firstName + " " + lastName,
                    firstName + "@example.com", "",
                    UserRole.PROJECT_MANAGER);
            System.out.println(event);
            user.add(TimelineEvent.fromObject(event, eventAuth)).get();
        }

        for (int i = 0; i < projectCount; i++) {
            String projectId = UUID.randomUUID().toString();
            projectIds.add(projectId);
            CreateProjectEvent event = new CreateProjectEvent(projectId, faker.educator().course());
            System.out.println(event.getName());
            project.add(TimelineEvent.fromObject(event, eventAuth)).get();
        }


        for (int i = 0; i < issueCount; i++) {
            String issueId = UUID.randomUUID().toString();
            String projectId = getRandomElement(projectIds);
            issueIds.add(issueId);
            CreateIssueEvent event = new CreateIssueEvent(issueId, faker.food().ingredient(), faker.shakespeare().hamletQuote(), projectId, null);
            System.out.println(event);
            issue.add(TimelineEvent.fromObject(event, eventAuth));
        }

        System.out.println("created data");
    }

    public static <T> T getRandomElement(List<T> list) {
        Random rand = new Random();
        return list.get(rand.nextInt(list.size()));
    }
}
