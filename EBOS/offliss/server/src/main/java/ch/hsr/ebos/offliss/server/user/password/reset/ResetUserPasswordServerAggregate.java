package ch.hsr.ebos.offliss.server.user.password.reset;

import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;

import javax.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.email.IEmailService;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.reset.ResetUserPasswordEvent;
import ch.hsr.ebos.offliss.server.KeycloakService;
import ch.hsr.ebos.offliss.server.TemplateService;
import ch.hsr.ebos.offliss.server.user.PasswordGeneratorService;

public class ResetUserPasswordServerAggregate extends EventAggregate<ResetUserPasswordEvent> {

    @Inject
    IEmailService emailService;

    @Inject
    TemplateService templateService;

    @Inject
    EntityService entityService;

    @Inject
    PasswordGeneratorService passwordGenerator;

    @Inject
    KeycloakService keycloak;

    @Override
    public void aggregate(ApplicationEvent<ResetUserPasswordEvent> event) throws Throwable {
        String username = event.getData().getUsername();
        String password = passwordGenerator.generate();

        UserResource userResource = keycloak.getUserResourceByUsername(username);
        UserRepresentation user = userResource.toRepresentation();
        keycloak.setUserPassword(user, password);
        userResource.update(user);

        UserEntity entity = entityService.get(UserEntity.class, event.getData().getId());
        String template = templateService.getTemplate("issue-update.html");

        String content = template.replace("{{title}}", "Passwort reset")
                .replace("{{content}}", "Your password has been reset by one of our administrators. The new password is <b>" + password + "</b>");
        String subject = "Password reset";
        this.emailService.send(entity.getEmail(), subject, content);
    }

}
