package ch.hsr.ebos.offliss.server.issue;

import java.util.List;

import javax.inject.Inject;

import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.email.IEmailService;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueMemberEntity;
import ch.hsr.ebos.offliss.common.issue.IssueService;
import ch.hsr.ebos.offliss.common.issue.IssueState;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.server.TemplateService;

public class ServerIssueService extends IssueService {

    @Inject
    IEmailService emailService;

    @Inject
    TemplateService templateService;

    @Override
    public void changeIssueState(IssueEntity issue, IssueState state) throws Exception {
        super.changeIssueState(issue, state);

        if (issue.getState() != IssueState.CLOSED) {
            try {
                ProjectEntity project = entityService.get(ProjectEntity.class, issue.getProjectId());

                Query memberQuery = Query.build().and(Filter.equals("issueId", issue.getId()));
                List<IssueMemberEntity> issueMembers = entityService.query(IssueMemberEntity.class, memberQuery);

                String template = templateService.getTemplate("issue-update.html");
                String content = template.replace("{{title}}", "Closed issue")
                        .replace("{{content}}", "Closed issue \"" + issue.getName() + "\"");
                String subject = "[" + project.getName() + "] " + issue.getTitle() + " (#" + issue.getNumber() + ")";

                for (IssueMemberEntity member : issueMembers) {
                    UserEntity user = entityService.get(UserEntity.class, member.getUserId());
                    this.emailService.send(user.getEmail(), subject, content);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
