package ch.hsr.ebos.offliss.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class TemplateService {
    public String getTemplate(String name) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                ClassLoader.getSystemClassLoader().getResourceAsStream("templates/" + name)))) {
            return br.lines().collect(Collectors.joining(System.lineSeparator()));
        }
    }
}
