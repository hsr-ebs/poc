package ch.hsr.ebos.offliss.server;

import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;

public class KeycloakService {

    private String host;
    private String realm;
    private String username;
    private String password;
    private String adminClientId;
    private String aionClientId;
    private String aionClientSecret;

    private AccessTokenResponse accessTokenResponse;
    private Date accessTokenExpire;

    private Keycloak getAdminKeyCloak() {
        return Keycloak.getInstance(host, realm, username, password, adminClientId);
    }

    private Keycloak getAionKeyCloak() {
        return Keycloak.getInstance(host, realm, username, password, aionClientId, aionClientSecret);
    }

    private UsersResource getUsersResource() {
        return getAdminKeyCloak().realm(realm).users();
    }

    public void createUser(String userId, String username, String password) throws Exception {
        UserRepresentation user = new UserRepresentation();
        user.setUsername(username);
        user.setEnabled(true);
        user.singleAttribute("userId", userId);
        setUserPassword(user, password);

        Response result = getUsersResource().create(user);
        if (result.getStatus() != 201) {
            throw new Exception("Could not create user " + username);
        }
    }

    public String getAccessToken() throws InvalidJwtException, MalformedClaimException {
        if (accessTokenResponse != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, 30);

            if (accessTokenExpire.before(calendar.getTime())) {
                accessTokenResponse = null;
                accessTokenExpire = null;
            }
        }
        if (accessTokenResponse == null) {
            accessTokenResponse = getAionKeyCloak().tokenManager().getAccessToken();
            JwtClaims claims = new JwtConsumerBuilder().setSkipAllValidators().
                    setSkipSignatureVerification().build().
                    processToClaims(accessTokenResponse.getToken());
            accessTokenExpire = new Date(claims.getExpirationTime().getValueInMillis());
        }

        return accessTokenResponse.getToken();
    }

    public List<UserRepresentation> getUsers() {
        return getUsersResource().list();
    }

    public void delete(String id) {
        getAdminKeyCloak().realm(realm).users().delete(id);
    }

    public UserResource getUserResourceByUsername(String username) throws Exception {
        List<UserRepresentation> users = getUsersResource().search(username.toLowerCase());
        if (users != null) {
            for (UserRepresentation user : users) {
                if (user.getUsername().equals(username.toLowerCase())) {
                    return getUsersResource().get(user.getId());
                }
            }
        }

        throw new Exception("Could not found user with username " + username);
    }

    public void setUserPassword(UserRepresentation user, String password) {
        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setValue(password);
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setTemporary(false);
        user.setCredentials(Collections.singletonList(credential));
    }

    public void setUserState(UserRepresentation user, boolean enable) {
        user.setEnabled(enable);
    }

    public void assignUserRoles(UserResource userResource, List<RoleRepresentation> roles) throws Exception {
        userResource.roles().realmLevel().add(roles);
    }

    public void unassignUserRoles(UserResource userResource, List<RoleRepresentation> roles) throws Exception {
        userResource.roles().realmLevel().remove(roles);
    }

    public List<RoleRepresentation> getAssignedUserRoles(UserResource userResource) throws Exception {
        return userResource.roles().realmLevel().listAll();
    }

    public RoleRepresentation getOrCreateUserRole(String roleName) {
        RoleResource resource = getAdminKeyCloak().realm(realm).roles().get(roleName);
        try {
            return resource.toRepresentation();
        } catch (NotFoundException e) {
            RoleRepresentation role = new RoleRepresentation();
            role.setName(roleName);
            getAdminKeyCloak().realm(realm).roles().create(role);

            return resource.toRepresentation();
        }
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdminClientId(String adminClientId) {
        this.adminClientId = adminClientId;
    }

    public void setAionClientId(String aionClientId) {
        this.aionClientId = aionClientId;
    }

    public void setAionClientSecret(String aionClientSecret) {
        this.aionClientSecret = aionClientSecret;
    }
}
