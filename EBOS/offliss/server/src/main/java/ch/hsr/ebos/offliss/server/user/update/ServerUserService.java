package ch.hsr.ebos.offliss.server.user.update;

import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.offliss.common.OfflissRoles;
import ch.hsr.ebos.offliss.common.StateChangeEvent;
import ch.hsr.ebos.offliss.common.user.UserRole;
import ch.hsr.ebos.offliss.common.user.UserService;
import ch.hsr.ebos.offliss.common.user.UserState;
import ch.hsr.ebos.offliss.common.user.update.UpdateUserEvent;
import ch.hsr.ebos.offliss.server.KeycloakService;

public class ServerUserService extends UserService {

    @Inject
    KeycloakService keycloak;

    @Override
    public void updateUser(ApplicationEvent<UpdateUserEvent> event) throws Exception {
        super.updateUser(event);
        try {
            assignUserRole(event.getData().getId(),
                    event.getData().getUsername(),
                    event.getData().getRole().getNewValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changeUserState(ApplicationEvent<? extends StateChangeEvent> event, UserState state) throws Exception {
        super.changeUserState(event, state);
        String username = event.getData().getName();
        UserResource userResource = keycloak.getUserResourceByUsername(username);
        UserRepresentation user = userResource.toRepresentation();
        keycloak.setUserState(user, UserState.ACTIVE == state);
        userResource.update(user);
    }

    public void createUser(String userId, String username, String password, UserRole role) throws Exception {
        keycloak.createUser(userId, username, password);
        assignUserRole(userId, username, role);
    }

    public void assignUserRole(String userId, String username, UserRole role) throws Exception {
        UserResource userResource = keycloak.getUserResourceByUsername(username);

        List<RoleRepresentation> userRoles = new ArrayList<>();
        List<RoleRepresentation> assignedRoles = keycloak.getAssignedUserRoles(userResource);

        switch (role) {
            case ADMINISTRATOR:
                userRoles.add(keycloak.getOrCreateUserRole(OfflissRoles.Administrator));
                userRoles.add(keycloak.getOrCreateUserRole("user_read"));
                userRoles.add(keycloak.getOrCreateUserRole("user_write"));
                break;
            case DEVELOPER:
                userRoles.add(keycloak.getOrCreateUserRole(OfflissRoles.Developer));
                userRoles.add(keycloak.getOrCreateUserRole("user_read"));
                userRoles.add(keycloak.getOrCreateUserRole("project_read"));
                userRoles.add(keycloak.getOrCreateUserRole("issue_read"));
                userRoles.add(keycloak.getOrCreateUserRole("issue_write"));
                userRoles.add(keycloak.getOrCreateUserRole("timebooking-" + userId + "_read"));
                userRoles.add(keycloak.getOrCreateUserRole("timebooking-" + userId + "_write"));
                break;
            case PROJECT_MANAGER:
                userRoles.add(keycloak.getOrCreateUserRole(OfflissRoles.ProjectManager));
                userRoles.add(keycloak.getOrCreateUserRole("user_read"));
                userRoles.add(keycloak.getOrCreateUserRole("project_read"));
                userRoles.add(keycloak.getOrCreateUserRole("project_write"));
                userRoles.add(keycloak.getOrCreateUserRole("issue_read"));
                userRoles.add(keycloak.getOrCreateUserRole("issue_write"));
                break;
        }

        List<RoleRepresentation> addedRoles = new ArrayList<>();
        List<RoleRepresentation> removedRoles = new ArrayList<>();


        for (RoleRepresentation userRole : userRoles) {
            boolean alreadyAssigned = assignedRoles.stream().anyMatch(assigned -> assigned.getId().equals(userRole.getId()));
            if (!alreadyAssigned) {
                addedRoles.add(userRole);
            }
        }
        if (!addedRoles.isEmpty()) {
            keycloak.assignUserRoles(userResource, addedRoles);
        }

        for (RoleRepresentation assigned : assignedRoles) {
            boolean shouldBeAssigned = userRoles.stream().anyMatch(newRole -> newRole.getId().equals(assigned.getId()));
            if (!shouldBeAssigned) {
                removedRoles.add(assigned);
            }
        }
        if (!removedRoles.isEmpty()) {
            keycloak.unassignUserRoles(userResource, removedRoles);
        }
    }
}
