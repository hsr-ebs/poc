package ch.hsr.ebos.offliss.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

public class Preferences {
    private static Properties loadProperty(String filename) throws IOException {
        InputStream inputStream = null;
        try {
            String configEnv = System.getenv("CONFIG_PATH");

            if (configEnv != null && configEnv.length() > 0) {
                inputStream = new FileInputStream(new File(configEnv, filename));
            } else {
                inputStream = Main.class.getClassLoader().getResourceAsStream(filename);
            }

            Properties prop = new Properties();
            if (inputStream != null) {
                prop.load(inputStream);
            }

            return prop;
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    public static Properties loadProperties() throws IOException {
        Properties defaultProperties = loadProperty("config.properties");
        Properties productionProperties = loadProperty("config-production.properties");
        return mergeProperties(defaultProperties, productionProperties);
    }

    private static Properties mergeProperties(Properties... properties) {
        return Stream.of(properties)
                .collect(Properties::new, Map::putAll, Map::putAll);
    }
}
