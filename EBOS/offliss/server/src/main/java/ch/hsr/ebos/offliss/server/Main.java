package ch.hsr.ebos.offliss.server;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationFactory;
import ch.hsr.ebos.aion.core.processor.TimelineProcessor;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.aion.data.sqlite.connection.JavaSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.state.SqliteDataModule;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineFactory;
import ch.hsr.ebos.aion.email.mock.MockEmailModule;
import ch.hsr.ebos.aion.email.smtp.SmtpEmailModule;
import ch.hsr.ebos.aion.email.smtp.SmtpEmailOptions;
import ch.hsr.ebos.aion.http.HttpRequestInterceptor;
import ch.hsr.ebos.aion.http.HttpTimelineFactory;
import ch.hsr.ebos.offliss.common.issue.IssueModule;
import ch.hsr.ebos.offliss.common.project.ProjectModule;
import ch.hsr.ebos.offliss.common.user.UserModule;

public class Main {
    public static void main(String[] args) throws Exception {
        Properties properties = Preferences.loadProperties();

        KeycloakService keycloakService = Keycloak.setup(properties);

        SqliteConnectionFactory connectionFactory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());

        HttpTimelineFactory remoteTimelineFactory = new HttpTimelineFactory(properties.getProperty("general.timeline.host"), new HttpRequestInterceptor() {
            @Override
            public CompletableFuture<String> provideAuthorizationHeader() {
                CompletableFuture<String> future = new CompletableFuture<>();
                try {
                    future.complete("Bearer " + keycloakService.getAccessToken());
                } catch (Exception e) {
                    future.completeExceptionally(e);
                }
                return future;
            }
        });
        SqliteTimelineFactory resultTimelineFactory = new SqliteTimelineFactory(properties.getProperty("general.server_data_dir"), connectionFactory);

        ITimeline issueTimeline = remoteTimelineFactory.get("issue");
        ITimeline userTimeline = remoteTimelineFactory.get("user");
        ITimeline projectTimeline = remoteTimelineFactory.get("project");

        IDynamicTimeline issueResultTimeline = resultTimelineFactory.get("issue-result");
        IDynamicTimeline userResultTimeline = resultTimelineFactory.get("user-result");
        IDynamicTimeline projectResultTimeline = resultTimelineFactory.get("project-result");

        OfflissServerModule module = new OfflissServerModule(keycloakService);

        setupEmail(module, properties);
        setupOfflissModules(module, connectionFactory, properties);

        Application application = ApplicationFactory.create(module).get();

        TimelineProcessor userProcessor = new TimelineProcessor(application, userTimeline, userResultTimeline);
        TimelineProcessor projectProcessor = new TimelineProcessor(application, projectTimeline, projectResultTimeline);
        TimelineProcessor issueProcessor = new TimelineProcessor(application, issueTimeline, issueResultTimeline);

        int pollInterval = Integer.valueOf(properties.getProperty("general.timeline.poll_interval"));

        while (true) {
            userProcessor.run();
            projectProcessor.run();
            issueProcessor.run();

            Thread.sleep(pollInterval);
        }
    }

    private static void setupEmail(OfflissServerModule module, Properties properties) {
        boolean smtpEnabled = Boolean.valueOf(properties.getProperty("email.enabled"));

        if (smtpEnabled) {
            SmtpEmailOptions emailOptions = new SmtpEmailOptions();
            emailOptions.setFromEmail(properties.getProperty("email.smtp.fromEmail"));
            emailOptions.setFromName(properties.getProperty("email.smtp.fromName"));
            emailOptions.setHost(properties.getProperty("email.smtp.host"));
            emailOptions.setUsername(properties.getProperty("email.smtp.username"));
            emailOptions.setPassword(properties.getProperty("email.smtp.password"));
            module.addModule(new SmtpEmailModule(emailOptions));
        } else {
            module.addModule(new MockEmailModule());
        }
    }

    private static void setupOfflissModules(OfflissServerModule module, SqliteConnectionFactory connectionFactory, Properties properties) {
        Path directory = Paths.get(properties.getProperty("general.server_data_dir"), "data-state.db");
        DataModule dataModule = new SqliteDataModule(connectionFactory, directory.toString(), false);

        UserModule userModule = new UserModule();
        userModule.setDataModule(dataModule);

        module.addModule(userModule);

        IssueModule issueModule = new IssueModule();
        issueModule.setDataModule(dataModule);

        module.addModule(issueModule);

        ProjectModule projectModule = new ProjectModule();
        projectModule.setDataModule(dataModule);

        module.addModule(projectModule);
    }
}
