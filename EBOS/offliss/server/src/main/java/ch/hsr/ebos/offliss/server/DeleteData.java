package ch.hsr.ebos.offliss.server;

import org.keycloak.representations.idm.UserRepresentation;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DeleteData {
    public static void main(String[] args) throws Exception {
        Properties properties = Preferences.loadProperties();

        List<String> whitelistUsernames = new ArrayList<>();
        whitelistUsernames.add("admin");

        KeycloakService keycloakService = Keycloak.setup(properties);

        for (UserRepresentation user : keycloakService.getUsers()) {
            if (whitelistUsernames.contains(user.getUsername())) {
                continue;
            }

            keycloakService.delete(user.getId());
        }

    }
}
