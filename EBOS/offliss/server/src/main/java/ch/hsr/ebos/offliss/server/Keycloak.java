package ch.hsr.ebos.offliss.server;

import java.util.Properties;

public class Keycloak {
    public static KeycloakService setup(Properties properties) {
        KeycloakService keycloakService = new KeycloakService();
        keycloakService.setAdminClientId(properties.getProperty("keycloak.admin.clientId"));
        keycloakService.setAionClientId(properties.getProperty("keycloak.aion.clientId"));
        keycloakService.setAionClientSecret(properties.getProperty("keycloak.aion.clientSecret"));
        keycloakService.setHost(properties.getProperty("keycloak.host"));
        keycloakService.setPassword(properties.getProperty("keycloak.password"));
        keycloakService.setRealm(properties.getProperty("keycloak.realm"));
        keycloakService.setUsername(properties.getProperty("keycloak.username"));
        return keycloakService;
    }
}
