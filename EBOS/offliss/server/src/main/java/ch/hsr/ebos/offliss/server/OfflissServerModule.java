package ch.hsr.ebos.offliss.server;

import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.offliss.common.issue.IssueService;
import ch.hsr.ebos.offliss.common.issue.assign.AssignIssueEvent;
import ch.hsr.ebos.offliss.common.issue.unassign.UnassignIssueEvent;
import ch.hsr.ebos.offliss.common.user.UserService;
import ch.hsr.ebos.offliss.common.user.create.CreateUserEvent;
import ch.hsr.ebos.offliss.common.user.reset.ResetUserPasswordEvent;
import ch.hsr.ebos.offliss.server.issue.ServerIssueService;
import ch.hsr.ebos.offliss.server.issue.assign.AssignIssueServerAggregate;
import ch.hsr.ebos.offliss.server.issue.unassign.UnassignIssueServerAggregate;
import ch.hsr.ebos.offliss.server.user.PasswordGeneratorService;
import ch.hsr.ebos.offliss.server.user.create.CreateUserServerAggregate;
import ch.hsr.ebos.offliss.server.user.password.reset.ResetUserPasswordServerAggregate;
import ch.hsr.ebos.offliss.server.user.update.ServerUserService;

public class OfflissServerModule extends Module {
    private KeycloakService keycloakService;

    public OfflissServerModule(KeycloakService keycloakService) {
        this.keycloakService = keycloakService;
    }

    @Override
    protected void setup() {
        addEventAggregate(CreateUserEvent.class, CreateUserServerAggregate.class);
        addEventAggregate(ResetUserPasswordEvent.class, ResetUserPasswordServerAggregate.class);
        addEventAggregate(AssignIssueEvent.class, AssignIssueServerAggregate.class);
        addEventAggregate(UnassignIssueEvent.class, UnassignIssueServerAggregate.class);

        bind(IssueService.class).to(ServerIssueService.class);
        bind(TemplateService.class);
        bind(PasswordGeneratorService.class);
        bind(UserService.class).to(ServerUserService.class);
        bind(KeycloakService.class).toInstance(keycloakService);
    }
}
