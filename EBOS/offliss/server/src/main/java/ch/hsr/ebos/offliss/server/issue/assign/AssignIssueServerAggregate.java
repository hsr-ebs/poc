package ch.hsr.ebos.offliss.server.issue.assign;

import javax.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.email.IEmailService;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.assign.AssignIssueEvent;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.server.TemplateService;

public class AssignIssueServerAggregate extends EventAggregate<AssignIssueEvent> {
    @Inject
    EntityService entityService;

    @Inject
    IEmailService emailService;

    @Inject
    TemplateService templateService;

    @Override
    public void aggregate(ApplicationEvent<AssignIssueEvent> event) throws Throwable {
        try {
            String issueId = event.getData().getId();

            IssueEntity issue = entityService.get(IssueEntity.class, issueId);
            ProjectEntity project = entityService.get(ProjectEntity.class, issue.getProjectId());
            UserEntity user = entityService.get(UserEntity.class, event.getData().getUserId());

            String template = templateService.getTemplate("issue-update.html");

            String content = template.replace("{{title}}", "Issue assigned")
                    .replace("{{content}}", issue.getDescription());
            String subject = "[" + project.getName() + "] " + issue.getTitle() + " (#" + issue.getNumber() + ")";
            this.emailService.send(user.getEmail(), subject, content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
