package ch.hsr.ebos.app.ui.util;

import android.content.Context;
import android.widget.Toast;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.app.ui.common.OfflissApplication;

public class AppUtil {

    public static void emitEvent(Context context, AionClientModule aionClientModule, Object event) {
        try {
            aionClientModule.emit(event, getApplication(context).getEventAuth());
        } catch (Exception e) {
            Toast toast = Toast.makeText(context, "Event failed: " + e.getMessage(), Toast.LENGTH_LONG);
            toast.show();
            e.printStackTrace();
        }
    }

    public static String getCurrentUserId(Context context) {
        OfflissApplication application = getApplication(context);
        return application.getCurrentUserId();
    }

    public static OfflissApplication getApplication(Context context) {
        return ((OfflissApplication) context.getApplicationContext());
    }
}
