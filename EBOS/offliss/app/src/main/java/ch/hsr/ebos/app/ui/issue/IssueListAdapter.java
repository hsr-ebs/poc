package ch.hsr.ebos.app.ui.issue;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.adapter.EntityRecyclerListAdapter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.orm.query.QueryOrder;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.component.Images;
import ch.hsr.ebos.app.ui.util.AvatarUtil;
import ch.hsr.ebos.app.ui.util.EntityLoadUtil;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueState;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.UserState;

public class IssueListAdapter extends EntityRecyclerListAdapter<IssueEntity, IssueListAdapter.IssueViewHolder> {

    public IssueListAdapter(Activity activity, AionClientModule clientModule, Query query) {
        super(activity, R.layout.row_issue, clientModule, IssueEntity.class, query);
    }

    public IssueListAdapter(Activity activity, AionClientModule clientModule) {
        this(activity, clientModule, Query.build()
                .orderBy(QueryOrder.asc("number"), QueryOrder.asc("title", false)));
    }

    @Override
    protected IssueViewHolder createViewHolder(View view) {
        return new IssueViewHolder(view);
    }

    @Override
    protected void bindView(IssueEntity issue, IssueViewHolder viewHolder) {
        int color;
        if (issue.getState() == IssueState.CLOSED) {
            color = Color.rgb(178, 190, 195);
        } else {
            color = Color.rgb(0, 0, 0);
        }
        viewHolder.title.setTextColor(color);
        viewHolder.project.setTextColor(color);

        viewHolder.title.setText(issue.getName());
        ProjectEntity project = EntityLoadUtil.getProject(activity, issue.getProjectId());
        if (project != null) {
            viewHolder.project.setText(project.getName());
        }

        viewHolder.assignees.removeAll();
        viewHolder.assignees.setPlaceholder(R.drawable.ic_person);
        List<UserEntity> issueMembers = EntityLoadUtil.getIssueMember(activity, issue.getId());
        if (issueMembers != null) {
            for (UserEntity member : issueMembers) {
                if (member.getAvatar() != null) {
                    viewHolder.assignees.addImage(activity, AvatarUtil.getBitmap(member.getAvatar()),
                            member.getId());
                }
            }
        }
    }

    class IssueViewHolder extends RecyclerView.ViewHolder {
        Images assignees;
        TextView title;
        TextView project;

        IssueViewHolder(View view) {
            super(view);

            assignees = view.findViewById(R.id.issueRowAssignees);
            title = view.findViewById(R.id.issueRowTitle);
            project = view.findViewById(R.id.issueRowProject);
        }
    }
}
