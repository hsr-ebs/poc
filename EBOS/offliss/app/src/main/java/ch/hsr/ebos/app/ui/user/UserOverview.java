package ch.hsr.ebos.app.ui.user;

import android.support.design.widget.FloatingActionButton;
import android.view.View;

import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissOverviewFragment;
import ch.hsr.ebos.app.ui.component.List;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.offliss.common.user.UserEntity;

public class UserOverview extends OfflissOverviewFragment<UserListAdapter> {

    public UserOverview() {
        super(R.layout.fragment_user_overview, "Users");
    }

    @Override
    protected void setupView(View view) {
        final List list = view.findViewById(R.id.userList);
        final FloatingActionButton addUserButton = view.findViewById(R.id.addUser);

        listAdapter = new UserListAdapter(getActivity(), AppUtil.getApplication(getContext()).getUserModule());
        listAdapter.setSelectListener((UserEntity user) -> {
            UserDetail detail = new UserDetail();
            detail.setEntity(user);
            openDetail(detail);
        });
        list.setAdapter(listAdapter);

        addUserButton.setOnClickListener(clickedView -> openDetail(new UserDetail()));
    }
}
