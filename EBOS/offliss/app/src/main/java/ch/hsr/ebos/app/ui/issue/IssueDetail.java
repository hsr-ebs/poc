package ch.hsr.ebos.app.ui.issue;

import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissDetailFragment;
import ch.hsr.ebos.app.ui.component.DropDown;
import ch.hsr.ebos.app.ui.component.Images;
import ch.hsr.ebos.app.ui.component.Label;
import ch.hsr.ebos.app.ui.component.SearchableDialog;
import ch.hsr.ebos.app.ui.component.TextInput;
import ch.hsr.ebos.app.ui.project.ActiveProjectListAdapter;
import ch.hsr.ebos.app.ui.user.ActiveUserListAdapter;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.app.ui.util.AvatarUtil;
import ch.hsr.ebos.app.ui.util.EntityLoadUtil;
import ch.hsr.ebos.app.ui.util.Validation;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.StateChangeEvent;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueState;
import ch.hsr.ebos.offliss.common.issue.assign.AssignIssueEvent;
import ch.hsr.ebos.offliss.common.issue.close.CloseIssueEvent;
import ch.hsr.ebos.offliss.common.issue.create.CreateIssueEvent;
import ch.hsr.ebos.offliss.common.issue.open.OpenIssueEvent;
import ch.hsr.ebos.offliss.common.issue.unassign.UnassignIssueEvent;
import ch.hsr.ebos.offliss.common.issue.update.UpdateIssueEvent;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.user.UserEntity;

public class IssueDetail extends OfflissDetailFragment<IssueEntity> {
    private AionClientModule clientModule;
    private TextInput title;
    private TextInput description;
    private Label number;
    private DropDown chooseProject;
    private RadioButton activeState;
    private DropDown parent;
    private Images assigned;
    private Set<UserEntity> actualMembers;
    private SearchableDialog userDialog;

    public IssueDetail() {
        super(R.layout.fragment_issue_detail, "issue");
    }

    @Override
    protected void setupView(View view) {
        clientModule = AppUtil.getApplication(getContext()).getIssueModule();
        title = view.findViewById(R.id.issueTitle);
        description = view.findViewById(R.id.issueDescription);
        chooseProject = view.findViewById(R.id.issueChooseProject);
        number = view.findViewById(R.id.issueNumber);
        Label project = view.findViewById(R.id.issueProject);
        RadioGroup state = view.findViewById(R.id.issueState);
        activeState = view.findViewById(R.id.radio_issue_active);
        RadioButton closeState = view.findViewById(R.id.radio_issue_closed);
        parent = view.findViewById(R.id.issueParent);
        assigned = view.findViewById(R.id.issueAssigned);
        actualMembers = new HashSet<>();

        SearchableDialog projectDialog = new SearchableDialog(getContext(), "Choose a project",
                new ActiveProjectListAdapter(getActivity(), AppUtil.getApplication(getContext()).getProjectModule()),
                false, 1);
        projectDialog.setOnSingleSelectApplyListener(chooseProject::setSelection);
        chooseProject.setOnClickListener(dropDownView -> projectDialog.show());

        SearchableDialog parentDialog = new SearchableDialog(getContext(), "Choose an issue",
                new OpenIssueListAdapter(getActivity(), clientModule), false, 1);
        parentDialog.setOnSingleSelectApplyListener(parent::setSelection);
        parent.setOnClickListener(dropDownView -> parentDialog.show());

        userDialog = new SearchableDialog(getContext(), "Choose a user",
                new ActiveUserListAdapter(getActivity(), AppUtil.getApplication(getContext()).getUserModule()),
                true, 5);
        userDialog.setOnMultiSelectApplyListener(selectedEntities -> {
            assigned.removeAll();
            for (Entity entity : selectedEntities) {
                UserEntity member = (UserEntity) entity;
                assigned.addImage(getContext(), AvatarUtil.getBitmap(member.getAvatar()),
                        member.getId());
            }
        });

        assigned.setPlaceholder(R.drawable.ic_person_add);
        assigned.removeAll();
        assigned.setOnImageLayoutClickListener(userDialog::show);

        if (entity == null) {
            number.setVisibility(View.GONE);
            state.setVisibility(View.GONE);
            project.setVisibility(View.GONE);
            chooseProject.setVisibility(View.VISIBLE);
        } else {
            number.setText(String.valueOf(entity.getNumber()));
            title.setText(entity.getTitle());
            description.setText(entity.getDescription());
            ProjectEntity actualProject = EntityLoadUtil.getProject(getContext(), entity.getProjectId());
            if (actualProject != null) {
                project.setText(actualProject.getName());
            } else {
                project.setText("No project");
            }
            parent.setSelection(EntityLoadUtil.getIssue(getContext(), entity.getParentId()));
            if (entity.getState() == IssueState.OPEN) {
                activeState.setChecked(true);
                closeState.setChecked(false);
            } else {
                activeState.setChecked(false);
                closeState.setChecked(true);
            }
            actualMembers.addAll(EntityLoadUtil.getIssueMember(getContext(), entity.getId()));
            for (UserEntity member : actualMembers) {
                assigned.addImage(getContext(), AvatarUtil.getBitmap(member.getAvatar()),
                        member.getId());
                userDialog.setSelectedEntity(member);
            }
            project.setVisibility(View.VISIBLE);
            chooseProject.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean isValid() {
        boolean isValid = Validation.isRequiredValid(title);
        if (entity == null) {
            isValid = Validation.isRequiredValid(chooseProject) && isValid;
        }
        return isValid;
    }

    @Override
    public void save() {
        IssueEntity savedIssue = new IssueEntity();
        savedIssue.setId(entity == null ? UUID.randomUUID().toString() : entity.getId());
        savedIssue.setTitle(title.getText());
        savedIssue.setDescription(description.getText());
        if (activeState.isChecked()) {
            savedIssue.setState(IssueState.OPEN);
        } else {
            savedIssue.setState(IssueState.CLOSED);
        }
        if (entity == null) {
            savedIssue.setProjectId(chooseProject.getSelection().getId());
        }
        if (parent.getSelection() != null) {
            savedIssue.setParentId(parent.getSelection().getId());
        }
        Set<Entity> selectedMembers = userDialog.getSelectedEntities();

        if (entity == null) {
            CreateIssueEvent event = new CreateIssueEvent();
            event.setId(savedIssue.getId());
            event.setTitle(savedIssue.getTitle());
            event.setDescription(savedIssue.getDescription());
            event.setProjectId(savedIssue.getProjectId());
            event.setParentId(savedIssue.getParentId());
            AppUtil.emitEvent(getContext(), clientModule, event);
        } else {
            if (!savedIssue.getTitle().equals(entity.getTitle()) ||
                    (savedIssue.getDescription() != null &&
                            !savedIssue.getDescription().equals(entity.getDescription())) ||
                    (savedIssue.getParentId() != null &&
                            !savedIssue.getParentId().equals(entity.getParentId()))) {
                UpdateIssueEvent event = new UpdateIssueEvent();
                event.setId(savedIssue.getId());
                event.setTitle(new ChangeSet<>(entity.getTitle(), savedIssue.getTitle()));
                event.setDescription(new ChangeSet<>(entity.getDescription(), savedIssue.getDescription()));
                event.setParentId(new ChangeSet<>(entity.getParentId(), savedIssue.getParentId()));
                AppUtil.emitEvent(getContext(), clientModule, event);
            }
            if (savedIssue.getState() != entity.getState()) {
                StateChangeEvent event;
                if (IssueState.OPEN == savedIssue.getState()) {
                    event = new OpenIssueEvent();
                } else {
                    event = new CloseIssueEvent();
                }
                event.setId(savedIssue.getId());
                event.setName(savedIssue.getName());
                AppUtil.emitEvent(getContext(), clientModule, event);
            }
        }

        if (!actualMembers.equals(selectedMembers)) {
            for (Entity selectedMember : selectedMembers) {
                UserEntity newMember = (UserEntity) selectedMember;
                if (!actualMembers.contains(newMember)) {
                    AssignIssueEvent assignIssueEvent = new AssignIssueEvent();
                    assignIssueEvent.setId(savedIssue.getId());
                    assignIssueEvent.setUserId(newMember.getId());
                    assignIssueEvent.setIssueName(savedIssue.getName());
                    assignIssueEvent.setUserFullName(newMember.getFullName());
                    AppUtil.emitEvent(getContext(), clientModule, assignIssueEvent);
                }
            }
            for (UserEntity actualMember : actualMembers) {
                if (!selectedMembers.contains(actualMember)) {
                    UnassignIssueEvent unassignIssueEvent = new UnassignIssueEvent();
                    unassignIssueEvent.setId(savedIssue.getId());
                    unassignIssueEvent.setUserId(actualMember.getId());
                    unassignIssueEvent.setIssueName(savedIssue.getName());
                    unassignIssueEvent.setUserFullName(actualMember.getFullName());
                    AppUtil.emitEvent(getContext(), clientModule, unassignIssueEvent);
                }
            }
        }
    }
}
