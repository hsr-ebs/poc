package ch.hsr.ebos.app.ui.user;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.adapter.EntityRecyclerListAdapter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.orm.query.QueryOrder;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.component.RoundImage;
import ch.hsr.ebos.app.ui.util.AvatarUtil;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.UserState;

public class UserListAdapter extends EntityRecyclerListAdapter<UserEntity, UserListAdapter.UserViewHolder> {

    public UserListAdapter(Activity activity, AionClientModule clientModule, Query query) {
        super(activity, R.layout.row_user, clientModule, UserEntity.class, query);
    }

    public UserListAdapter(@NonNull Activity activity, AionClientModule clientModule) {
        this(activity, clientModule, Query.build().orderBy(QueryOrder.asc("fullName", false)));
    }

    @Override
    protected UserViewHolder createViewHolder(View view) {
        return new UserViewHolder(view);
    }

    @Override
    protected void bindView(UserEntity entity, UserViewHolder viewHolder) {
        int color;
        if (entity.getState() == UserState.INACTIVE) {
            color = Color.rgb(178, 190, 195);
            viewHolder.avatar.setImageBitmap(null);
        } else {
            color = Color.rgb(0, 0, 0);
            viewHolder.avatar.setImageBitmap(AvatarUtil.getBitmap(entity.getAvatar()));
        }
        viewHolder.name.setText(entity.getFullName());
        viewHolder.email.setText(entity.getEmail());
        viewHolder.username.setText(entity.getUsername());
        viewHolder.role.setText(entity.getRole().getName());
        viewHolder.name.setTextColor(color);
        viewHolder.email.setTextColor(color);
        viewHolder.role.setTextColor(color);
        viewHolder.username.setTextColor(color);
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
        RoundImage avatar;
        TextView name;
        TextView email;
        TextView username;
        TextView role;

        UserViewHolder(View view) {
            super(view);

            avatar = view.findViewById(R.id.userRowAvatar);
            name = view.findViewById(R.id.userRowName);
            email = view.findViewById(R.id.userRowEmail);
            username = view.findViewById(R.id.userRowUsername);
            role = view.findViewById(R.id.userRowRole);
        }
    }
}
