package ch.hsr.ebos.app.ui.login;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationService;
import net.openid.appauth.AuthorizationServiceConfiguration;
import net.openid.appauth.TokenRequest;

import java.util.HashMap;

import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissApplication;
import ch.hsr.ebos.app.ui.component.TextInput;
import ch.hsr.ebos.app.ui.util.Validation;

public class Login extends AppCompatActivity {
    private AuthorizationServiceConfiguration configuration;
    private String clientId, clientSecret;
    private AuthorizationService authorizationService;
    private OfflissApplication offlissApplication;

    private TextInput username;
    private TextInput password;
    private Button loginButton;
    private ProgressBar progressBar;
    private TextView loginError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Login");
        }

        offlissApplication = (OfflissApplication) getApplicationContext();
        try {
            offlissApplication.setAuthState(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ApplicationInfo applicationInfo = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            clientId = applicationInfo.metaData.getString("clientId");
            clientSecret = applicationInfo.metaData.getString("clientSecret");
            configuration = new AuthorizationServiceConfiguration(
                    Uri.parse(applicationInfo.metaData.getString("authorizationEndpoint")),
                    Uri.parse(applicationInfo.metaData.getString("tokenEndpoint"))
            );
            authorizationService = new AuthorizationService(this);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        username = findViewById(R.id.loginUserName);
        password = findViewById(R.id.loginPassword);

        loginButton = findViewById(R.id.login);
        progressBar = findViewById(R.id.loginProgress);
        loginError = findViewById(R.id.loginError);

        loginButton.setOnClickListener(v -> {

            boolean isValid = Validation.isRequiredValid(username);
            isValid = Validation.isRequiredValid(password) && isValid;

            if (!isValid) {
                return;
            }

            loginError.setVisibility(View.GONE);
            loginButton.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

            login(username.getText(), password.getText());
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        authorizationService.dispose();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    private void login(String username, String password) {
        TokenRequest req = new TokenRequest.Builder(configuration, clientId)
                .setGrantType("password")
                .setAdditionalParameters(new HashMap<String, String>() {{
                    put("username", username);
                    put("password", password);
                    put("client_secret", clientSecret);
                }})
                .build();

        authorizationService.performTokenRequest(req, (response, ex) -> {
            if (ex == null) {
                AuthState authState = new AuthState();
                authState.update(response, ex);

                try {
                    offlissApplication.setAuthState(authState);
                    loginHandler(null);
                } catch (Exception e) {
                    loginHandler(e);
                }
            } else {
                loginHandler(ex);
            }
        });
    }

    private void loginHandler(Exception e) {
        if (e != null) {
            loginError.setVisibility(View.VISIBLE);
            loginError.setText(R.string.wrong_login);

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm.getActiveNetworkInfo() == null) {
                loginError.setText(R.string.no_connection);
            }

            e.printStackTrace();
            loginButton.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        } else {
            finish();
        }
    }

}
