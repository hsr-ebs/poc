package ch.hsr.ebos.app.ui.component;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Date;

import ch.hsr.ebos.app.R;
import ch.hsr.ebos.offliss.common.FormatUtils;

public class DateTime extends LinearLayout {
    private Calendar value;
    private Label date;
    private Label time;
    private OnDateTimeSelectListener dateTimeSelectListener;

    public DateTime(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(HORIZONTAL);

        value = Calendar.getInstance();

        String labelText = null;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Component);
        if (typedArray != null) {
            labelText = typedArray.getString(R.styleable.Component_label);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.component_date_time, this, true);
        }

        OnClickListener dateClickListener = new DatePickerDialogListener();

        LinearLayout dateLayout = (LinearLayout) getChildAt(0);
        date = (Label) dateLayout.getChildAt(1);
        date.setLabelText(labelText + " date");
        date.setText(getDateFormat());
        date.setOnClickListener(dateClickListener);

        ImageButton datePicker = (ImageButton) dateLayout.getChildAt(0);
        datePicker.setOnClickListener(dateClickListener);

        OnClickListener timeClickListener = new TimePickerDialogListener();

        LinearLayout timeLayout = (LinearLayout) getChildAt(1);
        time = (Label) timeLayout.getChildAt(1);
        time.setLabelText(labelText + " time");
        time.setText(getTimeFormat());
        time.setOnClickListener(timeClickListener);
        ImageButton timePicker = (ImageButton) timeLayout.getChildAt(0);
        timePicker.setOnClickListener(timeClickListener);
    }

    private String getDateFormat() {
        return FormatUtils.formatDate(value.getTime());
    }

    private String getTimeFormat() {
        return FormatUtils.formatTime(value.getTime());
    }

    public Date getDateTime() {
        return value.getTime();
    }

    public void setDateTime(Date dateTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);

        value = calendar;
        date.setText(FormatUtils.formatDate(dateTime));
        time.setText(FormatUtils.formatTime(dateTime));
    }

    public void setDateTimeSelectListener(OnDateTimeSelectListener dateTimeSelectListener) {
        this.dateTimeSelectListener = dateTimeSelectListener;
    }

    private class DatePickerDialogListener implements OnClickListener {

        @Override
        public void onClick(View view) {
            DatePickerDialog picker = new DatePickerDialog(getContext(), (datePicker, year, month, day) -> {
                value.set(year, month, day, value.get(Calendar.HOUR_OF_DAY), value.get(Calendar.MINUTE));
                date.setText(getDateFormat());
                dateTimeSelectListener.onSelect(getDateTime());
            }, value.get(Calendar.YEAR), value.get(Calendar.MONTH), value.get(Calendar.DATE));
            picker.show();
        }
    }

    private class TimePickerDialogListener implements OnClickListener {

        @Override
        public void onClick(View view) {
            TimePickerDialog picker = new TimePickerDialog(getContext(), (TimePicker timePicker, int hour, int min) -> {
                value.set(value.get(Calendar.YEAR), value.get(Calendar.MONTH), value.get(Calendar.DATE), hour, min);
                time.setText(getTimeFormat());
                dateTimeSelectListener.onSelect(getDateTime());
            }, value.get(Calendar.HOUR_OF_DAY), value.get(Calendar.MINUTE), true);
            picker.show();
        }
    }
}
