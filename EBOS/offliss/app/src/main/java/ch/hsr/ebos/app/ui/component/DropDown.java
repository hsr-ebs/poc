package ch.hsr.ebos.app.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.app.R;

public class DropDown extends LinearLayout {

    private Entity selection;
    private TextView selectionText;
    private OnClickListener clickListener;

    public DropDown(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);

        String labelText = null;
        String hint = "Select an item...";

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Component);
        if (typedArray != null) {
            labelText = typedArray.getString(R.styleable.Component_label);
            hint = typedArray.getString(R.styleable.Component_hint);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.component_drop_down, this, true);
        }

        TextView label = (TextView) getChildAt(0);
        if (labelText == null) {
            label.setVisibility(GONE);
        } else {
            label.setText(labelText);
            label.setVisibility(VISIBLE);
        }

        CoordinatorLayout spinner = (CoordinatorLayout) getChildAt(1);
        spinner.setOnClickListener(view -> clickListener.onClick(this));

        selectionText = (TextView) spinner.getChildAt(0);
        selectionText.setText(hint);
    }

    public void setOnClickListener(OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public Entity getSelection() {
        return selection;
    }

    public void setSelection(Entity selection) {
        this.selection = selection;
        if (selection != null) {
            selectionText.setText(selection.toString());
        }
    }

    public void setError(String error) {
        selectionText.setError(error);
        selectionText.setTextColor(Color.RED);
    }
}
