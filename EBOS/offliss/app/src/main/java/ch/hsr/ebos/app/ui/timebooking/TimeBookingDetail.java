package ch.hsr.ebos.app.ui.timebooking;


import android.view.View;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissDetailFragment;
import ch.hsr.ebos.app.ui.component.DateTime;
import ch.hsr.ebos.app.ui.component.DropDown;
import ch.hsr.ebos.app.ui.component.Label;
import ch.hsr.ebos.app.ui.component.SearchableDialog;
import ch.hsr.ebos.app.ui.issue.OpenIssueListAdapter;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.app.ui.util.EntityLoadUtil;
import ch.hsr.ebos.app.ui.util.TimeBookingUtil;
import ch.hsr.ebos.app.ui.util.Validation;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingEntity;
import ch.hsr.ebos.offliss.common.timebooking.create.CreateTimeBookingEvent;
import ch.hsr.ebos.offliss.common.timebooking.update.UpdateTimeBookingEvent;

public class TimeBookingDetail extends OfflissDetailFragment<TimeBookingEntity> {

    private AionClientModule clientModule;
    private Label issueName;
    private DropDown chooseIssue;
    private DateTime from;
    private DateTime to;
    private Label duration;

    public TimeBookingDetail() {
        super(R.layout.fragment_time_booking_detail, "time booking");
    }

    @Override
    protected void setupView(View view) {
        clientModule = AppUtil.getApplication(getContext()).getTimebookingModule();
        issueName = view.findViewById(R.id.timeBookingIssue);
        chooseIssue = view.findViewById(R.id.timeBookingChooseIssue);
        Label projectName = view.findViewById(R.id.timeBookingProject);
        from = view.findViewById(R.id.timeBookingFrom);
        to = view.findViewById(R.id.timeBookingTo);
        duration = view.findViewById(R.id.timeBookingDuration);

        SearchableDialog issueDialog = new SearchableDialog(getContext(), "Choose an issue",
                new OpenIssueListAdapter(getActivity(), AppUtil.getApplication(getContext()).getIssueModule()),
                false, 1);
        issueDialog.setOnSingleSelectApplyListener(selectedEntity -> {
            chooseIssue.setSelection(selectedEntity);
            IssueEntity issue = (IssueEntity) selectedEntity;
            ProjectEntity project = EntityLoadUtil.getProject(getContext(), issue.getProjectId());
            if (project != null) {
                projectName.setText(project.getName());
            }
            issueName.setText(issue.getName());
        });
        chooseIssue.setOnClickListener(dropDownView -> issueDialog.show());

        from.setDateTimeSelectListener(start -> {
            Date stop = to.getDateTime();
            duration.setText(TimeBookingUtil.getDurationText(start, stop));
        });
        to.setDateTimeSelectListener(stop -> {
            Date start = from.getDateTime();
            duration.setText(TimeBookingUtil.getDurationText(start, stop));
        });

        if (entity == null) {
            duration.setText(TimeBookingUtil.getDurationText(null, null));
            issueName.setVisibility(View.GONE);
            chooseIssue.setVisibility(View.VISIBLE);

            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.HOUR, -1);

            from.setDateTime(calendar.getTime());
            duration.setText(TimeBookingUtil.getDurationText(from.getDateTime(), to.getDateTime()));
        } else {
            IssueEntity issue = EntityLoadUtil.getIssue(getContext(), entity.getIssueId());
            if (issue != null) {
                issueName.setText(issue.getName());

                ProjectEntity project = EntityLoadUtil.getProject(getContext(), issue.getProjectId());
                if (project != null) {
                    projectName.setText(project.getName());
                }
            }
            from.setDateTime(entity.getStart());
            to.setDateTime(entity.getStop());
            duration.setText(TimeBookingUtil.getDurationText(entity.getStart(), entity.getStop()));
            issueName.setVisibility(View.VISIBLE);
            chooseIssue.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean isValid() {
        boolean isValid = Validation.isDurationPositive(from.getDateTime(), to.getDateTime(), duration);
        if (entity == null) {
            isValid = Validation.isRequiredValid(chooseIssue) && isValid;
        }
        return isValid;
    }

    @Override
    public void save() {
        TimeBookingEntity savedTimeBooking = new TimeBookingEntity();
        savedTimeBooking.setId(entity == null ? UUID.randomUUID().toString() : entity.getId());
        savedTimeBooking.setIssueId(entity == null ? chooseIssue.getSelection().getId() : null);
        savedTimeBooking.setUserId(AppUtil.getCurrentUserId(getContext()));
        savedTimeBooking.setStart(from.getDateTime());
        savedTimeBooking.setStop(to.getDateTime());

        if (entity == null) {
            CreateTimeBookingEvent event = new CreateTimeBookingEvent();
            event.setId(savedTimeBooking.getId());
            event.setIssueId(savedTimeBooking.getIssueId());
            event.setUserId(savedTimeBooking.getUserId());
            event.setStart(savedTimeBooking.getStart());
            event.setStop(savedTimeBooking.getStop());
            event.setIssueName(issueName.getText());
            AppUtil.emitEvent(getContext(), clientModule, event);
        } else if (!savedTimeBooking.equals(entity)) {
            UpdateTimeBookingEvent event = new UpdateTimeBookingEvent();
            event.setId(savedTimeBooking.getId());
            event.setStart(new ChangeSet<>(entity.getStart(), savedTimeBooking.getStart()));
            event.setStop(new ChangeSet<>(entity.getStop(), savedTimeBooking.getStop()));
            AppUtil.emitEvent(getContext(), clientModule, event);
        }
    }
}
