package ch.hsr.ebos.app.ui.timebooking;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.adapter.EntityRecyclerListAdapter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.orm.query.QueryOrder;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.util.EntityLoadUtil;
import ch.hsr.ebos.app.ui.util.TimeBookingUtil;
import ch.hsr.ebos.offliss.common.FormatUtils;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingEntity;

public class TimeBookingListAdapter extends EntityRecyclerListAdapter<TimeBookingEntity,
        TimeBookingListAdapter.TimeBookingViewHolder> {

    public TimeBookingListAdapter(Activity activity, AionClientModule clientModule) {
        super(activity, R.layout.row_time_booking, clientModule, TimeBookingEntity.class,
                Query.build().orderBy(QueryOrder.asc("start")));
    }

    @Override
    protected TimeBookingViewHolder createViewHolder(View view) {
        return new TimeBookingViewHolder(view);
    }

    @Override
    protected void bindView(TimeBookingEntity entity, TimeBookingViewHolder viewHolder) {
        IssueEntity issue = EntityLoadUtil.getIssue(this.activity, entity.getIssueId());
        if (issue != null) {
            viewHolder.issueName.setText(issue.getName());

            ProjectEntity project = EntityLoadUtil.getProject(this.activity, issue.getProjectId());
            if (project != null) {
                viewHolder.projectName.setText(project.getName());
            }
        }

        viewHolder.duration.setText(TimeBookingUtil.getDurationText(entity.getStart(), entity.getStop()));
        viewHolder.begin.setText(FormatUtils.formatDate(entity.getStart()));
    }

    public void deleteItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class TimeBookingViewHolder extends RecyclerView.ViewHolder {
        TextView issueName;
        TextView projectName;
        TextView duration;
        TextView begin;

        TimeBookingViewHolder(View view) {
            super(view);

            issueName = view.findViewById(R.id.timeBookingRowIssue);
            projectName = view.findViewById(R.id.timeBookingRowProject);
            duration = view.findViewById(R.id.timeBookingRowDuration);
            begin = view.findViewById(R.id.timeBookingRowBegin);
        }
    }
}
