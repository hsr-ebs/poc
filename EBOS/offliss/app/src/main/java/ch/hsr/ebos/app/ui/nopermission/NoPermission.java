package ch.hsr.ebos.app.ui.nopermission;


import android.view.View;

import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissFragment;

public class NoPermission extends OfflissFragment {

    public NoPermission() {
        super(R.layout.fragment_no_permission, false);
    }

    @Override
    protected void setupView(View view) {
    }

    @Override
    public String getToolbarTitle() {
        return "No Permissions";
    }
}
