package ch.hsr.ebos.app.ui.about;

import android.view.View;

import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissFragment;
import ch.hsr.ebos.app.ui.component.Label;

public class About extends OfflissFragment {

    public About() {
        super(R.layout.fragment_about, false);
    }

    @Override
    protected void setupView(View view) {
        final Label version = view.findViewById(R.id.version);
        version.setText("1.0");

        Label authors = view.findViewById(R.id.authors);
        authors.setText("Michelle Kunz and Pascal Bertschi");
        Label framework = view.findViewById(R.id.frameworkUrl);
        framework.setText("https://aion.bertschi.io");
    }

    @Override
    public String getToolbarTitle() {
        return "About";
    }
}
