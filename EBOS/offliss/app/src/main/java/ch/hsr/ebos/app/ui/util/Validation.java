package ch.hsr.ebos.app.ui.util;

import android.graphics.Color;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.Date;

import ch.hsr.ebos.app.ui.component.DropDown;
import ch.hsr.ebos.app.ui.component.Label;
import ch.hsr.ebos.app.ui.component.TextInput;

public class Validation {

    public static boolean isRequiredValid(TextInput inputField) {
        if (inputField.getText().trim().length() == 0) {
            inputField.setError("Value is required");
            return false;
        }
        return true;
    }

    public static boolean isRequiredValid(DropDown dropDown) {
        if (dropDown.getSelection() == null) {
            dropDown.setError("Value is required");
            return false;
        }
        return true;
    }

    public static boolean isRequiredValid(RadioGroup radioGroup) {
        if (radioGroup.getCheckedRadioButtonId() == -1) {
            for (int i = 0; i < radioGroup.getChildCount(); i++) {
                RadioButton button = (RadioButton) radioGroup.getChildAt(i);
                button.setTextColor(Color.RED);
            }
            return false;
        }
        return true;
    }

    public static boolean isEmailValid(TextInput emailField) {
        if (emailField.getText().
                matches("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$")) {
            return true;
        }
        emailField.setError("Value is not a mail-address");
        return false;
    }

    public static boolean isDurationPositive(Date start, Date stop, Label durationField) {
        double duration = TimeBookingUtil.getDuration(start, stop);
        if (duration < 0) {
            durationField.setError();
            return false;
        }
        return true;
    }

}
