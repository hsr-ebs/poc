package ch.hsr.ebos.app.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import ch.hsr.ebos.aion.android.ApplicationState;
import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.about.About;
import ch.hsr.ebos.app.ui.common.OfflissApplication;
import ch.hsr.ebos.app.ui.common.OfflissFragment;
import ch.hsr.ebos.app.ui.issue.IssueOverview;
import ch.hsr.ebos.app.ui.login.Login;
import ch.hsr.ebos.app.ui.nopermission.NoPermission;
import ch.hsr.ebos.app.ui.project.ProjectOverview;
import ch.hsr.ebos.app.ui.synchronization.SynchronizationOverview;
import ch.hsr.ebos.app.ui.timebooking.TimeBookingOverview;
import ch.hsr.ebos.app.ui.user.UserOverview;
import ch.hsr.ebos.app.ui.util.AvatarUtil;
import ch.hsr.ebos.app.ui.util.EntityLoadUtil;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.UserRole;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        OfflissApplication application = (OfflissApplication) getApplicationContext();
        ApplicationState applicationState = application.getApplicationState();

        if (applicationState.getAuthState() == null || !applicationState.getAuthState().isAuthorized()) {
            startActivityForResult(new Intent(this, Login.class), 1);
        } else {
            TimelineEventAuth auth = applicationState.getTimelineEventAuth();
            List<String> roles = auth.getRoles();
            setUserInfo(auth.getUserId());

            NavigationView navigation = findViewById(R.id.navigation);
            initNavigation(navigation, roles);
            Menu menu = navigation.getMenu();

            if (roles == null) {
                startActivityForResult(new Intent(this, Login.class), 1);
            } else if (roles.contains(UserRole.ADMINISTRATOR.getValue())) {
                menu.findItem(R.id.users).setChecked(true);
                showFragment(new UserOverview());
            } else if (roles.contains(UserRole.PROJECT_MANAGER.getValue())) {
                menu.findItem(R.id.projects).setChecked(true);
                showFragment(new ProjectOverview());
            } else if (roles.contains(UserRole.DEVELOPER.getValue())) {
                menu.findItem(R.id.issues).setChecked(true);
                showFragment(new IssueOverview());
            } else {
                showFragment(new NoPermission());
            }
        }
    }

    private void initNavigation(NavigationView navigation, List<String> roles) {
        drawerLayout = findViewById(R.id.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        }

        navigation.setNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.users:
                    showFragment(new UserOverview());
                    break;
                case R.id.projects:
                    showFragment(new ProjectOverview());
                    break;
                case R.id.issues:
                    showFragment(new IssueOverview());
                    break;
                case R.id.timeBookings:
                    showFragment(new TimeBookingOverview());
                    break;
                case R.id.synchronisation:
                    showFragment(new SynchronizationOverview());
                    break;
                case R.id.logout:
                    startActivity(new Intent(this, Login.class));
                    break;
                case R.id.about:
                    showFragment(new About());
                    break;
                default:
                    break;
            }
            drawerLayout.closeDrawers();
            return true;
        });

        Menu menu = navigation.getMenu();
        menu.findItem(R.id.users).setVisible(false);
        menu.findItem(R.id.projects).setVisible(false);
        menu.findItem(R.id.issues).setVisible(false);
        menu.findItem(R.id.timeBookings).setVisible(false);
        menu.findItem(R.id.synchronisation).setVisible(false);

        if (roles.contains(UserRole.ADMINISTRATOR.getValue())) {
            menu.findItem(R.id.users).setVisible(true);
            menu.findItem(R.id.synchronisation).setVisible(true);
        }
        if (roles.contains(UserRole.PROJECT_MANAGER.getValue())) {
            menu.findItem(R.id.projects).setVisible(true);
            menu.findItem(R.id.issues).setVisible(true);
            menu.findItem(R.id.synchronisation).setVisible(true);
        }
        if (roles.contains(UserRole.DEVELOPER.getValue())) {
            menu.findItem(R.id.issues).setVisible(true);
            menu.findItem(R.id.timeBookings).setVisible(true);
            menu.findItem(R.id.synchronisation).setVisible(true);
        }
    }

    private void showFragment(OfflissFragment fragment) {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment, fragment.getToolbarTitle())
                .commit();
    }

    private void setUserInfo(String userId) {
        View headerView = ((NavigationView) findViewById(R.id.navigation)).getHeaderView(0);
        TextView username = headerView.findViewById(R.id.logged_username);
        TextView fullName = headerView.findViewById(R.id.logged_fullName);
        TextView mail = headerView.findViewById(R.id.logged_mail);
        ImageView avatar = headerView.findViewById(R.id.logged_avatar);

        UserEntity user = EntityLoadUtil.getUserById(getApplicationContext(), userId);
        if (user == null) {
            username.setText("no_user");
            fullName.setText("No Name");
            mail.setText("no.name@example.ch");
            try {
                avatar.setImageBitmap(AvatarUtil.generateRandom(getAssets(), 250));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            username.setText(user.getUsername());
            fullName.setText(user.getFullName());
            mail.setText(user.getEmail());
            avatar.setImageBitmap(AvatarUtil.getBitmap(user.getAvatar()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Fragment fragment = getFragmentManager().findFragmentById(R.id.container);
                if (fragment instanceof OfflissFragment && ((OfflissFragment) fragment).isNavigatingBack()) {
                    onBackPressed();
                } else {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            finish();
        }
    }
}