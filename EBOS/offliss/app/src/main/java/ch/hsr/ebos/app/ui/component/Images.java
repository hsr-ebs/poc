package ch.hsr.ebos.app.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ch.hsr.ebos.app.R;

public class Images extends LinearLayout {

    private LinearLayout imageLayout;
    private RoundImage placeholder;
    private OnImageLayoutClickListener onImageLayoutClickListener;
    private int imageSize;

    public Images(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);

        String labelText = null;

        TypedArray componentTypeArray = context.obtainStyledAttributes(attrs, R.styleable.Component);
        if (componentTypeArray != null) {
            labelText = componentTypeArray.getString(R.styleable.Component_label);
        }
        TypedArray imageTypedArray = context.obtainStyledAttributes(attrs, R.styleable.RoundImage);
        if (imageTypedArray != null) {
            imageSize = (int) imageTypedArray.getDimension(R.styleable.RoundImage_image_size, 84);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.component_images, this, true);
        }

        TextView label = (TextView) getChildAt(0);
        if (labelText == null) {
            label.setVisibility(GONE);
        } else {
            label.setText(labelText);
            label.setVisibility(VISIBLE);
        }

        imageLayout = (LinearLayout) getChildAt(1);
        imageLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onImageLayoutClickListener != null) {
                    onImageLayoutClickListener.onClick();
                }
            }
        });
        placeholder = findViewById(R.id.imagePlaceholder);
        placeholder.setSize(imageSize);
    }

    public void setPlaceholder(int imageResource) {
        placeholder.setImageResource(imageResource);
    }

    public void setOnImageLayoutClickListener(OnImageLayoutClickListener onImageLayoutClickListener) {
        this.onImageLayoutClickListener = onImageLayoutClickListener;
    }

    public void addImage(Context context, Bitmap bitmap, String tagId) {
        if (imageLayout.getChildCount() > 0) {
            placeholder.setVisibility(GONE);
        }
        if (imageLayout.findViewWithTag(tagId) == null) {
            RoundImage roundImage = new RoundImage(context, null);
            roundImage.setTag(tagId);
            roundImage.setSize(imageSize);
            roundImage.setPadding(8, 8, 8, 8);
            roundImage.setImageBitmap(bitmap);
            imageLayout.addView(roundImage);

        }
    }

    public void removeAll() {
        imageLayout.removeAllViews();
        imageLayout.addView(placeholder);
        placeholder.setVisibility(VISIBLE);
    }

}
