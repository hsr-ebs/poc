package ch.hsr.ebos.app.ui.project;


import android.view.View;

import java.util.UUID;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissDetailFragment;
import ch.hsr.ebos.app.ui.component.SwitchToggle;
import ch.hsr.ebos.app.ui.component.TextInput;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.app.ui.util.Validation;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.StateChangeEvent;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.project.ProjectState;
import ch.hsr.ebos.offliss.common.project.close.CloseProjectEvent;
import ch.hsr.ebos.offliss.common.project.create.CreateProjectEvent;
import ch.hsr.ebos.offliss.common.project.open.OpenProjectEvent;
import ch.hsr.ebos.offliss.common.project.update.UpdateProjectEvent;

public class ProjectDetail extends OfflissDetailFragment<ProjectEntity> {

    private AionClientModule clientModule;
    private TextInput name;
    private SwitchToggle isActive;

    public ProjectDetail() {
        super(R.layout.fragment_project_detail, "project");
    }

    @Override
    protected void setupView(View view) {
        clientModule = AppUtil.getApplication(getContext()).getProjectModule();
        name = view.findViewById(R.id.projectName);
        isActive = view.findViewById(R.id.projectActive);

        if (entity == null) {
            isActive.setVisibility(View.GONE);
            isActive.setChecked(true);
        } else {
            isActive.setChecked(entity.getState() == ProjectState.ACTIVE);
            name.setText(entity.getName());
        }
    }

    @Override
    public boolean isValid() {
        return Validation.isRequiredValid(name);
    }

    @Override
    public void save() {
        ProjectEntity savedProject = new ProjectEntity();
        savedProject.setId(entity == null ? UUID.randomUUID().toString() : entity.getId());
        savedProject.setName(name.getText());
        savedProject.setState(isActive.isChecked() ? ProjectState.ACTIVE : ProjectState.INACTIVE);

        if (entity == null) {
            CreateProjectEvent event = new CreateProjectEvent();
            event.setId(savedProject.getId());
            event.setName(savedProject.getName());
            AppUtil.emitEvent(getContext(), clientModule, event);
        } else {
            if (!savedProject.getName().equals(entity.getName())) {
                UpdateProjectEvent event = new UpdateProjectEvent();
                event.setId(savedProject.getId());
                event.setName(new ChangeSet<>(entity.getName(), savedProject.getName()));
                AppUtil.emitEvent(getContext(), clientModule, event);
            }
            if (savedProject.getState() != entity.getState()) {
                StateChangeEvent event;
                if (ProjectState.ACTIVE == savedProject.getState()) {
                    event = new OpenProjectEvent();
                } else {
                    event = new CloseProjectEvent();
                }
                event.setName(savedProject.getName());
                event.setId(savedProject.getId());
                AppUtil.emitEvent(getContext(), clientModule, event);
            }
        }
    }
}
