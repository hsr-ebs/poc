package ch.hsr.ebos.app.ui.component;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import ch.hsr.ebos.aion.android.adapter.EntityListAdapter;
import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.app.R;

public class SearchableDialog {

    private Dialog dialog;
    private ListView list;
    private OnMultiSelectApplyListener onMultiSelectApplyListener;
    private OnSingleSelectApplyListener onSingleSelectApplyListener;
    private Set<Entity> selectedEntities;
    private boolean isMultiSelect;

    public SearchableDialog(Context context, String titleText, EntityListAdapter adapter,
                            boolean isMultiSelect, int maxItemsNumber) {
        this.isMultiSelect = isMultiSelect;
        selectedEntities = new HashSet<>();

        dialog = new Dialog(context);
        dialog.setContentView(R.layout.component_searchable_dialog);

        TextView title = dialog.findViewById(R.id.dialogTitle);
        title.setText(titleText);

        TextView maxItems = dialog.findViewById(R.id.maxItems);
        maxItems.setText(String.format(Locale.ENGLISH, "Max. %d items", maxItemsNumber));


        EditText searchInput = dialog.findViewById(R.id.searchInput);
        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
                // do nothing
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // do nothing
            }
        });

        list = dialog.findViewById(R.id.items);
        list.setAdapter(adapter);

        Button dialogCancel = dialog.findViewById(R.id.dialogCancel);
        dialogCancel.setOnClickListener(view -> dialog.dismiss());
        Button dialogOk = dialog.findViewById(R.id.dialogOk);

        if (isMultiSelect) {
            list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            dialogOk.setOnClickListener(view -> {
                if (maxItemsNumber < list.getCheckedItemCount()) {
                    maxItems.setTextColor(ContextCompat.getColor(context, R.color.colorTextError));
                } else {
                    dialog.dismiss();
                    selectedEntities.clear();
                    int numberOfElements = list.getCount();
                    SparseBooleanArray sparseBooleanArray = list.getCheckedItemPositions();
                    for (int i = 0; i < numberOfElements; i++) {
                        if (sparseBooleanArray.get(i)) {
                            Entity entity = (Entity) list.getItemAtPosition(i);
                            selectedEntities.add(entity);
                        }
                    }
                    onMultiSelectApplyListener.onApply(selectedEntities);
                }
            });
        } else {
            dialogOk.setVisibility(View.GONE);
            list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            list.setOnItemClickListener((adapterView, view, position, id) -> {
                if (maxItemsNumber < list.getCheckedItemCount()) {
                    maxItems.setTextColor(ContextCompat.getColor(context, R.color.colorTextError));
                } else {
                    dialog.dismiss();
                    list.setSelection(position);
                    Entity entity = (Entity) adapterView.getItemAtPosition(position);
                    onSingleSelectApplyListener.onApply(entity);
                }
            });
        }
    }

    public void setSelectedEntity(Entity entity) {
        EntityListAdapter adapter = (EntityListAdapter) list.getAdapter();
        if (isMultiSelect) {
            list.setItemChecked(adapter.getPosition(entity), true);
        } else {
            list.setSelection(adapter.getPosition(entity));
        }
    }

    public void show() {
        dialog.show();
    }

    public Set<Entity> getSelectedEntities() {
        return selectedEntities;
    }

    public void setOnMultiSelectApplyListener(OnMultiSelectApplyListener onMultiSelectApplyListener) {
        this.onMultiSelectApplyListener = onMultiSelectApplyListener;
    }

    public void setOnSingleSelectApplyListener(OnSingleSelectApplyListener onSingleSelectApplyListener) {
        this.onSingleSelectApplyListener = onSingleSelectApplyListener;
    }
}
