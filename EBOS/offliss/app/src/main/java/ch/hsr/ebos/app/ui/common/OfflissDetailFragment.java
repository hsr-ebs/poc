package ch.hsr.ebos.app.ui.common;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import ch.hsr.ebos.app.R;

public abstract class OfflissDetailFragment<T> extends OfflissFragment {

    protected T entity;
    private String title;

    public OfflissDetailFragment(int resource, String title) {
        super(resource, true);
        this.title = title;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    @Override
    public String getToolbarTitle() {
        if (entity == null) {
            return "Create " + title;
        } else {
            return "Edit " + title;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu_detail, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (R.id.save == item.getItemId()) {
            if (isValid()) {
                save();
                getFragmentManager().popBackStack();
            }
            return true;
        }
        return false;
    }


    public abstract void save();

    public abstract boolean isValid();
}
