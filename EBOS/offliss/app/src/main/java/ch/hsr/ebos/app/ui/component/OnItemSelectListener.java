package ch.hsr.ebos.app.ui.component;

public interface OnItemSelectListener {
    void onSelect(Object entity);
}
