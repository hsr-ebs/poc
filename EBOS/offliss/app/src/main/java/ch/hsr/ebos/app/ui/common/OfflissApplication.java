package ch.hsr.ebos.app.ui.common;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import net.openid.appauth.AuthState;

import java.util.Collection;

import ch.hsr.ebos.aion.android.AionClient;
import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.AionClientSqliteOptions;
import ch.hsr.ebos.aion.android.ApplicationState;
import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;
import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.aion.data.sqlite.state.SqliteDataModule;
import ch.hsr.ebos.offliss.common.OfflissModule;
import ch.hsr.ebos.offliss.common.issue.IssueModule;
import ch.hsr.ebos.offliss.common.project.ProjectModule;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingModule;
import ch.hsr.ebos.offliss.common.user.UserModule;

public class OfflissApplication extends android.app.Application {

    public static final String User = "user";
    public static final String Issue = "issue";
    public static final String Project = "project";
    public static final String TimeBooking = "timebooking-";

    private String dataDir;
    private AionClientSqliteOptions clientOptions;
    private AionClient client = null;

    private AionClientModule userModule;
    private AionClientModule issueModule;
    private AionClientModule projectModule;
    private AionClientModule timebookingModule;

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            ApplicationInfo applicationInfo = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            String apiUrl = applicationInfo.metaData.getString("api_url");

            dataDir = applicationInfo.dataDir;
            clientOptions = new AionClientSqliteOptions(getApplicationContext(), dataDir, apiUrl);
            client = new AionClient(getApplicationContext(), clientOptions);

            if (getApplicationState().isAuthorized()) {
                registerModules();
                client.startSynchronization();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAuthState(AuthState authState) throws Exception {
        if (authState == null) {
            clientOptions.getApplicationState().setAuthState(null);
            client.removeModules();
            client.stopSynchronization();
        } else {
            clientOptions.getApplicationState().setAuthState(authState);
            registerModules();
            synchronize();
            client.startSynchronization();
        }
    }

    private void registerModules() throws Exception {
        if (getEventAuth().getRoles().contains("user_read")) {
            userModule = addOfflissModule(User, new UserModule());
        } else {
            userModule = null;
        }
        if (getEventAuth().getRoles().contains("issue_read")) {
            issueModule = addOfflissModule(Issue, new IssueModule());
        } else {
            issueModule = null;
        }
        if (getEventAuth().getRoles().contains("project_read")) {
            projectModule = addOfflissModule(Project, new ProjectModule());
        } else {
            projectModule = null;
        }
        if (getEventAuth().getRoles().contains("timebooking-" + getCurrentUserId() + "_read")) {
            timebookingModule = addOfflissModule(TimeBooking + getCurrentUserId(), new TimeBookingModule());
        } else {
            timebookingModule = null;
        }
    }

    private AionClientModule addOfflissModule(String timelineName, OfflissModule module) throws Exception {
        DataModule dataModule = new SqliteDataModule(clientOptions.getConnectionFactory(),
                dataDir + "/" + getCurrentUserId() + "-" + timelineName + "-state.db", true);
        module.setDataModule(dataModule);
        return client.addModule(timelineName, getCurrentUserId() + "-" + timelineName, module);
    }

    public boolean synchronize() {
        boolean result = this.client.synchronize();
        if (result) {
            getApplicationState().setLastSyncDate();
        }
        return result;
    }

    public String getCurrentUserId() {
        return this.getEventAuth().getUserId();
    }

    public TimelineEventAuth getEventAuth() {
        return getApplicationState().getTimelineEventAuth();
    }

    public ApplicationState getApplicationState() {
        return this.clientOptions.getApplicationState();
    }

    public AionClientModule getUserModule() {
        return userModule;
    }

    public AionClientModule getIssueModule() {
        return issueModule;
    }

    public AionClientModule getProjectModule() {
        return projectModule;
    }

    public AionClientModule getTimebookingModule() {
        return timebookingModule;
    }

    public Collection<AionClientModule> getModules() {
        return client.getModules();
    }
}
