package ch.hsr.ebos.app.ui.project;

import android.app.Activity;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.adapter.EntityListAdapter;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.orm.query.QueryOrder;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.project.ProjectState;

public class ActiveProjectListAdapter extends EntityListAdapter {
    public ActiveProjectListAdapter(Activity activity, AionClientModule clientModule) {
        super(activity, clientModule, ProjectEntity.class, Query.build()
                        .and(Filter.equals("state", ProjectState.ACTIVE))
                        .orderBy(QueryOrder.asc("name", false)),
                android.R.layout.simple_list_item_1);
    }
}
