package ch.hsr.ebos.app.ui.common;

import ch.hsr.ebos.aion.android.adapter.EntityRecyclerListAdapter;
import ch.hsr.ebos.app.R;

public abstract class OfflissOverviewFragment<T extends EntityRecyclerListAdapter> extends OfflissFragment {
    protected T listAdapter;
    private String title;

    public OfflissOverviewFragment(int resource, String title) {
        super(resource, false);
        this.title = title;
    }

    @Override
    public String getToolbarTitle() {
        return title;
    }

    protected void openDetail(OfflissFragment fragment) {
        getActivity()
                .getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment, fragment.getToolbarTitle())
                .addToBackStack(fragment.getToolbarTitle())
                .commit();
    }

    @Override
    public void onDestroy() {
        if (listAdapter != null) {
            listAdapter.close();
        }
        super.onDestroy();
    }
}
