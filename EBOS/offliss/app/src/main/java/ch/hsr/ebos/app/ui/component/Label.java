package ch.hsr.ebos.app.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import ch.hsr.ebos.app.R;

public class Label extends LinearLayout {

    private TextView label;
    private TextView text;

    public Label(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);

        String labelText = null;
        int id = 0;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Component);
        if (typedArray != null) {
            labelText = typedArray.getString(R.styleable.Component_label);
            id = typedArray.getInt(R.styleable.Component_android_id, 0);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.component_label, this, true);
        }

        label = (TextView) getChildAt(0);
        if (labelText == null) {
            label.setVisibility(GONE);
        } else {
            label.setText(labelText);
        }

        text = (TextView) getChildAt(1);
        text.setId(id);
    }

    public String getText() {
        return text.getText().toString();
    }

    public void setText(String input) {
        text.setText(input);
    }

    public void setLabelText(String labelText) {
        if (labelText == null) {
            label.setVisibility(GONE);
        } else {
            label.setText(labelText);
            label.setVisibility(VISIBLE);
        }
    }

    public void setError() {
        text.setTextColor(ContextCompat.getColor(getContext(), R.color.colorTextError));
    }

}
