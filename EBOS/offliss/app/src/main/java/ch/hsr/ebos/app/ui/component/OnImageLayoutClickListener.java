package ch.hsr.ebos.app.ui.component;

public interface OnImageLayoutClickListener {
    void onClick();
}
