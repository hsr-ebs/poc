package ch.hsr.ebos.app.ui.issue;

import android.support.design.widget.FloatingActionButton;
import android.view.View;

import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissOverviewFragment;
import ch.hsr.ebos.app.ui.component.List;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;

public class IssueOverview extends OfflissOverviewFragment<IssueListAdapter> {

    public IssueOverview() {
        super(R.layout.fragment_issue_overview, "Issues");
    }

    @Override
    protected void setupView(View view) {
        final List list = view.findViewById(R.id.issueList);
        final FloatingActionButton addUserButton = view.findViewById(R.id.addIssue);

        listAdapter = new IssueListAdapter(getActivity(), AppUtil.getApplication(getContext()).getIssueModule());
        listAdapter.setSelectListener((IssueEntity entity) -> {
            IssueDetail detail = new IssueDetail();
            detail.setEntity(entity);
            openDetail(detail);
        });
        list.setAdapter(listAdapter);

        addUserButton.setOnClickListener(clickedView -> openDetail(new IssueDetail()));
    }
}
