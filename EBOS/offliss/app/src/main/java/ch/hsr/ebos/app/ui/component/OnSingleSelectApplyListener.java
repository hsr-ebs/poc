package ch.hsr.ebos.app.ui.component;

import ch.hsr.ebos.aion.data.orm.Entity;

public interface OnSingleSelectApplyListener {
    void onApply(Entity selectEntity);
}
