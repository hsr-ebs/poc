package ch.hsr.ebos.app.ui.project;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.adapter.EntityRecyclerListAdapter;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.orm.query.QueryOrder;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueState;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.project.ProjectState;

public class ProjectListAdapter extends EntityRecyclerListAdapter<ProjectEntity,
        ProjectListAdapter.ProjectViewHolder> {

    public ProjectListAdapter(Activity activity, AionClientModule clientModule, Query query) {
        super(activity, R.layout.row_project, clientModule, ProjectEntity.class, query);
    }

    public ProjectListAdapter(Activity activity, AionClientModule clientModule) {
        this(activity, clientModule, Query.build().orderBy(QueryOrder.asc("name", false)));
    }

    @Override
    protected ProjectViewHolder createViewHolder(View view) {
        return new ProjectViewHolder(view);
    }

    @Override
    protected void bindView(ProjectEntity entity, ProjectViewHolder viewHolder) {
        int projectStateImageId;
        if (entity.getState() == ProjectState.INACTIVE) {
            projectStateImageId = R.drawable.red_dot;
        } else {
            projectStateImageId = R.drawable.green_dot;
        }

        viewHolder.image.setImageResource(projectStateImageId);
        viewHolder.name.setText(entity.getName());
        viewHolder.openIssues.setText(String.format(Locale.GERMAN, "Open Issues: %d",
                getNumberOfOpenIssues(entity.getId())));
    }

    private long getNumberOfOpenIssues(String projectId) {
        EntityService entityService = AppUtil.getApplication(activity).getIssueModule().getEntityService();
        Query countIssues = Query.build().and(Filter.equals("projectId", projectId),
                Filter.equals("state", IssueState.OPEN));

        long openIssues = 0;
        try {
            openIssues = entityService.count(IssueEntity.class, countIssues);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return openIssues;
    }

    class ProjectViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        TextView openIssues;

        ProjectViewHolder(View view) {
            super(view);

            image = view.findViewById(R.id.projectRowState);
            name = view.findViewById(R.id.projectRowName);
            openIssues = view.findViewById(R.id.projectRowIssues);
        }
    }
}
