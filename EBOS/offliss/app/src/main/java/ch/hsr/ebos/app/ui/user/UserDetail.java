package ch.hsr.ebos.app.ui.user;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissDetailFragment;
import ch.hsr.ebos.app.ui.component.Label;
import ch.hsr.ebos.app.ui.component.SwitchToggle;
import ch.hsr.ebos.app.ui.component.TextInput;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.app.ui.util.AvatarUtil;
import ch.hsr.ebos.app.ui.util.Validation;
import ch.hsr.ebos.offliss.common.ChangeSet;
import ch.hsr.ebos.offliss.common.StateChangeEvent;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.UserRole;
import ch.hsr.ebos.offliss.common.user.UserState;
import ch.hsr.ebos.offliss.common.user.activate.ActivateUserEvent;
import ch.hsr.ebos.offliss.common.user.create.CreateUserEvent;
import ch.hsr.ebos.offliss.common.user.inactivate.InactivateUserEvent;
import ch.hsr.ebos.offliss.common.user.reset.ResetUserPasswordEvent;
import ch.hsr.ebos.offliss.common.user.update.UpdateUserEvent;

public class UserDetail extends OfflissDetailFragment<UserEntity> {

    private AionClientModule clientModule;
    private ImageView avatar;
    private TextInput chooseUsername;
    private Label username;
    private TextInput fullName;
    private TextInput email;
    private RadioGroup userRole;
    private RadioButton administrator;
    private RadioButton projectManager;
    private RadioButton developer;
    private SwitchToggle isActive;

    public UserDetail() {
        super(R.layout.fragment_user_detail, "entity");
    }

    @Override
    protected void setupView(View view) {
        clientModule = AppUtil.getApplication(getContext()).getUserModule();
        avatar = view.findViewById(R.id.userAvatar);
        chooseUsername = view.findViewById(R.id.userChooseUsername);
        username = view.findViewById(R.id.userUsername);
        fullName = view.findViewById(R.id.fullName);
        email = view.findViewById(R.id.userEmail);
        userRole = view.findViewById(R.id.userRole);
        administrator = view.findViewById(R.id.userAdministrator);
        projectManager = view.findViewById(R.id.userProjectManager);
        developer = view.findViewById(R.id.userDeveloper);
        isActive = view.findViewById(R.id.userActive);
        Button generatePassword = view.findViewById(R.id.generatePassword);

        avatar.setOnClickListener(v -> setRandomAvatar(avatar));
        generatePassword.setOnClickListener(v -> {
            ResetUserPasswordEvent event = new ResetUserPasswordEvent();
            event.setId(entity.getId());
            event.setUsername(entity.getUsername());
            AppUtil.emitEvent(getContext(), clientModule, event);

            Toast toast = Toast.makeText(getContext(), "Sent password reset", Toast.LENGTH_SHORT);
            toast.show();
        });

        if (entity == null) {
            chooseUsername.setVisibility(View.VISIBLE);
            isActive.setVisibility(View.GONE);
            username.setVisibility(View.GONE);
            generatePassword.setVisibility(View.GONE);
            isActive.setChecked(true);
            developer.setChecked(true);
            setRandomAvatar(avatar);
        } else {
            chooseUsername.setVisibility(View.GONE);
            username.setVisibility(View.VISIBLE);
            username.setText(entity.getUsername());
            fullName.setText(entity.getFullName());
            email.setText(entity.getEmail());
            Bitmap avatarBitmap = AvatarUtil.getBitmap(entity.getAvatar());
            avatar.setImageBitmap(avatarBitmap);
            avatar.setBackgroundColor(AvatarUtil.getBackgroundColor(avatarBitmap));
            isActive.setVisibility(View.VISIBLE);
            isActive.setChecked(entity.getState() == UserState.ACTIVE);
            generatePassword.setVisibility(View.VISIBLE);
            switch (entity.getRole()) {
                case DEVELOPER:
                    developer.setChecked(true);
                    break;
                case ADMINISTRATOR:
                    administrator.setChecked(true);
                    break;
                case PROJECT_MANAGER:
                    projectManager.setChecked(true);
                    break;
            }
        }
    }

    private void setRandomAvatar(ImageView avatarImage) {
        try {
            Bitmap avatar = AvatarUtil.generateRandom(getActivity().getAssets(), 250);
            avatarImage.setImageBitmap(avatar);
            avatarImage.setBackgroundColor(AvatarUtil.getBackgroundColor(avatar));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isValid() {
        boolean isValid = entity != null || Validation.isRequiredValid(chooseUsername);
        isValid = Validation.isRequiredValid(fullName) && isValid;
        isValid = Validation.isEmailValid(email) && isValid;
        isValid = Validation.isRequiredValid(email) && isValid;
        isValid = Validation.isRequiredValid(userRole) && isValid;
        return isValid;
    }

    @Override
    public void save() {
        UserEntity savedUser = new UserEntity();
        savedUser.setId(entity == null ? UUID.randomUUID().toString() : entity.getId());
        savedUser.setUsername(entity == null ? chooseUsername.getText() : username.getText());
        savedUser.setFullName(fullName.getText());
        savedUser.setEmail(email.getText());
        savedUser.setState(isActive.isChecked() ? UserState.ACTIVE : UserState.INACTIVE);
        savedUser.setAvatar(AvatarUtil.convertToBase64(((BitmapDrawable) avatar.getDrawable()).getBitmap()));
        if (administrator.isChecked()) {
            savedUser.setRole(UserRole.ADMINISTRATOR);
        } else if (projectManager.isChecked()) {
            savedUser.setRole(UserRole.PROJECT_MANAGER);
        } else {
            savedUser.setRole(UserRole.DEVELOPER);
        }

        if (entity == null) {
            CreateUserEvent event = new CreateUserEvent();
            event.setId(savedUser.getId());
            event.setUsername(savedUser.getUsername());
            event.setFullName(savedUser.getFullName());
            event.setEmail(savedUser.getEmail());
            event.setAvatar(savedUser.getAvatar());
            event.setRole(savedUser.getRole());
            AppUtil.emitEvent(getContext(), clientModule, event);
        } else {
            if (!savedUser.getFullName().equals(entity.getFullName()) ||
                    !savedUser.getEmail().equals(entity.getEmail()) ||
                    !savedUser.getAvatar().equals(entity.getAvatar()) ||
                    !savedUser.getRole().equals(entity.getRole())) {
                UpdateUserEvent event = new UpdateUserEvent();
                event.setId(savedUser.getId());
                event.setUsername(savedUser.getUsername());
                event.setFullName(new ChangeSet<>(entity.getFullName(), savedUser.getFullName()));
                event.setEmail(new ChangeSet<>(entity.getEmail(), savedUser.getEmail()));
                event.setAvatar(new ChangeSet<>(entity.getAvatar(), savedUser.getAvatar()));
                event.setRole(new ChangeSet<>(entity.getRole(), savedUser.getRole()));
                AppUtil.emitEvent(getContext(), clientModule, event);
            }
            if (savedUser.getState() != entity.getState()) {
                StateChangeEvent event;
                if (savedUser.getState() == UserState.ACTIVE) {
                    event = new ActivateUserEvent();
                } else {
                    event = new InactivateUserEvent();
                }
                event.setName(entity.getUsername());
                event.setId(savedUser.getId());
                AppUtil.emitEvent(getContext(), clientModule, event);
            }
        }
    }

}
