package ch.hsr.ebos.app.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import ch.hsr.ebos.app.R;

public class SwitchToggle extends LinearLayout {

    private Switch switchComponent;

    public SwitchToggle(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);

        String labelText = "";
        int id = 0;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Component);
        if (typedArray != null) {
            labelText = typedArray.getString(R.styleable.Component_label);
            id = typedArray.getInt(R.styleable.Component_android_id, 0);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.component_switch, this, true);

        TextView label = (TextView) getChildAt(0);
        label.setText(labelText);

        switchComponent = (Switch) getChildAt(1);
        switchComponent.setId(id);
        switchComponent.setOnFocusChangeListener((view, focused) -> {
            if (focused)
                label.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            else
                label.setTextColor(ContextCompat.getColor(context, R.color.colorText));
        });
    }

    public boolean isChecked() {
        return switchComponent.isChecked();
    }

    public void setChecked(boolean checked) {
        switchComponent.setChecked(checked);
    }

}
