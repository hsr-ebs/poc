package ch.hsr.ebos.app.ui.user;

import android.app.Activity;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.adapter.EntityListAdapter;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.orm.query.QueryOrder;
import ch.hsr.ebos.offliss.common.user.UserEntity;
import ch.hsr.ebos.offliss.common.user.UserState;

public class ActiveUserListAdapter extends EntityListAdapter {
    public ActiveUserListAdapter(Activity activity, AionClientModule clientModule) {
        super(activity, clientModule, UserEntity.class, Query.build()
                        .and(Filter.equals("state", UserState.ACTIVE))
                        .orderBy(QueryOrder.asc("fullName", false)),
                android.R.layout.simple_list_item_multiple_choice);
    }
}
