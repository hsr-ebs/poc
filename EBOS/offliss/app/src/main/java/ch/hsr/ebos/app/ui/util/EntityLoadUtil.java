package ch.hsr.ebos.app.ui.util;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueMemberEntity;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;
import ch.hsr.ebos.offliss.common.user.UserEntity;

public class EntityLoadUtil {

    public static IssueEntity getIssue(Context context, String issueId) {
        EntityService entityService = AppUtil.getApplication(context).getIssueModule().getEntityService();
        Query getIssue = Query.build().and(Filter.equals("id", issueId));

        try {
            List<IssueEntity> issues = entityService.query(IssueEntity.class, getIssue);
            if (!issues.isEmpty()) {
                return issues.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ProjectEntity getProject(Context context, String projectId) {
        EntityService entityService = AppUtil.getApplication(context).getProjectModule().getEntityService();
        Query getIssue = Query.build().and(Filter.equals("id", projectId));

        try {
            List<ProjectEntity> projects = entityService.query(ProjectEntity.class, getIssue);
            if (!projects.isEmpty()) {
                return projects.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<UserEntity> getIssueMember(Context context, String issueId) {
        EntityService entityService = AppUtil.getApplication(context).getIssueModule().getEntityService();
        Query getMember = Query.build().and(Filter.equals("issueId", issueId));
        try {
            List<IssueMemberEntity> members = entityService.query(IssueMemberEntity.class, getMember);
            List<UserEntity> users = new ArrayList<>();
            for (IssueMemberEntity member : members) {
                users.add(getUserById(context, member.getUserId()));
            }
            return users;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static UserEntity getUserById(Context context, String userId) {
        Query query = Query.build().and(Filter.equals("id", userId));
        return getUser(context, query);
    }

    private static UserEntity getUser(Context context, Query query) {
        EntityService entityService = AppUtil.getApplication(context).getUserModule().getEntityService();
        try {
            List<UserEntity> users = entityService.query(UserEntity.class, query);
            if (!users.isEmpty()) {
                return users.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
