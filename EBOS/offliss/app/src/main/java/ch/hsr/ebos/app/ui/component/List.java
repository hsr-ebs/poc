package ch.hsr.ebos.app.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ch.hsr.ebos.app.R;

public class List extends LinearLayout {

    private RecyclerView list;
    private LinearLayout emptyListLayout;
    private TextView emptyText;
    private TextView titleText;

    public List(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);

        String resourceName = "";

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.EmptyList);
        if (typedArray != null) {
            resourceName = typedArray.getString(R.styleable.EmptyList_resource_name);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.component_list, this, true);
        }

        list = (RecyclerView) getChildAt(0);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        emptyListLayout = (LinearLayout) getChildAt(1);
        emptyText = (TextView) emptyListLayout.getChildAt(0);
        emptyText.setText("No " + resourceName + " found!");

        titleText = (TextView) emptyListLayout.getChildAt(1);
    }

    public void setTitle(String text) {
        emptyText.setText(text);
    }

    public void setHelperText(String text) {
        titleText.setText(text);
    }


    public void setAdapter(RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter) {
        try {
            if (adapter.getItemCount() == 0) {
                list.setVisibility(View.GONE);
                emptyListLayout.setVisibility(View.VISIBLE);
            } else {
                list.setAdapter(adapter);
                list.setVisibility(View.VISIBLE);
                emptyListLayout.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setItemTouchHelper(ItemTouchHelper itemTouchHelper) {
        itemTouchHelper.attachToRecyclerView(list);
    }
}
