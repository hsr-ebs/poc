package ch.hsr.ebos.app.ui.timebooking;


import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.MainActivity;
import ch.hsr.ebos.app.ui.common.OfflissOverviewFragment;
import ch.hsr.ebos.app.ui.component.List;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.offliss.common.timebooking.TimeBookingEntity;
import ch.hsr.ebos.offliss.common.timebooking.remove.RemoveTimeBookingEvent;


public class TimeBookingOverview extends OfflissOverviewFragment<TimeBookingListAdapter> {
    private AionClientModule clientModule;

    public TimeBookingOverview() {
        super(R.layout.fragment_time_booking_overview, "Time bookings");
    }

    @Override
    protected void setupView(View view) {
        clientModule = AppUtil.getApplication(getContext()).getTimebookingModule();

        final List list = view.findViewById(R.id.timeBookingList);
        final FloatingActionButton addTimeBooking = view.findViewById(R.id.addTimeBooking);

        MainActivity activity = (MainActivity) getActivity();
        listAdapter = new TimeBookingListAdapter(activity, clientModule);
        listAdapter.setSelectListener((TimeBookingEntity entity) -> {
            TimeBookingDetail detail = new TimeBookingDetail();
            detail.setEntity(entity);
            openDetail(detail);
        });
        list.setAdapter(listAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        int position = viewHolder.getAdapterPosition();

                        TimeBookingEntity timeBooking = listAdapter.getItem(position);
                        RemoveTimeBookingEvent event = new RemoveTimeBookingEvent();
                        event.setId(timeBooking.getId());
                        AppUtil.emitEvent(getContext(), clientModule, event);

                        listAdapter.deleteItem(position);
                    }
                });
        list.setItemTouchHelper(itemTouchHelper);

        addTimeBooking.setOnClickListener(clickedView -> openDetail(new TimeBookingDetail()));
    }
}
