package ch.hsr.ebos.app.ui.synchronization;

import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissApplication;
import ch.hsr.ebos.app.ui.common.OfflissOverviewFragment;
import ch.hsr.ebos.app.ui.component.List;
import ch.hsr.ebos.offliss.common.FormatUtils;

public class SynchronizationOverview extends OfflissOverviewFragment {
    private OfflissApplication application;

    public SynchronizationOverview() {
        super(R.layout.fragment_synchronization_overview, "Synchronization");
    }

    @Override
    protected void setupView(View view) {
        application = ((OfflissApplication) getActivity().getApplicationContext());

        final List list = view.findViewById(R.id.eventList);
        final TextView syncDate = view.findViewById(R.id.syncDate);
        final FloatingActionButton syncButton = view.findViewById(R.id.syncTimelines);

        list.setAdapter(new EventListAdapter(getContext()));
        syncDate.setText("Last synchronization: " + FormatUtils.formatFullDate(application.getApplicationState().getLastSyncDate()));

        list.setTitle("All events are synchronized");
        list.setHelperText("No failed or pending events");

        syncButton.setOnClickListener(clickedView -> {
            boolean success = application.synchronize();

            if (success) {
                Toast toast = Toast.makeText(getContext(), "Sync successfull", Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Toast toast = Toast.makeText(getContext(), "Sync failed", Toast.LENGTH_SHORT);
                toast.show();
            }

            list.setAdapter(new EventListAdapter(getContext()));
        });
    }
}
