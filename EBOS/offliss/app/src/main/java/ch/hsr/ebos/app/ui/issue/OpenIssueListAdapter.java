package ch.hsr.ebos.app.ui.issue;

import android.app.Activity;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.adapter.EntityListAdapter;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.orm.query.QueryOrder;
import ch.hsr.ebos.offliss.common.issue.IssueEntity;
import ch.hsr.ebos.offliss.common.issue.IssueState;

public class OpenIssueListAdapter extends EntityListAdapter {
    public OpenIssueListAdapter(Activity activity, AionClientModule clientModule) {
        super(activity, clientModule, IssueEntity.class, Query.build()
                        .and(Filter.equals("state", IssueState.OPEN))
                        .orderBy(QueryOrder.asc("number"), QueryOrder.asc("title", false)),
                android.R.layout.simple_list_item_1);
    }
}
