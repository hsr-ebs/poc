package ch.hsr.ebos.app.ui.component;

import java.util.Set;

import ch.hsr.ebos.aion.data.orm.Entity;

public interface OnMultiSelectApplyListener {
    void onApply(Set<Entity> selectedEntities);
}
