package ch.hsr.ebos.app.ui.component;

import java.util.Date;

public interface OnDateTimeSelectListener {
    void onSelect(Date dateTime);
}
