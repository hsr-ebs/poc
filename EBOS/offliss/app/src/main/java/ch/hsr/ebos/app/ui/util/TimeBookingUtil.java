package ch.hsr.ebos.app.ui.util;

import java.util.Date;

public class TimeBookingUtil {
    public static double getDuration(Date start, Date stop) {
        double duration = 0.0;
        if (start != null && stop != null) {
            duration = (stop.getTime() - start.getTime()) / 3600000.0;
        }
        return duration;
    }

    public static String getDurationText(Date start, Date stop) {
        double duration = getDuration(start, stop);
        return String.format("%.2f hours", duration);
    }

}
