package ch.hsr.ebos.app.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import ch.hsr.ebos.app.R;

public class TextInput extends LinearLayout {
    private EditText textInput;

    public TextInput(Context context, AttributeSet attrs) {
        super(context, attrs);

        String labelText = "";
        int id = 0;
        int inputType = EditorInfo.TYPE_CLASS_TEXT;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Component);
        if (typedArray != null) {
            labelText = typedArray.getString(R.styleable.Component_label);
            id = typedArray.getInt(R.styleable.Component_android_id, 0);
            inputType = typedArray.getInt(R.styleable.Component_android_inputType, EditorInfo.TYPE_CLASS_TEXT);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.component_text_input, this, true);

        TextInputLayout textInputLayout = (TextInputLayout) getChildAt(0);
        textInputLayout.setHint(labelText);

        textInput = textInputLayout.getEditText();
        textInput.setId(id);
        textInput.setInputType(inputType);
        textInput.setOnFocusChangeListener((view, focused) -> {
            InputMethodManager keyboard = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (focused)
                keyboard.showSoftInput(textInput, 0);
            else
                keyboard.hideSoftInputFromWindow(textInput.getWindowToken(), 0);
        });
    }

    public String getText() {
        return textInput.getText().toString();
    }

    public void setText(String value) {
        textInput.setText(value);
    }

    public void setError(String error) {
        textInput.setError(error);
    }

}
