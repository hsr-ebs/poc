package ch.hsr.ebos.app.ui.common;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ch.hsr.ebos.app.R;

public abstract class OfflissFragment extends Fragment {
    private int resource;
    private boolean navigatingBack;

    public OfflissFragment(int resource, boolean navigatingBack) {
        this.resource = resource;
        this.navigatingBack = navigatingBack;
    }

    protected abstract void setupView(View view);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(resource, container, false);
        setupView(rootView);
        setHasOptionsMenu(true);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getToolbarTitle());
            if (navigatingBack) {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_back_button);
            } else {
                actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            }
        }
        return rootView;
    }

    public boolean isNavigatingBack() {
        return navigatingBack;
    }

    public abstract String getToolbarTitle();
}
