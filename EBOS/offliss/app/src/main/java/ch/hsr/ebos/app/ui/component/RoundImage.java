package ch.hsr.ebos.app.ui.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import ch.hsr.ebos.app.R;

public class RoundImage extends LinearLayout {

    private CardView cardView;
    private ImageView roundImage;

    public RoundImage(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);

        String description = "";
        int source = 0;
        int size = 84;

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RoundImage);
        if (typedArray != null) {
            description = typedArray.getString(R.styleable.RoundImage_android_contentDescription);
            source = typedArray.getInt(R.styleable.RoundImage_android_src, 0);
            size = (int) typedArray.getDimension(R.styleable.RoundImage_image_size, 84);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.component_round_image, this, true);

        cardView = (CardView) getChildAt(0);
        roundImage = (ImageView) cardView.getChildAt(0);
        roundImage.setContentDescription(description);
        if (source != 0) {
            roundImage.setImageResource(source);
        }
        setSize(size);
    }

    public void setImageResource(int resourceId) {
        roundImage.setImageResource(resourceId);
    }

    public void setImageBitmap(Bitmap bitmap) {
        roundImage.setImageBitmap(bitmap);
    }

    public void setSize(int size) {
        cardView.setLayoutParams(new LayoutParams(size, size));
        cardView.setRadius(size / 2);
    }

}
