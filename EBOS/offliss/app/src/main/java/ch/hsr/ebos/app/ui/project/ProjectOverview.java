package ch.hsr.ebos.app.ui.project;

import android.support.design.widget.FloatingActionButton;
import android.view.View;

import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.common.OfflissOverviewFragment;
import ch.hsr.ebos.app.ui.component.List;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.offliss.common.project.ProjectEntity;


public class ProjectOverview extends OfflissOverviewFragment<ProjectListAdapter> {

    public ProjectOverview() {
        super(R.layout.fragment_project_overview, "Projects");
    }

    @Override
    protected void setupView(View view) {
        final List list = view.findViewById(R.id.projectList);
        final FloatingActionButton addProjectButton = view.findViewById(R.id.addProject);

        listAdapter = new ProjectListAdapter(getActivity(), AppUtil.getApplication(getContext()).getProjectModule());
        listAdapter.setSelectListener((ProjectEntity entity) -> {
            ProjectDetail detail = new ProjectDetail();
            detail.setEntity(entity);
            openDetail(detail);
        });
        list.setAdapter(listAdapter);

        addProjectButton.setOnClickListener(clickedView -> {
            ProjectDetail detail = new ProjectDetail();
            openDetail(detail);
        });
    }

}
