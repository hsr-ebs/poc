package ch.hsr.ebos.app.ui.util;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;

public class AvatarUtil {
    private static final String AssetEyePath = "avatar/eyes";
    private static final String AssetNosePath = "avatar/nose";
    private static final String AssetMouthPath = "avatar/mouth";
    private static final SecureRandom random = new SecureRandom();

    public static Bitmap getBitmap(String encodedImage) {
        if (encodedImage == null) {
            return null;
        }

        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    private static <T> T randomItem(T[] array) {
        return array[random.nextInt(array.length)];
    }

    public static int getBackgroundColor(Bitmap bitmapAvatar) {
        if (bitmapAvatar == null) {
            return Color.BLACK;
        }
        return bitmapAvatar.getPixel(0, 0);
    }

    public static Bitmap generateRandom(AssetManager assetManager, int size) throws IOException {
        String[] eyes = assetManager.list(AssetEyePath);
        String[] noses = assetManager.list(AssetNosePath);
        String[] mouths = assetManager.list(AssetMouthPath);

        Integer[] colors = new Integer[]{
                Color.rgb(246, 229, 141),
                Color.rgb(255, 190, 118),
                Color.rgb(255, 121, 121),
                Color.rgb(186, 220, 88),
                Color.rgb(126, 214, 223),
                Color.rgb(224, 86, 253),
                Color.rgb(223, 249, 251),
        };

        String eye = AssetEyePath + "/" + randomItem(eyes);
        String nose = AssetNosePath + "/" + randomItem(noses);
        String mouth = AssetMouthPath + "/" + randomItem(mouths);
        int color = randomItem(colors);

        return generate(assetManager, size, eye, nose, mouth, color);
    }

    public static String convertToBase64(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static Bitmap generate(AssetManager assetManager, int size, String eye, String nose, String mouth, int color) throws IOException {
        Bitmap result = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Rect resultRect = new Rect(0, 0, size, size);

        Paint background = new Paint();
        background.setColor(color);

        Bitmap eyeBitmap = BitmapFactory.decodeStream(assetManager.open(eye));
        Bitmap noseBitmap = BitmapFactory.decodeStream(assetManager.open(nose));
        Bitmap mouthBitmap = BitmapFactory.decodeStream(assetManager.open(mouth));

        Canvas comboImage = new Canvas(result);

        comboImage.drawRect(resultRect, background);

        comboImage.drawBitmap(eyeBitmap, new Rect(0, 0, eyeBitmap.getWidth(), eyeBitmap.getHeight()), resultRect, null);
        comboImage.drawBitmap(noseBitmap, new Rect(0, 0, noseBitmap.getWidth(), noseBitmap.getHeight()), resultRect, null);
        comboImage.drawBitmap(mouthBitmap, new Rect(0, 0, mouthBitmap.getWidth(), mouthBitmap.getHeight()), resultRect, null);

        return result;
    }
}
