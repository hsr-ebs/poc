package ch.hsr.ebos.app.ui.synchronization;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.event.Description;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregateService;
import ch.hsr.ebos.aion.core.processor.ProcessResultEvent;
import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncState;
import ch.hsr.ebos.aion.core.synchronizer.SyncState;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.TimelineEvent;
import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;
import ch.hsr.ebos.app.R;
import ch.hsr.ebos.app.ui.util.AppUtil;
import ch.hsr.ebos.offliss.common.FormatUtils;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventViewHolder> {
    private List<TimelineEventViewModel> items = new ArrayList<>();
    private Context context;

    public EventListAdapter(Context context) {
        this.context = context;
        this.reload();
    }

    private void reload() {
        try {
            TimelineEventAuth eventAuth = AppUtil.getApplication(context).getEventAuth();
            Collection<AionClientModule> modules = AppUtil.getApplication(context).getModules();

            for (AionClientModule module : modules) {
                ITimelineSyncState syncState = module.getSyncState();
                IDynamicTimeline resultTimeline = module.getResultTimeline();
                IDynamicTimeline localTimeline = module.getLocalTimeline();
                EventAggregateService eventAggregateService = module.getApplication().injector.getInstance(EventAggregateService.class);

                TimelineEvent event = localTimeline.last().get();
                while (event != null) {
                    TimelineEventViewModel viewModel = new TimelineEventViewModel();

                    if (!event.getAuth().getUsername().equals(eventAuth.getUsername())) {
                        event = localTimeline.prev(event.getId()).get();
                        continue;
                    }

                    TimelineEvent resultEvent = resultTimeline.get(event.getId()).get();
                    if (resultEvent != null) {
                        ProcessResultEvent result = resultEvent.getData(ProcessResultEvent.class);
                        viewModel.setEmitResult(result.getResult());
                    }

                    viewModel.setState(syncState.get(event.getId()));

                    if (viewModel.getEmitResult() != null && viewModel.getEmitResult().isSuccessfull() && viewModel.getState() != null) {
                        event = localTimeline.prev(event.getId()).get();
                        continue;
                    }

                    viewModel.setName(event.getType());
                    viewModel.setCreated(event.getCreated());
                    viewModel.setUsername(event.getAuth().getUsername());
                    viewModel.setTimeline(module.getTimelineName());

                    Class eventClass = eventAggregateService.getEventType(event.getType());
                    if (eventClass != null) {
                        Description desc = (Description) eventClass.getAnnotation(Description.class);
                        if (desc != null) {
                            viewModel.setName(desc.value());
                        }
                        Object data = event.getData(eventClass);
                        viewModel.setDescription(data.toString());
                    }

                    items.add(viewModel);

                    event = localTimeline.prev(event.getId()).get();
                }

            }

            Collections.sort(items, new TimelineEventViewModelComparator());
        } catch (Exception e) {

        }
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.row_event, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventViewHolder viewHolder, int position) {
        TimelineEventViewModel item = items.get(position);
        viewHolder.eventName.setText(item.getName());
        viewHolder.eventDescription.setText(item.getDescription());
        if (item.getState() == null) {
            viewHolder.eventStatus.setText("Pending");
            viewHolder.eventStatus.setTextColor(Color.rgb(230, 126, 34));
        } else if (item.getEmitResult() == null) {
            viewHolder.eventStatus.setText("Processing");
            viewHolder.eventStatus.setTextColor(Color.rgb(230, 126, 34));
        } else if (item.getEmitResult().isSuccessfull()) {
            viewHolder.eventStatus.setText("Successfull");
            viewHolder.eventStatus.setTextColor(Color.rgb(46, 204, 113));
        } else {
            viewHolder.eventStatus.setText("Failed");
            viewHolder.eventStatus.setTextColor(Color.rgb(231, 76, 60));
        }

        if (item.getEmitResult() != null && !item.getEmitResult().isSuccessfull()) {
            viewHolder.eventError.setText(join("\n", item.getEmitResult().getErrorMessages()));
            viewHolder.eventError.setTextColor(Color.rgb(231, 76, 60));
        }

        viewHolder.eventCreated.setText(FormatUtils.formatFullDate(item.getCreated()));
    }

    private <T> String join(String separator, List<String> list) {
        StringBuilder value = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                value.append(separator);
            }
            value.append(list.get(i));
        }
        return value.toString();
    }


    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    class TimelineEventViewModel {
        private String timeline;
        private SyncState state;
        private String name;
        private String description;
        private Date created;
        private String username;
        private EmitResult emitResult;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Date getCreated() {
            return created;
        }

        public void setCreated(Date created) {
            this.created = created;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public SyncState getState() {
            return state;
        }

        public void setState(SyncState state) {
            this.state = state;
        }

        public EmitResult getEmitResult() {
            return emitResult;
        }

        public void setEmitResult(EmitResult emitResult) {
            this.emitResult = emitResult;
        }

        public String getTimeline() {
            return timeline;
        }

        public void setTimeline(String timeline) {
            this.timeline = timeline;
        }
    }

    public class EventViewHolder extends RecyclerView.ViewHolder {
        TextView eventName;
        TextView eventDescription;
        TextView eventStatus;
        TextView eventCreated;
        TextView eventError;

        public EventViewHolder(View view) {
            super(view);

            eventName = view.findViewById(R.id.eventRowName);
            eventDescription = view.findViewById(R.id.eventRowDescription);
            eventStatus = view.findViewById(R.id.eventRowStatus);
            eventCreated = view.findViewById(R.id.eventRowCreated);
            eventError = view.findViewById(R.id.eventRowError);
        }
    }

    class TimelineEventViewModelComparator implements Comparator<TimelineEventViewModel> {
        @Override
        public int compare(TimelineEventViewModel o1, TimelineEventViewModel o2) {
            return o2.getCreated().compareTo(o1.getCreated());
        }
    }
}
