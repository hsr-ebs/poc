package ch.hsr.ebos.app.ui;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SqliteAndroidDataStateTest.class,
        SqliteAndroidTimelineTest.class,
})
public class AndroidTestSuite {
}
