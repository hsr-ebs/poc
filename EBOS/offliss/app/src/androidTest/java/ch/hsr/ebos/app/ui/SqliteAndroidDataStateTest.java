package ch.hsr.ebos.app.ui;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.junit.Before;

import ch.hsr.ebos.aion.data.sqlite.SqliteDataStateTest;
import ch.hsr.ebos.aion.data.sqlite.connection.AndroidSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactoryOptions;

public class SqliteAndroidDataStateTest extends SqliteDataStateTest {
    private String databaseFile = null;

    @Before
    public void setUp() throws Exception {
        Context context = InstrumentationRegistry.getTargetContext();
        databaseFile = context.getApplicationInfo().dataDir + "/test.db";
        super.setUp();
    }

    @Override
    public SqliteConnectionFactoryOptions getConnectionFactoryOptions() {
        return new AndroidSqliteConnectionFactoryOptions();
    }

    @Override
    public String getDatabaseFile() {
        return databaseFile;
    }
}