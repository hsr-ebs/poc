package ch.hsr.ebos.app.ui;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.junit.Before;

import ch.hsr.ebos.aion.data.sqlite.SqliteTimelineTest;
import ch.hsr.ebos.aion.data.sqlite.connection.AndroidSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactoryOptions;

public class SqliteAndroidTimelineTest extends SqliteTimelineTest {
    private String databaseDir = null;

    @Before
    public void setUp() throws Exception {
        Context context = InstrumentationRegistry.getTargetContext();
        databaseDir = context.getApplicationInfo().dataDir;
        super.setUp();
    }

    @Override
    public SqliteConnectionFactoryOptions getConnectionFactoryOptions() {
        return new AndroidSqliteConnectionFactoryOptions();
    }

    @Override
    public String getDatabaseDir() {
        return databaseDir;
    }
}
