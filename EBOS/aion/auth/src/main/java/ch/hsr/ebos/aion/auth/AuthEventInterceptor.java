package ch.hsr.ebos.aion.auth;

import java.util.List;

import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;

public class AuthEventInterceptor implements IEventInterceptor {
    @Override
    public void intercept(IEventInterceptorContext context) {
        if (context.getEvent().getAuth() == null) {
            context.next();
            return;
        }

        List<String> actualRoles = context.getEvent().getAuth().getRoles();
        for (IEventAggregate aggregate : context.getAggregators()) {

            boolean allowed = false;
            AllowRole[] allowedRoles = aggregate.getClass().getAnnotationsByType(AllowRole.class);
            if (allowedRoles.length == 0) {
                allowed = true;
            } else if (actualRoles != null) {
                for (AllowRole role : allowedRoles) {
                    if (role == null || actualRoles.contains(role.value())) {
                        allowed = true;
                        break;
                    }
                }
            }
            if (!allowed) {
                context.abort(new Exception("Missing allowed roles: " + allowedRoles));
                return;
            }

            boolean deny = false;
            DenyRole[] denyRoles = aggregate.getClass().getAnnotationsByType(DenyRole.class);
            for (DenyRole role : denyRoles) {
                if (actualRoles.contains(role.value())) {
                    deny = true;
                    break;
                }
            }
            if (deny) {
                context.abort(new Exception("Not allowed roles: " + denyRoles));
                return;
            }
        }
        context.next();
    }
}
