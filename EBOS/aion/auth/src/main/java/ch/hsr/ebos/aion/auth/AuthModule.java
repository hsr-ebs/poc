package ch.hsr.ebos.aion.auth;

import ch.hsr.ebos.aion.core.module.Module;

public class AuthModule extends Module {
    @Override
    protected void setup() {
        addEventInterceptors(AuthEventInterceptor.class);
    }
}
