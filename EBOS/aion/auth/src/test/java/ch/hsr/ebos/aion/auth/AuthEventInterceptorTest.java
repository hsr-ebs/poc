package ch.hsr.ebos.aion.auth;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.auth.aggregate.AllowDenyRoleEventAggregate;
import ch.hsr.ebos.aion.auth.aggregate.AllowRoleEventAggregate;
import ch.hsr.ebos.aion.auth.aggregate.AllowRolesEventAggregate;
import ch.hsr.ebos.aion.auth.aggregate.DenyRoleEventAggregate;
import ch.hsr.ebos.aion.auth.aggregate.DenyRolesEventAggregate;
import ch.hsr.ebos.aion.auth.aggregate.NoRoleEventAggregate;
import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationFactory;
import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;
import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;

public class AuthEventInterceptorTest {

    @Test
    public void testInterceptNoAllowRoleDefined() throws ExecutionException, InterruptedException {
        List<String> userRoles = new ArrayList<>();
        userRoles.add("Role 1");
        userRoles.add("Role 2");
        userRoles.add("Role 3");
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", userRoles);

        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, NoRoleEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertTrue(result.isSuccessfull());
    }

    @Test
    public void testInterceptUserHaNoRoleNotAllowed() throws ExecutionException, InterruptedException {
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", null);

        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, AllowRoleEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertFalse(result.isSuccessfull());
        Assert.assertTrue(result.getErrorMessages().get(0).contains("Missing allowed roles"));
    }

    @Test
    public void testInterceptUserHaNoRoleDenyed() throws ExecutionException, InterruptedException {
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", new ArrayList<>());

        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, DenyRoleEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertTrue(result.isSuccessfull());
    }

    @Test
    public void testInterceptAllowAllRoles() throws ExecutionException, InterruptedException {
        List<String> userRoles = new ArrayList<>();
        userRoles.add("Role 2");
        userRoles.add("Role 3");
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", userRoles);

        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, AllowRolesEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertTrue(result.isSuccessfull());
    }

    @Test
    public void testInterceptAllowMatchWithOneUserRole() throws ExecutionException, InterruptedException {
        List<String> userRoles = new ArrayList<>();
        userRoles.add("Role 2");
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", userRoles);

        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, AllowRolesEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertTrue(result.isSuccessfull());
    }

    @Test
    public void testInterceptAllowMatchWithOneAggregateRole() throws ExecutionException, InterruptedException {
        List<String> userRoles = new ArrayList<>();
        userRoles.add("Role 1");
        userRoles.add("Role 2");
        userRoles.add("Role 3");
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", userRoles);

        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, AllowRoleEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertTrue(result.isSuccessfull());
    }

    @Test
    public void testInterceptDenyRole() throws ExecutionException, InterruptedException {
        List<String> userRoles = new ArrayList<>();
        userRoles.add("Role 1");
        userRoles.add("Role 2");
        userRoles.add("Role 3");
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", userRoles);

        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, DenyRoleEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertFalse(result.isSuccessfull());
        Assert.assertTrue(result.getErrorMessages().get(0).contains("Not allowed roles"));
    }

    @Test
    public void testInterceptDenyRoles() throws ExecutionException, InterruptedException {
        List<String> userRoles = new ArrayList<>();
        userRoles.add("Role 1");
        userRoles.add("Role 2");
        userRoles.add("Role 3");
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", userRoles);
        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, DenyRolesEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertFalse(result.isSuccessfull());
        Assert.assertTrue(result.getErrorMessages().get(0).contains("Not allowed roles"));
    }

    @Test
    public void testMixedRoles() throws ExecutionException, InterruptedException {
        List<String> userRoles = new ArrayList<>();
        userRoles.add("Role 1");
        userRoles.add("Role 2");
        userRoles.add("Role 3");
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", userRoles);
        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, AllowDenyRoleEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertFalse(result.isSuccessfull());
        Assert.assertTrue(result.getErrorMessages().get(0).contains("Not allowed roles"));
    }

    @Test
    public void testMixedRolesAllow() throws ExecutionException, InterruptedException {
        List<String> userRoles = new ArrayList<>();
        userRoles.add("Role 1");
        userRoles.add("Role 3");
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", userRoles);
        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, AllowDenyRoleEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertTrue(result.isSuccessfull());
    }

    @Test
    public void testMixedRolesNotAllowed() throws ExecutionException, InterruptedException {
        List<String> userRoles = new ArrayList<>();
        userRoles.add("Role 2");
        userRoles.add("Role 3");
        TimelineEventAuth auth = new TimelineEventAuth(null, "myUserName", userRoles);
        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(AuthEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, AllowDenyRoleEventAggregate.class);
            }
        }).get();
        EmitResult result = application.emit(new ExampleEvent(), auth).get();
        Assert.assertFalse(result.isSuccessfull());
        Assert.assertTrue(result.getErrorMessages().get(0).contains("Missing allowed roles"));
    }

}
