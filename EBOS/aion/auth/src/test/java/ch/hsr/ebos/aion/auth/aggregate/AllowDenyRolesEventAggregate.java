package ch.hsr.ebos.aion.auth.aggregate;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.auth.DenyRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;

@DenyRole("Role 1")
@DenyRole("Role 2")
@AllowRole("Role 3")
@AllowRole("Role 4")
public class AllowDenyRolesEventAggregate extends EventAggregate<ExampleEvent> {

    @Override
    public void aggregate(ApplicationEvent<ExampleEvent> event) throws Throwable {
        // do nothing
    }
}
