package ch.hsr.ebos.aion.auth;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AuthEventInterceptorTest.class,
})
public class AionAuthTestSuite {
}
