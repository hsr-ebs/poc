package ch.hsr.ebos.aion.auth.aggregate;

import ch.hsr.ebos.aion.auth.AllowRole;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;

@AllowRole("Role 2")
public class AllowRoleEventAggregate extends EventAggregate<ExampleEvent> {

    @Override
    public void aggregate(ApplicationEvent<ExampleEvent> event) throws Throwable {
        // do nothing
    }
}
