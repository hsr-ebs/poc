package ch.hsr.ebos.aion.logging;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptor;
import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptorContext;

public class LoggingApplicationInterceptor implements IApplicationInterceptor {
    @Inject
    LoggingService log;

    @Override
    public void intercept(IApplicationInterceptorContext context) {
        log.debug("starting application");
        context.next().whenComplete((r, throwable) -> {
            log.debug("stopping application");
            if (throwable == null) {
                throwable.printStackTrace();
            }
        });
    }
}
