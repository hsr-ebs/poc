package ch.hsr.ebos.aion.logging;

import com.google.inject.name.Names;

import ch.hsr.ebos.aion.core.module.Module;

public class LoggingModule extends Module {
    private LogLevel logLevel;

    public LoggingModule() {
        this(LogLevel.WARN);
    }

    public LoggingModule(LogLevel logLevel) {
        this.logLevel = logLevel;
    }

    @Override
    protected void setup() {
        addApplicationInterceptors(LoggingApplicationInterceptor.class);
        addEventInterceptors(LoggingEventInterceptor.class);

        bindConstant().annotatedWith(Names.named("logLevel")).to(logLevel);

        bind(LoggingService.class);
    }
}
