package ch.hsr.ebos.aion.logging;

import com.google.inject.Inject;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;

public class LoggingEventInterceptor implements IEventInterceptor {
    @Inject
    LoggingService log;

    @Override
    public void intercept(IEventInterceptorContext context) {
        log.debug("--------------------------------------------");
        log.debug("start process event " + context.getEvent().getId());
        if (context.getAggregators().size() > 0) {
            for (IEventAggregate aggregate : context.getAggregators()) {
                log.debug(" - run event aggregate: " + aggregate.getClass().getSimpleName());
            }
        } else {
            log.warn("no matching event aggregate found for event " + context.getEvent().getId());
        }

        context.next(result -> {
            if (result.isSuccessfull()) {
                log.debug("processing event " + context.getEvent().getId() + " was successfull");
            } else {
                log.warn("processing event " + context.getEvent().getId() + " failed");
                for (String errorMessage : result.getErrorMessages()) {
                    log.warn(" - error: " + errorMessage);
                }
            }
            log.debug("--------------------------------------------");

            return CompletableFuture.completedFuture(null);
        });
    }
}
