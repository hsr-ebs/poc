package ch.hsr.ebos.aion.logging;

public enum LogLevel {
    DEBUG(0, "DEBUG"), INFO(1, "INFO"), WARN(2, "WARN"), ERROR(3, "ERROR");

    private int value;
    private String name;

    LogLevel(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public boolean contains(LogLevel other) {
        return this.value <= other.value;
    }

    @Override
    public String toString() {
        return name;
    }
}
