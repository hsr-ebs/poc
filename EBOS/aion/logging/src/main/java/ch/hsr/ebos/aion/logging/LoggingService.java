package ch.hsr.ebos.aion.logging;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class LoggingService {
    @Inject
    @Named("logLevel")
    LogLevel logLevel;

    public void log(LogLevel level, String message) {
        if (logLevel.contains(level)) {
            System.out.println(level.toString() + ": " + message);
        }
    }

    public void warn(String message) {
        log(LogLevel.WARN, message);
    }

    public void error(String message) {
        log(LogLevel.ERROR, message);
    }

    public void info(String message) {
        log(LogLevel.INFO, message);
    }

    public void debug(String message) {
        log(LogLevel.DEBUG, message);
    }
}
