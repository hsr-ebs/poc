package ch.hsr.ebos.aion.email;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationFactory;
import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.aion.email.mock.MockEmailModule;

import static org.junit.Assert.assertTrue;

public class MockEmailServiceTest {
    @Test
    public void testEmailService() throws ExecutionException, InterruptedException {

        Module module = new Module() {
            @Override
            protected void setup() {
                addEventAggregate(SmtpEmailServiceTest.EmailEvent.class, SmtpEmailServiceTest.EmailEventAggregate.class);
            }
        };
        module.addModule(new MockEmailModule());

        Application application = ApplicationFactory.create(module).get();

        SmtpEmailServiceTest.EmailEvent event = new SmtpEmailServiceTest.EmailEvent();

        EmitResult result = application.emit(event).get();

        assertTrue(result.isSuccessfull());
    }
}
