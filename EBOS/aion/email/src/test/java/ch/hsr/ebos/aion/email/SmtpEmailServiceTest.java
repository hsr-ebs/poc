package ch.hsr.ebos.aion.email;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.subethamail.wiser.Wiser;
import org.subethamail.wiser.WiserMessage;

import java.util.List;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.application.ApplicationFactory;
import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.aion.email.smtp.SmtpEmailModule;
import ch.hsr.ebos.aion.email.smtp.SmtpEmailOptions;
import ch.hsr.ebos.aion.email.smtp.SmtpEmailService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SmtpEmailServiceTest {
    private final String smtpServerHost = "localhost";
    private Wiser wiser;

    @Before
    public void setUp() {
        this.wiser = new Wiser();
        this.wiser.setHostname(smtpServerHost);
        this.wiser.start();
    }

    @After
    public void tearDown() {
        this.wiser.stop();
    }

    @Test
    public void testEmailToExampleCom() throws Exception {
        String fromEmail = "noreply@example.com";
        String fromName = "noreply";
        String toEmail = "test@example.com";
        String subject = "subject";
        String content = "foo bar";

        SmtpEmailOptions options = new SmtpEmailOptions();
        options.setHost(smtpServerHost);
        options.setFromEmail(fromEmail);
        options.setFromName(fromName);
        options.setPort(wiser.getServer().getPort());
        options.setUseAuth(false);
        options.setUseTls(false);

        SmtpEmailService service = new SmtpEmailService(options);
        service.send(toEmail, subject, content);

        List<WiserMessage> messages = wiser.getMessages();
        assertEquals(0, messages.size());
    }

    @Test
    public void testEmailSend() throws Exception {
        String fromEmail = "noreply@example.com";
        String fromName = "noreply";
        String toEmail = "example@hsr.ch";
        String subject = "subject";
        String content = "foo bar";

        SmtpEmailOptions options = new SmtpEmailOptions();
        options.setHost(smtpServerHost);
        options.setFromEmail(fromEmail);
        options.setFromName(fromName);
        options.setPort(wiser.getServer().getPort());
        options.setUseAuth(false);
        options.setUseTls(false);

        Module module = new Module() {
            @Override
            protected void setup() {
                addEventAggregate(EmailEvent.class, EmailEventAggregate.class);
            }
        };
        module.addModule(new SmtpEmailModule(options));

        Application application = ApplicationFactory.create(module).get();

        EmailEvent event = new EmailEvent();
        event.setTo(toEmail);
        event.setSubject(subject);
        event.setContent(content);

        EmitResult result = application.emit(event).get();

        assertTrue(result.isSuccessfull());

        List<WiserMessage> messages = wiser.getMessages();
        assertEquals(1, messages.size());

        WiserMessage wiserMessage = messages.get(0);

        assertEquals(fromEmail, wiserMessage.getEnvelopeSender());
        assertEquals(toEmail, wiserMessage.getEnvelopeReceiver());

        MimeMessage mimeMessage = wiserMessage.getMimeMessage();

        assertEquals(subject, mimeMessage.getSubject());
        assertEquals(content + "\r\n", mimeMessage.getContent());
    }

    @EventType("email.event")
    static class EmailEvent implements IEvent {
        private String to;
        private String subject;
        private String content;

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    static class EmailEventAggregate extends EventAggregate<EmailEvent> {
        @Inject
        IEmailService email;

        @Override
        public void aggregate(ApplicationEvent<EmailEvent> event) throws Throwable {
            email.send(event.getData().getTo(), event.getData().getSubject(), event.getData().getContent());
        }
    }
}
