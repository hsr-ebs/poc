package ch.hsr.ebos.aion.email;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SmtpEmailServiceTest.class,
        MockEmailServiceTest.class
})
public class AionEmailTestSuite {
}
