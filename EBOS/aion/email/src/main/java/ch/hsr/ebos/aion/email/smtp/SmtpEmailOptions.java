package ch.hsr.ebos.aion.email.smtp;

public class SmtpEmailOptions {
    private String host;
    private int port;
    private String username;
    private String password;
    private String fromEmail;
    private String fromName;
    private boolean useAuth;
    private boolean useTls;

    public SmtpEmailOptions() {
        port = 587;
        useAuth = false;
        useTls = true;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        this.useAuth = true;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        this.useAuth = true;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        if (fromName == null || fromName.length() == 0) {
            return getFromEmail();
        }

        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public boolean isUseTls() {
        return useTls;
    }

    public void setUseTls(boolean useTls) {
        this.useTls = useTls;
    }

    public boolean isUseAuth() {
        return useAuth;
    }

    public void setUseAuth(boolean useAuth) {
        this.useAuth = useAuth;
    }
}
