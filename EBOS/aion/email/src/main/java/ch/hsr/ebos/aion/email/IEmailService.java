package ch.hsr.ebos.aion.email;

public interface IEmailService {
    void send(String to, String subject, String content) throws Exception;
}
