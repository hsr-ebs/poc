package ch.hsr.ebos.aion.email.mock;

import java.util.ArrayList;
import java.util.List;

import ch.hsr.ebos.aion.email.IEmailService;

public class MockEmailService implements IEmailService {
    private List<MockMail> mails = new ArrayList<>();

    @Override
    public void send(String to, String subject, String content) throws Exception {
        this.mails.add(new MockMail(to, subject, content));
    }

    public List<MockMail> getMails() {
        return mails;
    }

    public static class MockMail {
        private String to;
        private String subject;
        private String content;

        public MockMail(String to, String subject, String content) {
            this.to = to;
            this.subject = subject;
            this.content = content;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }
    }
}
