package ch.hsr.ebos.aion.email.smtp;

import ch.hsr.ebos.aion.email.EmailModule;
import ch.hsr.ebos.aion.email.IEmailService;

public class SmtpEmailModule extends EmailModule {
    private SmtpEmailOptions options;

    public SmtpEmailModule(SmtpEmailOptions options) {
        this.options = options;
    }

    @Override
    protected void setup() {
        bind(SmtpEmailOptions.class).toInstance(options);
        bind(IEmailService.class).to(SmtpEmailService.class);
    }
}
