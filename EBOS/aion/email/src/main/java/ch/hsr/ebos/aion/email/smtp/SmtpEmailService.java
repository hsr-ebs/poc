package ch.hsr.ebos.aion.email.smtp;

import java.util.Date;
import java.util.Properties;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import ch.hsr.ebos.aion.email.IEmailService;

@Singleton
public class SmtpEmailService implements IEmailService {
    private Session session;
    private SmtpEmailOptions options;

    @Inject
    public SmtpEmailService(SmtpEmailOptions options) {
        this.options = options;

        Properties properties = new Properties();
        properties.put("mail.smtp.host", options.getHost());
        properties.put("mail.smtp.port", String.valueOf(options.getPort()));
        if (options.isUseAuth()) {
            properties.put("mail.smtp.auth", "true");
        }

        if (options.isUseTls()) {
            properties.put("mail.smtp.starttls.enable", "true");
        }

        Authenticator authenticator = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(options.getUsername(), options.getPassword());
            }
        };

        session = Session.getInstance(properties, authenticator);
    }

    public void send(String to, String subject, String content) throws Exception {
        if (to.endsWith("@example.com")) {
            return;
        }

        MimeMessage message = new MimeMessage(session);

        message.addHeader("Content-type", "text/HTML; charset=UTF-8");
        message.setFrom(new InternetAddress(options.getFromEmail(), options.getFromName()));
        message.setReplyTo(InternetAddress.parse(options.getFromEmail(), false));
        message.setSubject(subject, "UTF-8");
        message.setContent(content, "text/html");
        message.setSentDate(new Date());
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));

        Transport.send(message);
    }
}
