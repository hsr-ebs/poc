package ch.hsr.ebos.aion.email.mock;

import ch.hsr.ebos.aion.email.EmailModule;
import ch.hsr.ebos.aion.email.IEmailService;

public class MockEmailModule extends EmailModule {
    @Override
    protected void setup() {
        bind(IEmailService.class).to(MockEmailService.class);
    }
}
