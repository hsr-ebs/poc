package ch.hsr.ebos.aion.data.orm;

import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("example.insert")
public class ExampleInsertEvent implements IEvent {
    private ExampleEntity entity;

    public ExampleInsertEvent() {

    }

    public ExampleInsertEvent(ExampleEntity entity) {
        this.entity = entity;
    }

    public ExampleEntity getEntity() {
        return entity;
    }
}
