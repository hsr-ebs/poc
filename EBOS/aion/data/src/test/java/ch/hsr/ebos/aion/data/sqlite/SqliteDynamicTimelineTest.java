package ch.hsr.ebos.aion.data.sqlite;

import ch.hsr.ebos.aion.core.timeline.DynamicTimelineTest;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.data.sqlite.connection.JavaSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineFactory;

public class SqliteDynamicTimelineTest extends DynamicTimelineTest {
    private SqliteConnectionFactory connectionFactory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
    private SqliteTimelineFactory timelineFactory = new SqliteTimelineFactory("unittestdb", connectionFactory);

    @Override
    public ITimelineFactory<IDynamicTimeline> getTimelineFactory() {
        return timelineFactory;
    }
}