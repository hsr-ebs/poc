package ch.hsr.ebos.aion.data.sqlite;

import org.junit.After;
import org.junit.Before;

import java.sql.SQLException;

import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.aion.data.DataStateTest;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.state.SqliteDataModule;

public abstract class SqliteDataStateTest extends DataStateTest {
    protected SqliteConnectionFactory connectionFactory;

    public abstract SqliteConnectionFactoryOptions getConnectionFactoryOptions();

    public abstract String getDatabaseFile();

    @Before
    public void setUp() throws Exception {
        connectionFactory = new SqliteConnectionFactory(getConnectionFactoryOptions());
        this.clearDatabase();
        super.setUp();
    }

    @After
    public void tearDown() throws SQLException {
        this.clearDatabase();
        connectionFactory = null;
    }

    private void clearDatabase() throws SQLException {
        connectionFactory.delete(getDatabaseFile());
    }

    @Override
    public DataModule getDataModule() {
        return new SqliteDataModule(connectionFactory, getDatabaseFile(), false);
    }
}
