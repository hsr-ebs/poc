package ch.hsr.ebos.aion.data.memory;

import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineTest;
import ch.hsr.ebos.aion.data.memory.timeline.MemoryTimelineFactory;

public class MemoryTimelineTest extends TimelineTest {
    MemoryTimelineFactory timelineFactory = new MemoryTimelineFactory();

    @Override
    public ITimelineFactory<?> getTimelineFactory() {
        return timelineFactory;
    }
}
