package ch.hsr.ebos.aion.data.sqlite;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncStateFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineSyncStateTest;
import ch.hsr.ebos.aion.data.sqlite.connection.JavaSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineSyncStateFactory;

public class SqliteTimelineSyncStateTest extends TimelineSyncStateTest {
    @Override
    public ITimelineSyncStateFactory getFactory() throws Exception {
        SqliteConnectionFactory connectionFactory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
        return new SqliteTimelineSyncStateFactory("unittest", connectionFactory);
    }
}
