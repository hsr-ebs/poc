package ch.hsr.ebos.aion.data.orm;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;

public class ExampleUpdateAggregate extends EventAggregate<ExampleUpdateEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<ExampleUpdateEvent> event) throws Throwable {
        entityService.update(event.getData().getEntity());
    }
}
