package ch.hsr.ebos.aion.data;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.application.ApplicationFactory;
import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.event.EventTypeUtil;
import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.data.orm.ExampleDeleteAggregate;
import ch.hsr.ebos.aion.data.orm.ExampleDeleteEvent;
import ch.hsr.ebos.aion.data.orm.ExampleEntity;
import ch.hsr.ebos.aion.data.orm.ExampleEntityEnum;
import ch.hsr.ebos.aion.data.orm.ExampleInsertAggregate;
import ch.hsr.ebos.aion.data.orm.ExampleInsertEvent;
import ch.hsr.ebos.aion.data.orm.ExampleUpdateAggregate;
import ch.hsr.ebos.aion.data.orm.ExampleUpdateEvent;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.sequence.TestSequence;
import ch.hsr.ebos.aion.logging.LogLevel;
import ch.hsr.ebos.aion.logging.LoggingModule;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public abstract class DataStateTest {
    protected Application app;
    protected EntityService service;

    public abstract DataModule getDataModule();

    @Before
    public void setUp() throws Exception {
        Module module = new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleDeleteEvent.class, ExampleDeleteAggregate.class);
                addEventAggregate(ExampleUpdateEvent.class, ExampleUpdateAggregate.class);
                addEventAggregate(ExampleInsertEvent.class, ExampleInsertAggregate.class);
            }
        };

        DataModule dataModule = getDataModule();
        dataModule.addEntity(ExampleEntity.class);
        dataModule.addSequence(TestSequence.class);

        module.addModule(dataModule);
        module.addModule(new LoggingModule(LogLevel.DEBUG));

        app = ApplicationFactory.create(module).get();
        service = app.injector.getInstance(EntityService.class);
    }

    @Test
    public void testInsertAndGet() throws Exception {
        assertNull(get("1"));

        assertTrue(insert(new ExampleEntity("1", "foo")));
        ExampleEntity entity = get("1");

        assertNotNull(entity);
        assertEquals("foo", entity.getName());
    }

    @Test
    public void testCount() throws Exception {
        assertNull(get("1"));

        assertTrue(insert(new ExampleEntity("1", "foo")));
        assertTrue(insert(new ExampleEntity("2", "bar")));
        assertTrue(insert(new ExampleEntity("3", "baz")));

        long count = service.count(ExampleEntity.class, Query.build());
        assertEquals(3, count);
    }

    @Test
    public void testEmptyCount() throws Exception {
        long count = service.count(ExampleEntity.class, Query.build());
        assertEquals(0, count);
    }

    @Test
    public void testInsertAndGetAndRemove() throws Exception {
        assertNull(get("1"));

        assertTrue(insert(new ExampleEntity("1", "foo")));

        ExampleEntity entity = get("1");
        assertNotNull(entity);
        assertEquals("foo", entity.getName());

        assertTrue(delete("1"));

        entity = get("1");
        assertNull(entity);
    }

    @Test
    public void testInsertAndUpdateAndGet() throws Exception {
        assertNull(get("1"));

        assertTrue(insert(new ExampleEntity("1", "foo")));

        ExampleEntity entity = get("1");
        assertNotNull(entity);
        assertEquals("foo", entity.getName());

        assertTrue(update(new ExampleEntity("1", "bar")));

        entity = get("1");
        assertNotNull(entity);
        assertEquals("bar", entity.getName());
    }

    @Test
    public void testQuery() throws Exception {
        assertNull(get("1"));

        assertTrue(insert(new ExampleEntity("1", "foo")));

        List<ExampleEntity> result = service.query(ExampleEntity.class, Query.build().and(
                Filter.equals("name", "foo")
        ));
        assertEquals(1, result.size());
    }

    @Test
    public void testUniqueConstraint() throws Exception {
        assertNull(get("1"));

        assertTrue(insert(new ExampleEntity("1", "foo")));

        ExampleEntity result = service.get(ExampleEntity.class, "1");
        assertNotNull(result);

        assertFalse(insert(new ExampleEntity("2", "foo")));

        result = service.get(ExampleEntity.class, "2");
        assertNull(result);
    }

    @Test
    public void testDateValue() throws Exception {
        assertNull(get("1"));

        ExampleEntity entity = new ExampleEntity("1", "foo");
        entity.setCreated(new Date());

        assertTrue(insert(entity));

        ExampleEntity result = service.get(ExampleEntity.class, "1");
        assertNotNull(result);
        assertEquals(entity.getCreated(), result.getCreated());
    }

    @Test
    public void testDoubleValue() throws Exception {
        assertNull(get("1"));

        ExampleEntity entity = new ExampleEntity("1", "foo");
        entity.setAmount(2.5);

        assertTrue(insert(entity));

        ExampleEntity result = service.get(ExampleEntity.class, "1");
        assertNotNull(result);
        assertEquals(entity.getAmount(), result.getAmount(), 0.001);
    }

    @Test
    public void testIntegerValue() throws Exception {
        assertNull(get("1"));

        ExampleEntity entity = new ExampleEntity("1", "foo");
        entity.setNumber(3);

        assertTrue(insert(entity));

        ExampleEntity result = service.get(ExampleEntity.class, "1");
        assertNotNull(result);
        assertEquals(entity.getAmount(), result.getAmount(), 0);
    }

    @Test
    public void testBooleanValue() throws Exception {
        assertNull(get("1"));

        ExampleEntity entity = new ExampleEntity("1", "foo");
        entity.setActive(false);

        assertTrue(insert(entity));

        ExampleEntity result = service.get(ExampleEntity.class, "1");
        assertNotNull(result);
        assertEquals(entity.isActive(), result.isActive());
    }

    @Test
    public void testEnumValue() throws Exception {
        assertNull(get("1"));

        ExampleEntity entity = new ExampleEntity("1", "foo");
        entity.setState(ExampleEntityEnum.Value1);

        assertTrue(insert(entity));

        ExampleEntity result = service.get(ExampleEntity.class, "1");
        assertNotNull(result);
        assertEquals(entity.getState(), result.getState());
    }

    @Test
    public void testSequence() throws Exception {
        assertTrue(insert(new ExampleEntity("a", "test a")));
        assertTrue(insert(new ExampleEntity("b", "test b")));

        ExampleEntity first = get("a");
        assertEquals(1, first.getNumber());
        ExampleEntity second = get("b");
        assertEquals(2, second.getNumber());
    }

    @Test
    public void testSequenceRollback() throws Exception {
        assertTrue(insert(new ExampleEntity("a", "test a")));
        assertTrue(insert(new ExampleEntity("b", "test b")));
        assertFalse(insert(new ExampleEntity("b", "test b")));

        ExampleEntity first = get("a");
        assertEquals(1, first.getNumber());
        ExampleEntity second = get("b");
        assertEquals(2, second.getNumber());

        assertTrue(insert(new ExampleEntity("c", "test c")));

        ExampleEntity third = get("c");
        assertEquals(3, third.getNumber());
    }

    protected ExampleEntity get(String id) throws Exception {
        return service.get(ExampleEntity.class, id);
    }

    protected boolean insert(ExampleEntity entity) throws Exception {
        return insert(UUID.randomUUID().toString(), entity);
    }

    protected boolean insert(String id, ExampleEntity entity) throws Exception {
        EmitResult result = app.emit(new ApplicationEvent(id, EventTypeUtil.getEventType(ExampleInsertEvent.class), new ExampleInsertEvent(entity), null)).get();
        return result.isSuccessfull();
    }

    protected boolean update(ExampleEntity entity) throws Exception {
        return update(UUID.randomUUID().toString(), entity);
    }

    protected boolean update(String id, ExampleEntity entity) throws Exception {
        EmitResult result = app.emit(new ApplicationEvent(id, EventTypeUtil.getEventType(ExampleUpdateEvent.class), new ExampleUpdateEvent(entity), null)).get();
        return result.isSuccessfull();
    }

    protected boolean delete(String entityId) throws Exception {
        return delete(UUID.randomUUID().toString(), entityId);
    }

    protected boolean delete(String id, String entityId) throws Exception {
        EmitResult result = app.emit(new ApplicationEvent(id, EventTypeUtil.getEventType(ExampleDeleteEvent.class), new ExampleDeleteEvent(ExampleEntity.class, entityId), null)).get();
        return result.isSuccessfull();
    }
}
