package ch.hsr.ebos.aion.data.orm;

import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("example.delete")
public class ExampleDeleteEvent implements IEvent {
    private Class entityClass;
    private String id;

    public ExampleDeleteEvent() {

    }

    public ExampleDeleteEvent(Class entityClass, String id) {
        this.entityClass = entityClass;
        this.id = id;
    }

    public Class getEntityClass() {
        return entityClass;
    }

    public String getId() {
        return id;
    }
}
