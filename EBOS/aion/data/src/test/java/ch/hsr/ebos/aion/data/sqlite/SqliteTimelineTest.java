package ch.hsr.ebos.aion.data.sqlite;

import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineTest;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineFactory;

public abstract class SqliteTimelineTest extends TimelineTest {
    private SqliteConnectionFactory connectionFactory = new SqliteConnectionFactory(getConnectionFactoryOptions());

    public abstract SqliteConnectionFactoryOptions getConnectionFactoryOptions();

    public abstract String getDatabaseDir();

    @Override
    public ITimelineFactory<?> getTimelineFactory() {
        return new SqliteTimelineFactory(getDatabaseDir(), connectionFactory);
    }
}