package ch.hsr.ebos.aion.data.orm;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EntityTest {
    @Test
    public void testEmptyNewEquals() {
        ExampleEntity entity1 = new ExampleEntity();
        ExampleEntity entity2 = new ExampleEntity();

        assertTrue(entity1.equals(entity2));
    }

    @Test
    public void testSameStringValueEquals() {
        ExampleEntity entity1 = new ExampleEntity();
        ExampleEntity entity2 = new ExampleEntity();

        entity1.setName("foo");
        entity2.setName("foo");

        assertTrue(entity1.equals(entity2));
    }

    @Test
    public void testDifferentStringValueEquals() {
        ExampleEntity entity1 = new ExampleEntity();
        ExampleEntity entity2 = new ExampleEntity();

        entity1.setName("foo");
        entity2.setName("bar");

        assertFalse(entity1.equals(entity2));
    }

    @Test
    public void testSameIntValueEquals() {
        ExampleEntity entity1 = new ExampleEntity();
        ExampleEntity entity2 = new ExampleEntity();

        entity1.setNumber(1);
        entity2.setNumber(1);

        assertTrue(entity1.equals(entity2));
    }

    @Test
    public void testDifferentIntValueEquals() {
        ExampleEntity entity1 = new ExampleEntity();
        ExampleEntity entity2 = new ExampleEntity();

        entity1.setNumber(3);
        entity2.setNumber(4);

        assertFalse(entity1.equals(entity2));
    }
}
