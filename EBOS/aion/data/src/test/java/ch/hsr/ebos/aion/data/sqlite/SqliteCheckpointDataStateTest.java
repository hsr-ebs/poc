package ch.hsr.ebos.aion.data.sqlite;

import org.junit.Test;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.processor.ProcessResetEvent;
import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.aion.data.ICheckpointDataState;
import ch.hsr.ebos.aion.data.orm.ExampleEntity;
import ch.hsr.ebos.aion.data.sqlite.state.SqliteCheckpointDataState;
import ch.hsr.ebos.aion.data.sqlite.state.SqliteDataModule;
import ch.hsr.ebos.aion.data.sqlite.state.operations.SqliteOperation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public abstract class SqliteCheckpointDataStateTest extends SqliteDataStateTest {
    @Override
    public DataModule getDataModule() {
        return new SqliteDataModule(connectionFactory, getDatabaseFile(), true);
    }

    @Test
    public void testResetInsert() throws Exception {
        assertTrue(insert("a", new ExampleEntity("1", "foo")));
        assertTrue(insert("b", new ExampleEntity("2", "bar")));

        assertNotNull(get("1"));
        assertNotNull(get("2"));

        assertTrue(reset("b"));

        assertNotNull(get("1"));
        assertNull(get("2"));
    }

    @Test
    public void testOperations() throws Exception {
        assertTrue(insert("a", new ExampleEntity("1", "foo")));
        assertTrue(insert("b", new ExampleEntity("2", "bar")));

        SqliteCheckpointDataState dataState = (SqliteCheckpointDataState)app.injector.getInstance(ICheckpointDataState.class);

        dataState.begin("test");

        SqliteOperation operation = dataState.getLastOperation();
        assertEquals("b", operation.getEventId());

        dataState.popOperation();
        dataState.popOperation();

        operation = dataState.getLastOperation();
        assertEquals("a", operation.getEventId());

        dataState.popOperation();
        dataState.popOperation();

        operation = dataState.getLastOperation();
        assertNull(operation);
    }

    @Test
    public void testResetUpdate() throws Exception {
        assertTrue(insert("a", new ExampleEntity("1", "foo")));
        assertTrue(update("b", new ExampleEntity("1", "bar")));

        ExampleEntity entity = get("1");
        assertNotNull(entity);
        assertEquals("bar", entity.getName());

        assertTrue(reset("b"));

        entity = get("1");
        assertNotNull(entity);
        assertEquals("foo", entity.getName());
    }

    @Test
    public void testResetDelete() throws Exception {
        assertTrue(insert("a", new ExampleEntity("1", "foo")));
        assertTrue(delete("b", "1"));

        ExampleEntity entity = get("1");
        assertNull(entity);

        assertTrue(reset("b"));

        entity = get("1");
        assertNotNull(entity);
    }


    @Test
    public void testResetUpdates() throws Exception {
        assertTrue(insert("a", new ExampleEntity("1", "foo")));
        assertTrue(update("b", new ExampleEntity("1", "bar")));
        assertTrue(update("c", new ExampleEntity("1", "bar2")));

        ExampleEntity entity = get("1");
        assertNotNull(entity);
        assertEquals("bar2", entity.getName());

        assertTrue(reset("c"));

        entity = get("1");
        assertNotNull(entity);
        assertEquals("bar", entity.getName());
    }

    private boolean reset(String id) throws Exception {
        EmitResult result = app.emit(new ProcessResetEvent(id)).get();
        return result.isSuccessfull();
    }
}
