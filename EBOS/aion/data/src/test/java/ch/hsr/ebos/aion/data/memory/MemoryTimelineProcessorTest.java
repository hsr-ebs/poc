package ch.hsr.ebos.aion.data.memory;

import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineProcessorTest;
import ch.hsr.ebos.aion.data.memory.timeline.MemoryTimelineFactory;

public class MemoryTimelineProcessorTest extends TimelineProcessorTest {
    private MemoryTimelineFactory factory = new MemoryTimelineFactory();

    @Override
    public ITimelineFactory<IDynamicTimeline> getFactory() {
        return factory;
    }
}
