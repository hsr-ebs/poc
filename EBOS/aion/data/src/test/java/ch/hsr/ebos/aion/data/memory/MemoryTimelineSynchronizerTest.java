package ch.hsr.ebos.aion.data.memory;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncStateFactory;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineSynchronizerTest;
import ch.hsr.ebos.aion.data.memory.timeline.MemoryTimelineFactory;
import ch.hsr.ebos.aion.data.memory.timeline.MemoryTimelineSyncStateFactory;

public class MemoryTimelineSynchronizerTest extends TimelineSynchronizerTest {
    MemoryTimelineFactory timelineFactory = new MemoryTimelineFactory();
    MemoryTimelineSyncStateFactory syncStateFactory = new MemoryTimelineSyncStateFactory();

    @Override
    public ITimelineFactory<IDynamicTimeline> getTimelineFactory() {
        return timelineFactory;
    }

    @Override
    public ITimelineSyncStateFactory getSyncStateFactory() {
        return syncStateFactory;
    }
}
