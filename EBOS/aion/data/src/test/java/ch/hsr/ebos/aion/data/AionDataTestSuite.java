package ch.hsr.ebos.aion.data;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ch.hsr.ebos.aion.data.memory.DynamicMemoryTimelineTest;
import ch.hsr.ebos.aion.data.memory.MemoryTimelineProcessorTest;
import ch.hsr.ebos.aion.data.memory.MemoryTimelineSynchronizerTest;
import ch.hsr.ebos.aion.data.memory.MemoryTimelineTest;
import ch.hsr.ebos.aion.data.sqlite.SqliteConnectionFactoryTest;
import ch.hsr.ebos.aion.data.sqlite.SqliteDynamicTimelineTest;
import ch.hsr.ebos.aion.data.sqlite.SqliteJavaCheckpointDataStateTest;
import ch.hsr.ebos.aion.data.sqlite.SqliteJavaDataStateTest;
import ch.hsr.ebos.aion.data.sqlite.SqliteJavaTimelineTest;
import ch.hsr.ebos.aion.data.sqlite.SqliteTimelineProcessorTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        MemoryTimelineSynchronizerTest.class,
        MemoryTimelineTest.class,
        MemoryTimelineProcessorTest.class,
        DynamicMemoryTimelineTest.class,
        SqliteJavaTimelineTest.class,
        SqliteJavaDataStateTest.class,
        SqliteJavaCheckpointDataStateTest.class,
        SqliteTimelineProcessorTest.class,
        SqliteDynamicTimelineTest.class,
        SqliteConnectionFactoryTest.class,
})
public class AionDataTestSuite {
}
