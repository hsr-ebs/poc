package ch.hsr.ebos.aion.data.sqlite;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncStateFactory;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineSynchronizerTest;
import ch.hsr.ebos.aion.data.sqlite.connection.JavaSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineFactory;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineSyncStateFactory;

public class SqliteTimelineSynchronizerTest extends TimelineSynchronizerTest {
    SqliteConnectionFactory connectionFactory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
    SqliteTimelineFactory timelineFactory = new SqliteTimelineFactory("unittestdb", connectionFactory);
    SqliteTimelineSyncStateFactory syncStateFactory = new SqliteTimelineSyncStateFactory("unittestdb", connectionFactory);

    @Override
    public ITimelineFactory<IDynamicTimeline> getTimelineFactory() {
        return timelineFactory;
    }

    @Override
    public ITimelineSyncStateFactory getSyncStateFactory() {
        return syncStateFactory;
    }
}
