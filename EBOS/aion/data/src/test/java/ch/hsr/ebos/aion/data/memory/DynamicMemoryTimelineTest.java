package ch.hsr.ebos.aion.data.memory;

import ch.hsr.ebos.aion.core.timeline.DynamicTimelineTest;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.data.memory.timeline.MemoryTimelineFactory;

public class DynamicMemoryTimelineTest extends DynamicTimelineTest {
    MemoryTimelineFactory timelineFactory = new MemoryTimelineFactory();

    @Override
    public ITimelineFactory<IDynamicTimeline> getTimelineFactory() {
        return timelineFactory;
    }
}
