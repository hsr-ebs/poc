package ch.hsr.ebos.aion.data.sqlite;

import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineProcessorTest;
import ch.hsr.ebos.aion.data.sqlite.connection.JavaSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineFactory;

public class SqliteTimelineProcessorTest extends TimelineProcessorTest {

    @Override
    public ITimelineFactory<IDynamicTimeline> getFactory() {
        SqliteConnectionFactory connectionFactory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
        return new SqliteTimelineFactory("unittestdb", connectionFactory);
    }
}
