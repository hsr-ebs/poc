package ch.hsr.ebos.aion.data.orm;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.data.sequence.TestSequence;

public class ExampleInsertAggregate extends EventAggregate<ExampleInsertEvent> {
    @Inject
    EntityService entityService;

    @Inject
    TestSequence sequence;

    @Override
    public void aggregate(ApplicationEvent<ExampleInsertEvent> event) throws Throwable {
        ExampleEntity entity = event.getData().getEntity();
        entity.setNumber(sequence.getNext());
        entityService.insert(entity);
    }
}
