package ch.hsr.ebos.aion.data.orm;

import java.util.Date;

import ch.hsr.ebos.aion.data.orm.annotation.Field;
import ch.hsr.ebos.aion.data.orm.annotation.Unique;

public class ExampleEntity extends Entity {
    @Unique
    @Field
    private String name;

    @Field
    private int number;

    @Field
    private double amount;

    @Field
    private Date created;

    @Field
    private boolean active;

    @Field(name = "status")
    private ExampleEntityEnum state;

    public ExampleEntity() {

    }

    public ExampleEntity(String id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ExampleEntityEnum getState() {
        return state;
    }

    public void setState(ExampleEntityEnum state) {
        this.state = state;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}