package ch.hsr.ebos.aion.data.sqlite;

import ch.hsr.ebos.aion.data.sqlite.connection.JavaSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactoryOptions;

public class SqliteJavaTimelineTest extends SqliteTimelineTest {
    @Override
    public SqliteConnectionFactoryOptions getConnectionFactoryOptions() {
        return new JavaSqliteConnectionFactoryOptions();
    }

    @Override
    public String getDatabaseDir() {
        return "./";
    }
}
