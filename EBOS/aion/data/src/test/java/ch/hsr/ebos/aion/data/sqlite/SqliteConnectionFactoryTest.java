package ch.hsr.ebos.aion.data.sqlite;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import ch.hsr.ebos.aion.data.sqlite.connection.JavaSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;

import static org.junit.Assert.assertNotNull;

public class SqliteConnectionFactoryTest {
    private SqliteConnectionFactory factory;

    @Before
    public void setUp() {
        factory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
    }

    @After
    public void tearDown() throws SQLException {
        factory.close("test.db");
        factory.close("test2.db");
    }

    @Test
    public void testOpenSingleConnection() throws Exception {
        Connection connection = factory.get("test.db");
        assertNotNull(connection);
    }

    @Test
    public void testOpenMultipleConnectionOnDifferentDbs() throws Exception {
        SqliteConnectionFactory factory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
        Connection connection1 = factory.get("test.db");
        Connection connection2 = factory.get("test2.db");
        assertNotNull(connection1);
        assertNotNull(connection2);
    }

    @Test
    public void testOpenMultipleConnectionOnSameDbs() throws Exception {
        SqliteConnectionFactory factory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
        Connection connection1 = factory.get("test.db");
        assertNotNull(connection1);
        connection1.close();
        Connection connection2 = factory.get("test.db");
        assertNotNull(connection2);
        connection2.close();
    }

//    @Test
//    public void testOpenMultipleConnectionOnSameDbsDifferentThreads() throws Exception {
//        SqliteConnectionFactory factory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
//
//        int timeout = 1000;
//        Connection connection1 = factory.get("test.db");
//
//        Thread t1 = new Thread(() -> {
//            try {
//                Thread.sleep(timeout);
//                connection1.close();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
//
//        System.out.println("start thread");
//        t1.start();
//
//        long start = System.currentTimeMillis();
//        factory.get("test.db");
//        long stop = System.currentTimeMillis();
//
//        t1.join();
//
//        assertTrue(timeout < stop - start);
//    }
}
