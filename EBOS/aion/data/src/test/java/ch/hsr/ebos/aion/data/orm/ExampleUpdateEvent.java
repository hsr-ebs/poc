package ch.hsr.ebos.aion.data.orm;

import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("example.update")
public class ExampleUpdateEvent implements IEvent {
    private ExampleEntity entity;

    public ExampleUpdateEvent() {

    }

    public ExampleUpdateEvent(ExampleEntity entity) {
        this.entity = entity;
    }

    public ExampleEntity getEntity() {
        return entity;
    }
}
