package ch.hsr.ebos.aion.data.orm;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;

public class ExampleDeleteAggregate extends EventAggregate<ExampleDeleteEvent> {
    @Inject
    EntityService entityService;

    @Override
    public void aggregate(ApplicationEvent<ExampleDeleteEvent> event) throws Throwable {
        entityService.delete(event.getData().getEntityClass(), event.getData().getId());
    }
}
