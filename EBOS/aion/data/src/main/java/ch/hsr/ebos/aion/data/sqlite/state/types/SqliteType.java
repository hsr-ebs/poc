package ch.hsr.ebos.aion.data.sqlite.state.types;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class SqliteType<T> {
    public abstract String getTypeName();

    public abstract boolean canHandle(Class type);

    public abstract T getValue(ResultSet resultSet, int column, Class type) throws SQLException;

    public abstract void setValue(PreparedStatement statement, int column, T value) throws SQLException;
}
