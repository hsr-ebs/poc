package ch.hsr.ebos.aion.data.orm.query;

public enum FilterOperator {
    EQUALS, LIKE
}
