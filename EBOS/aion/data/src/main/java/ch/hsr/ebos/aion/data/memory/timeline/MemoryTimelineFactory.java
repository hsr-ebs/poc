package ch.hsr.ebos.aion.data.memory.timeline;

import java.util.HashMap;
import java.util.Map;

import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;

public class MemoryTimelineFactory implements ITimelineFactory {
    private Map<String, MemoryTimeline> timelineMap = new HashMap<>();

    @Override
    public void delete(String name) {
        timelineMap.remove(name);
    }

    @Override
    public void close(String name) throws Exception {
        timelineMap.remove(name);
    }

    @Override
    public ITimeline get(String name) {
        if (timelineMap.containsKey(name)) {
            return timelineMap.get(name);
        }
        MemoryTimeline timeline = new MemoryTimeline();
        timelineMap.put(name, timeline);
        return timeline;
    }
}
