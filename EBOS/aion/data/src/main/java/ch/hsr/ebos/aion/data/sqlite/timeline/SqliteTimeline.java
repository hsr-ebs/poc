package ch.hsr.ebos.aion.data.sqlite.timeline;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.exception.CreateEventException;
import ch.hsr.ebos.aion.core.exception.DuplicateEventException;
import ch.hsr.ebos.aion.core.exception.GetEventException;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.TimelineEvent;
import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;
import ch.hsr.ebos.aion.core.timeline.TimelineInfo;
import ch.hsr.ebos.aion.core.util.JsonUtil;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;

public class SqliteTimeline implements ITimeline, IDynamicTimeline {
    private SqliteConnectionFactory connectionFactory;
    private String fileName;
    private Gson gson = JsonUtil.getGson();

    public SqliteTimeline(SqliteConnectionFactory connectionFactory, String fileName) {
        this.connectionFactory = connectionFactory;
        this.fileName = fileName;
    }

    private TimelineEvent parseEvent(ResultSet result) throws SQLException {
        if (result.next()) {
            TimelineEvent event = new TimelineEvent();
            event.setId(result.getString("id"));
            event.setType(result.getString("type"));
            event.setCreated(new Date(result.getLong("created")));
            event.setData(gson.fromJson(result.getString("data"), JsonObject.class));

            String auth = result.getString("auth");
            if (auth != null) {
                event.setAuth(gson.fromJson(auth, TimelineEventAuth.class));
            }

            return event;
        } else {
            return null;
        }
    }

    private void insertEvent(Connection connection, TimelineEvent event, String prevEventId, String nextEventId) throws CreateEventException, DuplicateEventException {
        try (PreparedStatement statement = connection.prepareStatement("INSERT INTO event (id, next, prev, type, data, auth, created) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
            statement.setString(1, event.getId());
            statement.setString(2, nextEventId);
            statement.setString(3, prevEventId);
            statement.setString(4, event.getType());
            statement.setString(5, event.getStringData());
            statement.setString(6, event.getAuth() != null ? gson.toJson(event.getAuth()) : null);
            statement.setLong(7, event.getCreated().getTime());
            if (statement.executeUpdate() != 1) {
                throw new CreateEventException("Failed to insert new event");
            }
        } catch (SQLException e) {
            throw new DuplicateEventException(event.getId());
        }
    }

    @Override
    public CompletableFuture<TimelineEvent> get(String id) {
        CompletableFuture<TimelineEvent> future = new CompletableFuture<>();
        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT id, type, data, auth, created FROM event WHERE id = ?")) {
                statement.setString(1, id);
                ResultSet result = statement.executeQuery();
                future.complete(parseEvent(result));
            }
        } catch (Exception e) {
            future.completeExceptionally(new GetEventException(e.getMessage()));
        }

        return future;
    }

    @Override
    public CompletableFuture<TimelineEvent> prev(String id) {
        CompletableFuture<TimelineEvent> future = new CompletableFuture<>();

        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT id, type, data, auth, created FROM event WHERE next = ?")) {
                statement.setString(1, id);
                ResultSet result = statement.executeQuery();
                future.complete(parseEvent(result));
            }
        } catch (Exception e) {
            future.completeExceptionally(new GetEventException(e.getMessage()));
        }
        return future;
    }

    @Override
    public CompletableFuture<TimelineEvent> next(String id) {
        if (id == null) {
            return first();
        }

        CompletableFuture<TimelineEvent> future = new CompletableFuture<>();

        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT id, type, data, auth, created FROM event WHERE prev = ?")) {
                statement.setString(1, id);
                ResultSet result = statement.executeQuery();
                future.complete(parseEvent(result));
            }
        } catch (Exception e) {
            future.completeExceptionally(new GetEventException(e.getMessage()));
        }
        return future;
    }

    @Override
    public CompletableFuture<TimelineEvent> first() {
        CompletableFuture<TimelineEvent> future = new CompletableFuture<>();

        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT id, type, data, auth, created FROM event WHERE prev IS NULL")) {
                ResultSet result = statement.executeQuery();
                future.complete(parseEvent(result));
            }
        } catch (Exception e) {
            future.completeExceptionally(new GetEventException(e.getMessage()));
        }
        return future;
    }

    @Override
    public CompletableFuture<TimelineEvent> last() {
        CompletableFuture<TimelineEvent> future = new CompletableFuture<>();

        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT id, type, data, auth, created FROM event WHERE next IS NULL")) {
                ResultSet result = statement.executeQuery();
                future.complete(parseEvent(result));
            }
        } catch (Exception e) {
            future.completeExceptionally(new GetEventException(e.getMessage()));
        }
        return future;
    }

    @Override
    public CompletableFuture<Void> add(TimelineEvent event) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        try (Connection connection = connectionFactory.get(fileName)) {
            connection.setAutoCommit(false);

            String prevEventId = null;

            try (PreparedStatement statement = connection.prepareStatement("SELECT id FROM event WHERE next IS NULL")) {
                ResultSet result = statement.executeQuery();
                if (result.next()) {
                    prevEventId = result.getString("id");
                }
            }

            if (prevEventId != null) {
                try (PreparedStatement statement = connection.prepareStatement("UPDATE event SET next = ? where id = ?")) {
                    statement.setString(1, event.getId());
                    statement.setString(2, prevEventId);
                    if (statement.executeUpdate() != 1) {
                        throw new CreateEventException("Failed to update prev event");
                    }
                }
            }

            insertEvent(connection, event, prevEventId, null);

            connection.commit();
            future.complete(null);
        } catch (Exception e) {
            future.completeExceptionally(e);
        }

        return future;
    }

    @Override
    public CompletableFuture<Void> insertBefore(TimelineEvent event, String eventId) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        if (eventId == null) {
            return this.add(event);
        }

        try (Connection connection = connectionFactory.get(fileName)) {
            connection.setAutoCommit(false);

            String prevEventId = null;
            String nextEventId = null;

            try (PreparedStatement statement = connection.prepareStatement("SELECT prev, next FROM event WHERE id = ?")) {
                statement.setString(1, eventId);
                ResultSet result = statement.executeQuery();
                if (result.next()) {
                    prevEventId = result.getString("prev");
                    nextEventId = result.getString("next");
                }
            }

            if (prevEventId != null) {
                try (PreparedStatement statement = connection.prepareStatement("UPDATE event SET next = ? where id = ?")) {
                    statement.setString(1, event.getId());
                    statement.setString(2, prevEventId);
                    if (statement.executeUpdate() != 1) {
                        throw new CreateEventException("Failed to update prev event");
                    }
                }
            }

            try (PreparedStatement statement = connection.prepareStatement("UPDATE event SET prev = ? where id = ?")) {
                statement.setString(1, event.getId());
                statement.setString(2, eventId);
                if (statement.executeUpdate() != 1) {
                    throw new CreateEventException("Failed to update next event");
                }
            }

            insertEvent(connection, event, prevEventId, eventId);

            connection.commit();
            future.complete(null);
        } catch (Exception e) {
            future.completeExceptionally(e);
        }

        return future;
    }

    @Override
    public CompletableFuture<Void> insertAfter(TimelineEvent event, String eventId) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        if (eventId == null) {
            return this.add(event);
        }

        synchronized (this) {
            try (Connection connection = connectionFactory.get(fileName)) {
                connection.setAutoCommit(false);

                String nextEventId = null;

                try (PreparedStatement statement = connection.prepareStatement("SELECT next FROM event WHERE id = ?")) {
                    statement.setString(1, eventId);
                    ResultSet result = statement.executeQuery();
                    if (result.next()) {
                        nextEventId = result.getString("next");
                    } else {
                        throw new CreateEventException("Failed to get event " + eventId);
                    }
                }

                try (PreparedStatement statement = connection.prepareStatement("UPDATE event SET next = ? where id = ?")) {
                    statement.setString(1, event.getId());
                    statement.setString(2, eventId);
                    if (statement.executeUpdate() != 1) {
                        throw new CreateEventException("Failed to update next event");
                    }
                }

                if (nextEventId != null) {
                    try (PreparedStatement statement = connection.prepareStatement("UPDATE event SET prev = ? where id = ?")) {
                        statement.setString(1, event.getId());
                        statement.setString(2, nextEventId);
                        if (statement.executeUpdate() != 1) {
                            throw new CreateEventException("Failed to update next event");
                        }
                    }
                }

                insertEvent(connection, event, eventId, nextEventId);

                connection.commit();
                future.complete(null);
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        }

        return future;
    }

    @Override
    public CompletableFuture<String> pop() {
        CompletableFuture<String> future = new CompletableFuture<>();

        synchronized (this) {
            try (Connection connection = connectionFactory.get(fileName)) {
                connection.setAutoCommit(false);

                String eventId = null;
                String prevEventId = null;
                try (PreparedStatement statement = connection.prepareStatement("SELECT id, prev FROM event WHERE next IS NULL")) {
                    ResultSet result = statement.executeQuery();
                    if (result.next()) {
                        eventId = result.getString("id");
                        prevEventId = result.getString("prev");
                    } else {
                        future.complete(null);
                        return future;
                    }
                }

                try (PreparedStatement statement = connection.prepareStatement("DELETE FROM event WHERE id = ?")) {
                    statement.setString(1, eventId);
                    if (statement.executeUpdate() != 1) {
                        throw new CreateEventException("Failed to delete event");
                    }
                }

                if (prevEventId != null) {
                    try (PreparedStatement statement = connection.prepareStatement("UPDATE event SET next = NULL where id = ?")) {
                        statement.setString(1, prevEventId);
                        if (statement.executeUpdate() != 1) {
                            throw new CreateEventException("Failed to update prev event");
                        }
                    }
                }

                connection.commit();
                future.complete(eventId);
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        }

        return future;
    }

    @Override
    public CompletableFuture<TimelineInfo> info() {
        CompletableFuture<TimelineInfo> future = new CompletableFuture<>();

        try {
            TimelineInfo info = new TimelineInfo();

            CompletableFuture<TimelineEvent> firstEvent = first();
            if (!firstEvent.isCompletedExceptionally() && firstEvent.get() != null) {
                info.setFirstEvent(firstEvent.get().getId());
            }

            CompletableFuture<TimelineEvent> lastEvent = last();
            if (!lastEvent.isCompletedExceptionally() && lastEvent.get() != null) {
                info.setLastEvent(lastEvent.get().getId());
            }

            future.complete(info);
        } catch (Exception e) {
            future.completeExceptionally(new GetEventException(e.getMessage()));
        }

        return future;
    }

}
