package ch.hsr.ebos.aion.data.sqlite.state.operations;

public class SqliteSequenceOperationData implements SqliteOperationData {
    private String name;

    public SqliteSequenceOperationData() {

    }

    public SqliteSequenceOperationData(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
