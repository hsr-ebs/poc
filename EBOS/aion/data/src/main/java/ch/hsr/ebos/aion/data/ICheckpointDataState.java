package ch.hsr.ebos.aion.data;

public interface ICheckpointDataState extends IDataState {
    void reset(String id) throws Exception;
}
