package ch.hsr.ebos.aion.data.sqlite.state.operations;

import java.util.UUID;

public class SqliteOperation<O extends SqliteOperationData> {
    private String id;
    private String className;
    private String type;
    private String eventId;
    private O data;

    public SqliteOperation(String className, String eventId, String type, O data) {
        this.id = UUID.randomUUID().toString();
        this.className = className;
        this.eventId = eventId;
        this.type = type;
        this.data = data;
    }

    public SqliteOperation() {

    }

    public O getData() {
        return data;
    }

    public void setData(O data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }
}
