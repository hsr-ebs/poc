package ch.hsr.ebos.aion.data.sqlite.state;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.hsr.ebos.aion.data.IDataState;
import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.aion.data.orm.EntityMetadata;
import ch.hsr.ebos.aion.data.orm.EntityMetadataField;
import ch.hsr.ebos.aion.data.orm.EntityMetadataService;
import ch.hsr.ebos.aion.data.orm.Sequence;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.logging.LoggingService;

@Singleton
public class SqliteDataState implements IDataState {

    protected Connection connection;

    @Inject
    protected SqliteConnectionFactory connectionFactory;

    @Inject
    @Named("sqliteFileName")
    protected String fileName;

    @Inject
    protected LoggingService log;

    @Inject
    protected SqliteSqlService sql;

    @Inject
    private Set<Entity> entities;

    @Inject
    private Set<Sequence> sequences;

    @Override
    public void init() throws Exception {
        try (Connection connection = connectionFactory.get(fileName)) {
            connection.setAutoCommit(false);
            for (Entity entity : entities) {
                EntityMetadata<?> metadata = new EntityMetadataService().getMetadata(entity.getClass());
                sql.createTable(connection, metadata);
                for (Map.Entry<String, List<EntityMetadataField>> unique : metadata.getUniqueFields().entrySet()) {
                    sql.createUniqueIndex(connection, metadata.getName(), unique.getKey(), unique.getValue());
                    log.debug("created unique index " + unique.getKey());
                }
            }

            sql.createSequenceTable(connection);
            for (Sequence sequence : sequences) {
                sql.insertSequence(connection, sequence.getName());
                log.debug("created sequence " + sequence.getName());
            }

            connection.commit();
        }
    }

    @Override
    public void close() throws SQLException {
        connectionFactory.close(fileName);
        connection = null;
    }

    @Override
    public void begin(String id) throws Exception {
        connection = connectionFactory.get(fileName);
        connection.setAutoCommit(false);
    }

    @Override
    public void commit(String id) throws SQLException {
        log.debug("commit for event " + id);
        connection.commit();
        connection.close();
        connection = null;
    }

    @Override
    public void rollback(String id) throws SQLException {
        log.debug("rollback for event " + id);
        connection.rollback();
        connection.close();
        connection = null;
    }

    @Override
    public void insert(EntityMetadata metadata, String id, Object object) throws Exception {
        if (connection != null) {
            log.debug("insert entity " + metadata.getName() + " with id " + id);
            sql.insertRecord(connection, metadata, object);
        } else {
            throw new Exception("it is not allowed to manipulate data outside of an application");
        }
    }

    @Override
    public void update(EntityMetadata metadata, String id, Object object) throws Exception {
        if (connection != null) {
            log.debug("update entity " + metadata.getName() + " with id " + id);
            sql.updateRecord(connection, metadata, id, object);
        } else {
            throw new Exception("it is not allowed to manipulate data outside of an application");
        }
    }

    @Override
    public void delete(EntityMetadata metadata, String id) throws Exception {
        if (connection != null) {
            log.debug("delete entity " + metadata.getName() + " with id " + id);
            sql.deleteRecord(connection, metadata, id);
        } else {
            throw new Exception("it is not allowed to manipulate data outside of an application");
        }
    }

    @Override
    public Integer getSequenceValue(String name) throws Exception {
        if (connection != null) {
            int value = sql.getSequence(connection, name, 1);
            log.debug("get sequence value " + value + " for " + name);
            return value;
        } else {
            throw new Exception("it is not allowed to manipulate data outside of an application");
        }
    }

    @Override
    public <E> List<E> query(EntityMetadata<E> metadata, Query query) throws Exception {
        log.debug("query entity " + metadata.getName());
        if (connection != null) {
            return sql.queryRecords(connection, metadata, query);
        } else {
            try (Connection connection = connectionFactory.get(fileName)) {
                return sql.queryRecords(connection, metadata, query);
            }
        }
    }

    @Override
    public <E> long count(EntityMetadata<E> metadata, Query query) throws Exception {
        log.debug("count entity " + metadata.getName());
        if (connection != null) {
            return sql.countRecords(connection, metadata, query);
        } else {
            try (Connection connection = connectionFactory.get(fileName)) {
                return sql.countRecords(connection, metadata, query);
            }
        }
    }

    @Override
    public <E> E get(EntityMetadata<E> metadata, String id) throws Exception {
        log.debug("get entity " + metadata.getName() + " with id " + id);
        if (connection != null) {
            return sql.selectRecord(connection, metadata, id);
        } else {
            try (Connection connection = connectionFactory.get(fileName)) {
                return sql.selectRecord(connection, metadata, id);
            }
        }
    }
}
