package ch.hsr.ebos.aion.data.orm;

import java.util.List;

import ch.hsr.ebos.aion.data.orm.annotation.Field;
import ch.hsr.ebos.aion.data.orm.util.ReflectionUtil;

public abstract class Entity {
    @Field
    private String id;

    public Entity() {

    }

    public Entity(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() != this.getClass()) {
            return super.equals(o);
        }

        List<java.lang.reflect.Field> fields = ReflectionUtil.getColumnFields(this.getClass());
        try {
            for (java.lang.reflect.Field field : fields) {
                Object value = field.get(this);
                Object otherValue = field.get(o);
                if (value == null && otherValue == null) {
                    continue;
                }
                if (value == null || otherValue == null) {
                    return false;
                }
                if (!value.equals(otherValue)) {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
