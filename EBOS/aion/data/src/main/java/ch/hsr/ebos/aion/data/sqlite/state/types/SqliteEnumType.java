package ch.hsr.ebos.aion.data.sqlite.state.types;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqliteEnumType extends SqliteType<Object> {
    @Override
    public String getTypeName() {
        return "varchar";
    }

    @Override
    public boolean canHandle(Class type) {
        return type.isEnum();
    }

    @Override
    public Object getValue(ResultSet resultSet, int column, Class type) throws SQLException {
        String value = resultSet.getString(column);
        if (resultSet.wasNull() || value == null || value.equals("null")) {
            return null;
        }
        return Enum.valueOf(type, value);
    }

    @Override
    public void setValue(PreparedStatement statement, int column, Object value) throws SQLException {
        if (value == null) {
            statement.setString(column, null);
        }
        statement.setString(column, String.valueOf(value));
    }
}
