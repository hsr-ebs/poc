package ch.hsr.ebos.aion.data.sqlite.timeline;

import java.io.File;
import java.sql.SQLException;

import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;

public abstract class SqliteFactory {
    protected SqliteConnectionFactory connectionFactory;
    private String databaseDirectory;

    public SqliteFactory(String databaseDirectory, SqliteConnectionFactory connectionFactory) {
        this.databaseDirectory = databaseDirectory;
        this.connectionFactory = connectionFactory;
    }

    protected String getDatabasePath(String name) {
        return this.databaseDirectory + "/" + getDatabaseName(name);
    }

    protected abstract String getDatabaseName(String name);

    protected String createDatabasePath(String name) {
        String fileName = getDatabasePath(name);
        new File(fileName).getParentFile().mkdirs();
        return fileName;
    }

    public void delete(String name) throws SQLException {
        String fileName = getDatabasePath(name);
        connectionFactory.delete(fileName);
    }

    public void close(String name) throws SQLException {
        String fileName = getDatabasePath(name);
        connectionFactory.close(fileName);
    }
}
