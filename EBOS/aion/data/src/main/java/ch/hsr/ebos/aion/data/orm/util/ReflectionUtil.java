package ch.hsr.ebos.aion.data.orm.util;

import java.util.ArrayList;
import java.util.List;

import ch.hsr.ebos.aion.data.orm.annotation.Field;

public class ReflectionUtil {
    public static List<java.lang.reflect.Field> getColumnFields(Class entityClass) {
        List<java.lang.reflect.Field> fields = new ArrayList<>();
        traverseColumnFields(fields, entityClass);
        return fields;
    }

    private static void traverseColumnFields(List<java.lang.reflect.Field> fields, Class entityClass) {
        if (entityClass.getSuperclass() != null && entityClass.getSuperclass() != Object.class) {
            traverseColumnFields(fields, entityClass.getSuperclass());
        }

        for (java.lang.reflect.Field declaredField : entityClass.getDeclaredFields()) {
            Field field = declaredField.getAnnotation(Field.class);

            if (field == null) {
                continue;
            }

            declaredField.setAccessible(true);
            fields.add(declaredField);
        }
    }
}
