package ch.hsr.ebos.aion.data.sqlite.connection;

public abstract class SqliteConnectionFactoryOptions {
    private String className;
    private String jdbcType;

    public SqliteConnectionFactoryOptions(String className, String jdbcType) {
        this.className = className;
        this.jdbcType = jdbcType;
    }

    public String getClassName() {
        return className;
    }

    public String getJdbcType() {
        return jdbcType;
    }
}
