package ch.hsr.ebos.aion.data.sqlite.state.operations;

public class SqliteUpdateOperationData implements SqliteOperationData {
    private String id;
    private String oldData;

    public SqliteUpdateOperationData() {

    }

    public SqliteUpdateOperationData(String id, String oldData) {
        this.id = id;
        this.oldData = oldData;
    }

    public String getOldData() {
        return oldData;
    }

    public void setOldData(String oldData) {
        this.oldData = oldData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
