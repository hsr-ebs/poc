package ch.hsr.ebos.aion.data.orm.query;

import java.util.Arrays;

public abstract class Filter {
    public static Filter equals(String field, Object value) {
        return new FieldFilter(field, value, FilterOperator.EQUALS);
    }

    public static Filter like(String field, Object value) {
        return new FieldFilter(field, value, FilterOperator.LIKE);
    }

    public static Filter or(Filter... filters) {
        return new FilterGroup(FilterLogicOperator.OR, Arrays.asList(filters));
    }

    public static Filter and(Filter... filters) {
        return new FilterGroup(FilterLogicOperator.AND, Arrays.asList(filters));
    }
}
