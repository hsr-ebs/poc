package ch.hsr.ebos.aion.data.sqlite.timeline;

import java.sql.Connection;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncState;
import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncStateFactory;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;

public class SqliteTimelineSyncStateFactory extends SqliteFactory implements ITimelineSyncStateFactory {
    public SqliteTimelineSyncStateFactory(String databaseDirectory, SqliteConnectionFactory connectionFactory) {
        super(databaseDirectory, connectionFactory);
    }

    @Override
    protected String getDatabaseName(String name) {
        return name + "-sync-state.db";
    }

    @Override
    public ITimelineSyncState get(String name) throws Exception {
        String fileName = createDatabasePath(name);
        try (Connection connection = connectionFactory.get(fileName)) {
            connection.setAutoCommit(false);
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS state (id varchar primary key, value varchar not null);");
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS info (lastPulled varchar, lastPushed varchar);");
            connection.commit();
            return new SqliteTimelineSyncState(connectionFactory, fileName);
        }
    }
}
