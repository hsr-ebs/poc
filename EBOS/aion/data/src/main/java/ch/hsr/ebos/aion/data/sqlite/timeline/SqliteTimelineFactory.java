package ch.hsr.ebos.aion.data.sqlite.timeline;

import java.sql.Connection;

import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;

public class SqliteTimelineFactory extends SqliteFactory implements ITimelineFactory<IDynamicTimeline> {
    public SqliteTimelineFactory(String databaseDirectory, SqliteConnectionFactory connectionFactory) {
        super(databaseDirectory, connectionFactory);
    }

    @Override
    protected String getDatabaseName(String name) {
        return name + "-timeline.db";
    }

    public IDynamicTimeline get(String name) throws Exception {
        String fileName = createDatabasePath(name);
        try (Connection connection = connectionFactory.get(fileName)) {
            connection.setAutoCommit(false);
            connection.createStatement().execute("CREATE TABLE IF NOT EXISTS event (id varchar primary key, next varchar, prev varchar, type varchar, data varchar, auth varchar, created int);");
            connection.commit();
            return new SqliteTimeline(connectionFactory, fileName);
        }
    }
}
