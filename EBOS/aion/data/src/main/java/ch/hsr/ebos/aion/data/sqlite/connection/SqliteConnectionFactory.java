package ch.hsr.ebos.aion.data.sqlite.connection;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class SqliteConnectionFactory {
    private Map<String, ConnectionLocking> connectionLocks = new HashMap<>();
    private SqliteConnectionFactoryOptions options;

    public SqliteConnectionFactory(SqliteConnectionFactoryOptions options) {
        this.options = options;
    }

    public Connection get(String fileName) throws Exception {
        synchronized (this) {
            String databaseUrl = options.getJdbcType() + ":" + fileName;

            if (connectionLocks.containsKey(fileName)) {
                ConnectionLocking lock = connectionLocks.get(fileName);
                return lock.acquire();
            }

            Class.forName(options.getClassName());
            ConnectionLocking connectionLock = new ConnectionLocking(databaseUrl);
            connectionLocks.put(fileName, connectionLock);

            return connectionLock.acquire();
        }
    }

    public void delete(String fileName) throws SQLException {
        close(fileName);
        new File(fileName).delete();
    }

    public void close(String fileName) throws SQLException {
        synchronized (this) {
            ConnectionLocking writePool = connectionLocks.get(fileName);
            if (writePool != null) {
                connectionLocks.remove(fileName);
                writePool.close();
            }
        }
    }
}
