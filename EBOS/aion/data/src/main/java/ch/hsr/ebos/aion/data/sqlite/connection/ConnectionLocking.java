package ch.hsr.ebos.aion.data.sqlite.connection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.Semaphore;

public class ConnectionLocking implements InvocationHandler {
    private Connection connection;
    private String databaseUrl;
    private Semaphore semaphore;

    public ConnectionLocking(String databaseUrl) {
        this.databaseUrl = databaseUrl;
        this.semaphore = new Semaphore(1);
    }

    @Override
    public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
        if (method.getName().equals("close")) {
            if (!connection.getAutoCommit()) {
                connection.rollback();
            }

            connection.setAutoCommit(true);
        }
        Object returnValue = method.invoke(connection, objects);
        if (method.getName().equals("close")) {
            this.release();
        }
        return returnValue;
    }

    public Connection acquire() throws Exception {
        semaphore.acquire();

        connection = DriverManager.getConnection(databaseUrl);

        return (Connection) Proxy.newProxyInstance(
                getClass().getClassLoader(),
                new Class[]{Connection.class},
                this);
    }

    private void release() throws SQLException {
        connection = null;
        semaphore.release();
    }

    public void close() throws SQLException {
        if (connection != null) {
            this.connection.close();
        }
    }
}