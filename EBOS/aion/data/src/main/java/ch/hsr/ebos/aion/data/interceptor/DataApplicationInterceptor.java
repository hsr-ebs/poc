package ch.hsr.ebos.aion.data.interceptor;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptor;
import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptorContext;
import ch.hsr.ebos.aion.data.IDataState;

public class DataApplicationInterceptor implements IApplicationInterceptor {
    @Inject
    IDataState dataState;

    @Override
    public void intercept(IApplicationInterceptorContext context) {
        try {
            dataState.init();

            context.next().whenComplete((r, ex) -> {
                try {
                    dataState.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            context.abort(e);
        }
    }
}
