package ch.hsr.ebos.aion.data.sqlite.timeline;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncState;
import ch.hsr.ebos.aion.core.synchronizer.SyncState;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;

public class SqliteTimelineSyncState implements ITimelineSyncState {
    private SqliteConnectionFactory connectionFactory;
    private String fileName;

    public SqliteTimelineSyncState(SqliteConnectionFactory connectionFactory, String fileName) {
        this.connectionFactory = connectionFactory;
        this.fileName = fileName;
    }

    @Override
    public SyncState get(String id) throws Exception {
        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT value FROM state WHERE id = ?")) {
                statement.setString(1, id);
                ResultSet result = statement.executeQuery();
                if (result.next()) {
                    return Enum.valueOf(SyncState.class, result.getString(1));
                }
            }
        }
        return null;
    }

    @Override
    public void set(String id, SyncState state) throws Exception {
        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT id FROM state WHERE id = ?")) {
                statement.setString(1, id);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    try (PreparedStatement updateStatement = connection.prepareStatement("UPDATE state SET value = ? WHERE id = ?")) {
                        updateStatement.setString(1, String.valueOf(state));
                        updateStatement.setString(2, id);
                        updateStatement.execute();
                    }
                } else {
                    try (PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO state (id, value) VALUES (?,?)")) {
                        insertStatement.setString(1, id);
                        insertStatement.setString(2, String.valueOf(state));
                        insertStatement.execute();
                    }
                }
            }

            String field = null;
            switch (state) {
                case PULLED:
                    field = "lastPulled";
                    break;
                case PUSHED:
                    field = "lastPushed";
                    break;
                default:
                    throw new Exception("unknown state");
            }

            try (PreparedStatement statement = connection.prepareStatement("SELECT " + field + " FROM info")) {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    try (PreparedStatement updateStatement = connection.prepareStatement("UPDATE info SET " + field + " = ?")) {
                        updateStatement.setString(1, id);
                        updateStatement.execute();
                    }
                } else {
                    try (PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO info (" + field + ") VALUES (?)")) {
                        insertStatement.setString(1, id);
                        insertStatement.execute();
                    }
                }
            }
        }
    }

    @Override
    public boolean isSynced(String id) throws Exception {
        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT value FROM state WHERE id = ?")) {
                statement.setString(1, id);
                ResultSet resultSet = statement.executeQuery();
                return resultSet.next();
            }
        }
    }

    @Override
    public String getLastPulled() throws Exception {
        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT lastPulled FROM info")) {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getString(1);
                }
            }
        }
        return null;
    }

    @Override
    public String getLastPushed() throws Exception {
        try (Connection connection = connectionFactory.get(fileName)) {
            try (PreparedStatement statement = connection.prepareStatement("SELECT lastPushed FROM info")) {
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next()) {
                    return resultSet.getString(1);
                }
            }
        }
        return null;
    }
}
