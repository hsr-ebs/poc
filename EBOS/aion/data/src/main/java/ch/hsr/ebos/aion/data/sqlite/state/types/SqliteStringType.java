package ch.hsr.ebos.aion.data.sqlite.state.types;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqliteStringType extends SqliteType<String> {
    @Override
    public String getTypeName() {
        return "varchar";
    }

    @Override
    public boolean canHandle(Class type) {
        return type == String.class;
    }

    @Override
    public String getValue(ResultSet resultSet, int column, Class type) throws SQLException {
        return resultSet.getString(column);
    }

    @Override
    public void setValue(PreparedStatement statement, int column, String value) throws SQLException {
        statement.setString(column, value);
    }
}
