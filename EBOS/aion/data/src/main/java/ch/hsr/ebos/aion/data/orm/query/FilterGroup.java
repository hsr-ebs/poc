package ch.hsr.ebos.aion.data.orm.query;

import java.util.ArrayList;
import java.util.List;

public class FilterGroup extends Filter {
    private FilterLogicOperator operator;
    private List<Filter> filters;

    public FilterGroup(FilterLogicOperator operator, List<Filter> filters) {
        this.operator = operator;
        this.filters = filters;
    }

    public FilterGroup(FilterLogicOperator operator) {
        this(operator, new ArrayList<>());
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public FilterLogicOperator getOperator() {
        return operator;
    }
}
