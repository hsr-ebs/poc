package ch.hsr.ebos.aion.data.memory.timeline;

import java.util.HashMap;
import java.util.Map;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncState;
import ch.hsr.ebos.aion.core.synchronizer.SyncState;

public class MemoryTimelineSyncState implements ITimelineSyncState {
    private Map<String, SyncState> states = new HashMap<>();
    private String lastPulled = null;
    private String lastPushed = null;

    @Override
    public SyncState get(String id) {
        return states.get(id);
    }

    @Override
    public void set(String id, SyncState state) {
        states.put(id, state);
        if (state == SyncState.PULLED) {
            lastPulled = id;
        } else {
            lastPushed = id;
        }
    }

    @Override
    public boolean isSynced(String id) {
        return states.containsKey(id);
    }

    @Override
    public String getLastPulled() {
        return lastPulled;
    }

    @Override
    public String getLastPushed() {
        return lastPushed;
    }
}
