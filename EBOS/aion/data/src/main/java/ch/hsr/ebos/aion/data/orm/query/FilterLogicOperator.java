package ch.hsr.ebos.aion.data.orm.query;

public enum FilterLogicOperator {
    AND("AND"), OR("OR");

    private String value;

    FilterLogicOperator(String value) {
        this.value = value;
    }

    public String toString() {
        return value;
    }
}
