package ch.hsr.ebos.aion.data.sqlite.state;

import com.google.inject.name.Names;

import ch.hsr.ebos.aion.core.processor.ProcessResetEvent;
import ch.hsr.ebos.aion.data.DataModule;
import ch.hsr.ebos.aion.data.ICheckpointDataState;
import ch.hsr.ebos.aion.data.IDataState;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.aggregate.ProcessResetEventAggregate;

public class SqliteDataModule extends DataModule {
    private String fileName;
    private boolean useCheckpoint;
    private SqliteConnectionFactory sqliteConnectionFactory;

    public SqliteDataModule(SqliteConnectionFactory sqliteConnectionFactory, String fileName, boolean useCheckpoint) {
        this.fileName = fileName;
        this.sqliteConnectionFactory = sqliteConnectionFactory;
        this.useCheckpoint = useCheckpoint;
    }

    public SqliteDataModule(SqliteConnectionFactoryOptions options, String fileName, boolean useCheckpoint) {
        this.fileName = fileName;
        this.sqliteConnectionFactory = new SqliteConnectionFactory(options);
        this.useCheckpoint = useCheckpoint;
    }

    @Override
    protected void setup() {
        super.setup();
        bindConstant().annotatedWith(Names.named("sqliteFileName")).to(fileName);
        bind(SqliteConnectionFactory.class).toInstance(sqliteConnectionFactory);
        bind(SqliteSqlService.class);
        if (useCheckpoint) {
            bind(IDataState.class).to(SqliteCheckpointDataState.class);
            bind(ICheckpointDataState.class).to(SqliteCheckpointDataState.class);
            addEventAggregate(ProcessResetEvent.class, ProcessResetEventAggregate.class);
        } else {
            bind(IDataState.class).to(SqliteDataState.class);
        }
    }
}