package ch.hsr.ebos.aion.data.orm.query;

public class QueryOrder {
    private String field;
    private QueryOrderDirection direction;
    private boolean caseSensitive;

    public QueryOrder(String field, QueryOrderDirection direction, boolean caseSensitive) {
        this.field = field;
        this.direction = direction;
        this.caseSensitive = caseSensitive;
    }

    public QueryOrder(String field) {
        this(field, QueryOrderDirection.ASC, true);
    }

    public static QueryOrder asc(String field) {
        return asc(field, true);
    }

    public static QueryOrder asc(String field, boolean caseSensitive) {
        return new QueryOrder(field, QueryOrderDirection.ASC, caseSensitive);
    }

    public static QueryOrder desc(String field) {
        return desc(field, true);
    }

    public static QueryOrder desc(String field, boolean caseSensitive) {
        return new QueryOrder(field, QueryOrderDirection.DESC, caseSensitive);
    }

    public String getField() {
        return field;
    }

    public QueryOrderDirection getDirection() {
        return direction;
    }

    public boolean isCaseSensitive() {
        return caseSensitive;
    }
}
