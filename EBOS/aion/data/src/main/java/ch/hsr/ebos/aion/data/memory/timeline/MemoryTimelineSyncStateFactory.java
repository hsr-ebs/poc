package ch.hsr.ebos.aion.data.memory.timeline;

import java.util.HashMap;
import java.util.Map;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncState;
import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncStateFactory;

public class MemoryTimelineSyncStateFactory implements ITimelineSyncStateFactory {
    private Map<String, MemoryTimelineSyncState> states = new HashMap<>();

    @Override
    public ITimelineSyncState get(String name) {
        if (!states.containsKey(name)) {
            states.put(name, new MemoryTimelineSyncState());
        }

        return states.get(name);
    }

    @Override
    public void delete(String name) {
        states.remove(name);
    }

    @Override
    public void close(String name) {
        states.remove(name);
    }
}
