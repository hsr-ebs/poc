package ch.hsr.ebos.aion.data.orm;

import java.lang.reflect.Field;

public class EntityMetadataField {
    private Field field;
    private boolean required;
    private String name;

    public EntityMetadataField(Field field) {
        this.field = field;
    }

    public Field getField() {
        return field;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue(Object object) throws IllegalAccessException {
        return field.get(object);
    }

    public Class getType() {
        return field.getType();
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }
}
