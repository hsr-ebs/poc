package ch.hsr.ebos.aion.data.orm.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Query {
    private FilterGroup filter = new FilterGroup(FilterLogicOperator.AND);
    private List<QueryOrder> orders = new ArrayList<>();
    private Integer limit = null;
    private Integer skip = null;

    public static Query build() {
        return new Query();
    }

    public Query and(Filter... filters) {
        filter.getFilters().add(Filter.and(filters));
        return this;
    }

    public Query or(Filter... filters) {
        filter.getFilters().add(Filter.or(filters));
        return this;
    }

    public Query limit(int number) {
        this.limit = number;
        return this;
    }

    public Query skip(int number) {
        this.skip = number;
        return this;
    }

    public Query orderBy(QueryOrder... sort) {
        this.orders.addAll(Arrays.asList(sort));
        return this;
    }

    public Integer getSkip() {
        return skip;
    }

    public Integer getLimit() {
        return limit;
    }

    public List<QueryOrder> getOrders() {
        return orders;
    }

    public FilterGroup getFilter() {
        return filter;
    }
}
