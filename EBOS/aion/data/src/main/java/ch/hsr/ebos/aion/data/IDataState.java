package ch.hsr.ebos.aion.data;

import java.util.List;

import ch.hsr.ebos.aion.data.orm.EntityMetadata;
import ch.hsr.ebos.aion.data.orm.query.Query;

public interface IDataState {
    void init() throws Exception;

    void close() throws Exception;

    void begin(String id) throws Exception;

    void commit(String id) throws Exception;

    void rollback(String id) throws Exception;

    void insert(EntityMetadata metadata, String id, Object object) throws Exception;

    <E> E get(EntityMetadata<E> metadata, String id) throws Exception;

    void update(EntityMetadata metadata, String id, Object object) throws Exception;

    void delete(EntityMetadata metadata, String id) throws Exception;

    <E> List<E> query(EntityMetadata<E> metadata, Query query) throws Exception;

    <E> long count(EntityMetadata<E> metadata, Query query) throws Exception;

    Integer getSequenceValue(String name) throws Exception;
}
