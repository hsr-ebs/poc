package ch.hsr.ebos.aion.data.sqlite.state.types;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqliteIntegerType extends SqliteType<Integer> {
    @Override
    public String getTypeName() {
        return "int";
    }

    @Override
    public boolean canHandle(Class type) {
        return type == int.class || type == Integer.class;
    }

    @Override
    public Integer getValue(ResultSet resultSet, int column, Class type) throws SQLException {
        return resultSet.getInt(column);
    }

    @Override
    public void setValue(PreparedStatement statement, int column, Integer value) throws SQLException {
        statement.setInt(column, value);
    }
}
