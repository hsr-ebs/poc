package ch.hsr.ebos.aion.data.sqlite.state.types;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;

public class SqliteDateType extends SqliteType<Date> {
    @Override
    public String getTypeName() {
        return "int";
    }

    @Override
    public boolean canHandle(Class type) {
        return type == Date.class;
    }

    @Override
    public Date getValue(ResultSet resultSet, int column, Class type) throws SQLException {
        return new Date(resultSet.getLong(column));
    }

    @Override
    public void setValue(PreparedStatement statement, int column, Date value) throws SQLException {
        if(value != null) {
            statement.setLong(column, value.getTime());
        } else {
            statement.setNull(column, Types.BIGINT);
        }
    }
}
