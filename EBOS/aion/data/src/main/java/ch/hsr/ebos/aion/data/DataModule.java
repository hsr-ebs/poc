package ch.hsr.ebos.aion.data;

import java.util.ArrayList;
import java.util.List;

import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.aion.data.interceptor.DataApplicationInterceptor;
import ch.hsr.ebos.aion.data.interceptor.DataEventInterceptor;
import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.aion.data.orm.EntityMetadataService;
import ch.hsr.ebos.aion.data.orm.EntityService;
import ch.hsr.ebos.aion.data.orm.Sequence;
import ch.hsr.ebos.aion.logging.LogLevel;
import ch.hsr.ebos.aion.logging.LoggingModule;

public abstract class DataModule extends Module {
    private List<Class> entities = new ArrayList<>();
    private List<Class> sequences = new ArrayList<>();

    public DataModule() {
        addModule(new LoggingModule(LogLevel.DEBUG));
    }

    @Override
    protected void setup() {
        addEventInterceptors(DataEventInterceptor.class);
        addApplicationInterceptors(DataApplicationInterceptor.class);
        bind(EntityService.class);
        bind(EntityMetadataService.class);
    }

    @Override
    protected void configure() {
        this.ensureMultiBinder(Entity.class);
        this.ensureMultiBinder(Sequence.class);

        for (Class entity : entities) {
            bindMultiple(Entity.class, entity);
        }

        for (Class sequence : sequences) {
            bindMultiple(Sequence.class, sequence);
        }

        super.configure();
    }

    public <E extends Entity> void addEntity(Class<E> entity) {
        entities.add(entity);
    }

    public <S extends Sequence> void addSequence(Class<S> sequence) {
        sequences.add(sequence);
    }
}
