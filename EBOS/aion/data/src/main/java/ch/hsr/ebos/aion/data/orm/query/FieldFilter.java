package ch.hsr.ebos.aion.data.orm.query;

public class FieldFilter extends Filter {
    private String field;
    private Object value;
    private FilterOperator operator;

    public FieldFilter(String field, Object value, FilterOperator operator) {
        this.field = field;
        this.value = value;
        this.operator = operator;
    }

    public String getField() {
        return field;
    }

    public Object getValue() {
        return value;
    }

    public FilterOperator getOperator() {
        return operator;
    }
}
