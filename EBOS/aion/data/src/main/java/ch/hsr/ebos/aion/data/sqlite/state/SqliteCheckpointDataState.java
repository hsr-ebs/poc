package ch.hsr.ebos.aion.data.sqlite.state;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import ch.hsr.ebos.aion.core.util.JsonUtil;
import ch.hsr.ebos.aion.data.ICheckpointDataState;
import ch.hsr.ebos.aion.data.orm.EntityMetadata;
import ch.hsr.ebos.aion.data.orm.EntityMetadataService;
import ch.hsr.ebos.aion.data.sqlite.state.operations.SqliteDeleteOperationData;
import ch.hsr.ebos.aion.data.sqlite.state.operations.SqliteInsertOperationData;
import ch.hsr.ebos.aion.data.sqlite.state.operations.SqliteOperation;
import ch.hsr.ebos.aion.data.sqlite.state.operations.SqliteOperationData;
import ch.hsr.ebos.aion.data.sqlite.state.operations.SqliteSequenceOperationData;
import ch.hsr.ebos.aion.data.sqlite.state.operations.SqliteUpdateOperationData;

@Singleton
public class SqliteCheckpointDataState extends SqliteDataState implements ICheckpointDataState {
    @Inject
    EntityMetadataService entityMetadata;
    private String currentEventId;

    @Override
    public void reset(String id) throws Exception {
        SqliteOperation operation = getLastOperation();

        if (id == null || !existsOperation(id)) {
            return;
        }

        boolean foundEvent = false;
        while (operation != null) {
            if (operation.getEventId().equals(id)) {
                foundEvent = true;
            } else if (foundEvent) {
                break;
            }

            EntityMetadata<?> metadata = getMetadataForClass(operation.getClassName());

            if (operation.getData() instanceof SqliteSequenceOperationData) {
                SqliteSequenceOperationData sequenceData = ((SqliteSequenceOperationData) operation.getData());
                sql.getSequence(connection, sequenceData.getName(), -1);
            } else if (operation.getData() instanceof SqliteDeleteOperationData) {
                SqliteDeleteOperationData deleteData = ((SqliteDeleteOperationData) operation.getData());
                Object object = JsonUtil.getGson().fromJson(deleteData.getData(), metadata.getType());
                super.insert(metadata, deleteData.getId(), object);
            } else if (operation.getData() instanceof SqliteUpdateOperationData) {
                SqliteUpdateOperationData updateData = ((SqliteUpdateOperationData) operation.getData());
                Object object = JsonUtil.getGson().fromJson(updateData.getOldData(), metadata.getType());
                super.update(metadata, updateData.getId(), object);
            } else if (operation.getData() instanceof SqliteInsertOperationData) {
                SqliteInsertOperationData insertData = ((SqliteInsertOperationData) operation.getData());
                super.delete(metadata, insertData.getId());
            }

            popOperation();

            operation = getLastOperation();
        }
    }


    private EntityMetadata<?> getMetadataForClass(String className) throws IllegalAccessException {
        try {
            Class<?> entityClass = Class.forName(className);
            return entityMetadata.getMetadata(entityClass);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    @Override
    public void init() throws Exception {
        super.init();

        try (Connection connection = connectionFactory.get(fileName)) {
            connection.setAutoCommit(false);
            sql.createTable(connection, "operations",
                    "id varchar primary key",
                    "type varchar not null",
                    "eventId varchar not null",
                    "className varchar not null",
                    "prev varchar",
                    "next varchar",
                    "data varchar not null");

            connection.commit();
        }
    }

    private void addOperation(SqliteOperation<?> operation) throws Exception {
        addOperation(UUID.randomUUID().toString(), operation);
    }

    private boolean existsOperation(String eventId) throws SQLException {
        String sql = "SELECT id FROM operations WHERE eventId = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, eventId);
            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        }
    }

    public void popOperation() throws SQLException {
        String prevId = null;
        String id = null;

        String sql = "SELECT id, prev FROM operations WHERE next is null";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                id = resultSet.getString(1);
                prevId = resultSet.getString(2);
            }
        }

        if (id != null) {
            sql = "DELETE FROM operations WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, id);
                statement.execute();
            }
        }

        if (prevId != null) {
            sql = "UPDATE operations SET next = null WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, prevId);
                statement.execute();
            }
        }
    }

    public SqliteOperation getLastOperation() throws Exception {
        String sql = "SELECT id, eventId, className, type, data FROM operations WHERE next is null";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                return null;
            }

            SqliteOperation operation = new SqliteOperation();
            operation.setId(resultSet.getString(1));
            operation.setEventId(resultSet.getString(2));
            operation.setClassName(resultSet.getString(3));
            operation.setType(resultSet.getString(4));

            Class<? extends SqliteOperationData> operationDataType;
            switch (operation.getType()) {
                case "insert":
                    operationDataType = SqliteInsertOperationData.class;
                    break;
                case "update":
                    operationDataType = SqliteUpdateOperationData.class;
                    break;
                case "delete":
                    operationDataType = SqliteDeleteOperationData.class;
                    break;
                case "sequence_decrement":
                    operationDataType = SqliteSequenceOperationData.class;
                    break;
                default:
                    throw new Exception("unknown operation type " + operation.getType());
            }

            SqliteOperationData data = JsonUtil.getGson().fromJson(resultSet.getString(5), operationDataType);
            operation.setData(data);

            return operation;
        }
    }

    private void addOperation(String id, SqliteOperation<?> operation) throws Exception {
        if (operation.getEventId() == null) {
            throw new Exception("can not add operation without a current event");
        }

        String prevId = null;
        String sql = "SELECT id FROM operations WHERE next is null";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                prevId = resultSet.getString(1);
            }
        }

        sql = "INSERT INTO operations (id, className, eventId, type, prev, next, data) VALUES (?, ?, ?, ?, ?, null, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, operation.getClassName());
            statement.setString(3, operation.getEventId());
            statement.setString(4, operation.getType());
            statement.setString(5, prevId);
            statement.setString(6, JsonUtil.getGson().toJson(operation.getData()));
            statement.execute();
        }

        if (prevId != null) {
            sql = "UPDATE operations SET next = ? WHERE id = ?";
            try (PreparedStatement statement = connection.prepareStatement(sql)) {
                statement.setString(1, id);
                statement.setString(2, prevId);
                statement.execute();
            }
        }
    }

    @Override
    public void begin(String id) throws Exception {
        super.begin(id);
        currentEventId = id;
    }

    @Override
    public void commit(String id) throws SQLException {
        super.commit(id);
        currentEventId = null;
    }

    @Override
    public void rollback(String id) throws SQLException {
        super.rollback(id);
        currentEventId = null;
    }

    @Override
    public void insert(EntityMetadata metadata, String id, Object object) throws Exception {
        SqliteInsertOperationData insertData = new SqliteInsertOperationData(id);

        SqliteOperation operation = new SqliteOperation(metadata.getType().getName(), currentEventId, "insert", insertData);
        addOperation(operation);

        super.insert(metadata, id, object);
    }

    @Override
    public void update(EntityMetadata metadata, String id, Object object) throws Exception {
        Gson gson = JsonUtil.getGson();
        String oldData = gson.toJson(get(metadata, id));
        SqliteUpdateOperationData updateData = new SqliteUpdateOperationData(id, oldData);

        SqliteOperation operation = new SqliteOperation(metadata.getType().getName(), currentEventId, "update", updateData);
        addOperation(operation);

        super.update(metadata, id, object);
    }

    @Override
    public void delete(EntityMetadata metadata, String id) throws Exception {
        Gson gson = JsonUtil.getGson();
        String oldData = gson.toJson(get(metadata, id));
        SqliteDeleteOperationData deleteData = new SqliteDeleteOperationData(id, oldData);

        SqliteOperation operation = new SqliteOperation(metadata.getType().getName(), currentEventId, "delete", deleteData);
        addOperation(operation);

        super.delete(metadata, id);
    }

    @Override
    public Integer getSequenceValue(String name) throws Exception {
        SqliteSequenceOperationData sequenceData = new SqliteSequenceOperationData(name);
        SqliteOperation operation = new SqliteOperation("", currentEventId, "sequence_decrement", sequenceData);
        addOperation(operation);
        return super.getSequenceValue(name);
    }
}
