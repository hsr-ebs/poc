package ch.hsr.ebos.aion.data.sqlite.connection;

public class AndroidSqliteConnectionFactoryOptions extends SqliteConnectionFactoryOptions {
    public AndroidSqliteConnectionFactoryOptions() {
        super("org.sqldroid.SQLDroidDriver", "jdbc:sqldroid");
    }
}
