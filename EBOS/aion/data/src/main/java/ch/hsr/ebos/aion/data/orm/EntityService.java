package ch.hsr.ebos.aion.data.orm;

import com.google.inject.Inject;

import java.util.List;

import ch.hsr.ebos.aion.data.IDataState;
import ch.hsr.ebos.aion.data.orm.query.Query;

public class EntityService {
    @Inject
    IDataState dataState;

    @Inject
    EntityMetadataService entityMetadata;

    public <E extends Entity> E get(Class<E> entityClass, String id) throws Exception {
        EntityMetadata<E> metadata = entityMetadata.getMetadata(entityClass);
        E result = dataState.get(metadata, id);
        return result;
    }

    public void insert(Entity entity) throws Exception {
        EntityMetadata metadata = entityMetadata.getMetadata(entity.getClass());
        dataState.insert(metadata, entity.getId(), entity);
    }

    public void update(Entity entity) throws Exception {
        EntityMetadata metadata = entityMetadata.getMetadata(entity.getClass());
        dataState.update(metadata, entity.getId(), entity);
    }

    public <E extends Entity> void delete(Class<E> entityClass, String id) throws Exception {
        EntityMetadata metadata = entityMetadata.getMetadata(entityClass);
        dataState.delete(metadata, id);
    }

    public <E extends Entity> List<E> query(Class<E> entityClass, Query query) throws Exception {
        EntityMetadata metadata = entityMetadata.getMetadata(entityClass);
        List<E> result = dataState.query(metadata, query);
        return result;
    }

    public <E extends Entity> long count(Class<E> entityClass, Query query) throws Exception {
        EntityMetadata metadata = entityMetadata.getMetadata(entityClass);
        return dataState.count(metadata, query);
    }
}
