package ch.hsr.ebos.aion.data.orm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

import ch.hsr.ebos.aion.data.orm.annotation.Field;
import ch.hsr.ebos.aion.data.orm.annotation.Required;
import ch.hsr.ebos.aion.data.orm.annotation.Unique;
import ch.hsr.ebos.aion.data.orm.util.ReflectionUtil;

@Singleton
public class EntityMetadataService {
    private Map<Class, EntityMetadata> metadataCache = new HashMap<>();

    private void traverseFields(List<EntityMetadataField> fields, Map<String, List<EntityMetadataField>> uniqueFields, Class entityClass) throws IllegalAccessException {
        List<java.lang.reflect.Field> declaredFields = ReflectionUtil.getColumnFields(entityClass);
        for (java.lang.reflect.Field declaredField : declaredFields) {
            Field column = declaredField.getAnnotation(Field.class);
            Required required = declaredField.getAnnotation(Required.class);
            Unique unique = declaredField.getAnnotation(Unique.class);

            EntityMetadataField field = new EntityMetadataField(declaredField);

            field.setName(column.name().length() > 0 ? column.name() : declaredField.getName());
            field.setRequired(required != null);

            if (unique != null) {
                String name = unique.name().length() > 0 ? unique.name() : declaredField.getName();
                if (!uniqueFields.containsKey(name)) {
                    uniqueFields.put(name, new ArrayList<>());
                }
                uniqueFields.get(name).add(field);
            }

            fields.add(field);
        }
    }

    public <E> EntityMetadata<E> getMetadata(Class<E> entityClass) throws IllegalAccessException {
        if (metadataCache.containsKey(entityClass)) {
            return metadataCache.get(entityClass);
        }

        List<EntityMetadataField> fields = new ArrayList<>();
        Map<String, List<EntityMetadataField>> uniqueFields = new HashMap<>();

        traverseFields(fields, uniqueFields, entityClass);
        EntityMetadata metadata = new EntityMetadata(entityClass, entityClass.getSimpleName(), fields, uniqueFields);
        metadataCache.put(entityClass, metadata);
        return metadata;
    }
}
