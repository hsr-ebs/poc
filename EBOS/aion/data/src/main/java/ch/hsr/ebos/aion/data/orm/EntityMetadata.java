package ch.hsr.ebos.aion.data.orm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntityMetadata<E> {
    private String name;
    private Class<E> type;
    private List<EntityMetadataField> fields = new ArrayList<>();
    private Map<String, List<EntityMetadataField>> uniqueFields = new HashMap<>();

    public EntityMetadata() {

    }

    public EntityMetadata(Class type, String name, List<EntityMetadataField> fields, Map<String, List<EntityMetadataField>> uniqueFields) {
        this.type = type;
        this.name = name;
        this.fields = fields;
        this.uniqueFields = uniqueFields;
    }

    public String getName() {
        return name;
    }

    public List<EntityMetadataField> getFields() {
        return fields;
    }

    public Class<E> getType() {
        return type;
    }

    public Map<String, List<EntityMetadataField>> getUniqueFields() {
        return uniqueFields;
    }
}
