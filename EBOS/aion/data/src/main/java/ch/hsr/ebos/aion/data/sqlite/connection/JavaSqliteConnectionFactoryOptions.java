package ch.hsr.ebos.aion.data.sqlite.connection;

public class JavaSqliteConnectionFactoryOptions extends SqliteConnectionFactoryOptions {
    public JavaSqliteConnectionFactoryOptions() {
        super("org.sqlite.JDBC", "jdbc:sqlite");
    }
}
