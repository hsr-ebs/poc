package ch.hsr.ebos.aion.data.orm.query;

public enum QueryOrderDirection {
    ASC("ASC"), DESC("DESC");

    private String value;

    QueryOrderDirection(String value) {
        this.value = value;
    }

    public String toString() {
        return value;
    }
}
