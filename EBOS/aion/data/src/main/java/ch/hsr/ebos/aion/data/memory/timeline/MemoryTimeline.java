package ch.hsr.ebos.aion.data.memory.timeline;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.exception.DuplicateEventException;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.TimelineEvent;
import ch.hsr.ebos.aion.core.timeline.TimelineInfo;


public class MemoryTimeline implements ITimeline, IDynamicTimeline {
    private String first;
    private String last;
    private Map<String, MemoryEvent> events = new HashMap<>();

    @Override
    public CompletableFuture<TimelineEvent> get(String id) {
        CompletableFuture<TimelineEvent> future = new CompletableFuture<>();
        MemoryEvent event = events.get(id);
        if (event != null) {
            future.complete(event.event);
        } else {
            future.complete(null);
        }
        return future;
    }

    @Override
    public CompletableFuture<TimelineEvent> prev(String id) {
        CompletableFuture<TimelineEvent> future = new CompletableFuture<>();
        synchronized (this) {
            MemoryEvent event = events.get(id);
            if (event == null) {
                future.complete(null);
                return future;
            }
            String prev = event.prev;
            if (prev == null) {
                future.complete(null);
                return future;
            }
            future.complete(events.get(prev).event);
        }
        return future;
    }

    @Override
    public CompletableFuture<TimelineEvent> next(String id) {
        if (id == null) {
            return first();
        }

        CompletableFuture<TimelineEvent> future = new CompletableFuture<>();
        synchronized (this) {
            MemoryEvent event = events.get(id);
            if (event == null) {
                future.complete(null);
                return future;
            }
            String next = event.next;
            if (next == null) {
                future.complete(null);
                return future;
            }
            future.complete(events.get(next).event);
        }
        return future;
    }

    @Override
    public CompletableFuture<TimelineEvent> first() {
        if (first == null) {
            return CompletableFuture.completedFuture(null);
        }
        return CompletableFuture.completedFuture(events.get(first).event);
    }

    @Override
    public CompletableFuture<TimelineEvent> last() {
        if (last == null) {
            return CompletableFuture.completedFuture(null);
        }
        return CompletableFuture.completedFuture(events.get(last).event);
    }

    @Override
    public CompletableFuture<Void> add(TimelineEvent event) {
        return insertBefore(event, null);
    }

    @Override
    public CompletableFuture<TimelineInfo> info() {
        return CompletableFuture.completedFuture(new TimelineInfo(first, last));
    }

    @Override
    public CompletableFuture<Void> insertBefore(TimelineEvent event, String beforeId) {
        CompletableFuture<Void> future = new CompletableFuture();
        synchronized (this) {
            if (events.containsKey(event.getId())) {
                future.completeExceptionally(new DuplicateEventException(event.getId()));
                return future;
            }

            if (beforeId == null) {
                insertAfter(event, null);
            } else {
                MemoryEvent nextEvent = events.get(beforeId);
                if (nextEvent == null) {
                    future.completeExceptionally(new Exception("before event id " + beforeId + " not found"));
                    return future;
                }

                events.put(event.getId(), new MemoryEvent(nextEvent.prev, beforeId, event));
                if (nextEvent.prev != null) {
                    events.get(nextEvent.prev).next = event.getId();
                } else {
                    first = event.getId();
                }
                nextEvent.prev = event.getId();
            }


            future.complete(null);
        }

        return future;
    }

    @Override
    public CompletableFuture<Void> insertAfter(TimelineEvent event, String afterId) {
        CompletableFuture<Void> future = new CompletableFuture();
        synchronized (this) {
            if (events.containsKey(event.getId())) {
                future.completeExceptionally(new DuplicateEventException(event.getId()));
                return future;
            }
        }

        if (afterId == null) {
            if (first == null) {
                first = event.getId();
                last = event.getId();
                events.put(event.getId(), new MemoryEvent(null, null, event));
            } else {
                events.get(last).next = event.getId();
                events.put(event.getId(), new MemoryEvent(last, null, event));
                last = event.getId();
            }
        } else {
            MemoryEvent prevEvent = events.get(afterId);
            if (prevEvent == null) {
                future.completeExceptionally(new Exception("after event id " + afterId + " not found"));
                return future;
            }

            events.put(event.getId(), new MemoryEvent(afterId, prevEvent.next, event));

            if (prevEvent.next == null) {
                last = event.getId();
            }
            prevEvent.next = event.getId();
        }
        future.complete(null);

        return future;
    }

    @Override
    public CompletableFuture<String> pop() {
        CompletableFuture<String> future = new CompletableFuture();

        if (last == null) {
            future.complete(null);
            return future;
        }

        MemoryEvent lastEvent = events.get(last);
        events.remove(last);
        if (lastEvent.prev != null) {
            events.get(lastEvent.prev).next = null;
        } else {
            first = null;
        }
        last = lastEvent.prev;
        future.complete(lastEvent.event.getId());

        return future;
    }

    class MemoryEvent {
        private String prev;
        private String next;
        private TimelineEvent event;

        public MemoryEvent(String prev, String next, TimelineEvent event) {
            this.prev = prev;
            this.next = next;
            this.event = event;
        }
    }
}
