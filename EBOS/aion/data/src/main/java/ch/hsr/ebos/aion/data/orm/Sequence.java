package ch.hsr.ebos.aion.data.orm;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.data.IDataState;

public abstract class Sequence {
    @Inject
    IDataState dataState;
    private String name;

    public Sequence() {
        this.name = getClass().getSimpleName();
    }

    public String getName() {
        return name;
    }

    public Integer getNext() throws Exception {
        return dataState.getSequenceValue(this.name);
    }
}
