package ch.hsr.ebos.aion.data.aggregate;

import com.google.inject.Inject;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregate;
import ch.hsr.ebos.aion.core.processor.ProcessResetEvent;
import ch.hsr.ebos.aion.data.ICheckpointDataState;

public class ProcessResetEventAggregate extends EventAggregate<ProcessResetEvent> {
    @Inject
    private ICheckpointDataState dataState;

    @Override
    public void aggregate(ApplicationEvent<ProcessResetEvent> event) throws Throwable {
        dataState.reset(event.getData().getEventId());
    }
}
