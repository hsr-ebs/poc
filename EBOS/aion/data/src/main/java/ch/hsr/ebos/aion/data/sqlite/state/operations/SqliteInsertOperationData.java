package ch.hsr.ebos.aion.data.sqlite.state.operations;

public class SqliteInsertOperationData implements SqliteOperationData {
    private String id;

    public SqliteInsertOperationData() {

    }

    public SqliteInsertOperationData(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
