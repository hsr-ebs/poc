package ch.hsr.ebos.aion.data.interceptor;

import com.google.inject.Inject;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;
import ch.hsr.ebos.aion.data.IDataState;

public class DataEventInterceptor implements IEventInterceptor {
    @Inject
    IDataState dataState;

    @Override
    public void intercept(IEventInterceptorContext ctx) {
        try {
            dataState.begin(ctx.getEvent().getId());
        } catch (Exception e) {
            ctx.abort(e);
        }

        ctx.next(result -> {
            CompletableFuture<Void> future = new CompletableFuture<>();

            try {
                if (result.isSuccessfull()) {
                    dataState.commit(ctx.getEvent().getId());
                } else {
                    dataState.rollback(ctx.getEvent().getId());
                }
                future.complete(null);
            } catch (Exception e) {
                future.completeExceptionally(e);
            }

            return future;
        });
    }
}
