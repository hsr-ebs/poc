package ch.hsr.ebos.aion.data.sqlite.state.types;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqliteBooleanType extends SqliteType<Boolean> {
    @Override
    public String getTypeName() {
        return "bool";
    }

    @Override
    public boolean canHandle(Class type) {
        return type == boolean.class;
    }

    @Override
    public Boolean getValue(ResultSet resultSet, int column, Class type) throws SQLException {
        return resultSet.getBoolean(column);
    }

    @Override
    public void setValue(PreparedStatement statement, int column, Boolean value) throws SQLException {
        statement.setBoolean(column, value);
    }
}
