package ch.hsr.ebos.aion.data.sqlite.state;

import com.google.inject.Singleton;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.hsr.ebos.aion.data.orm.EntityMetadata;
import ch.hsr.ebos.aion.data.orm.EntityMetadataField;
import ch.hsr.ebos.aion.data.orm.query.FieldFilter;
import ch.hsr.ebos.aion.data.orm.query.Filter;
import ch.hsr.ebos.aion.data.orm.query.FilterGroup;
import ch.hsr.ebos.aion.data.orm.query.Query;
import ch.hsr.ebos.aion.data.sqlite.state.types.SqliteBooleanType;
import ch.hsr.ebos.aion.data.sqlite.state.types.SqliteDateType;
import ch.hsr.ebos.aion.data.sqlite.state.types.SqliteDoubleType;
import ch.hsr.ebos.aion.data.sqlite.state.types.SqliteEnumType;
import ch.hsr.ebos.aion.data.sqlite.state.types.SqliteIntegerType;
import ch.hsr.ebos.aion.data.sqlite.state.types.SqliteStringType;
import ch.hsr.ebos.aion.data.sqlite.state.types.SqliteType;

@Singleton
public class SqliteSqlService {
    private List<SqliteType<?>> types = new ArrayList<>();

    public SqliteSqlService() {
        types.add(new SqliteBooleanType());
        types.add(new SqliteDateType());
        types.add(new SqliteDoubleType());
        types.add(new SqliteEnumType());
        types.add(new SqliteIntegerType());
        types.add(new SqliteStringType());
    }

    void createTable(Connection connection, String tableName, String... fields) throws SQLException {
        String fieldNames = join(", ", Arrays.asList(fields), f -> f);
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" + fieldNames + ");";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.execute();
    }

    void createTable(Connection connection, EntityMetadata<?> metadata) throws Exception {
        createTable(connection, "entity_" + metadata.getName(), metadata.getFields().stream().map(field -> {
            SqliteType<?> type = getSqliteType(field.getType());
            String typeName = type.getTypeName();

            if (field.isRequired()) {
                return field.getName() + " " + typeName + " not null";
            } else {
                return field.getName() + " " + typeName;
            }
        }).toArray(size -> new String[size]));
    }

    void createUniqueIndex(Connection connection, String tableName, String indexName, List<EntityMetadataField> value) throws SQLException {
        String fieldNames = join(", ", value, field -> field.getName());
        String sql = "CREATE UNIQUE INDEX IF NOT EXISTS " + tableName + "_" + indexName + " ON entity_" + tableName + " (" + fieldNames + ");";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.execute();
    }

    void insertRecord(Connection connection, EntityMetadata<?> metadata, Object object) throws Exception {
        String fieldNames = mapFields(metadata, EntityMetadataField::getName);
        String filterPlaceholders = mapFields(metadata, field -> "?");

        String sql = "INSERT INTO entity_" + metadata.getName() + "(" + fieldNames + ") VALUES (" + filterPlaceholders + ")";

        PreparedStatement statement = connection.prepareStatement(sql);
        setParameters(statement, metadata.getFields(), object);

        try {
            statement.execute();
        } catch (Exception e) {
            parseException(e, metadata);
        }
    }

    private void parseException(Exception e, EntityMetadata metadata) throws Exception {
        Matcher m = Pattern.compile("UNIQUE constraint failed: entity_" + metadata.getName() + ".([a-zA-Z0-9]+)").matcher(e.getMessage());
        if (m.find()) {
            throw new Exception("Could not create because " + m.group(1) + " already exists");
        }

        throw e;
    }

    void createSequenceTable(Connection connection) throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS sequences (name varchar primary key, value int)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.execute();
    }

    void insertSequence(Connection connection, String name) throws SQLException {
        String sql = "SELECT value FROM sequences WHERE name = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, name);

        ResultSet sequenceValueResultSet = statement.executeQuery();
        if (!sequenceValueResultSet.next()) {
            sql = "INSERT INTO sequences (name, value) VALUES (?, ?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, name);
            statement.setInt(2, 0);
            statement.execute();
        }
    }

    Integer getSequence(Connection connection, String name, int increment) throws Exception {
        String sql = "SELECT value FROM sequences WHERE name = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, name);

        ResultSet sequenceValueResultSet = statement.executeQuery();
        if (!sequenceValueResultSet.next()) {
            throw new Exception("Could not find sequence " + name);
        }

        int value = sequenceValueResultSet.getInt(1) + increment;
        sql = "UPDATE sequences SET value = ? WHERE name = ?";
        statement = connection.prepareStatement(sql);
        statement.setInt(1, value);
        statement.setString(2, name);
        statement.execute();

        return value;
    }

    void updateRecord(Connection connection, EntityMetadata<?> metadata, String id, Object object) throws Exception {
        String fieldNames = mapFields(metadata, field -> field.getName() + " = ?");
        String sql = "UPDATE entity_" + metadata.getName() + " SET " + fieldNames + " WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        setParameters(statement, metadata.getFields(), object);
        statement.setString(metadata.getFields().size() + 1, id);

        try {
            statement.execute();
        } catch (Exception e) {
            parseException(e, metadata);
        }
    }

    void deleteRecord(Connection connection, EntityMetadata<?> metadata, String id) throws Exception {
        String sql = "DELETE FROM entity_" + metadata.getName() + " WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        if (statement.executeUpdate() != 1) {
            throw new Exception("Could not delete " + metadata.getName() + " because it did not exists");
        }
    }

    <E> List<E> queryRecords(Connection connection, EntityMetadata<E> metadata, Query query) throws Exception {
        String fieldNames = mapFields(metadata, EntityMetadataField::getName);
        String sql = "SELECT " + fieldNames + " FROM entity_" + metadata.getName();

        if (query.getFilter().getFilters().size() > 0) {
            sql += " WHERE " + buildConditions(query.getFilter());
        }

        if (query.getLimit() != null) {
            sql += " LIMIT " + query.getLimit();
        }

        if (query.getSkip() != null) {
            sql += " OFFSET " + query.getSkip();
        }

        if (query.getOrders().size() > 0) {
            sql += " ORDER BY " + join(", ", query.getOrders(),
                    sort -> sort.getField() + (sort.isCaseSensitive() ? " COLLATE NOCASE" : "") +
                            " " + sort.getDirection().toString()

            );
        }

        PreparedStatement statement = connection.prepareStatement(sql);
        setConditionParameters(statement, query.getFilter(), 1);

        ResultSet resultSet = statement.executeQuery();
        List<E> result = new ArrayList<>();

        while (resultSet.next()) {
            result.add(parseInstance(metadata, resultSet));
        }

        return result;
    }

    <E> long countRecords(Connection connection, EntityMetadata<E> metadata, Query query) throws Exception {
        String sql = "SELECT COUNT(id) FROM entity_" + metadata.getName();

        if (query.getFilter().getFilters().size() > 0) {
            sql += " WHERE " + buildConditions(query.getFilter());
        }

        PreparedStatement statement = connection.prepareStatement(sql);
        setConditionParameters(statement, query.getFilter(), 1);

        ResultSet resultSet = statement.executeQuery();
        long count = 0;

        while (resultSet.next()) {
            count = resultSet.getLong(1);
        }
        return count;
    }

    <E> E selectRecord(Connection connection, EntityMetadata<E> metadata, String id) throws Exception {
        String fieldNames = mapFields(metadata, EntityMetadataField::getName);
        String sql = "SELECT " + fieldNames + " FROM entity_" + metadata.getName() + " WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            return null;
        }

        return parseInstance(metadata, resultSet);
    }


    private void setConditionParameters(PreparedStatement statement, FilterGroup filterGroup, Integer index) throws SQLException {
        for (Filter filter : filterGroup.getFilters()) {
            if (filter instanceof FilterGroup) {
                setConditionParameters(statement, (FilterGroup) filter, index);
            } else if (filter instanceof FieldFilter) {
                if (((FieldFilter) filter).getValue() != null) {
                    statement.setObject(index, ((FieldFilter) filter).getValue());
                }
                index++;
            }
        }
    }

    private String buildConditions(FilterGroup filterGroup) {
        return join(" " + filterGroup.getOperator().toString() + " ", filterGroup.getFilters(), filter -> {
            if (filter instanceof FilterGroup) {
                String conditions = buildConditions((FilterGroup) filter);
                if (conditions.length() > 0) {
                    return "(" + conditions + ")";
                }
                return "";
            } else if (filter instanceof FieldFilter) {
                FieldFilter fieldFilter = ((FieldFilter) filter);
                switch (fieldFilter.getOperator()) {
                    case LIKE:
                        return fieldFilter.getField() + " LIKE ?";
                    case EQUALS:
                        return fieldFilter.getField() + " = ?";
                    default:
                        throw new RuntimeException("Unknown filter operator");
                }
            } else {
                throw new RuntimeException("Unknown filter type");
            }
        });
    }

    private void setParameters(PreparedStatement statement, List<EntityMetadataField> fields, Object object) throws Exception {
        for (int i = 1; i <= fields.size(); i++) {
            EntityMetadataField field = fields.get(i - 1);
            Object value = field.getValue(object);
            SqliteType type = getSqliteType(field.getType());
            type.setValue(statement, i, value);
        }
    }

    private <T> String join(String separator, List<T> list, Function<T, String> function) {
        StringBuilder value = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                value.append(separator);
            }
            value.append(function.apply(list.get(i)));
        }
        return value.toString();
    }

    private String mapFields(EntityMetadata<?> metadata, Function<EntityMetadataField, String> function) {
        return join(", ", metadata.getFields(), function);
    }

    private SqliteType<?> getSqliteType(Class fieldType) {
        for (SqliteType<?> type : types) {
            if (!type.canHandle(fieldType)) {
                continue;
            }

            return type;
        }

        throw new RuntimeException("Unsupported data type " + fieldType.toString());
    }

    private <E> E parseInstance(EntityMetadata<E> metadata, ResultSet resultSet) throws Exception {
        E instance = metadata.getType().getConstructor().newInstance();
        for (int i = 1; i <= metadata.getFields().size(); i++) {
            EntityMetadataField field = metadata.getFields().get(i - 1);
            SqliteType type = getSqliteType(field.getType());
            Object value = type.getValue(resultSet, i, field.getType());
            if (!resultSet.wasNull()) {
                field.getField().set(instance, value);
            }
        }

        return instance;
    }
}
