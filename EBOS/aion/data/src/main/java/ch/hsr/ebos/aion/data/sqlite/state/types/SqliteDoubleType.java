package ch.hsr.ebos.aion.data.sqlite.state.types;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqliteDoubleType extends SqliteType<Double> {
    @Override
    public String getTypeName() {
        return "float";
    }

    @Override
    public boolean canHandle(Class type) {
        return type == double.class || type == Double.class;
    }

    @Override
    public Double getValue(ResultSet resultSet, int column, Class type) throws SQLException {
        return resultSet.getDouble(column);
    }

    @Override
    public void setValue(PreparedStatement statement, int column, Double value) throws SQLException {
        statement.setDouble(column, value);
    }
}
