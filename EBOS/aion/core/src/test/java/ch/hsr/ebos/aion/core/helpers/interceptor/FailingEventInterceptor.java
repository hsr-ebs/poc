package ch.hsr.ebos.aion.core.helpers.interceptor;

import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;

public class FailingEventInterceptor implements IEventInterceptor {
    public static final String ErrorMessage = "failing interceptor";

    @Override
    public void intercept(IEventInterceptorContext ctx) {
        ctx.abort(new Exception(ErrorMessage));
    }
}
