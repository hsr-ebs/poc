package ch.hsr.ebos.aion.core.timeline;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.exception.DuplicateEventException;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public abstract class TimelineTest {
    private ITimeline timeline;

    public abstract ITimelineFactory<?> getTimelineFactory();

    public String getTimelineName() {
        return "test";
    }

    @Before
    public void setUp() throws Exception {
        getTimelineFactory().delete(getTimelineName());
        timeline = getTimelineFactory().get(getTimelineName());
    }

    @After
    public void tearDown() throws Exception {
        getTimelineFactory().delete(getTimelineName());
    }

    @Test
    public void testFirst() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.first().get();
        assertEquals(event.getId(), "a");
        TimelineHelper.containsEvents(timeline, "a", "b");
    }

    @Test
    public void testSaveAuth() throws Exception {
        List<String> roles = new ArrayList<>();
        roles.add("offline_access");

        TimelineEventAuth eventAuth = new TimelineEventAuth(null, "foo", roles);
        timeline.add(TimelineEvent.fromObject("a", new ExampleEvent(), eventAuth)).get();

        TimelineEvent event = timeline.first().get();
        assertEquals("a", event.getId());
        assertNotNull(event.getAuth());
        assertEquals("foo", event.getAuth().getUsername());
        assertEquals(1, event.getAuth().getRoles().size());
        assertEquals("offline_access", event.getAuth().getRoles().get(0));
    }

    @Test
    public void testEmptyFirst() throws Exception {
        TimelineEvent event = timeline.first().get();
        assertNull(event);
    }

    @Test
    public void testAddDuplicateId() throws Throwable {
        timeline.add(new TimelineEvent("a", "test", new Object(), null)).get();
        try {
            timeline.add(new TimelineEvent("a", "test 2", new Object(), null)).get();
            fail();
        } catch (ExecutionException e) {
            assertEquals(DuplicateEventException.class, e.getCause().getClass());
            TimelineEvent event = timeline.get("a").get();
            assertEquals("test", event.getType());
        }
        TimelineHelper.containsEvents(timeline, "a");
    }

    @Test
    public void testConflictIds() throws Throwable {
        timeline.add(new TimelineEvent("a", "test", new Object(), null)).get();
        timeline.add(new TimelineEvent("b", "test", new Object(), null)).get();
        timeline.add(new TimelineEvent("c", "test", new Object(), null)).get();

        try {
            timeline.add(new TimelineEvent("c", "test 2", new Object(), null)).get();
            fail();
        } catch (ExecutionException e) {
            assertEquals(DuplicateEventException.class, e.getCause().getClass());
            TimelineEvent event = timeline.get("c").get();
            assertEquals("test", event.getType());
        }

        timeline.add(new TimelineEvent("d", "test", new Object(), null)).get();

        TimelineHelper.containsEvents(timeline, "a", "b", "c", "d");
    }

    @Test
    public void testLast() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.last().get();
        assertEquals(event.getId(), "b");

        TimelineHelper.containsEvents(timeline, "a", "b");
    }

    @Test
    public void testEmptyLast() throws Exception {
        TimelineEvent event = timeline.last().get();
        assertNull(event);
    }

    @Test
    public void testNext() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.next("a").get();
        assertEquals(event.getId(), "b");
    }

    @Test
    public void testNextNullId() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.next(null).get();
        assertNotNull(event);
        assertEquals(event.getId(), "a");
    }

    @Test
    public void testEmptyNext() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.next("b").get();
        assertNull(event);
    }

    @Test
    public void testNonExistingNext() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.next("c").get();
        assertNull(event);
    }

    @Test
    public void testPrev() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.prev("b").get();
        assertEquals(event.getId(), "a");
    }

    @Test
    public void testNonExistingPrev() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.prev("c").get();
        assertNull(event);
    }

    @Test
    public void testEmptyPrev() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.prev("a").get();
        assertNull(event);
    }

    @Test
    public void testGet() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.get("a").get();
        assertEquals(event.getId(), "a");
    }

    @Test
    public void testNonExistingGet() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        TimelineEvent event = timeline.get("c").get();
        assertNull(event);
    }

    @Test
    public void testEmptyInfo() throws Exception {
        TimelineInfo info = timeline.info().get();
        assertNull(info.getLastEvent());
        assertNull(info.getFirstEvent());
    }

    @Test
    public void testInfo() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b", "c");
        TimelineInfo info = timeline.info().get();
        assertEquals("a", info.getFirstEvent());
        assertEquals("c", info.getLastEvent());
    }

    @Test
    public void testThreadSafeAdd() throws Exception {
        int eventCount = 100;
        int threadCount = 4;

        Runnable runnable = () -> {
            CompletableFuture<Void>[] futures = new CompletableFuture[eventCount];
            for (int i = 0; i < eventCount; i++) {
                String id = UUID.randomUUID().toString();
                TimelineEvent event = new TimelineEvent(id, "test", new Object(), null);
                futures[i] = timeline.add(event);
            }

            try {
                CompletableFuture.allOf(futures).get();
            } catch (Exception e) {
                assertNull(e);
            }
        };

        Thread[] threads = new Thread[threadCount];
        for (int i = 0; i < threadCount; i++) {
            threads[i] = new Thread(runnable);
        }

        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }

        int timelineEventsCount = TimelineHelper.countEvents(timeline);
        assertEquals(threadCount * eventCount, timelineEventsCount);
    }
}
