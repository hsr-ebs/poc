package ch.hsr.ebos.aion.core.timeline;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationFactory;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;
import ch.hsr.ebos.aion.core.helpers.aggregate.SuccessfullEventAggregate;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;
import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.aion.core.processor.ProcessResetEvent;
import ch.hsr.ebos.aion.core.processor.TimelineProcessor;

import static org.junit.Assert.assertEquals;


public abstract class TimelineProcessorTest {
    private static final String[] EventIds = new String[]{"a", "b", "c", "d", "e"};

    private IDynamicTimeline processTimeline;
    private IDynamicTimeline resultTimeline;

    public abstract ITimelineFactory<IDynamicTimeline> getFactory();

    @Before
    public void setUp() throws Exception {
        this.resultTimeline = getFactory().get("result");
        this.processTimeline = getFactory().get("process");
        TimelineHelper.addEvents(processTimeline, EventIds);
    }

    @After
    public void tearDown() throws Exception {
        getFactory().delete("result");
        getFactory().delete("process");
    }

    @Test
    public void testRunEveryEventInOrder() throws Exception {
        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
                addEventInterceptors(TestEventInterceptor.class);
            }
        }).get();

        TimelineProcessor timelineProcessor = new TimelineProcessor(application, processTimeline, resultTimeline);
        timelineProcessor.run();

        List<String> emittedEventIds = application.injector.getInstance(TestEventInterceptor.class).eventIds;
        assertEquals(Arrays.asList("a", "b", "c", "d", "e"), emittedEventIds);
    }

    @Test
    public void testRerunEventsAfterReset() throws Exception {
        Application application = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
                addEventInterceptors(TestEventInterceptor.class);
            }
        }).get();

        TimelineProcessor timelineProcessor = new TimelineProcessor(application, processTimeline, resultTimeline);
        timelineProcessor.run();
        timelineProcessor.reset();
        timelineProcessor.run();

        List<String> emittedEventIds = application.injector.getInstance(TestEventInterceptor.class).eventIds;
        assertEquals(Arrays.asList("a", "b", "c", "d", "e", "a", "b", "c", "d", "e"), emittedEventIds);
    }

    @Singleton
    public static class TestEventInterceptor implements IEventInterceptor {
        List<String> eventIds = new ArrayList<>();

        @Override
        public void intercept(IEventInterceptorContext ctx) {
            if (!(ctx.getEvent().getData() instanceof ProcessResetEvent)) {
                eventIds.add(ctx.getEvent().getId());
            }

            ctx.next();
        }
    }
}
