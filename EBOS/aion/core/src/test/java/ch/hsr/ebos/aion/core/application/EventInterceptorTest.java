package ch.hsr.ebos.aion.core.application;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;
import ch.hsr.ebos.aion.core.helpers.aggregate.SuccessfullEventAggregate;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;
import ch.hsr.ebos.aion.core.helpers.interceptor.FailingCallbackEventInterceptor;
import ch.hsr.ebos.aion.core.helpers.interceptor.FailingEventInterceptor;
import ch.hsr.ebos.aion.core.helpers.interceptor.SuccessfullCallbackEventInterceptor;
import ch.hsr.ebos.aion.core.helpers.interceptor.SuccessfullEventInterceptor;
import ch.hsr.ebos.aion.core.module.Module;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EventInterceptorTest {

    @Test
    public void testEventInterceptorAndAggregateAreCalled() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(SuccessfullEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        EmitResult result = app.emit(new ExampleEvent()).get();

        SuccessfullEventInterceptor interceptor = app.injector.getInstance(SuccessfullEventInterceptor.class);
        SuccessfullEventAggregate aggregate = app.injector.getInstance(SuccessfullEventAggregate.class);

        assertEquals(interceptor.called, 1);
        assertEquals(aggregate.called, 1);
        assertTrue(result.isSuccessfull());
    }

    @Test
    public void testEventInterceptorIsCalled() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        EmitResult result = app.emit(new ExampleEvent()).get();

        SuccessfullEventAggregate aggregate = app.injector.getInstance(SuccessfullEventAggregate.class);

        assertEquals(aggregate.called, 1);
        assertTrue(result.isSuccessfull());
    }

    @Test
    public void testFailingEventInterceptorsBlocksAggregate() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(FailingEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        EmitResult result = app.emit(new ExampleEvent()).get();

        SuccessfullEventAggregate aggregate = app.injector.getInstance(SuccessfullEventAggregate.class);

        assertEquals(aggregate.called, 0);
        assertFalse(result.isSuccessfull());
        assertEquals(result.getInterceptorResults().size(), 1);
        assertEquals(result.getAggregateResults().size(), 0);
        assertEquals(result.getInterceptorResults().get(0).getErrorMessage(), FailingEventInterceptor.ErrorMessage);
    }


    @Test
    public void testRemoveAggregateInterceptor() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(RemoveAggregatesEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        EmitResult result = app.emit(new ExampleEvent()).get();

        SuccessfullEventAggregate aggregate = app.injector.getInstance(SuccessfullEventAggregate.class);

        assertEquals(aggregate.called, 0);
        assertTrue(result.isSuccessfull());
        assertEquals(result.getInterceptorResults().size(), 1);
        assertEquals(result.getAggregateResults().size(), 0);
    }


    @Test
    public void testFailingCallbackInterceptor() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(FailingCallbackEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        EmitResult result = app.emit(new ExampleEvent()).get();

        SuccessfullEventAggregate aggregate = app.injector.getInstance(SuccessfullEventAggregate.class);

        assertEquals(aggregate.called, 1);
        assertFalse(result.isSuccessfull());
        assertEquals(result.getInterceptorResults().size(), 1);
        assertEquals(result.getAggregateResults().size(), 1);
    }

    @Test
    public void testSuccessfullCallbackInterceptor() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(SuccessfullCallbackEventInterceptor.class);
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        EmitResult result = app.emit(new ExampleEvent()).get();

        SuccessfullEventAggregate aggregate = app.injector.getInstance(SuccessfullEventAggregate.class);

        assertEquals(aggregate.called, 1);
        assertTrue(result.isSuccessfull());
        assertEquals(result.getInterceptorResults().size(), 1);
        assertEquals(result.getAggregateResults().size(), 1);
    }

    public static class RemoveAggregatesEventInterceptor implements IEventInterceptor {
        @Override
        public void intercept(IEventInterceptorContext ctx) {
            ctx.getAggregators().clear();
            ctx.next();
        }
    }
}
