package ch.hsr.ebos.aion.core.helpers.event;

import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("example.event")
public class ExampleEvent extends BaseEvent implements IEvent {

}
