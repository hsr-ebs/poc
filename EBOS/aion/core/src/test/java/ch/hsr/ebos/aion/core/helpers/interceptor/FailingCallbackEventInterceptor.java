package ch.hsr.ebos.aion.core.helpers.interceptor;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorResultCallback;

public class FailingCallbackEventInterceptor implements IEventInterceptor {
    public static final String ErrorMessage = "failing callback event interceptor";

    @Override
    public void intercept(IEventInterceptorContext ctx) {
        ctx.next(new IEventInterceptorResultCallback() {
            @Override
            public CompletableFuture<Void> onResult(EmitResult result) {
                CompletableFuture<Void> future = new CompletableFuture<>();
                future.completeExceptionally(new Exception(ErrorMessage));
                return future;
            }
        });
    }
}
