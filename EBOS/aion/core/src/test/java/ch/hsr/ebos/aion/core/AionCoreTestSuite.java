package ch.hsr.ebos.aion.core;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ch.hsr.ebos.aion.core.application.ApplicationInterceptorTest;
import ch.hsr.ebos.aion.core.application.ApplicationTest;
import ch.hsr.ebos.aion.core.application.EventAggregateTest;
import ch.hsr.ebos.aion.core.application.EventInterceptorTest;
import ch.hsr.ebos.aion.core.module.ModuleTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ApplicationInterceptorTest.class,
        EventAggregateTest.class,
        EventInterceptorTest.class,
        ApplicationTest.class,
        ModuleTest.class,
})
public class AionCoreTestSuite {
}
