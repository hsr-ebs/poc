package ch.hsr.ebos.aion.core.application;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.helpers.aggregate.SuccessfullEventAggregate;
import ch.hsr.ebos.aion.core.helpers.aggregate.TransientEventAggregate;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;
import ch.hsr.ebos.aion.core.module.Module;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ApplicationTest {

    @Test
    public void testStopLifetime() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        app.close().get();

        EmitResult result = app.emit(new ExampleEvent(), null).get();

        assertFalse(result.isSuccessfull());
        assertEquals(result.getErrorMessages().size(), 1);
    }

    @Test
    public void testStopWithException() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        String errorMessage = "some exception";
        try {
            app.close(new Throwable(errorMessage)).get();
        } catch (Throwable ex) {
            assertEquals("java.lang.Throwable: " + errorMessage, ex.getMessage());
        }

    }

    @Test
    public void testTransientEventAggregate() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, TransientEventAggregate.class);
            }
        }).get();

        assertTrue(app.emit(new ExampleEvent(), null).get().isSuccessfull());
        assertTrue(app.emit(new ExampleEvent(), null).get().isSuccessfull());
        assertTrue(app.emit(new ExampleEvent(), null).get().isSuccessfull());
    }
}
