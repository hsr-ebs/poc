package ch.hsr.ebos.aion.core.helpers.interceptor;

import javax.inject.Singleton;

import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptor;
import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptorContext;

@Singleton
public class SuccessfullApplicationInterceptor implements IApplicationInterceptor {
    public int called = 0;

    @Override
    public void intercept(IApplicationInterceptorContext context) {
        called++;
        context.next();
    }
}
