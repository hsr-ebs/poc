package ch.hsr.ebos.aion.core.helpers.interceptor;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorResultCallback;

public class SuccessfullCallbackEventInterceptor implements IEventInterceptor {
    @Override
    public void intercept(IEventInterceptorContext ctx) {
        ctx.next(new IEventInterceptorResultCallback() {
            @Override
            public CompletableFuture<Void> onResult(EmitResult result) {
                return CompletableFuture.completedFuture(null);
            }
        });
    }
}
