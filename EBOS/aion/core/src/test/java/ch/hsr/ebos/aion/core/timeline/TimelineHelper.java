package ch.hsr.ebos.aion.core.timeline;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

import ch.hsr.ebos.aion.core.event.EventTypeUtil;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;

import static org.junit.Assert.assertEquals;

public class TimelineHelper {
    public static void addEvent(ITimeline timeline, String id, Object event) throws ExecutionException, InterruptedException {
        String type = EventTypeUtil.getEventType(event.getClass());
        timeline.add(new TimelineEvent(id, type, event, null)).get();
    }

    public static void addEvents(ITimeline timeline, String... ids) throws Exception {
        for (String id : ids) {
            timeline.add(generateExampleEvent(id)).get();
        }
    }

    public static TimelineEvent generateExampleEvent(String id) {
        return new TimelineEvent(id, "example.event", new ExampleEvent(), null);
    }

    public static void interate(ITimeline timeline, Function<TimelineEvent, Void> function) throws Exception {
        String currentId = null;
        TimelineEvent event = null;
        do {
            event = timeline.next(currentId).get();

            if (event != null) {
                currentId = event.getId();
                function.apply(event);
            }
        } while (event != null);
    }

    public static int countEvents(ITimeline timeline) throws Exception {
        List<TimelineEvent> events = new ArrayList<>();

        interate(timeline, event -> {
            events.add(event);
            return null;
        });

        return events.size();
    }

    public static void containsEvents(ITimeline timeline, String... ids) throws Exception {
        List<String> timelineIds = new ArrayList<>();
        String expected = String.join(", ", ids);

        interate(timeline, event -> {
            timelineIds.add(event.getId());
            return null;
        });

        assertEquals(expected, String.join(", ", timelineIds));
    }
}
