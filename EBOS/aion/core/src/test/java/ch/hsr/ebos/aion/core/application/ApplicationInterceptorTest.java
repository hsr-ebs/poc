package ch.hsr.ebos.aion.core.application;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.helpers.interceptor.FailingApplicationInterceptor;
import ch.hsr.ebos.aion.core.helpers.interceptor.SuccessfullApplicationInterceptor;
import ch.hsr.ebos.aion.core.module.Module;

import static org.junit.Assert.assertEquals;

public class ApplicationInterceptorTest {

    @Test
    public void testSuccessfullInterceptor() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addApplicationInterceptors(SuccessfullApplicationInterceptor.class);
            }
        }).get();

        SuccessfullApplicationInterceptor interceptor = app.injector.getInstance(SuccessfullApplicationInterceptor.class);

        assertEquals(interceptor.called, 1);
    }

    @Test(expected = Exception.class)
    public void testFailingInterceptor() throws ExecutionException, InterruptedException {
        ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addApplicationInterceptors(FailingApplicationInterceptor.class);
            }
        }).get();
    }


    @Test(expected = Exception.class)
    public void testFailingAndSuccessfullInterceptor() throws ExecutionException, InterruptedException {
        ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addApplicationInterceptors(SuccessfullApplicationInterceptor.class);
                addApplicationInterceptors(FailingApplicationInterceptor.class);
            }
        }).get();
    }
}
