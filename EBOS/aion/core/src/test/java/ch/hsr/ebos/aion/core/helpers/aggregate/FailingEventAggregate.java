package ch.hsr.ebos.aion.core.helpers.aggregate;


import javax.inject.Singleton;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregateContext;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;

@Singleton
public class FailingEventAggregate implements IEventAggregate<ExampleEvent> {
    public static final String ErrorMessage = "failing event aggregate";
    public int called = 0;

    @Override
    public void aggregate(ApplicationEvent<ExampleEvent> event, IEventAggregateContext context) {
        this.called++;
        context.abort(new Exception(ErrorMessage));
    }
}