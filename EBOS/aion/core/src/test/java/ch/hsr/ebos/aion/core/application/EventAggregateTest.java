package ch.hsr.ebos.aion.core.application;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.helpers.aggregate.FailingEventAggregate;
import ch.hsr.ebos.aion.core.helpers.aggregate.SuccessfullEventAggregate;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;
import ch.hsr.ebos.aion.core.module.Module;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EventAggregateTest {

    @Test
    public void testSuccessfullEventAggregate() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        EmitResult result = app.emit(new ExampleEvent()).get();

        SuccessfullEventAggregate aggregate = app.injector.getInstance(SuccessfullEventAggregate.class);

        assertEquals(aggregate.called, 1);
        assertTrue(result.isSuccessfull());
        assertEquals(result.getAggregateResults().size(), 1);
    }

    @Test
    public void testFailingEventAggregate() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, FailingEventAggregate.class);
            }
        }).get();

        EmitResult result = app.emit(new ExampleEvent()).get();

        FailingEventAggregate aggregate = app.injector.getInstance(FailingEventAggregate.class);

        assertEquals(aggregate.called, 1);
        assertFalse(result.isSuccessfull());
        assertEquals(result.getAggregateResults().size(), 1);
        assertEquals(result.getAggregateResults().get(0).getErrorMessage(), FailingEventAggregate.ErrorMessage);
    }

    @Test
    public void testFailingAndSuccessfullEventAggregate() throws ExecutionException, InterruptedException {
        Application app = ApplicationFactory.create(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, FailingEventAggregate.class);
                addEventAggregate(ExampleEvent.class, SuccessfullEventAggregate.class);
            }
        }).get();

        EmitResult result = app.emit(new ExampleEvent()).get();

        FailingEventAggregate failingAggregate = app.injector.getInstance(FailingEventAggregate.class);
        SuccessfullEventAggregate successfullAggregate = app.injector.getInstance(SuccessfullEventAggregate.class);

        assertEquals(failingAggregate.called, 1);
        assertEquals(successfullAggregate.called, 1);
        assertFalse(result.isSuccessfull());
        assertEquals(result.getAggregateResults().size(), 2);
    }
}
