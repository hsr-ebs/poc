package ch.hsr.ebos.aion.core.timeline;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncState;
import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncStateFactory;
import ch.hsr.ebos.aion.core.synchronizer.SyncState;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public abstract class TimelineSyncStateTest {
    private ITimelineSyncState syncState;

    public abstract ITimelineSyncStateFactory getFactory() throws Exception;

    @Before
    public void setUp() throws Exception {
        getFactory().delete("test");
        syncState = getFactory().get("test");
    }

    @After
    public void tearDown() throws Exception {
        getFactory().delete("test");
    }

    @Test
    public void testEmptyState() throws Exception {
        assertNull(syncState.getLastPulled());
        assertNull(syncState.getLastPushed());
    }

    @Test
    public void testNonExistingGet() throws Exception {
        assertNull(syncState.get("1"));
    }

    @Test
    public void testExistingGet() throws Exception {
        syncState.set("1", SyncState.PULLED);
        assertEquals(SyncState.PULLED, syncState.get("1"));

        syncState.set("2", SyncState.PUSHED);
        assertEquals(SyncState.PUSHED, syncState.get("2"));
    }

    @Test
    public void testLastPulled() throws Exception {
        syncState.set("1", SyncState.PULLED);
        assertEquals("1", syncState.getLastPulled());
    }

    @Test
    public void testLastPushed() throws Exception {
        syncState.set("2", SyncState.PUSHED);
        assertEquals("2", syncState.getLastPushed());
    }
}
