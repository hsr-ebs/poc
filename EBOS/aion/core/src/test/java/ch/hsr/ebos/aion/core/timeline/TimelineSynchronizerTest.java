package ch.hsr.ebos.aion.core.timeline;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncState;
import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncStateFactory;
import ch.hsr.ebos.aion.core.synchronizer.SyncState;
import ch.hsr.ebos.aion.core.synchronizer.TimelineSynchronizer;

public abstract class TimelineSynchronizerTest {
    private final TimelineSynchronizer synchronizer = new TimelineSynchronizer();

    private IDynamicTimeline localTimeline;
    private ITimeline remoteTimeline;
    private ITimelineSyncState syncState;

    public abstract ITimelineFactory<IDynamicTimeline> getTimelineFactory();

    public abstract ITimelineSyncStateFactory getSyncStateFactory();

    @Before
    public void before() throws Exception {
        localTimeline = getTimelineFactory().get("local");
        remoteTimeline = getTimelineFactory().get("remote");
        syncState = getSyncStateFactory().get("test");
    }

    @After
    public void tearDown() throws Exception {
        getTimelineFactory().delete("local");
        getTimelineFactory().delete("remote");
        getSyncStateFactory().delete("test");
    }

    @Test
    public void testPushOnEmptyRemote() throws Throwable {
        TimelineHelper.addEvents(localTimeline, "a", "b");
        synchronizer.push(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(remoteTimeline, "a", "b");
    }

    @Test
    public void testPushOnNonEmptyRemote() throws Throwable {
        TimelineHelper.addEvents(remoteTimeline, "a", "b");
        TimelineHelper.addEvents(localTimeline, "c");
        synchronizer.push(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(remoteTimeline, "a", "b", "c");
    }

    @Test
    public void testPushOnAfterFirstSync() throws Throwable {
        TimelineHelper.addEvents(remoteTimeline, "a", "b");
        TimelineHelper.addEvents(localTimeline, "a", "b", "c");
        syncState.set("a", SyncState.PUSHED);
        syncState.set("b", SyncState.PUSHED);
        synchronizer.push(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(remoteTimeline, "a", "b", "c");
    }

    @Test
    public void testOngoingSync() throws Throwable {
        TimelineHelper.addEvents(remoteTimeline, "a", "b");
        synchronizer.sync(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(localTimeline, "a", "b");

        synchronizer.sync(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(localTimeline, "a", "b");

        TimelineHelper.addEvents(localTimeline, "c");
        synchronizer.sync(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(localTimeline, "a", "b", "c");
        TimelineHelper.containsEvents(remoteTimeline, "a", "b", "c");

        TimelineHelper.addEvents(remoteTimeline, "d");
        synchronizer.sync(localTimeline, remoteTimeline, syncState);

        TimelineHelper.containsEvents(localTimeline, "a", "b", "c", "d");
        TimelineHelper.containsEvents(remoteTimeline, "a", "b", "c", "d");
    }

    @Test
    public void testPullOnEmptyLocal() throws Throwable {
        TimelineHelper.addEvents(remoteTimeline, "a", "b");
        synchronizer.pull(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(localTimeline, "a", "b");
    }

    @Test
    public void testPullOnNonEmptyLocal() throws Throwable {
        TimelineHelper.addEvents(remoteTimeline, "a", "b");
        TimelineHelper.addEvents(localTimeline, "a");
        syncState.set("a", SyncState.PUSHED);
        synchronizer.pull(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(localTimeline, "a", "b");
    }

    @Test
    public void testSyncWithoutRemoteEvents() throws Throwable {
        TimelineHelper.addEvents(localTimeline, "a", "b", "c");
        synchronizer.sync(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(localTimeline, "a", "b", "c");
        TimelineHelper.containsEvents(remoteTimeline, "a", "b", "c");
    }

    @Test
    public void testSyncWithConflicts() throws Throwable {
        TimelineHelper.addEvents(remoteTimeline, "a", "b", "c", "d");
        TimelineHelper.addEvents(localTimeline, "a", "b", "e");
        syncState.set("a", SyncState.PUSHED);
        syncState.set("b", SyncState.PUSHED);
        synchronizer.sync(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(localTimeline, "a", "b", "c", "d", "e");
        TimelineHelper.containsEvents(remoteTimeline, "a", "b", "c", "d", "e");
    }

    @Test
    public void testSyncWithoutState() throws Throwable {
        TimelineHelper.addEvents(remoteTimeline, "a", "b", "c", "d");
        TimelineHelper.addEvents(localTimeline, "a", "b", "e");
        synchronizer.sync(localTimeline, remoteTimeline, syncState);
        TimelineHelper.containsEvents(localTimeline, "a", "b", "c", "d", "e");
        TimelineHelper.containsEvents(remoteTimeline, "a", "b", "c", "d", "e");
    }
}
