package ch.hsr.ebos.aion.core.helpers.aggregate;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregateContext;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;

import static org.junit.Assert.fail;

public class TransientEventAggregate implements IEventAggregate<ExampleEvent> {
    public int called = 0;

    @Override
    public void aggregate(ApplicationEvent<ExampleEvent> event, IEventAggregateContext context) {
        this.called++;
        if (called > 1) {
            fail("should not persist count");
        }
        context.complete();
    }
}