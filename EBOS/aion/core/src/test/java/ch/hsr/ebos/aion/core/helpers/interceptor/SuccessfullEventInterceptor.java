package ch.hsr.ebos.aion.core.helpers.interceptor;

import javax.inject.Singleton;

import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;

@Singleton
public class SuccessfullEventInterceptor implements IEventInterceptor {
    public int called = 0;

    @Override
    public void intercept(IEventInterceptorContext ctx) {
        this.called++;
        ctx.next();
    }
}
