package ch.hsr.ebos.aion.core.helpers.event;


import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("another.example.event")
public class AnotherExampleEvent extends BaseEvent implements IEvent {

}
