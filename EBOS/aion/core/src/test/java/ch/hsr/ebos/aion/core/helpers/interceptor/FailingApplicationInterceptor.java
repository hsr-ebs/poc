package ch.hsr.ebos.aion.core.helpers.interceptor;

import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptor;
import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptorContext;

public class FailingApplicationInterceptor implements IApplicationInterceptor {
    public static final String ErrorMessage = "failing interceptor";
    public int called = 0;

    @Override
    public void intercept(IApplicationInterceptorContext context) {
        called++;
        context.abort(new Exception(ErrorMessage));
    }
}
