package ch.hsr.ebos.aion.core.timeline;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.exception.DuplicateEventException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public abstract class DynamicTimelineTest {
    IDynamicTimeline timeline;

    public abstract ITimelineFactory<IDynamicTimeline> getTimelineFactory();

    @Before
    public void setUp() throws Exception {
        getTimelineFactory().delete("test");
        timeline = getTimelineFactory().get("test");
    }

    @After
    public void tearDown() throws Exception {
        getTimelineFactory().delete("test");
    }

    @Test
    public void testInsertAfterOnEmpty() throws Exception {
        timeline.insertAfter(TimelineHelper.generateExampleEvent("a"), null).get();
        TimelineHelper.containsEvents(timeline, "a");
        TimelineEvent first = timeline.first().get();
        assertNotNull(first);
        assertEquals("a", first.getId());
        TimelineEvent last = timeline.last().get();
        assertNotNull(last);
        assertEquals("a", last.getId());
    }

    @Test
    public void testInsertBeforeOnEmpty() throws Exception {
        timeline.insertBefore(TimelineHelper.generateExampleEvent("a"), null).get();
        TimelineHelper.containsEvents(timeline, "a");
        TimelineEvent first = timeline.first().get();
        assertNotNull(first);
        assertEquals("a", first.getId());
        TimelineEvent last = timeline.last().get();
        assertNotNull(last);
        assertEquals("a", last.getId());
    }

    @Test
    public void testInsertBeforeBetween() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        timeline.insertBefore(TimelineHelper.generateExampleEvent("c"), "b").get();
        TimelineHelper.containsEvents(timeline, "a", "c", "b");
        TimelineEvent first = timeline.first().get();
        assertNotNull(first);
        assertEquals("a", first.getId());
        TimelineEvent last = timeline.last().get();
        assertNotNull(last);
        assertEquals("b", last.getId());
    }

    @Test
    public void testInsertAfterBetween() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        timeline.insertAfter(TimelineHelper.generateExampleEvent("c"), "a").get();
        TimelineHelper.containsEvents(timeline, "a", "c", "b");
        TimelineEvent first = timeline.first().get();
        assertNotNull(first);
        assertEquals("a", first.getId());
        TimelineEvent last = timeline.last().get();
        assertNotNull(last);
        assertEquals("b", last.getId());
    }

    @Test
    public void testInsertBeforeOnBegin() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        timeline.insertBefore(TimelineHelper.generateExampleEvent("c"), "a").get();
        TimelineHelper.containsEvents(timeline, "c", "a", "b");
        TimelineEvent first = timeline.first().get();
        assertNotNull(first);
        assertEquals("c", first.getId());
    }

    @Test
    public void testInsertBeforeOnEnd() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        timeline.insertBefore(TimelineHelper.generateExampleEvent("c"), null).get();
        TimelineHelper.containsEvents(timeline, "a", "b", "c");
        TimelineEvent last = timeline.last().get();
        assertNotNull(last);
        assertEquals("c", last.getId());
    }

    @Test
    public void testInsertAfterOnEnd() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        timeline.insertAfter(TimelineHelper.generateExampleEvent("c"), null).get();
        TimelineHelper.containsEvents(timeline, "a", "b", "c");
        TimelineEvent last = timeline.last().get();
        assertNotNull(last);
        assertEquals("c", last.getId());
    }

    @Test(expected = Exception.class)
    public void testInsertBeforeNonExisting() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        timeline.insertBefore(TimelineHelper.generateExampleEvent("c"), "d").get();
    }

    @Test(expected = DuplicateEventException.class)
    public void testInsertBeforeExistingEvent() throws Throwable {
        TimelineHelper.addEvents(timeline, "a", "b");
        try {
            timeline.insertBefore(TimelineHelper.generateExampleEvent("a"), "b").get();
        } catch (ExecutionException e) {
            throw e.getCause();
        }
    }

    @Test(expected = Exception.class)
    public void testInsertAfterNonExisting() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        timeline.insertAfter(TimelineHelper.generateExampleEvent("c"), "d").get();
    }

    @Test(expected = DuplicateEventException.class)
    public void testInsertAfterExistingEvent() throws Throwable {
        TimelineHelper.addEvents(timeline, "a", "b");
        try {
            timeline.insertAfter(TimelineHelper.generateExampleEvent("b"), "a").get();
        } catch (ExecutionException e) {
            throw e.getCause();
        }
    }

    @Test
    public void testPopEmpty() throws Exception {
        String empty = timeline.pop().get();
        assertNull(empty);
    }

    @Test
    public void testPopNonEmpty() throws Exception {
        TimelineHelper.addEvents(timeline, "a", "b");
        String id = timeline.pop().get();
        assertEquals("b", id);
        TimelineEvent event = timeline.last().get();
        assertEquals("a", event.getId());
    }

    @Test
    public void testPopLast() throws Exception {
        TimelineHelper.addEvents(timeline, "a");
        String id = timeline.pop().get();
        assertEquals("a", id);

        TimelineEvent last = timeline.last().get();
        assertNull(last);

        TimelineEvent first = timeline.first().get();
        assertNull(first);
    }
}
