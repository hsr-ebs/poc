package ch.hsr.ebos.aion.core.module;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import com.google.inject.util.Types;

import org.junit.Test;

import java.util.List;
import java.util.Set;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptor;
import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptorContext;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregateContext;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorContext;
import ch.hsr.ebos.aion.core.helpers.event.AnotherExampleEvent;
import ch.hsr.ebos.aion.core.helpers.event.BaseEvent;
import ch.hsr.ebos.aion.core.helpers.event.ExampleEvent;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ModuleTest {

    @SuppressWarnings("unchecked")
    public static <T> TypeLiteral<Set<T>> setOf(Class<T> type) {
        return (TypeLiteral<Set<T>>) TypeLiteral.get(Types.setOf(type));
    }

    private void containsEvents(Injector injector, Class... events) {
        EventRouter service = injector.getInstance(EventRouter.class);
        List<Class> registeredEvents = service.getEvents();
        System.out.println(registeredEvents);
        for (Class event : events) {
            if (!registeredEvents.contains(event)) {
                fail("event " + event.toString() + " not registered");
            }
        }
    }

    private void containsAggregates(Injector injector, Class... aggregates) {
        EventRouter service = injector.getInstance(EventRouter.class);
        List<Class> registeredAggreagtes = service.getAggregates();
        for (Class aggregate : aggregates) {
            if (!registeredAggreagtes.contains(aggregate)) {
                fail("event aggregate " + aggregate.toString() + " not registered");
            }
        }
    }

    private void containsApplicationInterceptors(Injector injector, Class... interceptors) {
        Key<Set<IApplicationInterceptor>> interceptorKey = Key.get(setOf(IApplicationInterceptor.class));
        Set<IApplicationInterceptor> registeredInterceptors = injector.getInstance(interceptorKey);
        for (Class interceptor : interceptors) {
            boolean found = false;
            for (IApplicationInterceptor registeredInterceptor : registeredInterceptors) {
                if (registeredInterceptor.getClass() == interceptor) {
                    found = true;
                    break;
                }
            }

            assertTrue("application interceptor " + interceptor.toString() + " not registered", found);
        }
    }

    private <T> void containsMultiBinding(Injector injector, Class<T> key, Class<?>... bindings) {
        Key<Set<T>> bindingKey = Key.get(setOf(key));
        Set<T> registerdBindings = injector.getInstance(bindingKey);
        for (T registeredBinding : registerdBindings) {
            boolean found = false;
            for (Class<?> binding : bindings) {
                if (registeredBinding.getClass() == binding) {
                    found = true;
                    break;
                }
            }

            assertTrue(registeredBinding.toString() + " not registered", found);
        }
    }

    private void containsEventInterceptors(Injector injector, Class... interceptors) {
        Key<Set<IEventInterceptor>> interceptorKey = Key.get(setOf(IEventInterceptor.class));
        Set<IEventInterceptor> registeredInterceptors = injector.getInstance(interceptorKey);
        for (Class interceptor : interceptors) {
            boolean found = false;
            for (IEventInterceptor registeredInterceptor : registeredInterceptors) {
                if (registeredInterceptor.getClass() == interceptor) {
                    found = true;
                    break;
                }
            }

            assertTrue("event interceptor " + interceptor.toString() + " not registered", found);
        }
    }

    @Test
    public void testSingleEventAggregate() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, ExampleAggregate.class);
            }
        }.getInjectModule());

        containsEvents(injector, ExampleEvent.class);
        containsAggregates(injector, ExampleAggregate.class);
    }

    @Test
    public void testMultipleEventAggregates() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, ExampleAggregate.class);
                addEventAggregate(AnotherExampleEvent.class, AnotherExampleAggregate.class);
            }
        }.getInjectModule());

        containsEvents(injector, ExampleEvent.class, AnotherExampleEvent.class);
        containsAggregates(injector, ExampleAggregate.class, AnotherExampleAggregate.class);
    }

    @Test
    public void testMultipleEventAggregatesOnSameEvent() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, ExampleAggregate.class);
                addEventAggregate(ExampleEvent.class, AnotherExampleAggregate.class);
            }
        }.getInjectModule());

        containsEvents(injector, ExampleEvent.class);
        containsAggregates(injector, ExampleAggregate.class, AnotherExampleAggregate.class);
    }

    @Test
    public void testSingleEventInterceptor() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(ExampleEventInterceptor.class);
            }
        }.getInjectModule());

        containsEventInterceptors(injector, ExampleEventInterceptor.class);
    }

    @Test
    public void testMultipleEventInterceptor() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(ExampleEventInterceptor.class);
                addEventInterceptors(AnotherExampleEventInterceptor.class);
            }
        }.getInjectModule());

        containsEventInterceptors(injector, ExampleEventInterceptor.class, AnotherExampleEventInterceptor.class);
    }

    @Test
    public void testSingleApplicationInterceptor() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            protected void setup() {
                addApplicationInterceptors(ExampleApplicationInterceptor.class);
            }
        }.getInjectModule());

        containsApplicationInterceptors(injector, ExampleApplicationInterceptor.class);
    }

    @Test
    public void testMultipleApplicationInterceptor() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            protected void setup() {
                addApplicationInterceptors(ExampleApplicationInterceptor.class);
                addApplicationInterceptors(AnotherExampleApplicationInterceptor.class);
            }
        }.getInjectModule());

        containsApplicationInterceptors(injector, ExampleApplicationInterceptor.class, AnotherExampleApplicationInterceptor.class);
    }


    @Test
    public void testNestedModule() {
        Module module = new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(ExampleEventInterceptor.class);
            }
        };
        module.addModule(new Module() {
            @Override
            protected void setup() {
                addApplicationInterceptors(ExampleApplicationInterceptor.class);
            }
        });
        Injector injector = Guice.createInjector(module.getInjectModule());

        containsEventInterceptors(injector, ExampleEventInterceptor.class);
        containsApplicationInterceptors(injector, ExampleApplicationInterceptor.class);
    }

    @Test
    public void testDeepNestedModule() {
        Module rootModule = new Module() {
            @Override
            protected void setup() {
                addEventInterceptors(ExampleEventInterceptor.class);
            }
        };

        Module subModule = new Module() {
            @Override
            protected void setup() {
                addApplicationInterceptors(ExampleApplicationInterceptor.class);
            }
        };

        Module subSubModule = new Module() {
            @Override
            protected void setup() {
                addEventAggregate(ExampleEvent.class, ExampleAggregate.class);
            }
        };

        rootModule.addModule(subModule);
        subModule.addModule(subSubModule);

        Injector injector = Guice.createInjector(rootModule.getInjectModule());

        containsAggregates(injector, ExampleAggregate.class);
        containsEventInterceptors(injector, ExampleEventInterceptor.class);
        containsApplicationInterceptors(injector, ExampleApplicationInterceptor.class);
    }


    @Test
    public void testMultiBinding() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            protected void setup() {
                bindMultiple(BaseEvent.class, ExampleEvent.class);
                bindMultiple(BaseEvent.class, AnotherExampleEvent.class);
            }
        });

        containsMultiBinding(injector, BaseEvent.class, ExampleEvent.class, AnotherExampleEvent.class);
    }

    public static class ExampleAggregate implements IEventAggregate {
        @Override
        public void aggregate(ApplicationEvent event, IEventAggregateContext context) {

        }
    }

    public static class AnotherExampleAggregate implements IEventAggregate {
        @Override
        public void aggregate(ApplicationEvent event, IEventAggregateContext context) {

        }
    }

    public static class ExampleEventInterceptor implements IEventInterceptor {
        @Override
        public void intercept(IEventInterceptorContext ctx) {

        }
    }

    public static class AnotherExampleEventInterceptor implements IEventInterceptor {
        @Override
        public void intercept(IEventInterceptorContext ctx) {

        }
    }

    public static class ExampleApplicationInterceptor implements IApplicationInterceptor {
        @Override
        public void intercept(IApplicationInterceptorContext context) {

        }
    }

    public static class AnotherExampleApplicationInterceptor implements IApplicationInterceptor {
        @Override
        public void intercept(IApplicationInterceptorContext context) {

        }
    }
}

