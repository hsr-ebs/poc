package ch.hsr.ebos.aion.core.synchronizer;

public enum SyncState {
    PULLED, PUSHED
}
