package ch.hsr.ebos.aion.core.application;

import com.google.inject.Guice;
import com.google.inject.Injector;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.module.Module;


public class ApplicationFactory {
    public static CompletableFuture<Application> create(Module module) {
        Injector injector = Guice.createInjector(module.getInjectModule());
        ApplicationLifetime context = injector.getInstance(ApplicationLifetime.class);
        Application app = injector.getInstance(Application.class);

        CompletableFuture result = new CompletableFuture();
        context.start().whenComplete((r, throwable) -> {
            if (throwable == null) {
                result.complete(app);
            } else {
                result.completeExceptionally(throwable);
            }
        });

        return result;
    }
}
