package ch.hsr.ebos.aion.core.event.aggregate;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;

public abstract class EventAggregate<TEvent> implements IEventAggregate<TEvent> {
    public abstract void aggregate(ApplicationEvent<TEvent> event) throws Throwable;

    @Override
    public void aggregate(ApplicationEvent<TEvent> event, IEventAggregateContext context) {
        try {
            this.aggregate(event);
            context.complete();
        } catch (Throwable throwable) {
            context.abort(throwable);
        }
    }
}
