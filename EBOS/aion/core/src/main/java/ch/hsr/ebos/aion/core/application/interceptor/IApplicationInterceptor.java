package ch.hsr.ebos.aion.core.application.interceptor;

public interface IApplicationInterceptor {
    void intercept(IApplicationInterceptorContext context);
}
