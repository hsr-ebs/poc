package ch.hsr.ebos.aion.core.timeline;

public interface ITimelineFactory<T extends ITimeline> {
    void delete(String name) throws Exception;

    void close(String name) throws Exception;

    T get(String name) throws Exception;
}
