package ch.hsr.ebos.aion.core.timeline;

import java.util.concurrent.CompletableFuture;

public interface ITimeline {
    CompletableFuture<TimelineEvent> get(String id);

    CompletableFuture<TimelineEvent> prev(String id);

    CompletableFuture<TimelineEvent> next(String id);

    CompletableFuture<TimelineEvent> first();

    CompletableFuture<TimelineEvent> last();

    CompletableFuture<Void> add(TimelineEvent event);

    CompletableFuture<TimelineInfo> info();
}
