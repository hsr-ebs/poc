package ch.hsr.ebos.aion.core.application.interceptor;

import java.util.concurrent.CompletableFuture;

public interface IApplicationInterceptorContext {
    CompletableFuture<Void> next();

    CompletableFuture<Void> abort(Throwable throwable);
}
