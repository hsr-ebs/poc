package ch.hsr.ebos.aion.core.module;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.util.Modules;

import java.util.ArrayList;
import java.util.List;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationLifetime;
import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptor;
import ch.hsr.ebos.aion.core.event.IEvent;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;

public abstract class Module extends AbstractModule {

    private Module parent;
    private EventRouter eventRouter = new EventRouter();
    private List<Module> modules = new ArrayList<>();

    protected abstract void setup();

    public com.google.inject.Module getInjectModule() {
        if (modules.size() > 0) {
            List<com.google.inject.Module> injectModules = new ArrayList<>();
            for (Module module : modules) {
                injectModules.add(module.getInjectModule());
            }
            return Modules.override(injectModules).with(this);
        }
        return this;
    }


    @Override
    protected void configure() {
        setup();

        if (parent != null) {
            return;
        }

        for (Class aggregate : eventRouter.getAggregates()) {
            bind(aggregate);
        }

        ensureMultiBinder(IEventInterceptor.class);
        ensureMultiBinder(IApplicationInterceptor.class);

        bind(EventRouter.class).toInstance(eventRouter);
        bind(Application.class);
        bind(ApplicationLifetime.class);
    }

    public void addModule(Module module) {
        module.parent = this;

        eventRouter.addRouter(module.eventRouter);
        modules.add(module);
    }

    @SuppressWarnings("unchecked")
    public <TEvent extends IEvent, TAggregate extends IEventAggregate<TEvent>> void addEventAggregate(Class<TEvent> event, Class<TAggregate> aggregate) {
        this.eventRouter.addAggregate(event, aggregate);
    }

    @SuppressWarnings("unchecked")
    public <TInterceptor extends IApplicationInterceptor> void addApplicationInterceptors(Class<TInterceptor> interceptorClass) {
        bindMultiple(IApplicationInterceptor.class, interceptorClass);
    }

    @SuppressWarnings("unchecked")
    public <TInterceptor extends IEventInterceptor> void addEventInterceptors(Class<TInterceptor> interceptorClass) {
        bindMultiple(IEventInterceptor.class, interceptorClass);
    }

    public void bindMultiple(Class key, Class value) {
        Multibinder binders = ensureMultiBinder(key);
        binders.addBinding().to(value);
    }

    protected Multibinder ensureMultiBinder(Class key) {
        return Multibinder.newSetBinder(binder(), key);
    }
}