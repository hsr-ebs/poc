package ch.hsr.ebos.aion.core.application.interceptor;

import java.util.concurrent.CompletableFuture;

public class ApplicationInterceptorContext implements IApplicationInterceptorContext {
    private CompletableFuture<Void> applicationStopFuture;
    private CompletableFuture<Void> completeFuture = new CompletableFuture<>();

    public ApplicationInterceptorContext(CompletableFuture<Void> applicationStopFuture) {
        this.applicationStopFuture = applicationStopFuture;
    }

    @Override
    public CompletableFuture<Void> next() {
        this.completeFuture.complete(null);
        return this.applicationStopFuture;
    }

    @Override
    public CompletableFuture<Void> abort(Throwable throwable) {
        this.completeFuture.completeExceptionally(throwable);
        return this.applicationStopFuture;
    }

    public CompletableFuture<Void> getCompleteFuture() {
        return completeFuture;
    }
}
