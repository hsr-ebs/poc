package ch.hsr.ebos.aion.core.application;

import com.google.inject.Inject;
import com.google.inject.Injector;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.application.result.AggregateResult;
import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.application.result.InterceptorResult;
import ch.hsr.ebos.aion.core.event.EventTypeUtil;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregateContext;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregateService;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;
import ch.hsr.ebos.aion.core.event.interceptor.EventInterceptorContext;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptor;
import ch.hsr.ebos.aion.core.event.interceptor.IEventInterceptorResultCallback;
import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;

public class Application {

    @Inject
    public Injector injector;

    @Inject
    EventAggregateService aggregateService;

    @Inject
    Set<IEventInterceptor> eventInterceptors;

    @Inject
    ApplicationLifetime lifetime;

    private CompletableFuture<Void> runAggregates(EmitResult result, List<IEventAggregate> aggregates, ApplicationEvent event) {
        List<CompletableFuture<Void>> aggregateFutures = new ArrayList<>();
        for (IEventAggregate aggregate : aggregates) {
            EventAggregateContext aggregateContext = new EventAggregateContext();
            aggregate.aggregate(event, aggregateContext);
            aggregateFutures.add(aggregateContext.getCompleteFuture());
            aggregateContext.getCompleteFuture().whenComplete((aggregateResults, aggregateThrowable) -> {
                result.addAggregateResult(new AggregateResult(aggregate.getClass(), aggregateThrowable));
            });
        }

        return CompletableFuture.allOf(aggregateFutures.toArray(new CompletableFuture[aggregateFutures.size()]));
    }

    public CompletableFuture<EmitResult> emit(String type, Object event) {
        return this.emit(new ApplicationEvent(type, event, new TimelineEventAuth()));
    }

    public CompletableFuture<EmitResult> emit(String type, Object event, TimelineEventAuth auth) {
        return this.emit(new ApplicationEvent(type, event, auth));
    }

    public CompletableFuture<EmitResult> emit(Object event) {
        return emit(event, null);
    }

    public CompletableFuture<EmitResult> emit(Object event, TimelineEventAuth auth) {
        String type = null;
        if (event != null) {
            type = EventTypeUtil.getEventType(event.getClass());
        }
        return this.emit(type, event, auth);
    }

    public CompletableFuture<EmitResult> emit(ApplicationEvent event) {
        EmitResult result = new EmitResult();
        CompletableFuture<EmitResult> emitFuture = new CompletableFuture<>();

        if (!lifetime.isRunning()) {
            result.addThrowable(new Exception("application is not running"));
            emitFuture.complete(result);
            return emitFuture;
        }

        List<IEventAggregate> aggregates = this.aggregateService.getAggregates(event.getType());

        List<IEventInterceptorResultCallback> interceptorCallbacks = new ArrayList<>();
        List<CompletableFuture<IEventInterceptorResultCallback>> interceptorFutures = new ArrayList<>();
        for (IEventInterceptor interceptor : this.eventInterceptors) {
            EventInterceptorContext interceptorContext = new EventInterceptorContext(emitFuture, aggregates, event);
            interceptor.intercept(interceptorContext);
            interceptorFutures.add(interceptorContext.getInterceptFuture());
            interceptorContext.getInterceptFuture().whenComplete((r, throwable) -> {
                if (r != null) {
                    interceptorCallbacks.add(r);
                }
                result.addInterceptorResult(new InterceptorResult(interceptor.getClass(), throwable));
            });
        }

        CompletableFuture<Void> interceptorFuture = CompletableFuture.allOf(interceptorFutures.toArray(new CompletableFuture[interceptorFutures.size()]));

        interceptorFuture.whenComplete((interceptResult, interceptThrowable) -> {
            if (interceptThrowable != null) {
                emitFuture.complete(result);
            } else {
                CompletableFuture<Void> aggregateFuture = runAggregates(result, aggregates, event);

                aggregateFuture.whenComplete((aggregateResults, aggregateThrowable) -> {
                    List<CompletableFuture<Void>> interceptorCallbackFutures = new ArrayList<>();
                    for (IEventInterceptorResultCallback callback : interceptorCallbacks) {
                        CompletableFuture<Void> resultFuture = callback.onResult(result);
                        resultFuture.whenComplete((interceptCallbackResult, throwable) -> {
                            if (throwable != null) {
                                result.addThrowable(throwable);
                            }
                        });
                        interceptorCallbackFutures.add(resultFuture);
                    }
                    CompletableFuture<Void> interceptorResultFuture = CompletableFuture.allOf(interceptorCallbackFutures.toArray(new CompletableFuture[interceptorCallbackFutures.size()]));
                    interceptorResultFuture.whenComplete((interceptCallbackResult, throwable) -> {
                        emitFuture.complete(result);
                    });
                });
            }
        });

        return emitFuture;
    }

    public CompletableFuture<Void> close(Throwable throwable) {
        return this.lifetime.stop(throwable);
    }

    public CompletableFuture<Void> close() {
        return this.lifetime.stop();
    }
}