package ch.hsr.ebos.aion.core.processor;

import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.event.EventType;

@EventType("process.result")
public class ProcessResultEvent {
    private EmitResult result;

    public ProcessResultEvent(EmitResult result) {
        this.result = result;
    }

    public EmitResult getResult() {
        return result;
    }
}
