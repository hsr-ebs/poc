package ch.hsr.ebos.aion.core.event.interceptor;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;

public interface IEventInterceptorContext {

    List<IEventAggregate> getAggregators();

    ApplicationEvent getEvent();

    CompletableFuture<Void> next();

    CompletableFuture<Void> next(IEventInterceptorResultCallback callback);

    CompletableFuture<Void> abort(Throwable throwable);
}
