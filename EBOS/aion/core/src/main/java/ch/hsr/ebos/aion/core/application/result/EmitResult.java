package ch.hsr.ebos.aion.core.application.result;

import java.util.ArrayList;
import java.util.List;

public class EmitResult {
    private List<String> errorMessages = new ArrayList<>();
    private List<AggregateResult> aggregateResults = new ArrayList<>();

    private List<InterceptorResult> interceptorResults = new ArrayList<>();
    private Boolean successfull = true;

    public void addThrowable(Throwable throwable) {
        if (throwable.getMessage() != null) {
            this.errorMessages.add(throwable.getMessage());
        } else {
            this.errorMessages.add(throwable.toString());
        }
        this.successfull = false;
    }

    public void addInterceptorResult(InterceptorResult result) {
        this.interceptorResults.add(result);
        if (!result.isSuccessfull()) {
            this.successfull = false;
            this.errorMessages.add(result.getErrorMessage());
        }
    }

    public void addAggregateResult(AggregateResult result) {
        this.aggregateResults.add(result);
        if (!result.isSuccessfull()) {
            this.successfull = false;
            this.errorMessages.add(result.getErrorMessage());
        }
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public List<InterceptorResult> getInterceptorResults() {
        return interceptorResults;
    }

    public List<AggregateResult> getAggregateResults() {
        return aggregateResults;
    }

    public Boolean isSuccessfull() {
        return successfull;
    }
}
