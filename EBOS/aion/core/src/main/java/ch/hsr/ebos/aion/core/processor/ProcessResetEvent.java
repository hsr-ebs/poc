package ch.hsr.ebos.aion.core.processor;

import ch.hsr.ebos.aion.core.event.EventType;
import ch.hsr.ebos.aion.core.event.IEvent;

@EventType("process.reset.event")
public class ProcessResetEvent implements IEvent {
    private String eventId;

    public ProcessResetEvent(String eventId) {
        this.eventId = eventId;
    }

    public String getEventId() {
        return eventId;
    }
}
