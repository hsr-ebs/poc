package ch.hsr.ebos.aion.core.event.aggregate;

import com.google.inject.Injector;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ch.hsr.ebos.aion.core.module.EventRouter;

public class EventAggregateService {
    @Inject
    EventRouter eventRouter;

    @Inject
    Injector injector;

    public Class getEventType(String eventType) {
        return this.eventRouter.getEventType(eventType);
    }

    public List<IEventAggregate> getAggregates(String eventType) {

        List<Class> aggregateClasses = eventRouter.getAggregates(eventType);

        List<IEventAggregate> aggregates = new ArrayList<>();
        for (Class aggregateClass : aggregateClasses) {
            aggregates.add((IEventAggregate) this.injector.getInstance(aggregateClass));
        }

        return aggregates;
    }
}
