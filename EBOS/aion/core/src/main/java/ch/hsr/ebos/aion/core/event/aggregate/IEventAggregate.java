package ch.hsr.ebos.aion.core.event.aggregate;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;

public interface IEventAggregate<TEvent> {
    void aggregate(ApplicationEvent<TEvent> event, IEventAggregateContext context);
}
