package ch.hsr.ebos.aion.core.processor;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.event.EventTypeUtil;
import ch.hsr.ebos.aion.core.event.aggregate.EventAggregateService;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.TimelineEvent;

public class TimelineProcessor {
    private Application application;
    private EventAggregateService eventAggregateService;
    private ITimeline processTimeline;
    private IDynamicTimeline resultTimeline;

    public TimelineProcessor(Application application,
                             ITimeline processTimeline,
                             IDynamicTimeline resultTimeline) {
        this.application = application;
        this.processTimeline = processTimeline;
        this.resultTimeline = resultTimeline;
        this.eventAggregateService = this.application.injector.getInstance(EventAggregateService.class);
    }

    public void emit(Object event) throws Exception {
        String type = null;
        if (event != null) {
            type = EventTypeUtil.getEventType(event.getClass());
        }

        processTimeline.add(new TimelineEvent(type, event, null)).get();
        this.run();
    }

    public void reset() throws Exception {
        while (resultTimeline.pop().get() != null) {
            //pop results until timeline is empty
        }
        application.emit(new ProcessResetEvent(null), null).get();
    }

    public void reset(String eventId) throws Exception {
        if (eventId == null) {
            reset();
            return;
        }

        TimelineEvent targetEvent = resultTimeline.get(eventId).get();
        if (targetEvent == null) {
            return;
        }

        TimelineEvent lastEvent = resultTimeline.last().get();

        while (lastEvent != null && !lastEvent.getId().equals(eventId)) {
            resultTimeline.pop().get();
            application.emit(new ProcessResetEvent(lastEvent.getId()), null).get();
            lastEvent = resultTimeline.last().get();
        }

    }

    private String getCurrentEventId() throws ExecutionException, InterruptedException {
        TimelineEvent event = resultTimeline.last().get();
        if (event == null) {
            return null;
        }

        return event.getId();
    }

    private void setEventResult(String eventId, EmitResult result) throws ExecutionException, InterruptedException {
        resultTimeline.add(TimelineEvent.fromObject(eventId, new ProcessResultEvent(result), null)).get();
    }

    public void run() throws Exception {
        synchronized (this) {
            String currentEventId = getCurrentEventId();
            TimelineEvent event = null;

            do {
                event = processTimeline.next(currentEventId).get();

                if (event == null) {
                    return;
                }

                EmitResult result = null;
                Class eventClass = eventAggregateService.getEventType(event.getType());
                if (eventClass != null) {
                    ApplicationEvent appEvent = new ApplicationEvent(
                            event.getId(), event.getType(), event.getData(eventClass), event.getAuth());

                    result = application.emit(appEvent).get();
                } else {
                    result = new EmitResult();
                }

                currentEventId = event.getId();
                setEventResult(currentEventId, result);
            } while (true);
        }
    }
}
