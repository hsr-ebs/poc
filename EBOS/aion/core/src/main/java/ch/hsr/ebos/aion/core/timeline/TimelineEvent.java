package ch.hsr.ebos.aion.core.timeline;

import com.google.gson.JsonElement;

import java.util.Date;
import java.util.UUID;

import ch.hsr.ebos.aion.core.event.EventTypeUtil;
import ch.hsr.ebos.aion.core.util.JsonUtil;

public class TimelineEvent {
    private String id;
    private String type;
    private JsonElement data;
    private TimelineEventAuth auth;
    private Date created;

    public TimelineEvent() {

    }

    public TimelineEvent(String id, String type, Object data, TimelineEventAuth auth) {
        this(id, type, JsonUtil.getGson().toJsonTree(data), auth);
    }

    public TimelineEvent(String type, Object data, TimelineEventAuth auth) {
        this(type, JsonUtil.getGson().toJsonTree(data), auth);
    }

    public TimelineEvent(String type, JsonElement data, TimelineEventAuth auth) {
        this(UUID.randomUUID().toString(), type, data, auth);
    }

    public TimelineEvent(String id, String type, JsonElement data, TimelineEventAuth auth) {
        this.id = id;
        this.type = type;
        this.data = data;
        this.auth = auth;
        this.created = new Date();
    }

    public static TimelineEvent fromObject(String id, Object object, TimelineEventAuth auth) {
        String type = EventTypeUtil.getEventType(object.getClass());
        return new TimelineEvent(id, type, object, auth);
    }

    public static TimelineEvent fromObject(Object object, TimelineEventAuth auth) {
        String type = EventTypeUtil.getEventType(object.getClass());
        return new TimelineEvent(type, object, auth);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public JsonElement getData() {
        return data;
    }

    public void setData(JsonElement data) {
        this.data = data;
    }

    public String getStringData() {
        return JsonUtil.getGson().toJson(data);
    }

    public <T> T getData(Class<T> dataClass) {
        return JsonUtil.getGson().fromJson(data, dataClass);
    }

    public TimelineEventAuth getAuth() {
        return auth;
    }

    public void setAuth(TimelineEventAuth auth) {
        this.auth = auth;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
