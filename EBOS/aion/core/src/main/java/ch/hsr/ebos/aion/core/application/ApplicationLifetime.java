package ch.hsr.ebos.aion.core.application;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.application.interceptor.ApplicationInterceptorContext;
import ch.hsr.ebos.aion.core.application.interceptor.IApplicationInterceptor;

@Singleton
public class ApplicationLifetime {
    private CompletableFuture<Void> appStartFuture = new CompletableFuture<>();
    private CompletableFuture<Void> appStopFuture = new CompletableFuture<>();

    private Set<IApplicationInterceptor> applicationInterceptors;

    @Inject
    public ApplicationLifetime(Set<IApplicationInterceptor> applicationInterceptors) {
        this.applicationInterceptors = applicationInterceptors;
    }

    public boolean isRunning() {
        return appStartFuture.isDone() && !appStopFuture.isDone();
    }

    public CompletableFuture<Void> start() {
        List<CompletableFuture<Void>> nextFutures = new ArrayList<>();

        for (IApplicationInterceptor interceptor : applicationInterceptors) {
            ApplicationInterceptorContext interceptorContext = new ApplicationInterceptorContext(appStopFuture);
            interceptor.intercept(interceptorContext);
            nextFutures.add(interceptorContext.getCompleteFuture());
        }

        CompletableFuture<Void> nextFuture = CompletableFuture.allOf(nextFutures.toArray(new CompletableFuture[nextFutures.size()]));
        nextFuture.whenComplete((r, throwable) -> {
            if (throwable == null) {
                appStartFuture.complete(null);
            } else {
                appStartFuture.completeExceptionally(throwable);
                appStopFuture.completeExceptionally(throwable);
            }
        });

        return appStartFuture;
    }

    public CompletableFuture<Void> stop() {
        return this.stop(null);
    }

    public CompletableFuture<Void> stop(Throwable throwable) {
        if (throwable == null) {
            this.appStopFuture.complete(null);
        } else {
            this.appStopFuture.completeExceptionally(throwable);
        }

        return this.appStopFuture;
    }
}
