package ch.hsr.ebos.aion.core.application.result;

public class InterceptorResult {
    private String interceptor;
    private boolean successfull;
    private String errorMessage;

    public InterceptorResult(Class interceptor, Throwable throwable) {
        this.successfull = throwable == null;
        this.interceptor = interceptor.getName();
        this.errorMessage = throwable != null ? throwable.getMessage() : null;
    }

    public String getInterceptor() {
        return interceptor;
    }

    public boolean isSuccessfull() {
        return successfull;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
