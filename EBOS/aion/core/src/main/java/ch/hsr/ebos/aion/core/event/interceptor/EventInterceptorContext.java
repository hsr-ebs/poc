package ch.hsr.ebos.aion.core.event.interceptor;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;
import ch.hsr.ebos.aion.core.application.result.EmitResult;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;

public class EventInterceptorContext implements IEventInterceptorContext {
    private List<IEventAggregate> aggregators;
    private ApplicationEvent event;
    private CompletableFuture<EmitResult> emitFuture;
    private CompletableFuture<IEventInterceptorResultCallback> interceptFuture = new CompletableFuture<>();

    public EventInterceptorContext(CompletableFuture<EmitResult> emitFuture,
                                   List<IEventAggregate> aggregators,
                                   ApplicationEvent event) {
        this.emitFuture = emitFuture;
        this.aggregators = aggregators;
        this.event = event;
    }

    public List<IEventAggregate> getAggregators() {
        return aggregators;
    }

    public ApplicationEvent getEvent() {
        return event;
    }


    public CompletableFuture<Void> next() {
        return this.next(null);
    }

    @Override
    public CompletableFuture<Void> next(IEventInterceptorResultCallback callback) {
        this.interceptFuture.complete(callback);
        return emitFuture.thenApply(c -> null);
    }

    @Override
    public CompletableFuture<Void> abort(Throwable throwable) {
        this.interceptFuture.completeExceptionally(throwable);
        return emitFuture.thenApply(c -> null);
    }

    public CompletableFuture<IEventInterceptorResultCallback> getInterceptFuture() {
        return interceptFuture;
    }
}
