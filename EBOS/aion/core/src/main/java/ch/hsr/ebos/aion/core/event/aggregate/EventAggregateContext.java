package ch.hsr.ebos.aion.core.event.aggregate;

import java.util.concurrent.CompletableFuture;

public class EventAggregateContext implements IEventAggregateContext {
    private CompletableFuture<Void> completeFuture = new CompletableFuture<>();

    public CompletableFuture<Void> getCompleteFuture() {
        return completeFuture;
    }

    public void complete() {
        this.completeFuture.complete(null);
    }

    public void abort(Throwable throwable) {
        this.completeFuture.completeExceptionally(throwable);
    }
}
