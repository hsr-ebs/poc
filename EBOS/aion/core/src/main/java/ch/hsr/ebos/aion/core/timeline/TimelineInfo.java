package ch.hsr.ebos.aion.core.timeline;

public class TimelineInfo {
    private String firstEvent;
    private String lastEvent;

    public TimelineInfo() {
    }

    public TimelineInfo(String firstEvent, String lastEvent) {
        this.firstEvent = firstEvent;
        this.lastEvent = lastEvent;
    }

    public String getLastEvent() {
        return lastEvent;
    }

    public void setLastEvent(String lastEvent) {
        this.lastEvent = lastEvent;
    }

    public String getFirstEvent() {
        return firstEvent;
    }

    public void setFirstEvent(String firstEvent) {
        this.firstEvent = firstEvent;
    }
}
