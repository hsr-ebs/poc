package ch.hsr.ebos.aion.core.exception;

public class CreateEventException extends Exception {
    public CreateEventException(String message) {
        super(message);
    }
}
