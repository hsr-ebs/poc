package ch.hsr.ebos.aion.core.application;

import java.util.UUID;

import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;

public class ApplicationEvent<TData> {
    private String id;
    private String type;
    private TData data;
    private TimelineEventAuth auth;

    public ApplicationEvent(String id, String type, TData data, TimelineEventAuth auth) {
        this.id = id;
        this.type = type;
        this.data = data;
        this.auth = auth;
    }

    public ApplicationEvent(String type, TData data, TimelineEventAuth auth) {
        this(UUID.randomUUID().toString(), type, data, auth);
    }

    public TData getData() {
        return data;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public TimelineEventAuth getAuth() {
        return auth;
    }
}
