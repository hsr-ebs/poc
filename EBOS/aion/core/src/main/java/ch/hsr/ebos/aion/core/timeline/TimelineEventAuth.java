package ch.hsr.ebos.aion.core.timeline;

import java.util.ArrayList;
import java.util.List;

public class TimelineEventAuth {
    private String userId;
    private String username;
    private List<String> roles;

    public TimelineEventAuth() {
        roles = new ArrayList<>();
    }

    public TimelineEventAuth(String userId, String username, List<String> roles) {
        this.userId = userId;
        this.username = username;
        this.roles = roles;
    }

    public List<String> getRoles() {
        return roles;
    }

    public String getUsername() {
        return username;
    }

    public String getUserId() {
        return userId;
    }
}
