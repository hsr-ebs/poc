package ch.hsr.ebos.aion.core.application.result;

public class AggregateResult {
    private String aggregate;
    private boolean successfull;
    private String errorMessage;

    public AggregateResult(Class aggregate, Throwable throwable) {
        this.successfull = throwable == null;
        this.aggregate = aggregate.getName();
        this.errorMessage = throwable != null ? throwable.getMessage() : null;
    }

    public String getAggregate() {
        return aggregate;
    }

    public boolean isSuccessfull() {
        return successfull;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
