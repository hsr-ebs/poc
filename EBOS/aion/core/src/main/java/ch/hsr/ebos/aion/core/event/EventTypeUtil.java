package ch.hsr.ebos.aion.core.event;

public class EventTypeUtil {
    public static String getEventType(Class eventClass) {
        EventType type = ((EventType) eventClass.getAnnotation(EventType.class));
        if (type != null) {
            return type.value();
        }
        return null;
    }
}
