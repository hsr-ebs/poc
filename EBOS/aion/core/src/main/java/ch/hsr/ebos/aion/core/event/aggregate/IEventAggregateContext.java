package ch.hsr.ebos.aion.core.event.aggregate;

public interface IEventAggregateContext {
    void complete();

    void abort(Throwable throwable);
}
