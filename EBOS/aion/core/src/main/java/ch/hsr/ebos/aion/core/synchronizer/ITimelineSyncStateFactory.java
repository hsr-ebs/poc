package ch.hsr.ebos.aion.core.synchronizer;

public interface ITimelineSyncStateFactory {
    ITimelineSyncState get(String name) throws Exception;

    void delete(String name) throws Exception;

    void close(String name) throws Exception;
}
