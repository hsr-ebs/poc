package ch.hsr.ebos.aion.core.exception;

public class GetEventException extends Exception {

    public GetEventException(String message) {
        super(message);
    }

}
