package ch.hsr.ebos.aion.core.exception;

public class DuplicateEventException extends Exception {
    public DuplicateEventException(String id) {
        super("event id " + id + " is already in use");
    }
}
