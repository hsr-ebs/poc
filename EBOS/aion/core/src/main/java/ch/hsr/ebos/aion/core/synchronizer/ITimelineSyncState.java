package ch.hsr.ebos.aion.core.synchronizer;

public interface ITimelineSyncState {
    SyncState get(String id) throws Exception;

    void set(String id, SyncState state) throws Exception;

    boolean isSynced(String id) throws Exception;

    String getLastPulled() throws Exception;

    String getLastPushed() throws Exception;
}
