package ch.hsr.ebos.aion.core.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ch.hsr.ebos.aion.core.event.EventTypeUtil;
import ch.hsr.ebos.aion.core.event.IEvent;
import ch.hsr.ebos.aion.core.event.aggregate.IEventAggregate;

public class EventRouter {
    private Map<String, Class> eventTypes = new HashMap<>();
    private Map<String, List<Class>> eventAggregators = new HashMap<>();
    private List<EventRouter> routers = new ArrayList<>();

    public <TEvent extends IEvent, TAggregate extends IEventAggregate<TEvent>> void addAggregate(Class<TEvent> event, Class<TAggregate> aggregate) {
        String type = EventTypeUtil.getEventType(event);

        if (type == null) {
            System.out.println("missing EventType annotation on event class " + event.toString());
        }

        if (!eventTypes.containsKey(event)) {
            this.eventTypes.put(type, event);
        }

        if (!this.eventAggregators.containsKey(type)) {
            this.eventAggregators.put(type, new ArrayList<>());
        }

        this.eventAggregators.get(type).add(aggregate);
    }

    public void addRouter(EventRouter router) {
        this.routers.add(router);
    }

    public Class getEventType(String eventType) {
        Class event = this.eventTypes.get(eventType);
        if (event != null) {
            return event;
        }

        for (EventRouter router : routers) {
            event = router.getEventType(eventType);
            if (event != null) {
                return event;
            }
        }

        return null;
    }

    public List<Class> getEvents() {
        List<Class> result = new ArrayList<>();

        for (Class event : eventTypes.values()) {
            if (!result.contains(event)) {
                result.add(event);
            }
        }

        for (EventRouter router : routers) {
            for (Class event : router.getEvents()) {
                if (!result.contains(event)) {
                    result.add(event);
                }
            }
        }

        return result;
    }

    public List<Class> getAggregates() {
        List<Class> result = new ArrayList<>();

        for (List<Class> aggregates : eventAggregators.values()) {
            for (Class aggregate : aggregates) {
                if (!result.contains(aggregate)) {
                    result.add(aggregate);
                }
            }
        }

        for (EventRouter router : routers) {
            for (Class aggregate : router.getAggregates()) {
                if (!result.contains(aggregate)) {
                    result.add(aggregate);
                }
            }
        }

        return result;
    }

    public List<Class> getAggregates(String eventType) {
        List<Class> result = new ArrayList<>();

        List<Class> aggregates = this.eventAggregators.get(eventType);
        if (aggregates != null) {
            for (Class aggregate : aggregates) {
                if (!result.contains(aggregate)) {
                    result.add(aggregate);
                }
            }
        }

        for (EventRouter router : routers) {
            for (Class aggregate : router.getAggregates(eventType)) {
                if (!result.contains(aggregate)) {
                    result.add(aggregate);
                }
            }
        }

        return result;
    }
}
