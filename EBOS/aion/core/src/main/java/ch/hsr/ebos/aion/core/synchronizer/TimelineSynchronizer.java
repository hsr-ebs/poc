package ch.hsr.ebos.aion.core.synchronizer;

import java.util.concurrent.ExecutionException;

import ch.hsr.ebos.aion.core.exception.DuplicateEventException;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.TimelineEvent;
import ch.hsr.ebos.aion.core.timeline.TimelineInfo;

public class TimelineSynchronizer {
    public void push(ITimeline local, ITimeline remote, ITimelineSyncState state) throws Throwable {
        synchronized (this) {
            try {
                TimelineInfo localInfo = local.info().get();
                TimelineInfo remoteInfo = remote.info().get();

                if (localInfo.getLastEvent() == null && remoteInfo.getLastEvent() == null) {
                    return;
                }

                if (localInfo.getLastEvent() != null && remoteInfo.getLastEvent() != null &&
                        localInfo.getLastEvent().equals(remoteInfo.getLastEvent())) {
                    return;
                }

                String lastPushId = state.getLastPushed();
                TimelineEvent event;

                do {
                    event = local.next(lastPushId).get();

                    if (event != null) {
                        if(!state.isSynced(event.getId())) {
                            try {
                                remote.add(event).get();
                            } catch (ExecutionException e) {
                                if (!(e.getCause() instanceof DuplicateEventException)) {
                                    throw e.getCause();
                                }
                            }
                        }

                        state.set(event.getId(), SyncState.PUSHED);
                        lastPushId = event.getId();
                    }
                } while (event != null);
            } catch (ExecutionException e) {
                throw e.getCause();
            }
        }
    }

    public void pull(IDynamicTimeline local, ITimeline remote, ITimelineSyncState state) throws Throwable {
        synchronized (this) {
            try {
                TimelineInfo remoteInfo = remote.info().get();
                String lastPullId = state.getLastPulled();

                if (lastPullId != null && lastPullId.equals(remoteInfo.getLastEvent())) {
                    return;
                }

                TimelineEvent event;

                do {
                    event = remote.next(lastPullId).get();

                    if (event != null) {
                        TimelineEvent localEvent = local.next(lastPullId).get();
                        if (localEvent == null || !localEvent.getId().equals(event.getId())) {
                            try {
                                local.insertAfter(event, lastPullId).get();
                            } catch (ExecutionException e) {
                                if (!(e.getCause() instanceof DuplicateEventException)) {
                                    throw e.getCause();
                                }
                            }
                        }

                        state.set(event.getId(), SyncState.PULLED);
                        lastPullId = event.getId();
                    }
                } while (event != null);
            } catch (ExecutionException e) {
                throw e.getCause();
            }
        }
    }

    public void sync(IDynamicTimeline local, ITimeline remote, ITimelineSyncState state) throws Throwable {
        this.push(local, remote, state);
        this.pull(local, remote, state);
    }
}
