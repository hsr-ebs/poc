package ch.hsr.ebos.aion.core.event.interceptor;

public interface IEventInterceptor {
    void intercept(IEventInterceptorContext ctx);
}
