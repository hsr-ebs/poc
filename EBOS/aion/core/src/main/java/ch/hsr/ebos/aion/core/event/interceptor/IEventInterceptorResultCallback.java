package ch.hsr.ebos.aion.core.event.interceptor;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.application.result.EmitResult;

public interface IEventInterceptorResultCallback {
    CompletableFuture<Void> onResult(EmitResult result);
}
