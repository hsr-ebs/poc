package ch.hsr.ebos.aion.core.event.aggregate;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.application.ApplicationEvent;

public abstract class AsyncEventAggregate<TEvent> implements IEventAggregate<TEvent> {
    public abstract CompletableFuture<?> aggregate(ApplicationEvent<TEvent> event);

    @Override
    public void aggregate(ApplicationEvent<TEvent> event, IEventAggregateContext context) {
        this.aggregate(event).whenComplete((result, throwable) -> {
            if (throwable != null) {
                context.abort(throwable);
            } else {
                context.complete();
            }
        });
    }
}
