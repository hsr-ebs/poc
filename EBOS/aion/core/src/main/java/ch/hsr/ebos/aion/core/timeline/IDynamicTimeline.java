package ch.hsr.ebos.aion.core.timeline;

import java.util.concurrent.CompletableFuture;

public interface IDynamicTimeline extends ITimeline {
    CompletableFuture<Void> insertBefore(TimelineEvent event, String beforeId);

    CompletableFuture<Void> insertAfter(TimelineEvent event, String afterId);

    CompletableFuture<String> pop();
}
