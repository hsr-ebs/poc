package ch.hsr.ebos.aion.http;

import java.util.concurrent.CompletableFuture;

public interface HttpRequestInterceptor {
    CompletableFuture<String> provideAuthorizationHeader();
}
