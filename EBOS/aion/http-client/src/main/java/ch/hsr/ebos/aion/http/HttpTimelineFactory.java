package ch.hsr.ebos.aion.http;

import java.util.HashMap;
import java.util.Map;

import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;

public class HttpTimelineFactory implements ITimelineFactory {
    private String baseUrl;
    private HttpRequestInterceptor requestInterceptor;
    private Map<String, HttpTimeline> timelineMap = new HashMap<>();

    public HttpTimelineFactory(String baseUrl, HttpRequestInterceptor requestInterceptor) {
        this.baseUrl = baseUrl;
        this.requestInterceptor = requestInterceptor;
    }

    @Override
    public void delete(String name) {
        this.timelineMap.remove(name);
    }

    @Override
    public void close(String name) throws Exception {
        this.timelineMap.remove(name);
    }

    @Override
    public ITimeline get(String name) {
        if (timelineMap.containsKey(name)) {
            return timelineMap.get(name);
        }
        HttpTimeline timeline = new HttpTimeline(this.baseUrl, name, requestInterceptor);
        timelineMap.put(name, timeline);
        return timeline;
    }
}
