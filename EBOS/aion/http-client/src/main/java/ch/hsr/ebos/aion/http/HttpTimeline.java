package ch.hsr.ebos.aion.http;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.exception.DuplicateEventException;
import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.TimelineEvent;
import ch.hsr.ebos.aion.core.timeline.TimelineInfo;
import ch.hsr.ebos.aion.core.util.JsonUtil;

public class HttpTimeline implements ITimeline {
    private String timeline;
    private HttpRequestInterceptor requestInterceptor;
    private String url;
    private OkHttpClient client = new OkHttpClient();
    private Gson gson = JsonUtil.getGson();

    HttpTimeline(String url, String timeline, HttpRequestInterceptor requestInterceptor) {
        this.url = url;
        this.timeline = timeline;
        this.requestInterceptor = requestInterceptor;
    }

    private String getBaseUrl() {
        return url + "/api/timeline/" + timeline;
    }

    private void interceptRequest(Request.Builder requestBuilder) throws Throwable {
        if (this.requestInterceptor == null) {
            return;
        }

        CompletableFuture<String> authorizationFuture = this.requestInterceptor.provideAuthorizationHeader();
        if (authorizationFuture != null && authorizationFuture.get() != null) {
            requestBuilder.addHeader("Authorization", authorizationFuture.get());
        }
    }

    private CompletableFuture<TimelineEvent> getEvent(String url) {
        CompletableFuture<TimelineEvent> result = new CompletableFuture<>();
        Request.Builder requestBuilder = new Request.Builder()
                .url(getBaseUrl() + url);

        try {
            interceptRequest(requestBuilder);
        } catch (Throwable throwable) {
            result.completeExceptionally(throwable);
            return result;
        }

        Request request = requestBuilder
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                result.completeExceptionally(e);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (response.code() == 404) {
                    result.complete(null);
                } else if (response.code() == 200) {
                    try (ResponseBody body = response.body()) {
                        TimelineEvent event = gson.fromJson(body.string(), TimelineEvent.class);
                        result.complete(event);
                    }
                } else {
                    result.completeExceptionally(new Exception("unknown http status code"));
                }
            }
        });

        return result;
    }

    @Override
    public CompletableFuture<TimelineEvent> get(String id) {
        return getEvent("/event/" + id);
    }

    @Override
    public CompletableFuture<TimelineEvent> prev(String id) {
        return getEvent("/event/" + id + "/prev");
    }

    @Override
    public CompletableFuture<TimelineEvent> next(String id) {
        if (id == null) {
            return first();
        }

        return getEvent("/event/" + id + "/next");
    }

    @Override
    public CompletableFuture<TimelineEvent> first() {
        return getEvent("/first");
    }

    @Override
    public CompletableFuture<TimelineEvent> last() {
        return getEvent("/last");
    }

    @Override
    public CompletableFuture<Void> add(TimelineEvent event) {
        CompletableFuture<Void> result = new CompletableFuture<>();
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), gson.toJson(event));
        Request.Builder requestBuilder = new Request.Builder()
                .url(getBaseUrl() + "/event")
                .method("POST", body);

        try {
            interceptRequest(requestBuilder);
        } catch (Throwable throwable) {
            result.completeExceptionally(throwable);
            return result;
        }

        Request request = requestBuilder
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                result.completeExceptionally(e);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                if (response.code() == 200) {
                    result.complete(null);
                } else if (response.code() == 500) {
                    try (ResponseBody body = response.body()) {
                        JsonObject object = JsonUtil.getGson().fromJson(body.string(), JsonObject.class);
                        String type = object.get("type").getAsString();
                        String message = type;
                        if (object.has("message")) {
                            object.get("message").getAsString();
                        }

                        if (type.equals(DuplicateEventException.class.getSimpleName())) {
                            result.completeExceptionally(new DuplicateEventException(message));
                        } else {
                            result.completeExceptionally(new Exception(message));
                        }
                    }
                } else {
                    try (ResponseBody body = response.body()) {
                        System.out.println(body.string());
                    }
                    result.completeExceptionally(new Exception("unknown http status code " + response.code()));
                }
            }
        });

        return result;
    }

    @Override
    public CompletableFuture<TimelineInfo> info() {
        CompletableFuture<TimelineInfo> result = new CompletableFuture<>();
        Request.Builder requestBuilder = new Request.Builder()
                .url(getBaseUrl() + "/info");

        try {
            interceptRequest(requestBuilder);
        } catch (Throwable throwable) {
            result.completeExceptionally(throwable);
            return result;
        }

        Request request = requestBuilder
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                result.completeExceptionally(e);
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try (ResponseBody body = response.body()) {
                    if (response.code() == 404) {
                        result.complete(null);
                    } else if (response.code() == 200) {
                        TimelineInfo info = gson.fromJson(body.string(), TimelineInfo.class);
                        result.complete(info);
                    } else {
                        result.completeExceptionally(new Exception("unknown http status code"));
                    }
                }
            }
        });

        return result;
    }
}
