package ch.hsr.ebos.aion.http;

import org.junit.After;
import org.junit.Before;

import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineTest;
import ch.hsr.ebos.aion.server.AionHttpServer;
import ch.hsr.ebos.aion.server.config.AuthNoneConfiguration;
import ch.hsr.ebos.aion.server.config.Configuration;
import ch.hsr.ebos.aion.server.config.MemoryTimelineConfiguration;

public class HttpTimelineTest extends TimelineTest {
    private static final String timelineName = "test";
    private static final int serverPort = 8888;
    private static final String serverHost = "127.0.0.1";

    AionHttpServer httpServer;

    @Before
    public void setUp() throws Exception {
        Configuration serverConfig = new Configuration();
        serverConfig.addTimeline(timelineName, new MemoryTimelineConfiguration());
        serverConfig.setAuth(new AuthNoneConfiguration());

        httpServer = new AionHttpServer(serverConfig);
        httpServer.listen(serverPort).get();
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        if (httpServer != null) {
            httpServer.close().get();
        }
        super.tearDown();
    }

    @Override
    public ITimelineFactory<?> getTimelineFactory() {
        return new HttpTimelineFactory("http://" + serverHost + ":" + serverPort, null);
    }
}
