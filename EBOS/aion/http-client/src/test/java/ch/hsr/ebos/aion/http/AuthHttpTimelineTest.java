package ch.hsr.ebos.aion.http;

import org.junit.After;
import org.junit.Before;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineTest;
import ch.hsr.ebos.aion.server.AionHttpServer;
import ch.hsr.ebos.aion.server.config.AuthOAuthConfiguration;
import ch.hsr.ebos.aion.server.config.Configuration;
import ch.hsr.ebos.aion.server.config.MemoryTimelineConfiguration;

public class AuthHttpTimelineTest extends TimelineTest {
    private static final String timelineName = "testtimeline";
    private static final int serverPort = 8888;
    private static final String serverHost = "127.0.0.1";
    private static final String validToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ0Uko5M0xfTXQtenF6YWhIeUp1RUZMZERhNzRLalNpTzNyRmRPNkpjd1pJIn0.eyJqdGkiOiJjOTQzOTFmNy1jOWI0LTQyZTQtYmI0Zi1hY2JmYTM4NDU5MTYiLCJleHAiOjE1ODg5NDc3NDMsIm5iZiI6MCwiaWF0IjoxNTU3NDExNzQzLCJpc3MiOiJodHRwczovL2F1dGguZWJvcy5iZXJ0c2NoaS5pby9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiYWMxNGM4YmYtMjM5NC00Mjg2LThmMWItMTY3ZTBlOTFlZDEwIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoib2ZmbGlzcy1hcHAiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI4ZTk5ZTRkZC00MzUzLTQ5YTAtYWU3OS1lZmNiODAyY2JiMjIiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidGVzdHRpbWVsaW5lX3JlYWQiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInRlc3R0aW1lbGluZV93cml0ZSJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoidGVzdC11c2VyIn0.Ne1OgwvHAi4ELXK4s34FE_RkbGGEQmFJ2pDko8G2JRT0vON8E67srOVvwxow6Q89UGUNNy05aU3W7xp2V23TvjapDMmT2aVEp6SuPbDenT_c4iNvf1pBHtAPnWNCDZr1LxSpzMoK8kpsjavpBZ_0N4_03CJhNl1usEhD6LMJIyYCKMI2Xx8tMrvhUInE_Ogky4Zc0Z9THq9ZfsoQwVFreL9qSRJOzEjjGm26vYgGhFziEcqUSBvfSROYeIovBb_0I0RKXxsxBcO4U8HmHNRPpnQXnhPaabUdi8qdxkPZ1aL83WTk5T0q18Xlkb19Tht8oS5Gp9__z3-ptCqHfw19WQ";

    AionHttpServer httpServer;

    @Before
    public void setUp() throws Exception {
        Configuration serverConfig = new Configuration();
        serverConfig.addTimeline(timelineName, new MemoryTimelineConfiguration());
        AuthOAuthConfiguration oAuthConfiguration = new AuthOAuthConfiguration();
        oAuthConfiguration.setServerUrl("https://auth.ebos.bertschi.io/auth/realms/master");
        oAuthConfiguration.setClientId("f02943d2-6e25-440b-b1bb-a49831c9e959");
        serverConfig.setAuth(oAuthConfiguration);

        httpServer = new AionHttpServer(serverConfig);
        httpServer.listen(serverPort).get();
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        if (httpServer != null) {
            httpServer.close().get();
        }
        super.tearDown();
    }

    @Override
    public String getTimelineName() {
        return timelineName;
    }

    @Override
    public ITimelineFactory<?> getTimelineFactory() {
        return new HttpTimelineFactory("http://" + serverHost + ":" + serverPort,
                () -> CompletableFuture.completedFuture("Bearer " + validToken));
    }
}
