package ch.hsr.ebos.aion.http;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        HttpTimelineTest.class,
        AuthHttpTimelineTest.class,
})
public class AionHttpTestSuite {
}
