package ch.hsr.ebos.aion.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Closeable;
import java.util.List;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.data.IDataChangeListener;
import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.aion.data.orm.query.Query;

public abstract class EntityRecyclerListAdapter<E extends Entity, V extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<V> implements IDataChangeListener, Closeable {

    protected AionClientModule clientModule;
    protected Activity activity;
    protected List<E> items;

    private Query query;
    private Class<E> entityClass;
    private OnEntitySelectListener selectListener;
    private int resource;

    public EntityRecyclerListAdapter(Activity activity, int resource, AionClientModule clientModule,
                                     Class<E> entityClass, Query query) {
        this.activity = activity;
        this.resource = resource;
        this.clientModule = clientModule;
        this.entityClass = entityClass;
        this.query = query;

        try {
            items = clientModule.getEntityService().query(entityClass, query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        clientModule.addDataChangeListener(this);
    }

    protected abstract V createViewHolder(View view);

    protected abstract void bindView(E entity, V viewHolder);

    public void setSelectListener(OnEntitySelectListener<E> selectListener) {
        this.selectListener = selectListener;
    }

    @Override
    public V onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resource, parent, false);
        return createViewHolder(view);
    }

    @Override
    public void onBindViewHolder(V viewHolder, int position) {
        E item = items.get(position);
        viewHolder.itemView.setOnClickListener(v -> {
            if (selectListener != null) {
                selectListener.onSelect(item);
            }
        });
        bindView(item, viewHolder);
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public E getItem(int position) {
        return items.get(position);
    }

    @Override
    public void onDataChange() {
        try {
            items = clientModule.getEntityService().query(entityClass, query);
            activity.runOnUiThread(() -> {
                notifyDataSetChanged();
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void close() {
        clientModule.removeDataChangeListener(this);
    }
}
