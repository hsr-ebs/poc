package ch.hsr.ebos.aion.android.data;

public interface IDataChangeListener {
    void onDataChange();
}