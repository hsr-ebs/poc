package ch.hsr.ebos.aion.android;

import android.content.Context;

import net.openid.appauth.AuthState;
import net.openid.appauth.AuthorizationService;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncStateFactory;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.data.sqlite.connection.AndroidSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineFactory;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineSyncStateFactory;
import ch.hsr.ebos.aion.http.HttpRequestInterceptor;
import ch.hsr.ebos.aion.http.HttpTimelineFactory;

public class AionClientSqliteOptions implements AionClientOptions, HttpRequestInterceptor {
    private final Context context;
    private final ApplicationState applicationState;
    private final SqliteConnectionFactory connectionFactory;
    private final SqliteTimelineFactory localTimelineFactory;
    private final SqliteTimelineSyncStateFactory syncStateFactory;
    private final HttpTimelineFactory remoteTimlineFactory;

    public AionClientSqliteOptions(Context context, String dataDir, String apiUrl) {
        this.context = context;
        applicationState = new ApplicationState(context);
        connectionFactory = new SqliteConnectionFactory(new AndroidSqliteConnectionFactoryOptions());
        remoteTimlineFactory = new HttpTimelineFactory(apiUrl, this);
        localTimelineFactory = new SqliteTimelineFactory(dataDir, connectionFactory);
        syncStateFactory = new SqliteTimelineSyncStateFactory(dataDir, connectionFactory);
    }

    @Override
    public ITimelineFactory<IDynamicTimeline> getLocalTimelineFactory() {
        return localTimelineFactory;
    }

    @Override
    public ITimelineFactory<ITimeline> getRemoteTimelineFactory() {
        return remoteTimlineFactory;
    }

    @Override
    public ITimelineSyncStateFactory getSyncStateFactory() {
        return syncStateFactory;
    }

    public SqliteConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    @Override
    public CompletableFuture<String> provideAuthorizationHeader() {
        CompletableFuture<String> future = new CompletableFuture<>();

        AuthorizationService authorizationService = new AuthorizationService(context);
        AuthState authState = applicationState.getAuthState();

        if (authState == null) {
            future.complete(null);
            return future;
        }

        authState.performActionWithFreshTokens(authorizationService, (accessToken, idToken, ex) -> {
            if (ex != null) {
                future.completeExceptionally(ex);
            } else {
                future.complete("Bearer " + accessToken);
            }
        });

        return future;
    }

    public ApplicationState getApplicationState() {
        return applicationState;
    }
}
