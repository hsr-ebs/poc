package ch.hsr.ebos.aion.android;

import android.app.ActivityManager;
import android.content.Context;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ch.hsr.ebos.aion.core.module.Module;

public class AionClient {
    private static int syncInterval = 10000;
    private Thread backgroundSynchronizeThread;
    private boolean backgroundSynchronize = false;
    private AionClientOptions options;
    private Map<String, AionClientModule> modules = new HashMap<>();
    private Context context;

    public AionClient(Context context, AionClientOptions options) {
        this.context = context;
        this.options = options;
    }

    public void stopSynchronization() throws InterruptedException {
        this.backgroundSynchronize = false;
        if (this.backgroundSynchronizeThread != null) {
            this.backgroundSynchronizeThread.join();
        }
    }

    public void startSynchronization() {
        this.backgroundSynchronize = true;
        if (backgroundSynchronizeThread != null && backgroundSynchronizeThread.isAlive()) {
            return;
        }

        backgroundSynchronizeThread = new Thread(() -> {
            while (backgroundSynchronize) {
                if (isInForeground(context)) {
                    synchronize();
                }
                try {
                    Thread.sleep(syncInterval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        backgroundSynchronizeThread.start();
    }

    private boolean isInForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public void removeModules() throws Exception {
        Set<String> timelineNames = new HashSet(modules.keySet());
        for (String timeline : timelineNames) {
            removeModule(timeline);
        }
    }

    public void removeModule(String timelineName) throws Exception {
        AionClientModule clientModule = modules.get(timelineName);
        if (clientModule != null) {
            clientModule.close();
            modules.remove(timelineName);
        }
    }

    public AionClientModule addModule(String timelineName, String localName, Module module) throws Exception {
        AionClientModule aionClientModule = new AionClientModule(options, timelineName, localName, module);
        modules.put(timelineName, aionClientModule);
        return aionClientModule;
    }

    public Collection<AionClientModule> getModules() {
        return this.modules.values();
    }

    public boolean synchronize() {
        List<SyncThread> syncThreads = new ArrayList<>();
        boolean success = true;

        for (AionClientModule module : modules.values()) {
            syncThreads.add(new SyncThread(module));
        }

        for (SyncThread syncThread : syncThreads) {
            syncThread.start();
        }

        for (SyncThread syncThread : syncThreads) {
            try {
                syncThread.join();
                if (!syncThread.isSuccess()) {
                    success = false;
                }
            } catch (InterruptedException e) {
                success = false;
            }
        }

        return success;
    }
}
