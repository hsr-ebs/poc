package ch.hsr.ebos.aion.android.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.List;

import ch.hsr.ebos.aion.android.AionClientModule;
import ch.hsr.ebos.aion.android.data.IDataChangeListener;
import ch.hsr.ebos.aion.data.orm.Entity;
import ch.hsr.ebos.aion.data.orm.query.Query;

public class EntityListAdapter<E extends Entity> extends ArrayAdapter<E>
        implements Filterable, IDataChangeListener, Closeable {

    private Query query;
    private AionClientModule clientModule;
    private Class<E> entityClass;
    private List<E> items;

    public EntityListAdapter(Context context, AionClientModule clientModule,
                             Class<E> entityClass, Query query, int layout) {
        super(context, layout);

        this.query = query;
        this.clientModule = clientModule;
        this.entityClass = entityClass;

        try {
            items = clientModule.getEntityService().query(entityClass, query);
            addAll(items);
        } catch (Exception e) {
            e.printStackTrace();
        }

        clientModule.addDataChangeListener(this);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<E> temporaryList = new ArrayList<>();

                if (constraint == null || constraint.length() <= 0) {
                    results.count = items.size();
                    results.values = items;
                } else {
                    for (E entity : items) {
                        if (entity.toString().toLowerCase().contains(constraint.toString().toLowerCase())) {
                            temporaryList.add(entity);
                        }
                    }
                    results.count = temporaryList.size();
                    results.values = temporaryList;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                clear();
                addAll((ArrayList<E>) filterResults.values);
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public void onDataChange() {
        try {
            items = clientModule.getEntityService().query(entityClass, query);
            clear();
            addAll(items);
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        clientModule.removeDataChangeListener(this);
    }
}
