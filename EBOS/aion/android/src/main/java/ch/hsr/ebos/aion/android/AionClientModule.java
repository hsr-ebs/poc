package ch.hsr.ebos.aion.android;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import ch.hsr.ebos.aion.android.data.DataChangeObservable;
import ch.hsr.ebos.aion.android.data.DataChangeObserver;
import ch.hsr.ebos.aion.android.data.IDataChangeListener;
import ch.hsr.ebos.aion.core.application.Application;
import ch.hsr.ebos.aion.core.application.ApplicationFactory;
import ch.hsr.ebos.aion.core.event.EventTypeUtil;
import ch.hsr.ebos.aion.core.module.Module;
import ch.hsr.ebos.aion.core.processor.TimelineProcessor;
import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncState;
import ch.hsr.ebos.aion.core.synchronizer.TimelineSynchronizer;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.TimelineEvent;
import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;
import ch.hsr.ebos.aion.data.orm.EntityService;

public class AionClientModule {
    private AionClientOptions options;

    private String timelineName;
    private String localName;
    private Application application;
    private IDynamicTimeline localTimeline;
    private ITimeline remoteTimeline;
    private IDynamicTimeline resultTimeline;
    private DataChangeObservable dataChangeObservable;
    private Map<IDataChangeListener, DataChangeObserver> dataChangeObservers;
    private ITimelineSyncState syncState;
    private TimelineSynchronizer synchronizer = new TimelineSynchronizer();
    private TimelineProcessor timelineProcessor;

    public AionClientModule(AionClientOptions options, String timelineName, String localName, Module module) throws Exception {
        this.options = options;

        localTimeline = options.getLocalTimelineFactory().get(localName);
        resultTimeline = options.getLocalTimelineFactory().get(localName + "-result");
        remoteTimeline = options.getRemoteTimelineFactory().get(timelineName);
        syncState = options.getSyncStateFactory().get(localName + "-sync-state");

        this.dataChangeObservable = new DataChangeObservable();
        this.dataChangeObservers = new HashMap<>();

        this.localName = localName;
        this.timelineName = timelineName;
        this.application = ApplicationFactory.create(module).get();
        this.timelineProcessor = new TimelineProcessor(application, localTimeline, resultTimeline);
    }

    public void clear() throws Exception {
        application.close().get();
        options.getLocalTimelineFactory().delete(localName);
        options.getLocalTimelineFactory().delete(localName + "-result");
        options.getLocalTimelineFactory().delete(localName + "-sync-state");
    }

    public void close() throws Exception {
        application.close().get();
        options.getLocalTimelineFactory().close(localName);
        options.getLocalTimelineFactory().close(localName + "-result");
        options.getLocalTimelineFactory().close(localName + "-sync-state");
    }

    public void emit(String eventId, Object eventData, TimelineEventAuth auth) throws Exception {
        String type = EventTypeUtil.getEventType(eventData.getClass());
        if (eventId == null) {
            localTimeline.add(new TimelineEvent(type, eventData, auth)).get();
        } else {
            localTimeline.add(new TimelineEvent(eventId, type, eventData, auth)).get();
        }
        timelineProcessor.run();
    }

    public void emit(Object eventData, TimelineEventAuth auth) throws Exception {
        emit(null, eventData, auth);
    }

    public EntityService getEntityService() {
        return getApplication().injector.getInstance(EntityService.class);
    }

    public ITimelineSyncState getSyncState() {
        return syncState;
    }

    public IDynamicTimeline getResultTimeline() {
        return resultTimeline;
    }

    public IDynamicTimeline getLocalTimeline() {
        return localTimeline;
    }

    public String getTimelineName() {
        return timelineName;
    }

    public void sync() throws Throwable {
        synchronized (this) {
            String pullIdBefore = syncState.getLastPulled();
            synchronizer.sync(localTimeline, remoteTimeline, syncState);
            String pullIdAfter = syncState.getLastPulled();

            if (Objects.equals(pullIdAfter, pullIdBefore)) {
                return;
            }

            if (pullIdBefore != null) {
                timelineProcessor.reset(pullIdBefore);
            }
            timelineProcessor.run();

            dataChangeObservable.update();
        }
    }

    public Application getApplication() {
        return application;
    }

    public void addDataChangeListener(IDataChangeListener listener) {
        dataChangeObservable.addObserver(new DataChangeObserver(listener));
    }

    public void removeDataChangeListener(IDataChangeListener listener) {
        DataChangeObserver observer = dataChangeObservers.get(listener);
        if (observer != null) {
            dataChangeObservable.deleteObserver(observer);
        }
    }
}
