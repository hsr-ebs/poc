package ch.hsr.ebos.aion.android;

public class SyncThread extends Thread {
    private boolean success = true;
    private AionClientModule module;

    public SyncThread(AionClientModule module) {
        this.module = module;
    }

    @Override
    public void run() {
        try {
            module.sync();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            success = false;
        }
    }

    public boolean isSuccess() {
        return success;
    }
}