package ch.hsr.ebos.aion.android;

import ch.hsr.ebos.aion.core.synchronizer.ITimelineSyncStateFactory;
import ch.hsr.ebos.aion.core.timeline.IDynamicTimeline;
import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;

public interface AionClientOptions {
    ITimelineFactory<IDynamicTimeline> getLocalTimelineFactory();

    ITimelineFactory<ITimeline> getRemoteTimelineFactory();

    ITimelineSyncStateFactory getSyncStateFactory();
}
