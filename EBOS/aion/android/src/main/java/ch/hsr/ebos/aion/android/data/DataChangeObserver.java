package ch.hsr.ebos.aion.android.data;

import java.util.Observable;
import java.util.Observer;

public class DataChangeObserver implements Observer {
    private IDataChangeListener listener;

    public DataChangeObserver(IDataChangeListener listener) {
        this.listener = listener;
    }

    @Override
    public void update(Observable o, Object arg) {
        listener.onDataChange();
    }
}
