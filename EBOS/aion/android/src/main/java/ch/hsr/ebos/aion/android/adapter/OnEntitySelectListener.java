package ch.hsr.ebos.aion.android.adapter;

import ch.hsr.ebos.aion.data.orm.Entity;

public interface OnEntitySelectListener<E extends Entity> {
    void onSelect(E entity);
}
