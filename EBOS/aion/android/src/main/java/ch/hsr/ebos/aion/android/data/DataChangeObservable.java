package ch.hsr.ebos.aion.android.data;

import java.util.Observable;

public class DataChangeObservable extends Observable {
    public void update() {
        setChanged();
        notifyObservers();
    }
}
