package ch.hsr.ebos.aion.android;

import android.content.Context;
import android.content.SharedPreferences;

import net.openid.appauth.AuthState;

import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.json.JSONException;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import ch.hsr.ebos.aion.core.timeline.TimelineEventAuth;

public class ApplicationState {
    private static final String syncDataKey = "syncDate";
    private static final String authStateKey = "authState";

    private SharedPreferences sharedPreferences;
    private TimelineEventAuth timelineEventAuth;

    public ApplicationState(Context context) {
        this.sharedPreferences = context.getSharedPreferences("application", Context.MODE_PRIVATE);
    }

    public Date getLastSyncDate() {
        long value = sharedPreferences.getLong(syncDataKey, 0);
        if (value == 0) {
            return null;
        }
        return new Date(value);
    }

    public void setLastSyncDate() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(syncDataKey, new Date().getTime());
        editor.apply();
    }

    public AuthState getAuthState() {
        try {
            String serializedState = sharedPreferences.getString(authStateKey, "");
            if (serializedState.length() == 0) {
                return null;
            }
            return AuthState.jsonDeserialize(serializedState);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setAuthState(AuthState authState) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (authState != null) {
            editor.putString(authStateKey, authState.jsonSerializeString());
        } else {
            editor.remove(authStateKey);
        }
        editor.apply();
        timelineEventAuth = null;
    }

    public boolean isAuthorized() {
        return this.getAuthState() != null;
    }

    public TimelineEventAuth getTimelineEventAuth() {
        try {
            AuthState authState = getAuthState();
            if (timelineEventAuth == null && authState != null && authState.getAccessToken() != null) {
                JwtClaims claims = new JwtConsumerBuilder().setSkipAllValidators().
                        setSkipSignatureVerification().build().
                        processToClaims(authState.getAccessToken());

                String username = (String) claims.getClaimValue("preferred_username");
                String userId = (String) claims.getClaimValue("userId");
                List<String> roles = claims.flattenClaims().get("realm_access.roles").stream()
                        .map(object -> Objects.toString(object, null)).collect(Collectors.toList());
                timelineEventAuth = new TimelineEventAuth(userId, username, roles);
            }
        } catch (InvalidJwtException e) {
            e.printStackTrace();
        }
        return timelineEventAuth;
    }
}
