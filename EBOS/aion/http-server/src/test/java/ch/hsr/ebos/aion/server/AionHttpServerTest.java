package ch.hsr.ebos.aion.server;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import ch.hsr.ebos.aion.server.config.AuthNoneConfiguration;
import ch.hsr.ebos.aion.server.config.Configuration;
import io.vertx.core.Vertx;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClient;

@RunWith(VertxUnitRunner.class)
public class AionHttpServerTest {
    private static final int ServerPort = 8888;

    AionHttpServer server;

    @Before
    public void before(TestContext context) throws Exception {
        Configuration configuration = new Configuration();
        configuration.setAuth(new AuthNoneConfiguration());

        server = new AionHttpServer(configuration);
        server.listen(ServerPort).get();
    }

    @After
    public void after(TestContext context) throws Exception {
        server.close().get();
    }

    @Test
    public void testSpecRoute(TestContext context) {
        Async async = context.async();
        WebClient client = WebClient.create(Vertx.vertx());
        client.get(ServerPort, "localhost", "/api/spec").send(ar -> {
            if (ar.succeeded()) {
                context.assertEquals(ar.result().statusCode(), 200);
                client.close();
                async.complete();
            } else {
                context.fail();
            }
        });
    }

    @Test
    public void testHealthRoute(TestContext context) {
        Async async = context.async();
        WebClient client = WebClient.create(Vertx.vertx());
        client.get(ServerPort, "localhost", "/api/health").send(ar -> {
            if (ar.succeeded()) {
                context.assertEquals(ar.result().statusCode(), 200);
                client.close();
                async.complete();
            } else {
                context.fail();
            }
        });
    }
}
