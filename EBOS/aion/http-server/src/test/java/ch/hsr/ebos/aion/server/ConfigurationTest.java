package ch.hsr.ebos.aion.server;

import org.junit.Before;
import org.junit.Test;

import ch.hsr.ebos.aion.server.config.Configuration;
import ch.hsr.ebos.aion.server.config.MemoryTimelineConfiguration;
import ch.hsr.ebos.aion.server.config.SqliteTimelineConfiguration;
import ch.hsr.ebos.aion.server.config.TimelineConfiguration;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;

public class ConfigurationTest {
    Configuration config;

    @Before
    public void before() throws Exception {
        config = new Configuration();
    }

    private void addTimeline(String name, TimelineConfiguration configuration) {
        config.getTimelines().put(name, configuration);
    }

    @Test
    public void testSimpleRegex() {
        addTimeline("test", new SqliteTimelineConfiguration("^foo$"));

        TimelineConfiguration timeline = config.getTimeline("foo");
        boolean hasTimeline = config.hasTimeline("foo");
        assertNotNull(timeline);
        assertTrue(hasTimeline);
    }

    @Test
    public void testSimpleRegexFailing() {
        addTimeline("test", new SqliteTimelineConfiguration("^foo$"));

        TimelineConfiguration timeline = config.getTimeline("bar");
        boolean hasTimeline = config.hasTimeline("bar");
        assertNull(timeline);
        assertFalse(hasTimeline);
    }

    @Test
    public void testVariableRegex() {
        addTimeline("test", new SqliteTimelineConfiguration("^foo[-][a-z]{3}$"));

        TimelineConfiguration timeline = config.getTimeline("foo-abc");
        boolean hasTimeline = config.hasTimeline("foo-abc");
        assertNotNull(timeline);
        assertTrue(hasTimeline);
    }

    @Test
    public void testParseEmptyConfig() throws Exception {
        Configuration config = Configuration.parseResource("empty-configuration.yaml");
        assertEquals(0, config.getTimelines().size());
    }

    @Test
    public void testParseRegexConfig() throws Exception {
        Configuration config = Configuration.parseResource("regex-configuration.yaml");
        assertEquals(1, config.getTimelines().size());
        TimelineConfiguration timelineConfiguration = config.getTimeline("issue-12");
        assertNotNull(timelineConfiguration);
        assertEquals(SqliteTimelineConfiguration.class, timelineConfiguration.getClass());
    }

    @Test
    public void testParseMultipleConfig() throws Exception {
        Configuration config = Configuration.parseResource("multiple-configuration.yaml");
        assertEquals(2, config.getTimelines().size());
        TimelineConfiguration testTimelineConfiguration = config.getTimeline("test");
        TimelineConfiguration userTimelineConfiguration = config.getTimeline("user");
        assertNotNull(testTimelineConfiguration);
        assertNotNull(userTimelineConfiguration);
        assertEquals(SqliteTimelineConfiguration.class, userTimelineConfiguration.getClass());
        assertEquals(MemoryTimelineConfiguration.class, testTimelineConfiguration.getClass());
    }

    @Test
    public void testHasTimeline() {
        Configuration config = new Configuration();
        config.addTimeline("test", new MemoryTimelineConfiguration());
        assertTrue(config.hasTimeline("test"));
    }

    @Test
    public void testHasTimelineRegex() {
        Configuration config = new Configuration();
        config.addTimeline("test", new MemoryTimelineConfiguration("test-[a-c]"));
        assertTrue(config.hasTimeline("test-c"));
    }
}
