package ch.hsr.ebos.aion.server;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import ch.hsr.ebos.aion.server.config.AuthOAuthConfiguration;
import ch.hsr.ebos.aion.server.config.Configuration;
import ch.hsr.ebos.aion.server.config.MemoryTimelineConfiguration;
import io.vertx.core.Vertx;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClient;

@RunWith(VertxUnitRunner.class)
public class AionAuthServerTest {
    private static final int ServerPort = 8888;
    private static final String expiredToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ1VWVMQk0wV3VlbUNxYkE5bktfUVpRWl9SM3ZNV3l2clFRbHdsanB2V3RjIn0.eyJqdGkiOiIzYzJiOWI1ZC1lNzE2LTQ3NjYtYjdmZC1kNGM5NDUxOTk3ODYiLCJleHAiOjE1NTcwNTExNzUsIm5iZiI6MCwiaWF0IjoxNTU3MDUwMjc1LCJpc3MiOiJodHRwczovL2F1dGguZWJvcy5iZXJ0c2NoaS5pby9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOlsibWFzdGVyLXJlYWxtIiwiYWNjb3VudCJdLCJzdWIiOiJlNmNmNWIwZC03NzgyLTQ5NjItYjQ0OS01ZGM3ZDBjMzNmYTciLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJvZmZsaXNzLWFwcCIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjIzNWNmMWRjLThmNmQtNGI5Zi04ZmRjLTBmMWY0Yzk5OWRhOCIsImFjciI6IjEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiY3JlYXRlLXJlYWxtIiwib2ZmbGluZV9hY2Nlc3MiLCJhZG1pbiIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsibWFzdGVyLXJlYWxtIjp7InJvbGVzIjpbInZpZXctcmVhbG0iLCJ2aWV3LWlkZW50aXR5LXByb3ZpZGVycyIsIm1hbmFnZS1pZGVudGl0eS1wcm92aWRlcnMiLCJpbXBlcnNvbmF0aW9uIiwiY3JlYXRlLWNsaWVudCIsIm1hbmFnZS11c2VycyIsInF1ZXJ5LXJlYWxtcyIsInZpZXctYXV0aG9yaXphdGlvbiIsInF1ZXJ5LWNsaWVudHMiLCJxdWVyeS11c2VycyIsIm1hbmFnZS1ldmVudHMiLCJtYW5hZ2UtcmVhbG0iLCJ2aWV3LWV2ZW50cyIsInZpZXctdXNlcnMiLCJ2aWV3LWNsaWVudHMiLCJtYW5hZ2UtYXV0aG9yaXphdGlvbiIsIm1hbmFnZS1jbGllbnRzIiwicXVlcnktZ3JvdXBzIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInByZWZlcnJlZF91c2VybmFtZSI6ImFkbWluIn0.Ro9Pa4SxNAahvyp7casnVM8F6SePAuUqDMP4TAPSjC5zPyau-4MJYVJ8ty1iemAR5wZF0BQJwFAnVIALVHm06fw7PVa2h7nUYpdboRHsgUKH9jfC3eqEy_uPpLjG8HobWfnx7XrwNmaSBU-LbfqMIYLbaR2IEZH6NByXmYUDZBJ0tOhEA40-PSwstPNGzTvn_JnB7uadKP7Ij1OGcC7bj9rx6M6U7lbRvNK3IaDo0rRzQiK_xekFqL29vCX2Ydr7wni1MOLCS7bPws-yXT2os6yP3Qry8YhGvXHODp1Gjlb41LVT-UiuTSn08yX65ZH7ezC6PedHJ5AqOXuwkTou3w";
    private static final String validToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ0Uko5M0xfTXQtenF6YWhIeUp1RUZMZERhNzRLalNpTzNyRmRPNkpjd1pJIn0.eyJqdGkiOiJjOTQzOTFmNy1jOWI0LTQyZTQtYmI0Zi1hY2JmYTM4NDU5MTYiLCJleHAiOjE1ODg5NDc3NDMsIm5iZiI6MCwiaWF0IjoxNTU3NDExNzQzLCJpc3MiOiJodHRwczovL2F1dGguZWJvcy5iZXJ0c2NoaS5pby9hdXRoL3JlYWxtcy9tYXN0ZXIiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiYWMxNGM4YmYtMjM5NC00Mjg2LThmMWItMTY3ZTBlOTFlZDEwIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoib2ZmbGlzcy1hcHAiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiI4ZTk5ZTRkZC00MzUzLTQ5YTAtYWU3OS1lZmNiODAyY2JiMjIiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidGVzdHRpbWVsaW5lX3JlYWQiLCJ1bWFfYXV0aG9yaXphdGlvbiIsInRlc3R0aW1lbGluZV93cml0ZSJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoidGVzdC11c2VyIn0.Ne1OgwvHAi4ELXK4s34FE_RkbGGEQmFJ2pDko8G2JRT0vON8E67srOVvwxow6Q89UGUNNy05aU3W7xp2V23TvjapDMmT2aVEp6SuPbDenT_c4iNvf1pBHtAPnWNCDZr1LxSpzMoK8kpsjavpBZ_0N4_03CJhNl1usEhD6LMJIyYCKMI2Xx8tMrvhUInE_Ogky4Zc0Z9THq9ZfsoQwVFreL9qSRJOzEjjGm26vYgGhFziEcqUSBvfSROYeIovBb_0I0RKXxsxBcO4U8HmHNRPpnQXnhPaabUdi8qdxkPZ1aL83WTk5T0q18Xlkb19Tht8oS5Gp9__z3-ptCqHfw19WQ";

    AionHttpServer server;

    @Before
    public void before(TestContext context) throws Exception {
        Configuration configuration = new Configuration();
        AuthOAuthConfiguration auth = new AuthOAuthConfiguration();
        auth.setServerUrl("https://auth.ebos.bertschi.io/auth/realms/master");
        auth.setClientId("f02943d2-6e25-440b-b1bb-a49831c9e959");
        configuration.setAuth(auth);
        configuration.addTimeline("testtimeline", new MemoryTimelineConfiguration());

        server = new AionHttpServer(configuration);
        server.listen(ServerPort).get();
    }

    @After
    public void after(TestContext context) throws Exception {
        server.close().get();
    }

    @Test
    public void testNotAuthenticated(TestContext context) {
        Async async = context.async();
        WebClient client = WebClient.create(Vertx.vertx());
        client.get(ServerPort, "localhost", "/api/timeline/testtimeline/info").send(ar -> {
            if (ar.succeeded()) {
                context.assertEquals(ar.result().statusCode(), 401);
                client.close();
                async.complete();
            } else {
                context.fail();
            }
        });
    }

    @Test
    public void testExpiredToken(TestContext context) {
        Async async = context.async();
        WebClient client = WebClient.create(Vertx.vertx());
        client.get(ServerPort, "localhost", "/api/timeline/testtimeline/info")
                .bearerTokenAuthentication(expiredToken)
                .send(ar -> {
                    if (ar.succeeded()) {
                        context.assertEquals(ar.result().statusCode(), 401);
                        client.close();
                        async.complete();
                    } else {
                        context.fail();
                    }
                });
    }

    @Test
    public void testValidToken(TestContext context) {
        Async async = context.async();
        WebClient client = WebClient.create(Vertx.vertx());
        client.get(ServerPort, "localhost", "/api/timeline/testtimeline/info")
                .bearerTokenAuthentication(validToken)
                .send(ar -> {
                    if (ar.succeeded()) {
                        context.assertEquals(ar.result().statusCode(), 200);
                        client.close();
                        async.complete();
                    } else {
                        context.fail();
                    }
                });
    }

    @Test
    public void testValidTokenOnUnauthorizedTimeline(TestContext context) {
        Async async = context.async();
        WebClient client = WebClient.create(Vertx.vertx());
        client.get(ServerPort, "localhost", "/api/timeline/issue/info")
                .bearerTokenAuthentication(validToken)
                .send(ar -> {
                    if (ar.succeeded()) {
                        context.assertEquals(ar.result().statusCode(), 403);
                        client.close();
                        async.complete();
                    } else {
                        context.fail();
                    }
                });
    }
}
