package ch.hsr.ebos.aion.server;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ConfigurationTest.class,
        AionHttpServerTest.class,
        AionAuthServerTest.class,
})
public class AionServerTestSuite {
}
