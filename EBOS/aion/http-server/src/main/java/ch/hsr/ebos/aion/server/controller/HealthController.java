package ch.hsr.ebos.aion.server.controller;

import java.util.concurrent.CompletableFuture;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;

public class HealthController extends Controller {
    @Override
    CompletableFuture<Void> prepareRouter(Vertx vertx, Router router) {
        router.get("/api/health").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response.setStatusCode(200);
            response.end();
        });
        return CompletableFuture.completedFuture(null);
    }
}
