package ch.hsr.ebos.aion.server.controller;

import com.google.gson.Gson;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.core.util.JsonUtil;
import ch.hsr.ebos.aion.server.responses.ErrorResponse;
import ch.hsr.ebos.aion.server.responses.NotFoundResponse;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.LoggerFormat;
import io.vertx.ext.web.handler.LoggerHandler;

public abstract class Controller {
    protected Gson gson = JsonUtil.getGson();

    abstract CompletableFuture<Void> prepareRouter(Vertx vertx, Router router);

    public Router getRouter(Vertx vertx) throws Exception {
        Router router = Router.router(vertx);

        router.route().handler(LoggerHandler.create(LoggerFormat.DEFAULT));

        router.route().failureHandler(failedRoutingContext -> {
            if (failedRoutingContext.statusCode() == 404) {
                returnJson(failedRoutingContext, 404, new NotFoundResponse("api"));
            } else if (failedRoutingContext.statusCode() == 500) {
                returnJson(failedRoutingContext, 500, new ErrorResponse(new Exception("internal server error")));
            } else {
                failedRoutingContext.next();
            }
        });

        prepareRouter(vertx, router).get();

        return router;
    }

    protected void returnJson(RoutingContext context, int status, Object data) {
        HttpServerResponse response = context.response();
        response.setStatusCode(status);
        if (data != null) {
            response.putHeader("content-type", "application/json; charset=utf-8")
                    .end(gson.toJson(data));
        } else {
            response.end();
        }
    }

}
