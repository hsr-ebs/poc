package ch.hsr.ebos.aion.server.controller;

import java.util.concurrent.CompletableFuture;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;

public class SpecController extends Controller {
    private static final String OpenApiResourceFile = "openapi.yaml";

    @Override
    CompletableFuture<Void> prepareRouter(Vertx vertx, Router router) {
        router.get("/api/spec").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();

            routingContext.response()
                    .putHeader("content-type", "text/yaml; charset=utf-8");
            response.sendFile(OpenApiResourceFile);
            response.setStatusCode(200);
            response.end();
        });
        return CompletableFuture.completedFuture(null);
    }
}
