package ch.hsr.ebos.aion.server.responses;

public class ErrorResponse extends Response {
    public ErrorResponse(Throwable throwable) {
        super(throwable.getClass().getSimpleName(), throwable.getMessage());
    }
}
