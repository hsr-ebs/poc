package ch.hsr.ebos.aion.server.config;

import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.data.sqlite.connection.JavaSqliteConnectionFactoryOptions;
import ch.hsr.ebos.aion.data.sqlite.connection.SqliteConnectionFactory;
import ch.hsr.ebos.aion.data.sqlite.timeline.SqliteTimelineFactory;

public class SqliteTimelineConfiguration extends TimelineConfiguration<SqliteTimelineConfigurationOptions> {
    private SqliteTimelineFactory factory;

    public SqliteTimelineConfiguration() {

    }

    public SqliteTimelineConfiguration(String regex) {
        super(regex);
    }

    public SqliteTimelineConfiguration(String regex, SqliteTimelineConfigurationOptions options) {
        super(regex);
        this.options = options;
    }

    public ITimelineFactory getFactory() {
        if (factory == null) {
            SqliteConnectionFactory connectionFactory = new SqliteConnectionFactory(new JavaSqliteConnectionFactoryOptions());
            factory = new SqliteTimelineFactory(getOptions().getDirectory(), connectionFactory);
        }
        return factory;
    }

    @Override
    public String toString() {
        return "Sqlite: [dir: " + this.options.getDirectory() + "]";
    }
}
