package ch.hsr.ebos.aion.server.config;

public class AuthOAuthConfiguration extends AuthConfiguration {
    private String serverUrl;
    private String clientId;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public boolean isConfigured() {
        return this.serverUrl != null && this.clientId != null;
    }
}
