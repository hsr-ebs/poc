package ch.hsr.ebos.aion.server.controller;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.BiFunction;

import ch.hsr.ebos.aion.core.timeline.ITimeline;
import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.core.timeline.TimelineEvent;
import ch.hsr.ebos.aion.server.BadRequestException;
import ch.hsr.ebos.aion.server.Permissions;
import ch.hsr.ebos.aion.server.config.AuthNoneConfiguration;
import ch.hsr.ebos.aion.server.config.AuthOAuthConfiguration;
import ch.hsr.ebos.aion.server.config.Configuration;
import ch.hsr.ebos.aion.server.config.TimelineConfiguration;
import ch.hsr.ebos.aion.server.responses.ErrorResponse;
import ch.hsr.ebos.aion.server.responses.NotFoundResponse;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import io.vertx.ext.auth.oauth2.OAuth2ClientOptions;
import io.vertx.ext.auth.oauth2.impl.OAuth2TokenImpl;
import io.vertx.ext.auth.oauth2.providers.OpenIDConnectAuth;
import io.vertx.ext.auth.oauth2.rbac.impl.KeycloakRBACImpl;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;

public class OpenApiController extends Controller {
    private static final String OpenApiResourceFile = "openapi.yaml";

    private Configuration configuration;

    public OpenApiController(Configuration configuration) {
        this.configuration = configuration;
    }

    private void prepareRoutes(OpenAPI3RouterFactory factory) {
        registerOperation(factory, "createEvent", Permissions.Write, (routingContext, timeline) -> {
            TimelineEvent event = gson.fromJson(routingContext.getBodyAsString(), TimelineEvent.class);

            OAuth2TokenImpl user = (OAuth2TokenImpl) routingContext.user();
            if (user != null && event.getAuth() != null) {
                JsonObject token = user.accessToken();
                JsonObject realmAccess = token.getJsonObject("realm_access");

                if (realmAccess == null) {
                    CompletableFuture future = new CompletableFuture();
                    future.completeExceptionally(new BadRequestException("realm_access is required in accessToken"));
                    return future;
                }

                Object roles = realmAccess.getMap().get("roles");
                if (roles == null) {
                    CompletableFuture future = new CompletableFuture();
                    future.completeExceptionally(new BadRequestException("realm_access.roles is required in accessToken"));
                    return future;
                }

                JsonArray grantedRoles = (JsonArray) roles;
                for (String role : event.getAuth().getRoles()) {
                    if (!grantedRoles.contains(role)) {
                        CompletableFuture future = new CompletableFuture();
                        future.completeExceptionally(new BadRequestException("role " + role + " is not in accessToken"));
                        return future;
                    }
                }
            }

            return timeline.add(event).thenAccept(result -> {
                returnJson(routingContext, 200, null);
            });
        });

        registerOperation(factory, "info", Permissions.Read, (routingContext, timeline) -> {
            return timeline.info().thenAccept(result -> {
                returnJson(routingContext, 200, result);
            });
        });

        registerOperation(factory, "firstEvent", Permissions.Read, (routingContext, timeline) -> {
            return timeline.first().thenAccept(result -> {
                if (result != null) {
                    returnJson(routingContext, 200, result);
                } else {
                    returnJson(routingContext, 404, new NotFoundResponse("event"));
                }
            });
        });

        registerOperation(factory, "lastEvent", Permissions.Read, (routingContext, timeline) -> {
            return timeline.last().thenAccept(result -> {
                if (result != null) {
                    returnJson(routingContext, 200, result);
                } else {
                    returnJson(routingContext, 404, new NotFoundResponse("event"));
                }
            });
        });

        registerOperation(factory, "getEvent", Permissions.Read, (routingContext, timeline) -> {
            String id = routingContext.pathParam("id");
            return timeline.get(id).thenAccept(result -> {
                if (result != null) {
                    returnJson(routingContext, 200, result);
                } else {
                    returnJson(routingContext, 404, new NotFoundResponse("event"));
                }
            });
        });

        registerOperation(factory, "prevEvent", Permissions.Read, (routingContext, timeline) -> {
            String id = routingContext.pathParam("id");
            return timeline.prev(id).thenAccept(result -> {
                if (result != null) {
                    returnJson(routingContext, 200, result);
                } else {
                    returnJson(routingContext, 404, new NotFoundResponse("event"));
                }
            });
        });

        registerOperation(factory, "nextEvent", Permissions.Read, (routingContext, timeline) -> {
            String id = routingContext.pathParam("id");
            return timeline.next(id).thenAccept(result -> {
                if (result != null) {
                    returnJson(routingContext, 200, result);
                } else {
                    returnJson(routingContext, 404, new NotFoundResponse("event"));
                }
            });
        });
    }

    private void addSecurityHandlers(Vertx vertx, OpenAPI3RouterFactory routerFactory, Handler<Exception> result) {
        if (this.configuration.getAuth() instanceof AuthNoneConfiguration) {
            routerFactory.addSecurityHandler("bearerAuth", routingContext -> {
                routingContext.next();
            });
            result.handle(null);
            return;
        }

        if (!(this.configuration.getAuth() instanceof AuthOAuthConfiguration) || !this.configuration.getAuth().isConfigured()) {
            result.handle(new Exception("auth is not configured"));
            return;
        }

        AuthOAuthConfiguration oAuthConfiguration = (AuthOAuthConfiguration) this.configuration.getAuth();

        OAuth2ClientOptions clientOptions = new OAuth2ClientOptions()
                .setSite(oAuthConfiguration.getServerUrl())
                .setClientID(oAuthConfiguration.getClientId());

        OpenIDConnectAuth.discover(vertx, clientOptions, oAuth2AuthAsyncResult -> {

            OAuth2Auth auth = oAuth2AuthAsyncResult.result();
            auth.rbacHandler(new KeycloakRBACImpl(clientOptions));

            routerFactory.addSecurityHandler("bearerAuth", routingContext -> {
                final String authorization = routingContext.request().headers().get(HttpHeaders.AUTHORIZATION);
                if (authorization == null || !authorization.startsWith("Bearer ")) {
                    returnJson(routingContext, 401, new ErrorResponse(new Exception("not authenticated")));
                    return;
                }

                String token = authorization.substring("Bearer ".length());

                JsonObject login = new JsonObject()
                        .put("access_token", token)
                        .put("token_type", "Bearer");

                auth.authenticate(login, userAsyncResult -> {
                    if (userAsyncResult.failed()) {
                        returnJson(routingContext, 401, new ErrorResponse(new Exception("token expired")));
                        return;
                    }

                    User user = userAsyncResult.result();

                    routingContext.setUser(user);
                    routingContext.next();
                });
            });

            result.handle(null);
        });
    }

    @Override
    CompletableFuture<Void> prepareRouter(Vertx vertx, Router router) {
        CompletableFuture future = new CompletableFuture();

        OpenAPI3RouterFactory.create(vertx, OpenApiResourceFile, ar -> {
            if (ar.succeeded()) {
                OpenAPI3RouterFactory routerFactory = ar.result();

                addSecurityHandlers(vertx, routerFactory, r -> {

                    prepareRoutes(routerFactory);

                    Router openApiRouter = routerFactory.getRouter();
                    router.mountSubRouter("/", openApiRouter);

                    future.complete(null);
                });
            } else {
                future.completeExceptionally(ar.cause());
            }
        });

        return future;
    }

    private CompletableFuture<Void> checkPermissions(RoutingContext routingContext, String permission) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        User user = routingContext.user();
        String timelineName = routingContext.pathParam("timeline");

        String claim = "realm:" + timelineName + "_" + permission;

        user.isAuthorized(claim, booleanAsyncResult -> {
            if (booleanAsyncResult.failed() || !booleanAsyncResult.result().booleanValue()) {
                Exception exception = new Exception("not authorized for \"" + claim + "\"");
                returnJson(routingContext, 403, new ErrorResponse(exception));
                future.completeExceptionally(exception);
            } else {
                future.complete(null);
            }
        });

        return future;
    }

    private void registerOperation(OpenAPI3RouterFactory factory, String operationId, String permission, BiFunction<RoutingContext, ITimeline, CompletableFuture<Void>> consumer) {

        factory.addHandlerByOperationId(operationId, routingContext -> {
            String timelineName = routingContext.pathParam("timeline");

            if (this.configuration.getAuth().isConfigured()) {
                try {
                    checkPermissions(routingContext, permission).get();
                } catch (Exception e) {
                    return;
                }
            }

            try {
                TimelineConfiguration config = configuration.getTimeline(timelineName);
                if (config == null) {
                    returnJson(routingContext, 404, new NotFoundResponse("timeline"));
                }

                ITimelineFactory<?> timelineFactory = config.getFactory();
                ITimeline timeline = timelineFactory.get(timelineName);

                consumer.apply(routingContext, timeline).whenComplete((result, throwable) -> {
                    if (throwable != null) {
                        if (throwable instanceof CompletionException) {
                            returnJson(routingContext, 500, new ErrorResponse(throwable.getCause()));
                        } else {
                            returnJson(routingContext, 500, new ErrorResponse(throwable));
                        }
                    }
                });
            } catch (BadRequestException e) {
                returnJson(routingContext, 400, new ErrorResponse(e));
            } catch (Exception e) {
                returnJson(routingContext, 500, new ErrorResponse(e));
            }
        });
    }
}
