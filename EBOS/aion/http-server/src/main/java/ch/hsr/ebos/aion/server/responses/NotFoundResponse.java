package ch.hsr.ebos.aion.server.responses;

public class NotFoundResponse extends Response {
    public NotFoundResponse(String resource) {
        super("not found", resource + " not found");
    }
}
