package ch.hsr.ebos.aion.server.config;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AuthOAuthConfiguration.class, name = "oauth"),
        @JsonSubTypes.Type(value = AuthNoneConfiguration.class, name = "none"),
})
public abstract class AuthConfiguration {
    public abstract boolean isConfigured();
}
