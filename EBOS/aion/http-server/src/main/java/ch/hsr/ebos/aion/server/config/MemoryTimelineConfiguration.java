package ch.hsr.ebos.aion.server.config;

import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;
import ch.hsr.ebos.aion.data.memory.timeline.MemoryTimelineFactory;

public class MemoryTimelineConfiguration extends TimelineConfiguration<MemoryTimelineConfigurationOptions> {
    private MemoryTimelineFactory factory = new MemoryTimelineFactory();

    public MemoryTimelineConfiguration() {
        super();
    }

    public MemoryTimelineConfiguration(String regex) {
        super(regex);
    }

    @Override
    public ITimelineFactory getFactory() {
        return factory;
    }

    @Override
    public String toString() {
        return "Memory []";
    }
}
