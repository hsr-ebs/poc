package ch.hsr.ebos.aion.server.responses;

public class Response {
    private String message;
    private String type;

    public Response(String type, String message) {
        this.type = type;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }
}
