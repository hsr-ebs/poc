package ch.hsr.ebos.aion.server.config;

public class AuthNoneConfiguration extends AuthConfiguration {
    @Override
    public boolean isConfigured() {
        return false;
    }
}
