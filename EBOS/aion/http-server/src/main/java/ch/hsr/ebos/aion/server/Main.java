package ch.hsr.ebos.aion.server;

import java.io.IOException;

import ch.hsr.ebos.aion.server.config.Configuration;

public class Main {
    private static final String DefaultConfigPath = "configuration.yaml";
    private static final int DefaultPort = 8080;

    public static void main(String[] args) throws Exception {
        Configuration config = evaluateConfig();
        int port = evaluatePort();

        AionHttpServer server = new AionHttpServer(config);
        server.listen(port).get();

        System.out.println("server is running on port " + port);
    }

    private static Configuration evaluateConfig() throws IOException {
        String configEnv = System.getenv("CONFIG_PATH");
        if (configEnv != null && configEnv.length() > 0) {
            return Configuration.parse(configEnv);
        }

        return Configuration.parseResource(DefaultConfigPath);
    }

    private static int evaluatePort() {
        String portEnv = System.getenv("PORT");
        if (portEnv != null && portEnv.length() > 0) {
            try {
                return Integer.parseInt(portEnv);
            } catch (NumberFormatException e) {
                System.out.println("invalid port environment variable " + portEnv);
            }
        }

        return DefaultPort;
    }
}
