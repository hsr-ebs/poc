package ch.hsr.ebos.aion.server.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Configuration {
    private Map<String, TimelineConfiguration> timelines;
    private AuthConfiguration auth;

    public Configuration() {
        this.timelines = new HashMap<>();
    }

    public static Configuration parseResource(String name) throws IOException {
        ClassLoader classLoader = Configuration.class.getClassLoader();
        return Configuration.parse(classLoader.getResource(name).getFile());
    }

    public static Configuration parse(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        File file = new File(filename);
        Configuration configuration = mapper.readValue(file, Configuration.class);
        if (configuration.getTimelines() == null) {
            configuration.setTimelines(new HashMap<>());
        }
        return configuration;
    }

    public TimelineConfiguration getTimeline(String name) {
        for (Map.Entry<String, TimelineConfiguration> timeline : timelines.entrySet()) {
            String regex = timeline.getValue().getRegex();
            if (regex == null) {
                regex = "^" + timeline.getKey() + "$";
            }

            if (name.matches(regex)) {
                return timeline.getValue();
            }
        }

        return null;
    }

    public void addTimeline(String name, TimelineConfiguration config) {
        this.timelines.put(name, config);
    }

    public boolean hasTimeline(String name) {
        return this.getTimeline(name) != null;
    }

    public Map<String, TimelineConfiguration> getTimelines() {
        return timelines;
    }

    public void setTimelines(Map<String, TimelineConfiguration> timelines) {
        this.timelines = timelines;
    }

    public AuthConfiguration getAuth() {
        return auth;
    }

    public void setAuth(AuthConfiguration auth) {
        this.auth = auth;
    }
}
