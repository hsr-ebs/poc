package ch.hsr.ebos.aion.server;

import java.util.concurrent.CompletableFuture;

import ch.hsr.ebos.aion.server.config.Configuration;
import ch.hsr.ebos.aion.server.controller.HealthController;
import ch.hsr.ebos.aion.server.controller.OpenApiController;
import ch.hsr.ebos.aion.server.controller.SpecController;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;

public class AionHttpServer {

    private Configuration configuration;
    private Vertx vertx = Vertx.vertx();
    private HttpServer server = vertx.createHttpServer();
    private Router router = Router.router(vertx);

    public AionHttpServer(Configuration configuration) {
        this.configuration = configuration;
    }

    public CompletableFuture<Void> listen(int port) {
        CompletableFuture future = new CompletableFuture();

        try {
            vertx.exceptionHandler(e -> {
                e.printStackTrace();
            });

            router.mountSubRouter("/", new HealthController().getRouter(vertx));
            router.mountSubRouter("/", new SpecController().getRouter(vertx));
            router.mountSubRouter("/", new OpenApiController(configuration).getRouter(vertx));

            server.requestHandler(router).listen(port, result -> {
                if (result.succeeded()) {
                    future.complete(null);
                } else {
                    future.completeExceptionally(result.cause());
                }
            });
        } catch (Exception e) {
            future.completeExceptionally(e);
        }


        return future;
    }

    public CompletableFuture<Void> close() {
        CompletableFuture<Void> future = new CompletableFuture();
        server.close(result -> {
            if (result.succeeded()) {
                future.complete(null);
            } else {
                future.completeExceptionally(result.cause());
            }
        });
        return future;
    }
}
