package ch.hsr.ebos.aion.server.config;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import ch.hsr.ebos.aion.core.timeline.ITimelineFactory;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SqliteTimelineConfiguration.class, name = "sqlite"),
        @JsonSubTypes.Type(value = MemoryTimelineConfiguration.class, name = "memory"),
})
public abstract class TimelineConfiguration<O> {
    protected O options;
    private String regex;
    private String type;

    public TimelineConfiguration() {

    }

    public TimelineConfiguration(String regex) {
        this.regex = regex;
    }

    public abstract ITimelineFactory<?> getFactory();

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public O getOptions() {
        return options;
    }

    public String getType() {
        return type;
    }
}
