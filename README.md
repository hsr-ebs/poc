# POC

## Build & Test
```
cd EBOS
./gradlew build
```

## Build server jar
```
cd EBOS
./gradlew shadowJar
```
  
## Artifacts
* [Last android apk](https://gitlab.com/hsr-ebs/poc/-/jobs/artifacts/master/raw/EBOS/offliss/app/build/outputs/apk/release/app-release.apk?job=build:test:java)